import direct.directbase.DirectStart
import glob
import os
import sys
import panda3d.core as engine
from direct.stdpy import threading as threading
import time

icon_size = 256

camera = {
    'dist': 5,
}

modifiers = {
    'bbat': (0.1, (0, 0, 0), (0, 0, 0)),
    'ammo_1': (1.0, (0, 0, 0), (0, 0, 0)),
    'drink_1': (1.0, (0, 0, 0), (0, 0, 0)),
    'flash1': (1.0, (0, 0, 0), (0, 0, 0)),
    'pistol1': (8.0, (0, 0, 0), (0, 0, 0)),
}

item_dir = "../items/"

def relative_path(path):

    cpath = engine.Filename.fromOsSpecific(
        os.path.abspath(sys.path[0])
    ).getFullpath()

    return cpath + "/" + path

def print_class(obj):
    for item in dir(obj):
        print item

def HDScreenShot(output):
    tex = engine.Texture()
    width = icon_size
    height = icon_size
    mybuffer=base.win.makeTextureBuffer('HDScreenShotBuff',width,height,tex,True)
    cam=engine.Camera('HDCam')
    cam.setLens(base.camLens.makeCopy())
    cam.getLens().setAspectRatio(width/height)
    pCam=engine.NodePath(cam)
    mycamera = base.makeCamera(mybuffer,useCamera=pCam)
    myscene = base.render
    mycamera.node().setScene(myscene)
    base.graphicsEngine.renderFrame()
    tex = mybuffer.getTexture()
    mybuffer.setActive(False)
    tex.write(output)
    base.graphicsEngine.removeWindow(mybuffer)

def init_window():
    win_props = engine.WindowProperties()
    win_props.setFullscreen(False)
    win_props.setSize(icon_size, icon_size)
    base.win.requestProperties(win_props)

class MainThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.deamon = True

    def pass_next(self):
        self.next = True

    def run(self):
        base.cam.setPos(camera['dist'], camera['dist'], camera['dist'])
        base.cam.lookAt(0, 0, 0)
        items = modifiers.keys()
        for item in items:
            try:
                item_model = loader.loadModel(relative_path("%s%s.egg" % (item_dir, item)))
                item_texture = loader.loadTexture(relative_path("%s%s.png" % (item_dir, item)))
                item_texture.setMagfilter(engine.Texture.FTNearest)
                item_texture.setMinfilter(engine.Texture.FTNearest)
                item_model.setTexture(item_texture)
                # item_model.setPos(modifiers[item][1][0], modifiers[item][1][1], modifiers[item][1][2])
                # item_model.setHpr(modifiers[item][2][0], modifiers[item][2][1], modifiers[item][2][2])
                item_model.setScale(modifiers[item][0])
                item_model.reparentTo(render)
                item_model.show()
                # Should take a screenshot
                while not self.next:
                    time.sleep(0.001)
                self.next = False
                item_model.hide()
            except IOError:
                pass

        exit()

init_window()
main_thread = MainThread()
main_thread.next = False
main_thread.deamon = True
main_thread.start()
base.accept("space", main_thread.pass_next)
run()
