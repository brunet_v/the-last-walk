import copy
import os

used_blocks = []
block_datas = []

# Generate standard wall for all texture
def wall_generator():
    constants = {
        'height': 2.00,
        'low': 0.00,
    }
    results = {}
    o = 1.0 / 16.0
    def add_wall(f1, f2):
        if not os.path.exists("./results/walls/walls-%02dx%02d" % (f1[0], f1[1])):
            os.mkdir("./results/walls/walls-%02dx%02d" % (f1[0], f1[1]))
        name = "walls/walls-%02dx%02d/_cwall_wall-%02dx%02d.egg" % (f1[0], f1[1], f2[0], f2[1])
        o2 = o * 2
        f1y = f1[0] * o * 2
        f1x = f1[1] * o
        f2y = f2[0] * o * 2
        f2x = f2[1] * o
        datas = (
            (f1x + o, f1y + o2),
            (f1x, f1y + o2),
            (f1x, f1y),
            (f1x + o, f1y),
            (f2x + o, f2y + o2),
            (f2x, f2y + o2),
            (f2x, f2y),
            (f2x + o, f2y),
        )
        results[name] = datas

    # inte/inte + inte/exte (toutes combinaisons)
    for i in xrange(0, 8):
        for j in xrange(0, 8):
            for k in xrange(0, 2):
                add_wall((1, i), (k, j))
    return constants, results

# Generate standard wall for all texture
def wall_window_generator():
    constants = {
        'height1': 0.75,
        'low1': 0.00,
        'height2': 2.00,
        'low2': 1.75,
    }
    results = {}
    o = 1.0 / 16.0
    def add_wall(f1, f2):
        if not os.path.exists("./results/window-walls/window-walls-%02dx%02d" % (f1[0], f1[1])):
            os.mkdir("./results/window-walls/window-walls-%02dx%02d" % (f1[0], f1[1]))
        name = "window-walls/window-walls-%02dx%02d/_cfence_window-wall-%02dx%02d.egg" % (f1[0], f1[1], f2[0], f2[1])
        o2 = o * 2.0
        f1y = f1[0] * o * 2
        f1x = f1[1] * o
        f2y = f2[0] * o * 2
        f2x = f2[1] * o
        h1 = constants['height1']
        l2 = constants['low2']
        datas = (
            # bottom
            (f1x + o, f1y + o * h1),
            (f1x, f1y + o * h1),
            (f1x, f1y),
            (f1x + o, f1y),
            (f2x + o, f2y + o * h1),
            (f2x, f2y + o * h1),
            (f2x, f2y),
            (f2x + o, f2y),

            # top
            (f1x + o, f1y + o2),
            (f1x, f1y + o2),
            (f1x, f1y + o * l2),
            (f1x + o, f1y + o * l2),
            (f2x + o, f2y + o2),
            (f2x, f2y + o2),
            (f2x, f2y + o * l2),
            (f2x + o, f2y + o * l2),
        )
        results[name] = datas

    # inte/inte + inte/exte (toutes combinaisons)
    for i in xrange(0, 8):
        for j in xrange(0, 8):
            for k in xrange(0, 2):
                add_wall((1, i), (k, j))
    return constants, results

# Generate standard corner for walls
def wall_corner_generator():
    constants = {
        'height': 1.999,
        'low': 0.001,
    }
    results = {}
    o = 1.0 / 16.0
    o2 = o * 2.0
    for y in xrange(0, 2):
        for x in xrange(0, 8):
            name = "walls/walls-corners/_cquarter_wall-corner-%02dx%02d" % (y, x)
            yo = y * o2
            xo = x * o
            s = o * 0.1
            datas = (
                (xo, yo + o2),
                (xo + s, yo + o2),
                (xo + s, yo),
                (xo, yo),

                #copy
                (xo, yo + o2),
                (xo + s, yo + o2),
                (xo + s, yo),
                (xo, yo),
                (xo, yo + o2),
                (xo + s, yo + o2),
                (xo + s, yo),
                (xo, yo),
                (xo, yo + o2),
                (xo + s, yo + o2),
                (xo + s, yo),
                (xo, yo),
            )
            results["%s.egg" % name] = datas
    return constants, results

# Generate standard floor for all texture
def floor_generator():
    constants = {
        'height': 0.01,
        'low': -0.05,
    }
    results = {}
    o = 1.0 / 16.0
    for y in xrange(0, 2):
        for x in xrange(0, 16):
            if not os.path.exists("./results/floors/floors-%02d/" % y):
                os.mkdir("./results/floors/floors-%02d/" % y)
            name = "floors/floors-%02d/_cfree_floor-%02dx%02d" % (y, y, x)
            yo = o * 4.0 + y * o
            xo = o * x
            datas = (
                (xo + o, yo),
                (xo, yo),
                (xo, yo + o),
                (xo + o, yo + o),
            )
            results["%s.egg" % name] = datas
    return constants, results

def main():
    model_generators = {
        "floor_model.egg": floor_generator,
        "wall_model.egg": wall_generator,
        "wall_window_model.egg": wall_window_generator,
        "wall_corner_model.egg": wall_corner_generator,
    }

    for model, generator in model_generators.iteritems():
        model_file = open(model)
        model_content = model_file.read()
        model_consts, model_generated = generator()
        model_file.close()

        for name, datas in model_generated.iteritems():
            generated_content = copy.copy(model_content)
            for key, value in model_consts.iteritems():
                generated_content = generated_content.replace(
                    "%%%s%%" % key,
                    str(value)
                )
            for i in xrange(0, len(datas)):
                generated_content = generated_content.replace(
                    "%%UV%d%%" % (i + 1),
                    "%.4f %.4f" % (datas[i][0], datas[i][1])
                )
            generated_file = open("./results/%s" % name, "w")
            generated_file.write(generated_content)
            generated_file.close()

main()