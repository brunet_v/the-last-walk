#!/usr/bin/env python
import sys
from math import pi, sin, cos, radians
from direct.showbase.ShowBase import ShowBase
from pandac.PandaModules import Texture
from pandac.PandaModules import PStatClient
from panda3d.core import WindowProperties, TransparencyAttrib
from direct.task import Task
from sources.utils.snippets import *

loaded_model = sys.argv[1]
loaded_texture = sys.argv[2]
loaded_folder = "/e/Dropbox/Others/panda3d/"

def print_name(object):
    print object.__class__.__name__

class ViewerApp(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)

        filterType = Texture.FTNearest
        self.angleDegrees = 0
        self.repeat = False
        self.zoom = 20
        self.tmp = self.loader.loadModel(loaded_folder + loaded_model)
        myTexture = loader.loadTexture(loaded_folder + loaded_texture)
        myTexture.setMagfilter(filterType)
        myTexture.setMinfilter(filterType)
        self.tmp.setTexture(myTexture)
        self.tmp.setTransparency(TransparencyAttrib.MDual)
        # Reparent the model to render.
        self.tmp.reparentTo(self.render)
        # Apply scale and position transforms on the model.
        self.tmp.setPos(0, 0, 0)
        self.tmp.setTwoSided(True)
        self.disableMouse()

        self.createRepeated()
        self.accept("space", self.toggleRepeat)
        self.accept("z", self.zooming, [-1,])
        self.accept("s", self.zooming, [1,])

        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
        self.setResolution(1024, 764, False)
        base.setFrameRateMeter(True)

    def zooming(self, ratio):
        self.zoom += self.zoom * ratio * 0.2

    def createRepeated(self):
        self.copys = {}

        grid_size = 11
        grid_mid = 5

        for x in xrange(0, grid_size):
            for y in xrange(0, grid_size):
                if x != grid_mid or y != grid_mid:
                    nnode = self.tmp.copyTo(render)
                    nnode.setPos(x - grid_mid, y - grid_mid, 0)
                    nnode.hide()
                    self.copys["%d:%d" % (x, y)] = nnode

    def toggleRepeat(self):

        self.repeat = not self.repeat

        for key in self.copys.keys():
            if (self.repeat):
                self.copys[key].show()
            else:
                self.copys[key].hide()

    def setResolution(self, width, height, fullscreen):
        wp = WindowProperties()
        wp.setSize(width, height) # there will be more resolutions
        wp.setFullscreen(fullscreen)
        base.win.requestProperties(wp)

    def spinCameraTask(self, task):
        self.angleDegrees += globalClock.getDt() * 20
        self.camera.setPos(self.zoom * cos(radians(self.angleDegrees)), self.zoom * sin(radians(self.angleDegrees)), self.zoom)
        self.camera.lookAt(0, 0, 0)
        return Task.cont


app = ViewerApp()
app.run()