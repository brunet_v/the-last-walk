import sys,os

from pandac.PandaModules import Texture
from panda3d.core import Filename


def RelativePath(path):

    cpath = Filename.fromOsSpecific(
                os.path.abspath(sys.path[0])
            ).getFullpath()

    return cpath + path

def LoadTexturedModel(loader, modelpath, texturepath):

    nTexture = loader.loadTexture(texturepath)
    nTexture.setMagfilter(Texture.FTNearest)
    nTexture.setMinfilter(Texture.FTNearest)
    nModel = loader.loadModel(modelpath)
    nModel.setTexture(nTexture)

    return nModel
