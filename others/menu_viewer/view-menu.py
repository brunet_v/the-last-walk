import os
import sys
import hashlib
import glob
import panda3d.core as engine
from multiprocessing import Process, Value
from panda3d.rocket import *

# engine.loadPrcFileData("", "win-size 1024 768")
engine.loadPrcFileData("", "win-size 1200 900")
document = None
i = 0



def get_menus_path(path, a_path):
    return engine.Filename(
        "%s/datas/ui/menus/%s/" % (a_path, path)
    ).toOsSpecific()

def reset_all():
    global region
    global input_handler
    global context
    global i
    i += 1
    region = RocketRegion.make('debug%d' % i, base.win)
    region.setActive(True)
    # region.initDebugger()
    # region.setDebuggerVisible(True)
    input_handler = RocketInputHandler()
    base.mouseWatcher.attachNewNode(input_handler)
    region.setInputHandler(input_handler)
    context = region.getContext()

def reset_doc(name, a_path):
    global context
    global document
    if document:
        document.Close()
    document = context.LoadDocument(get_menus_path(name, a_path))
    document.Show()

def get_checksum():
    def listdirectory(path):
        fichier = []
        l = glob.glob(path+'/*')
        for i in l:
            if os.path.isdir(i): fichier.extend(listdirectory(i))
            else: fichier.append(i)
        return fichier
    def get_hash(fichier):
        sha1 = hashlib.sha1()
        with open(fichier,'rb') as f:
            for chunk in iter(lambda: f.read(8192), b''):
                sha1.update(chunk)
        return sha1.hexdigest()
    def getchecksum(fichier):
        dico = {}
        l = len(fichier)
        i = 0
        while i < l:
            path = fichier[i].replace("\\", "/")
            dico[path] = get_hash(path)
            i += 1
        return dico
    return getchecksum(listdirectory('./datas'))

def main(res, name, a_path):

    wp = engine.WindowProperties.getDefault()
    wp.setTitle('RocketDebugging')
    wp.setForeground(False)
    engine.WindowProperties.setDefault(wp)

    checksum = get_checksum()

    import direct.directbase.DirectStart
    import direct.task.Task as task

    LoadFontFace(get_menus_path("homizio-medium.ttf", a_path))
    LoadFontFace(get_menus_path("simplymono.ttf", a_path))

    reset_all()
    reset_doc(name, a_path)

    def reload():
        res.value = 1.
        sys.exit(1)

    def quit():
        res.value = 0.
        sys.exit(0)

    def check(task):
        if checksum != get_checksum():
            reload()
        return task.again

    base.accept("escape", quit)
    base.accept("space", reload)

    taskMgr.doMethodLater(1, check, 'yolo')

    run()

if __name__ == '__main__':
    os.chdir("../../client")

    a_path = engine.Filename.fromOsSpecific(os.getcwd())

    if len(sys.argv) > 1:
        v = 0.
        name = sys.argv[1]
        main(v, name, a_path)
    else:
        print "Usage: view-menu [MENU_FILE]"
        v = 1.
        name = raw_input(" > ")

    v = Value('d', v)
    while v.value > 0:
        p = Process(target=main, args=(v, name, a_path))
        p.start()
        p.join()
