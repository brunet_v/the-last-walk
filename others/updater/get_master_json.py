#!/usr/bin/env python

import glob
import os.path
import json

def listdirectory(path):
    fichier = []
    l = glob.glob(path+'/*')
    for i in l:
        if os.path.isdir(i): fichier.extend(listdirectory(i))
        else: fichier.append(i)
    return fichier

def get_hash(fichier):
    import hashlib
    sha1 = hashlib.sha1()
    with open(fichier,'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            sha1.update(chunk)
    return sha1.hexdigest()

def getchecksum(fichier):
    import hashlib
    dico = {}
    l = len(fichier)
    i = 0
    while i < l:
        dico[fichier[i]] = get_hash(fichier[i])
        i += 1
    return dico

def main():
    dico = getchecksum(listdirectory('.'))
    Version = file("./master.json", "w")
    dump_json = json.dumps(dico)
    Version.write(dump_json)
    Version.close()

main()
