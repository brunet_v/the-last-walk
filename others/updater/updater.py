import os
import sys
import hashlib
import urllib
import json
import glob
import time

import sources.gui.splashscreen as splashscreen
import sources.config.build_config as build_config

def create_folder(path):
    prev_path = (path.rpartition('/'))[0]
    if not os.path.exists(prev_path) and prev_path != "":
        create_folder((path.rpartition('/'))[0])
    os.mkdir(path)

def listdirectory(path):
    fichier = []
    l = glob.glob(path+'/*')
    for i in l:
        if os.path.isdir(i): fichier.extend(listdirectory(i))
        else: fichier.append(i)
    return fichier

def get_hash(fichier):
    sha1 = hashlib.sha1()
    with open(fichier,'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            sha1.update(chunk)
    return sha1.hexdigest()

def getchecksum(fichier):
    dico = {}
    l = len(fichier)
    i = 0
    while i < l:
        path = fichier[i].replace("\\", "/")
        dico[path] = get_hash(path)
        i += 1
    return dico

def get_master_json():
    urlJSON = urllib.urlopen("http://update-dev.thelastwalk.net/master.json")
    master = json.load(urlJSON)
    return master

def getVersion():
    curent = getchecksum(listdirectory('.'))
    return curent

def compare_version(splash, master, current):
    changes = 0
    splash.update()
    for key, value in master.iteritems():
        try:
            url = "http://update-dev.thelastwalk.net/" + key
            url = url.replace("#", "%23")
            if key not in current or value != current[key]:
                print "Updating:", key
                if not os.path.exists((key.rpartition('/'))[0]):
                    create_folder((key.rpartition('/'))[0])
                splash.update()
                splash.downloading(key)
                urllib.urlretrieve(url, key)
                splash.done()
                changes += 1
        except Exception as e:
            print "Error:", str(e)
    splash.update()
    time.sleep(0.25)
    splash.quit()
    return changes

def restart_game():
    print "Restarting the game..."
    python = sys.executable
    os.execl(python, python, * sys.argv)

def update():
    try:
        master = get_master_json()
        current = getVersion()
        changes = 0
        for key, value in master.iteritems():
            if key not in current or value != current[key]:
                changes += 1
        if changes > 0:
            if build_config.build_option("no-update"):
                return None
            splash = splashscreen.SplashScreen(changes)
            changes = compare_version(splash, master, current)
            if changes:
                restart_game()
    except Exception as e:
        print "Error:", str(e)
