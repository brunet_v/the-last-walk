from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor

import sys

player_path = "./male-player1/"
player_anim_path = player_path + "animations/"
player_tex_path = player_path + "textures/"

def print_help():
    print "CONTROL"
    print "-/+: zoom in and zoom out"
    print "shift + wheel up/down: player rotation"
    print "4/6: next and prev anim"
    print "space: start current anim"
    print "a: change the item axis rotation"
    print "wheel up/down: item rotation"
    print "arrow left/right: move item on the x axis"
    print "arrow up/down: move item on the y axis"
    print "8/2: move item on the z axis"
    print "o/p: prev and next object to link"
    print "l: link item to the current object"
    print "enter: get the position relative to the bone"

if len(sys.argv) != 2 and len(sys.argv) != 8:
    print "Usage: python main.py [ITEM PATH]\n"
    print "Usage: python main.py x y z h p r\n"
    print_help()
    sys.exit(1)

if sys.argv[1] == "help":
    print_help()
    sys.exit(0)

if len(sys.argv) > 2:
    args = {}
    args['x'] = float(sys.argv[2])
    args['y'] = float(sys.argv[3])
    args['z'] = float(sys.argv[4])
    args['h'] = float(sys.argv[5])
    args['p'] = float(sys.argv[6])
    args['r'] = float(sys.argv[7])
else:
    args = None

class Link_item_to_bone(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)

        self.name_item = sys.argv[1].replace(".egg", "", 4)
        self.scale_item = {
            "ammo_1": 0.4,
            "drink_1": 0.20,
            "pistol1": 1.8,
            "flash1": 0.065,
            "m16": 0.7,
            "katana": 0.7,
            "wrench": 0.7,
            "axe": 0.7,
            "machete": 0.7,
            "handgun": 0.7,
            "knife": 0.7,
            "shotgun": 0.7,
            "bbat": 0.7,
            "flashlight": 0.5,
        }
        self.anim = [
            'aim',
            'use',
            'wait',
            'walk',
            'walk-slow',
            'sprint',
            'aim-walk-0',
            'aim-walk-90',
            'aim-walk-180',
            'aim-walk-270',
            'm16-aim',
            'm16-use',
            'm16-wait',
            'm16-walk',
            'm16-walk-slow',
            'katana-aim',
            'katana-use',
            'katana-wait',
            'katana-walk',
            'katana-walk-slow',
            'flashlight-walk-anim',
        ]
        self.axis = {
            0: "x",
            1: "z",
            2: "y"
        }
        self.bones = None
        self.cur_anim = 6
        self.max_anim = len(self.anim) - 1
        self.scale_player = 0.19
        self.is_anim = False
        self.move = 0.05
        self.item_rotate = 6
        self.item_axis_rotate = 0
        self.player_link = "male-player1"
        self.objects = {
            "male-player1": None
        }
        self.cur_link = 0

        self.loadPlayer()
        self.max_link = len(self.bones)
        self.loadItem()

        self.setCamera()
        self.setEvent()

        print "current anim: ", self.anim[self.cur_anim]
        print "current item axis rotation: ", self.axis[self.item_axis_rotate]
        print "current object to link: ", self.player_link
        print self.name_item.partition("/")[-1], "is linked to actor object"

    def setEvent(self):
        self.accept("4", self.animPrev)
        self.accept("6", self.animNext)
        self.accept("space", self.playAnim)

        self.accept("+", self.zoomIn)
        self.accept("-", self.zoomOut)
        self.accept("shift-wheel_up", self.rotatePlayerRight)
        self.accept("shift-wheel_down", self.rotatePlayerLeft)

        self.accept("a", self.change_item_axis_rotation)
        self.accept("wheel_up", self.rotateItemRight)
        self.accept("wheel_down", self.rotateItemLeft)

        self.accept("arrow_left", self.itemLeft)
        self.accept("arrow_up", self.itemUp)
        self.accept("arrow_down", self.itemDown)
        self.accept("arrow_right", self.itemRight)
        self.accept("8", self.itemForward)
        self.accept("2", self.itemBackward)

        self.accept("o", self.objToLinkPrev)
        self.accept("p", self.objToLinkNext)
        self.accept("l", self.linkItem)

        self.accept("enter", self.done)

    def done(self):
        pos = self.item.getPos()
        hpr = self.item.getHpr()
        print round(hpr[0], 2), ",", round(hpr[1], 2), ",", round(hpr[2], 2), ",", round(pos[0], 2), ",", round(pos[1], 2), ",", round(pos[2], 2)

    def linkItem(self):
        if self.cur_link == 0:
            self.item.reparentTo(self.player)
            if args == None:
                self.item.setPos(0, 0, 0)
                self.item.setHpr(0, 0, 0)
            else:
                self.item.setPos(args['x'], args['y'], args['z'])
                self.item.setHpr(args['h'], args['p'], args['r'])
            print self.name_item.partition("/")[-1], "is linked to actor"
        else:
            name = self.bones[self.cur_link - 1].getName()
            self.item.reparentTo(self.player.exposeJoint(None, "modelRoot", name))
            if args == None:
                self.item.setPos(0, 0, 0)
                self.item.setHpr(0, 0, 0)
            else:
                self.item.setPos(args['x'], args['y'], args['z'])
                self.item.setHpr(args['h'], args['p'], args['r'])
            print self.name_item.partition("/")[-1], "is linked to bone:", name

    def objToLinkPrev(self):
        self.cur_link -= 1
        if self.cur_link < 0:
            self.cur_link = self.max_link
        if self.cur_link == 0:
            print "current object to link: ", self.player_link
        else:
            print "current object to link: ", self.bones[self.cur_link - 1].getName()
    def objToLinkNext(self):
        self.cur_link += 1
        if self.cur_link > self.max_link:
            self.cur_link = 0
        if self.cur_link == 0:
            print "current object to link: ", self.player_link
        else:
            print "current object to link: ", self.bones[self.cur_link - 1].getName()
    def change_item_axis_rotation(self):
        self.item_axis_rotate += 1
        if self.item_axis_rotate > 2:
            self.item_axis_rotate = 0
        print "current item axis rotation: ", self.axis[self.item_axis_rotate]
    def animPrev(self):
        self.cur_anim -= 1
        if self.cur_anim < 0:
            self.cur_anim = self.max_anim
        print "current anim: ", self.anim[self.cur_anim]
    def animNext(self):
        self.cur_anim += 1
        if self.cur_anim > self.max_anim:
            self.cur_anim = 0
        print "current anim: ", self.anim[self.cur_anim]
    def itemLeft(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0] - self.move, pos[1], pos[2])
    def itemRight(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0] + self.move, pos[1], pos[2])
    def itemUp(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0], pos[1], pos[2] + self.move)
    def itemDown(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0], pos[1], pos[2] - self.move)
    def itemForward(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0], pos[1] + self.move, pos[2])
    def itemBackward(self):
        pos = self.item.getPos()
        self.item.setPos(pos[0], pos[1] - self.move, pos[2])

    def zoomIn(self):
        pos = self.camera.getPos()
        self.camera.setPos(pos[0], pos[1] + 0.5, pos[2])
    def zoomOut(self):
        pos = self.camera.getPos()
        self.camera.setPos(pos[0], pos[1] - 0.5, pos[2])

    def rotatePlayerRight(self):
        hpr = self.player.getHpr()
        self.player.setHpr(hpr[0] + 8, hpr[1], hpr[2])
    def rotatePlayerLeft(self):
        hpr = self.player.getHpr()
        self.player.setHpr(hpr[0] - 8, hpr[1], hpr[2])

    def rotateItemRight(self):
        hpr = self.item.getHpr()
        hpr[self.item_axis_rotate] += self.item_rotate
        self.item.setHpr(hpr[0], hpr[1], hpr[2])
    def rotateItemLeft(self):
        hpr = self.item.getHpr()
        hpr[self.item_axis_rotate] -= self.item_rotate
        self.item.setHpr(hpr[0], hpr[1], hpr[2])

    def setCamera(self):
        self.camera.setPos(0, 15, 1.5)
        self.camera.setHpr(0, -5, 0)
        self.disableMouse()

    def loadItem(self):
        self.item = self.loader.loadModel(sys.argv[1])
        tex = self.loader.loadTexture(self.name_item + ".png")
        self.item.setTexture(tex, 1)
        self.item.reparentTo(self.player)

        # print self.bones[0].getName()
        # self.item.reparentTo(self.player.exposeJoint(None, "modelRoot", self.bones[3].getName()))

        name = self.name_item.partition("/")[-1]
        if name in self.scale_item:
            scale = self.scale_item[name]
        else:
            print "Error: '", name, "': bad item name"
            sys.exit(1)
        scale /= self.scale_player
        self.item.setScale(scale, scale, scale)
        if args == None:
            self.item.setPos(0, 0, 0)
            self.item.setHpr(0, 0, 0)
        else:
            self.item.setPos(args['x'], args['y'], args['z'])
            self.item.setHpr(args['h'], args['p'], args['r'])

    def loadPlayer(self):
        anims = {}
        for key, anim in enumerate(self.anim):
            anims[anim] = player_anim_path + anim + ".egg"
        self.player = Actor(player_path + "male-player1.egg", anims)
        #     {
        #     "aim": player_anim_path + "aim.egg",
        #     "aim-walk-0": player_anim_path + "aim-walk-0.egg",
        #     "aim-walk-90": player_anim_path + "aim-walk-90.egg",
        #     "aim-walk-180": player_anim_path + "aim-walk-180.egg",
        #     "aim-walk-270": player_anim_path + "aim-walk-270.egg",
        #     "use": player_anim_path + "use.egg",
        #     "wait": player_anim_path + "wait.egg",
        #     "walk": player_anim_path + "walk.egg"
        # }
        self.player.play(self.anim[self.cur_anim])
        self.player.stop(self.anim[self.cur_anim])
        tex = self.loader.loadTexture(player_tex_path + "male-player1.png")
        self.player.setTexture(tex, 1)
        self.player.reparentTo(self.render)
        self.player.setScale(self.scale_player, self.scale_player, self.scale_player)
        self.player.setPos(0, 20, 0)
        self.player.setHpr(0, 0, 0)

        self.bones = self.player.getJoints()
        # for bone in self.bones:
        #     print bone
        # print self.bones

    def playAnim(self):
        animCtrl = self.player.getAnimControl(self.anim[self.cur_anim])
        if not animCtrl.isPlaying():
            self.player.play(self.anim[self.cur_anim])
            self.player.loop(self.anim[self.cur_anim])
        else:
            self.player.stop(self.anim[self.cur_anim])

app = Link_item_to_bone()
app.run()
sys.exit(0)
