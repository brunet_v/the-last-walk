
"""
    Used to be the common game structure between client and server,
    optimisations datastructure and implementation details now make it different.

    /!\ Do not sync this config file with the server
"""

WORLD_STRUCT = {
    'infos': { # general world states infos
        'view':{
            'day': 0,
            'daytime': 0,
            'ppos': 0,
            'xpos': 0,
            'ypos': 0,
            'zpos': 0,
            'oxpos': 0,
            'oypos': 0,
            'ozpos': 0,
            'rotation': 45,
            'rotationed': 45,
            'zoom': 11,
            'zoomed': 11,
            'zoomdecal': 0.,
        },
        'hooked': None,
        'size': None,
        'map_def': None,
        'map_grid': None,
        'frametime': None,
        'pspawns': [],
        'zspawns': [],
        'ispawns': [],
        'world_grid': {},
    },
    'lights': None, # LightHandler
    'blocks': {     # undestructibles objects with collision
        'rendered': {},
        'rendered-ns': {},
        'datas': {},
        'fog': None,
    },
    'players': {},  # player list
    'zombies': {    # moving, IA, ennemies
        'tree': {},
        'list': {},
    },
    'doors': None,          # Door handler
    'collision_tree': None, # Collision handler
    'items': {
        'list': {},
        'tree': {},
    },
}
