
ONLY_TORSO = set(['torso',])
ONLY_LEGS = set(['legs',])
ALL_PARTS = set(['torso', 'legs'])

HAND_STATS = {
    'timings': [0.37, 0.71],
    'damage': 6,
    'need_ammo': False,
    'need_energy': 6.,
    'zone': {'type': 'spoint', 'dist': 0.3, 'radius': 0.60},

    # No light configuration
    'light': {
        'color': (0., 0., 0., 0.),
        'attenuation': (0, 0, 0.8),
        'fov': 10,
        'exponent': 0.8,
        'near-far': (10000, 10001),
    },
}
