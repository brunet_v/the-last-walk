
"""
    Contain players/zombies lists, statistics,
    animations names, models names and textures names
"""

PLAYERS_LIST = [
    'male-player1',
    # 'player',
]
ZOMBIES_LIST = [
    'zombie-1', 'zombie-2', 'zombie-3', 'zombie-4', 'zombie-5',
]

PLAYER_STATS = {
    'sprint-speed': 4.5,
    'walk-speed': 3.2,
    'slow-speed': 2.1,
    'aim-speed': 1.6,
    'life': 55,
}

ZOMBIE_STATS = {
    # Entity
    'speed': 2.8,
    'life': 35,

    # Attack
    'damage': 4,
    'aradius': 0.5,
    'arange': 0.8 ** 2,
    'atiming': (0.35, 0.8, 1.2),
}

PLAYER_ANIMS = [
    ## Base animations
    'aim-anim',
    'walk-anim',
    'walk-slow-anim',
    'sprint-anim',

    'wait1-anim',
    #'wait2-anim', # Not sure if needed

    'use1-anim',
    'use2-anim',


    ## Weapon specific animations
    'm16/m16-aim-anim',
    'm16/m16-use-anim',
    'm16/m16-wait-anim',
    'm16/m16-walk-anim',
    'm16/m16-walk-slow-anim',
    'm16/m16-sprint-anim',

    'katana/katana-aim-anim',
    'katana/katana-use1-anim',
    'katana/katana-use2-anim',
    'katana/katana-wait-anim',
    'katana/katana-walk-anim',
    'katana/katana-walk-slow-anim',
    'katana/katana-sprint-anim',

    'flashlight/flashlight-walk-anim',
    'flashlight/flashlight-walk-slow-anim',
    'flashlight/flashlight-sprint-anim',
    'flashlight/flashlight-wait-anim',


    ## Aiming legs
    'aim-walk-0',
    'aim-walk-90',
    'aim-walk-180',
    'aim-walk-270',
    'wait-aim-anim',
]

PLAYER_ANIMS_ALIAS = {
    'bbat-aim': 'katana-aim',
    'bbat-use': 'katana-use',
    'bbat-wait': 'katana-wait',
    'bbat-walk': 'katana-walk',
    'bbat-walk-slow': 'katana-walk-slow',
}

ZOMBIE_ANIMS = [
    'wait',
    'walk1',
    'walk2',
    'walk3',
    'attack1-1',
    'attack1-2',
    'attack2-1',
    'attack2-2',
    'attack3-1',
    'attack3-2',
]

old_ZOMBIE_ANIMS = [
    'player-zombie-wait',
    'player-zombie-walk',
    'player-zombie-attack',
    'player-zombie-hit',
    'player-zombie-die'
]

OBJECT_LIST = {
    'male-player1': {
        'txt': 'male-player1.png',
        'scale': 0.16,
        'anims': PLAYER_ANIMS,
        'stats': PLAYER_STATS,
    },
    'male-player2': {
        'txt': 'male-player2.png',
        'scale': 0.16,
        'anims': PLAYER_ANIMS,
        'stats': PLAYER_STATS,
    },
    'male-player3': {
        'txt': 'male-player3.png',
        'scale': 0.16,
        'anims': PLAYER_ANIMS,
        'stats': PLAYER_STATS,
    },
    'player': {
        'txt': 'player-texture1.png',
        # 'scale': 1.0,
        # 'anims': [],
        'scale': 0.19,
        'anims': PLAYER_ANIMS,
        'stats': PLAYER_STATS,
    },
    'zombie-1': {
        'txt': 'zombie-1.png',
        'scale': 0.16,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie-2': {
        'txt': 'zombie-2.png',
        'scale': 0.16,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie-3': {
        'txt': 'zombie-3.png',
        'scale': 0.16,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie-4': {
        'txt': 'zombie-4.png',
        'scale': 0.16,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie-5': {
        'txt': 'zombie-5.png',
        'scale': 0.16,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie1': {
        'txt': 'zombie1.png',
        'scale': 0.200,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie2': {
        'txt': 'zombie2.png',
        'scale': 0.205,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
    'zombie3': {
        'txt': 'zombie3.png',
        'scale': 0.195,
        'anims': ZOMBIE_ANIMS,
        'stats': ZOMBIE_STATS,
    },
}

DOOR_MAX_LIFE = 50.
