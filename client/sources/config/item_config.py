import sources.config.items.base as base
import sources.config.items.m16 as m16
import sources.config.items.katana as katana
import sources.config.items.flashlight as flashlight

"""
    Contain graphical details, properties, and timings of the game items
"""

ITEM_ALLOWED = [
    "m16",
    "katana",
    # "bbat",
    # "flashlight",
]

HAND_STATS = base.HAND_STATS

ITEM_STATS = {

    # M16 yolo weapon.
    'm16': m16.INFOS,

    # Shotgun yolo weapon.
    'shotgun': {
        'equipable': True,
        'visible': True,
        'need_ammo': True,
        'need_energy': False,
        'scale': 0.7,
        'timings': (0.1, 0.2), # Timing de l'effet, Timing de la fin de l'animation
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'shotgun-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'shotgun-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'shotgun-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'shotgun-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 10, # Taille d'un chargeur
        'damage': 10,
        'zone': {
            'type': 'rproj', # Ranged projection (une balle)
            'range': 15., # Longeur de la trainee de l'impact
            'size': 0.25, # Largeur de la trainee de l'impact
        },
    },

    # Handgun yolo weapon.
    'handgun': {
        'equipable': True,
        'visible': True,
        'need_ammo': True,
        'need_energy': False,
        'scale': 0.7,
        'timings': (0.1, 0.2), # Timing de l'effet, Timing de la fin de l'animation
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'handgun-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'handgun-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'handgun-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'handgun-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 10, # Taille d'un chargeur
        'damage': 10,
        'zone': {
            'type': 'rproj', # Ranged projection (une balle)
            'range': 15., # Longeur de la trainee de l'impact
            'size': 0.25, # Largeur de la trainee de l'impact
        },
    },

    # Basic Flashlight.
    'flashlight': flashlight.INFOS,

    # Red axe
    'axe': {
        'equipable': True,
        'visible': True,
        'need_ammo': False,
        'need_energy': 4.,
        'scale': 0.7,
        'timings': (0.37, 0.71),
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'axe-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'axe-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'axe-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'axe-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 0,
        'damage': 10,
        'zone': {
            'type': 'spoint', # Simple point
            'dist': 0.7,
            'radius': 0.8,
        },
    },

    # Baseball Bat
    'bbat': {
        'equipable': True,
        'visible': True,
        'need_ammo': False,
        'need_energy': 4.,
        'scale': 0.7,
        'timings': (0.37, 0.71),
        'custom_anims': set(['aim', 'walk', 'walk-slow', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'bbat-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'bbat-walk': (-306.0, -372.0, -120.0, -0.95, 0.9, -0.3, "Right Hand"),
            'bbat-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'bbat-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 0,
        'damage': 10,
        'zone': {
            'type': 'spoint', # Simple point
            'dist': 0.7,
            'radius': 0.8,
        },
    },

    # Katana sword
    'katana': katana.INFOS,

    # Machete sword
    'machete': {
        'equipable': True,
        'visible': True,
        'need_ammo': False,
        'need_energy': 4.,
        'scale': 0.7,
        'timings': (0.37, 0.71),
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'machete-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'machete-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'machete-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'machete-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 0,
        'damage': 10,
        'zone': {
            'type': 'spoint', # Simple point
            'dist': 0.7,
            'radius': 0.8,
        },
    },

    # Combat knife
    'knife': {
        'equipable': True,
        'visible': True,
        'need_ammo': False,
        'need_energy': 4.,
        'scale': 0.7,
        'timings': (0.37, 0.71),
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'knife-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 0,
        'damage': 10,
        'zone': {
            'type': 'spoint', # Simple point
            'dist': 0.7,
            'radius': 0.8,
        },
    },

    # Pipe Wrench
    'wrench': {
        'equipable': True,
        'visible': True,
        'need_ammo': False,
        'need_energy': 4.,
        'scale': 0.7,
        'timings': (0.37, 0.71),
        # 'custom_anims': set(['aim', 'walk', 'wait', 'use']),
        'display_offsets': {
            'on_floor': (0, 0, 0, 0, 0, 0),
            'on_back': [
                (0, 0, 0, 0, 0, 0, "Torso"),
            ],
            'knife-aim': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-walk': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-wait': (0, 0, 0, 0, 0, 0, "Right Hand"),
            'knife-use': (0, 0, 0, 0, 0, 0, "Right Hand"),
        },
        'load': 0,
        'damage': 10,
        'zone': {
            'type': 'spoint', # Simple point
            'dist': 0.7,
            'radius': 0.8,
        },
    },
}
