import json
import Tkinter
import collections
import sources.objects.utils as utils
import __builtin__
import sources.gui.Languages as languages

# default resolutions
resolutions = collections.OrderedDict([
    ("1024x768", (1024, 768)),
    ("1280x960", (1280, 960)),
    ("1400x1050", (1400, 1050)),
    ("1600x1200", (1600, 1200)),
    ("1280x800", (1280, 800)),
    ("1440x900", (1440, 900)),
    ("1680x1050", (1680, 1050)),
    ("1920x1200", (1920, 1200)),
    ("1024x576", (1024, 576)),
    ("1280x720", (1280, 720)),
    ("1366x768", (1366, 768)),
    ("1920x1080", (1920, 1080)),
])

def use_resolution(x, y, failed):
    global resolutions
    if not failed:
        for name, sizes in resolutions.iteritems():
            xsize, ysize = sizes
            if x == xsize and y == ysize:
                return utils.Struct({
                    "name": name,
                    "xsize": xsize,
                    "ysize": ysize,
                })
        name = "%dx%d" % (x, y)
        resolutions[name] = (x, y)
        return utils.Struct({
            "name": name,
            "xsize": x,
            "ysize": y,
        })
    else:
        return utils.Struct({
            "name": "1024x768",
            "xsize": 1024,
            "ysize": 768,
        })

class ConfigFile(object):

    path = "./configuration.json"
    datas = {}

    @staticmethod
    def init(failed):
        root = Tkinter.Tk()
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()
        res = use_resolution(screen_width, screen_height, failed)
        # Default values
        ConfigFile.set("wxsize", res.xsize)
        ConfigFile.set("wysize", res.ysize)
        ConfigFile.set("language", "EN")
        ConfigFile.set("fullscreen", 1)
        ConfigFile.set("anti-alias", 0)
        ConfigFile.set("squality", 2048)
        ConfigFile.set("extrashadow", 0)
        ConfigFile.set("vsync", 90)
        ConfigFile.set("helpbutton", 1)
        ConfigFile.set("decalnumber", 100)
        ConfigFile.set("volume_music", 1.)
        ConfigFile.set("volume_sfx", 1.)
        if not failed:
            ConfigFile.load()
        # Overwritten every time
        ConfigFile.set("desktop_width", screen_width)
        ConfigFile.set("desktop_height", screen_height)
        languages.set_language_used(ConfigFile.get("language"))

    @staticmethod
    def init_res(displayinfo):
        global resolutions
        new_resolutions = __builtin__.set([])
        sx = ConfigFile.get("desktop_width")
        sy = ConfigFile.get("desktop_height")
        for index in range(displayinfo.getTotalDisplayModes()):
            xsize = displayinfo.getDisplayModeWidth(index)
            ysize = displayinfo.getDisplayModeHeight(index)
            if xsize >= 1024 and ysize >= 768 and xsize <= sx and ysize <= sy:
                name = "%dx%d" % (xsize, ysize)
                new_resolutions.add((name, (xsize, ysize)))
        new_resolutions.add(("%dx%d" % (sx, sy), (sx, sy)))
        resolutions = collections.OrderedDict(
            sorted(new_resolutions, key=lambda t: t[1][0] * 100000 + t[1][1])
        )

    @staticmethod
    def load():
        try:
            fileconf = open(ConfigFile.path, "r")
            ConfigFile.datas.update(json.loads(fileconf.read()))
            fileconf.close()
        except:
            ConfigFile.save() # I don't fucking care, just use the default configuration

    @staticmethod
    def save():
        try:
            fileconf = open(ConfigFile.path, "w")
            fileconf.write(json.dumps(
                ConfigFile.datas,
                separators=(',', ': '),
                indent=4,
                sort_keys=True
            ))
            fileconf.close()
        except:
            pass # I don't care either

    @staticmethod
    def set(key, value):
        ConfigFile.datas[key] = value

    @staticmethod
    def get(key):
        return ConfigFile.datas[key]

    @staticmethod
    def update(opts):
        # Language
        langselect = opts.GetElementById('langselect')
        ConfigFile.set("language", langselect.value)
        languages.set_language_used(langselect.value)
        # Display mode
        fullscreenselect = opts.GetElementById('fullscreenselect')
        ConfigFile.set("fullscreen", int(fullscreenselect.value))
        # Shadow quality
        shadowselect = opts.GetElementById('shadowselect')
        ConfigFile.set("squality", int(shadowselect.value))
        # Extra shadow quality
        extrashadowselect = opts.GetElementById('extrashadowselect')
        ConfigFile.set("extrashadow", int(extrashadowselect.value))
        # Vsync
        vsyncselect = opts.GetElementById('vsyncselect')
        ConfigFile.set("vsync", int(vsyncselect.value))
        # Anti-alias
        aaselect = opts.GetElementById('aaselect')
        ConfigFile.set("anti-alias", int(aaselect.value))
        # Help button
        hbselect = opts.GetElementById('hbselect')
        ConfigFile.set("helpbutton", int(hbselect.value))
        # Decal number
        dnselect = opts.GetElementById('dnselect')
        ConfigFile.set("decalnumber", int(dnselect.value))
        # Volume Music
        slide_music = opts.GetElementById('slide-music')
        ConfigFile.set("volume_music", float(slide_music.value))
        # Volume SFX
        slide_sfx = opts.GetElementById('slide-sfx')
        ConfigFile.set("volume_sfx", float(slide_sfx.value))
        # Resolution
        resolutionselect = opts.GetElementById('resolutionselect')
        res = resolutionselect.value
        if res in resolutions:
            xsize, ysize = resolutions[res]
            ConfigFile.set("wxsize", xsize)
            ConfigFile.set("wysize", ysize)
            import sources.world.init as init
            init.init_window(True)

set = ConfigFile.set
get = ConfigFile.get
