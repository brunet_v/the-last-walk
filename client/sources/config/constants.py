import sources.config.structure_config as structure_config
import sources.config.object_config as object_config
import sources.config.item_config as item_config

"""
    Contain every basic constants used by the client/server

    /!\ Think twice before changing very generic ones.
"""

# ENVIRONMENT VALUES
WORLD_SIZE = 4
WORLD_RETRY = 5000
BLOCKS_PER_SIZE = 32
COLLISION_SIZE = 0.6
UNEXPLORED_BLOCK_FOG = 0.97
CAMERA_HIGH_RATIO = 1.8
MAX_CAM_ZOOM = 11.5
MIN_CAM_ZOOM = 09.0
USE_DEBUG = True


# NETWORK
SOCKET_PORT = 22458
SOCKET_SIZE = 1024
SOCKET_TIMEOUT = 2.0
SOCKET_ERROR_MARGIN = 0.3
SOCKET_CLIENT_REFRESH = 0.1
MAX_BUFFER_SIZE = 30000


# GAMEPLAY
INVENTORY_SIZE = 9
WALK_HEIGHT_MARGIN = 0.2
OBJECT_GET_DISTANCE = 1.6
OBJECT_GET_MARGIN = 0.35
DOOR_OPEN_DIST = 1.6
STUN_TIME = 0.37
ZDIE_TIME = 3.00
FLASH_TIME = 0.03
VISION_HEIGHT = 1.3
ZOMBIE_WALK_MARGIN = 0.05
PLAYER_WALK_MARGIN = 0.10
DAYTIME_DELAY = 5.00
DAYTIME_SPEED = 0.50
DAYTIME_NIGHT_RATIO = 1.5
BLENDING_DELAY = 0.10
AIM_SPEED_PENALTY = 1.8
TIRED_TIME = 6.
ZOMBIE_SNAPSHOT_DELAY = 1. / 10.
PLAYER_SNAPSHOT_DELAY = 1. / 20.


# STATS RATES
HUNGER_RATE = 1.
THIRST_RATE = 1.
ENERGY_WALK_RATE = 3.5
ENERGY_REGEN_RATE = 3.
ENERGY_SPRINT_RATE = 8.5
STRESS_TEST_SIZE = 6.
HEALTH_REGEN_RATE = 0.2
HEALTH_DEATH_RATE = 0.6 # When too hungry/thirsty


# OBJECTS
PLAYERS_LIST = object_config.PLAYERS_LIST
ZOMBIES_LIST = object_config.ZOMBIES_LIST
PLAYER_STATS = object_config.PLAYER_STATS
ZOMBIE_STATS = object_config.ZOMBIE_STATS
PLAYER_ANIMS = object_config.PLAYER_ANIMS
ZOMBIE_ANIMS = object_config.ZOMBIE_ANIMS
OBJECT_LIST = object_config.OBJECT_LIST


# ITEMS
ITEM_STATS = item_config.ITEM_STATS
HAND_STATS = item_config.HAND_STATS
ITEM_ALLOWED = item_config.ITEM_ALLOWED


# GAME STRUCTURE
WORLD_STRUCT = structure_config.WORLD_STRUCT
RESOURCES_STRUCT = {'blocks_collisions': None}


# LIGHTS
SUNLIGHT_COLOR = (0.65, 0.65, 0.65, 1)
MOONLIGHT_COLOR = (0.30, 0.30, 0.42, 1)
AMBIENTLIGHT_DAY_COLOR = (0.55, 0.50, 0.50, 1)
AMBIENTLIGHT_NIGHT_COLOR = (0.09, 0.09, 0.13, 1)
SHADOWS_FILMSIZE = 32


# UTILS
ROTATION_FLIPS = ( # reverse x, reverse y, reverse x <=> y
    (False, False, False),
    (True, False, True),
    (True, True, False),
    (False, True, True),
)

TEST_GRID = (
    (-1,  1), ( 0,  1), ( 1,  1),
    (-1,  0), ( 0,  0), ( 1,  0),
    (-1, -1), ( 0, -1), ( 1, -1),
)
