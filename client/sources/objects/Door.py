import sources.objects.utils as utils
import sources.config.object_config as object_config

"""
    Door datas structure
"""
class Door(object):

    need_update = set()

    def __init__(self, id, block, rotation, door_model = None, door_node = None):
        utils.InstanceCounter.add("Door")
        self.id = id
        self.block = block
        self.door_model = door_model
        self.door_node = door_node
        self.r = (rotation + 1) % 4
        self.x = block['x']
        self.y = block['y']
        self.z = block['z']
        self.pos = None
        self.state = None
        self.need_link = False
        self.hidden = False
        self.rotation_time = 0
        self.rotation_start = 0
        self.rotation_goal = 0
        self.hit_progress = 1. + 1.
        self.life = object_config.DOOR_MAX_LIFE
        self.rotated = False

    def __del__(self):
        utils.InstanceCounter.rm("Door")
        self.clear()

    def clear(self):
        if self.door_model:
            self.door_model.detachNode()
            self.door_model.removeNode()
            self.door_model = None
        if self.door_node:
            self.door_node.detachNode()
            self.door_node.removeNode()
            self.door_node = None
        if self in Door.need_update:
            Door.need_update.remove(self)

    def hit(self, life):
        self.life = life
        self.hit_progress = 0.
        Door.need_update.add(self)

    def setR(self, rotation):
        # If first rotation set, do not interpolate
        if not self.rotated:
            self.rotation_goal = rotation
            self.door_model.setH(rotation)
            self.rotated = True
        else:
            self.rotation_time = 0
            self.rotation_start = self.door_model.getH()
            self.rotation_goal = rotation
            Door.need_update.add(self)

    def update(self, delay):
        again = False
        if self.door_model.getH() != self.rotation_goal:
            self.rotation_time += delay * 4.0
            if self.rotation_time > 1.0:
                self.rotation_time = 1.0
            self.door_model.setH(
                self.rotation_start * (1.0 - self.rotation_time)
                + self.rotation_goal * self.rotation_time
            )
            again = True
        if self.hit_progress < 1.:
            # Equation ratio
            def eq(v):
                if v > 0.75:
                    return (v - 0.75) * 4. - 1.
                elif v > 0.25:
                    return 1. - (v - 0.25) * 4.
                return v * 4.
            r = eq(self.hit_progress)
            r2 = eq(self.hit_progress * 2. % 1.)
            # Apply transformations
            self.door_model.setP(r * 1.5)
            self.door_model.setR(r * 1.5)
            self.door_model.setX(r2 * 0.03)
            self.hit_progress += delay * 7.
            again = True
        else:
            self.door_model.setP(0)
            self.door_model.setR(0)
            self.door_model.setX(0)
        return again

    def show(self, mask):
        if self.hidden:
            self.door_model.show(mask)
            self.hidden = False

    def hide(self, mask):
        if not self.hidden:
            self.door_model.hide(mask)
            self.hidden = True
