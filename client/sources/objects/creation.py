import random as rand
import sources.config.constants as const
import sources.utils.resources as resources
import sources.utils.positioning as positioning

from sources.objects.ZombieObject import ZombieObject
from sources.objects.PlayerObject import PlayerObject

def create_zombie(world, type, id, x, y, z, rotation = None):
    new_zombie = ZombieObject(id, world['cg'], world['collision_tree'], world['zombies']['tree'], type, x, y, z, rotation)
    new_zombie.applyFog(world)
    world['zombies']['list'][id] = new_zombie
    return new_zombie

def create_player(world, type, id, x, y, z, rotation = None):
    new_player = PlayerObject(id, world['cg'], world['collision_tree'], type, x, y, z, rotation)
    new_player.applyFog(world)
    world['players'][id] = new_player
    return new_player

def create_animated_static(world, type, x, y, z, rotation, anim = 'idle'):
    node = world['cg'].getNode(x, y, z).attachNewNode("%s_%d_%d_%d_%d" % (
        type.split("/")[-1], x, y, z, rotation)
    )
    actor = resources.load_static(
        type, "datas/blocks/texture.png",
    )
    actor.instanceTo(node)
    positioning.position_node(node, x, y, z, rotation, 1)
    return node, z

def create_item(world, type, x, y, z, equiped = False):
    model = "datas/objects/items/%s.egg" % type
    texture = "datas/objects/items/%s.png" % type
    item_node = resources.load_model_file(model, texture)
    item_stats = const.ITEM_STATS[type]
    if not equiped:
        angle = rand.randint(0, 360)
        item_node.setScale(item_stats['scale'])
        item_node.reparentTo(world['cg'].getNode(x, y, z))
        positioning.position_item(item_node, type, x, y, z, angle, 0, 0, "on_floor")
    else:
        angle = 0
        item_node.setScale(item_stats['scale'] / const.OBJECT_LIST['male-player1']['scale'])
    return item_node
