import time
import random as rand
import panda3d.core as engine
import sources.objects.utils as utils
import sources.utils.resources as resources
import sources.utils.positioning as positioning

"""

"""
class TextureAnimated(object):

    def __init__(self, filename=None, alpha=False, nearest=False, shadowing=True, node=None):
        utils.InstanceCounter.add("TextureAnimated")
        # init model
        self.node = node
        if not node:
            self.node = resources.load_model_file(filename, alpha=alpha, nearest=nearest)
        if not shadowing:
            self.node.hide(engine.BitMask32.bit(1))
        # inits
        self.shadowing = shadowing
        self.hidden = False
        self.textures = []
        self.frame = 0
        self.max_frame = 0
        self.origin_timing = [0., 0., False]
        self.timing = list(self.origin_timing)
        self.fps = 0
        self.animating = False
        self.pos = engine.Point3()
        self.epos = None
        self.stop_y = 0
        self.force_show = False
        self.last_frame = -1

    def __del__(self):
        utils.InstanceCounter.rm("TextureAnimated")
        self.clear()

    def clear(self):
        if self.node:
            self.node.detachNode()
            self.node.removeNode()
            self.node = None

    def hide(self):
        if not self.hidden:
            if not self.shadowing:
                self.node.hide(engine.BitMask32.bit(0))
            else:
                self.node.hide()
            self.hidden = True

    def show(self):
        if self.hidden:
            if not self.shadowing:
                self.node.show(engine.BitMask32.bit(0))
            else:
                self.node.show()
            self.hidden = False

    def applyFog(self, world):
        fog_texture_stage = world['blocks']['fog'][0]
        fog_texture = world['blocks']['fog'][1]
        wsize = world['infos']['map_def']['size']
        self.node.setTexture(fog_texture_stage, fog_texture)
        positioning.position_fog_texture(self.node, fog_texture_stage, 0, 0, 0, wsize)

    def animate(self, texture, frames, timing, fps, epos=None, force_show=True):
        self.epos = None
        if epos:
            self.epos = engine.Point3(*epos)
            self.epos.y *= 1. - rand.random() / 3.
        self.textures = []
        for frame in frames:
            self.textures.append(resources.load_texture(texture % frame))
        self.anim_start = time.time()
        self.origin_timing = timing
        self.timing = list(timing)
        self.fps = fps
        self.frame = 0
        self.max_frame = len(self.textures)
        self.animating = True
        self.atime = self.timing[1] - self.timing[0]
        self.stop_y = 100000000.
        self.force_show = force_show
        self.last_frame = -1

    def stopAtY(self, y):
        self.stop_y = y

    def reparentTo(self, node):
        self.node.reparentTo(node)

    def setPos(self, x, y, z):
        self.pos = engine.Point3(x, y, z)
        self.node.setPos(x, y, z)

    def setHpr(self, x, y, z):
        self.node.setHpr(x, y, z)

    def setH(self, h):
        self.node.setH(h)

    def setP(self, p):
        self.node.setP(p)

    def setR(self, r):
        self.node.setR(r)

    def setX(self, x):
        self.node.setX(x)

    def setY(self, y):
        self.node.setY(y)

    def setZ(self, z):
        self.node.setZ(z)

    def setScaleRatio(self, scale):
        self.node.setScale(scale)

    def setScale(self, x, y, z):
        self.node.setScale(x, y, z)

    def setLightOff(self):
        self.node.setLightOff()

    def setTwoSided(self, sided):
        self.node.setTwoSided(sided)

    def setAlphaScale(self, scale):
        self.node.setAlphaScale(scale)

    def frame_actions(self, frametime):
        if not self.animating:
            return self.hide()
        if self.node:
            self.timing[0] -= frametime
            self.timing[1] -= frametime
            # End animation
            if self.timing[1] < 0:
                if self.timing[2]:
                    self.timing = list(self.origin_timing)
                else:
                    self.animating = False
                    self.hide()
                    return None
            # Animation started
            if self.timing[0] < 0:
                if self.force_show:
                    self.show()
                frame = int(-self.timing[0] * self.fps)
                frame %= self.max_frame
                if frame != self.last_frame:
                    self.node.setTexture(self.textures[frame])
                self.last_frame = frame
                # Eventually interpolate
                if self.epos:
                    ratio = -self.timing[0] / self.atime
                    iratio = 1. - ratio
                    self.node.setPos(self.pos * iratio + self.epos * ratio)
                    # Eventually stop at Y
                    if self.node.getY() < -self.stop_y:
                        self.animating = False
                        self.hide()
                        return None
