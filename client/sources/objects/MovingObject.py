import math
import sources.config.constants as const
import sources.utils.trees as trees
import sources.utils.positioning as positioning
import sources.config.configfile as configfile
import sources.objects.utils as utils
import panda3d.core as engine

"""
    Generic class for moving object,
    will be used as parent by Players and Zombies
"""
class MovingObject(object):

    def __init__(self, id, culling_grid, collision_tree, ptree = None):
        utils.InstanceCounter.add("MovingObject")
        self.id = id
        self.collision_tree = collision_tree
        self.culling_grid = culling_grid
        self.node = None
        self.type = None
        self.analysed = None
        self.goal = None
        self.stats = None
        self.anim_played = {}
        self.anim_played_name = {}
        self.last_lookat = None
        self.shown = True
        self.anim_paused = False
        self.hitted = None
        self.dead = None
        self.dead_time = None
        self.mx = -1
        self.my = -1
        self.hidden = False
        self.position_tree = ptree
        self.is_main = False
        self.shadowing = True
        if configfile.get("squality") < 1024:
            self.shadowing = False
        self.last_xlook = 0
        self.last_ylook = 0
        self.lsmooth = 3.0
        self.pos = engine.Point3()
        self.ittime = -1
        self.itpos = None
        self.itratio = 10.
        self.extra_culling_updater = None
        self.extra_fog_applyier = None
        self.parent = None

    def __del__(self):
        utils.InstanceCounter.rm("MovingObject")
        self.clear()

    def clear(self):
        self.world = None
        if self.position_tree:
            self.position_tree.deleteId(self.id)
        if self.collision_tree:
            self.collision_tree.deleteId(self.id)
        self.node.delete()
        self.collision_tree = None
        self.culling_grid = None
        self.position_tree = None
        self.extra_culling_updater = None

    # Apply fog texture on object
    def applyFog(self, world):
        fog_texture_stage = world['blocks']['fog'][0]
        fog_texture = world['blocks']['fog'][1]
        wsize = world['infos']['map_def']['size']
        self.node.setTexture(fog_texture_stage, fog_texture)
        positioning.position_fog_texture(self.node, fog_texture_stage, 0, 0, 0, wsize)
        if self.extra_fog_applyier:
            self.extra_fog_applyier(world)

    def _lookAt(self, x, y, z = None):
        tx = self.getX() - x
        if self.rotation_correction[0]:
            tx = -tx
        ty = self.getY() - y
        if self.rotation_correction[1]:
            ty = -ty
        if self.rotation_correction[2]:
            tx, ty = ty, tx
        tx += self.getX()
        ty += self.getY()
        if self.node:
            self.node.lookAt(tx, ty, self.node.getZ())

    def lookAt(self, x, y, z, smooth = None):
        if smooth is not None:
            px = self.getX()
            py = self.getY()
            rx = px - x
            ry = py - y
            if self.last_lookat:
                ratio = min(1., smooth * self.lsmooth)
                nx = (self.last_lookat[0] - rx) * ratio
                ny = (self.last_lookat[1] - ry) * ratio
                self.last_lookat[0] -= nx
                self.last_lookat[1] -= ny
                self._lookAt(px - self.last_lookat[0], py - self.last_lookat[1])
                return True
            self.last_lookat = [rx, ry]
        else:
            self._lookAt(x, y, z)

    def _getHeightFromPos(self, world_blocks, x, y, z):
        block = trees.read_from_tree(world_blocks, math.floor(x), math.floor(y), math.floor(z))
        if block:
            return block['c'].getCollisionHeight(x, y, z)
        else:
            return -100

    def _getMapZPos(self, wb, x, y, z):
        zz = z % 1
        cz = self._getHeightFromPos(wb, x, y, z)
        if zz <= const.WALK_HEIGHT_MARGIN:
            cz = max(self._getHeightFromPos(wb, x, y, z - 1.0), cz)
        if zz >= 1.0 - const.WALK_HEIGHT_MARGIN:
            cz = max(self._getHeightFromPos(wb, x, y, z + 1.0), cz)
        return cz

    def setPos(self, new_pos, world_blocks = None, no_recursion = False):
        if world_blocks:
            old_z = self.getZ()
            nx = new_pos.getX()
            ny = new_pos.getY()
            new_z = self._getMapZPos(world_blocks, nx, ny, old_z)
            if (math.fabs(old_z - new_z) < const.WALK_HEIGHT_MARGIN
                and self.collision_tree.isValueWalkable(self.id, nx, ny, new_z)):
                new_pos.setZ(new_z)
                self.pos = engine.Point3(new_pos)
            elif not no_recursion:
                new_pos.setX(self.getX())
                self.setPos(new_pos, world_blocks, True)
                new_pos.setX(nx)
                new_pos.setY(self.getY())
                self.setPos(new_pos, world_blocks, True)
        else:
            self.pos = engine.Point3(new_pos)
        self.collision_tree.setPosVal(self.id, self.getPos(), circle=True)
        if self.position_tree:
            self.position_tree.setPosVal(self.id, self.getPos(), circle=True)
        self.refreshCulling()

    def getPos(self):
        return self.pos

    def getVisualPos(self):
        return self.node.getPos()

    def getX(self):
        return self.pos.getX()

    def getY(self):
        return self.pos.getY()

    def getZ(self):
        return self.pos.getZ()

    def visibilityChange(self):
        pass

    def hide(self):
        if not self.hidden:
            if self.shadowing:
                self.node.hide()
            else:
                self.node.hide(engine.BitMask32.bit(0))
            self.hidden = True
            self.visibilityChange()

    def show(self):
        if self.hidden:
            if self.shadowing:
                self.node.show()
            else:
                self.node.show(engine.BitMask32.bit(0))
            self.hidden = False
            self.visibilityChange()

    # Reparent object to correct scene sub-node
    def refreshCulling(self):
        nx = int(self.getX()) / const.BLOCKS_PER_SIZE
        ny = int(self.getY()) / const.BLOCKS_PER_SIZE
        if nx != self.mx or ny != self.my:
            self.parent = self.culling_grid.get(nx, ny)
            self.node.reparentTo(self.parent)
            self.mx = nx
            self.my = ny
        if self.extra_culling_updater:
            self.extra_culling_updater(self)

    # Simple animation activation
    def playAnim(self, anim):
        subpart = "body"
        anim_name = self.getAnimName(anim)
        if self.anim_played_name.get(subpart, "") != anim_name:
            self.anim_played_name[subpart] = anim_name
            self.anim_played[subpart] = anim
            self.node.loop(anim_name)
            self.onPlayAnim()

    # Called when an animation is played
    def onPlayAnim(self):
        pass

    # Stop current animation
    def freeze(self):
        self.anim_played_name = {}
        self.anim_played = {}
        self.node.stop()

    # Called each frame
    def frameActions(self, frametime):
        # Do visual interpolation
        self.ittime -= frametime * self.itratio
        if self.ittime > 0. and self.itpos:
            rtime = 1. - self.ittime
            self.node.setPos(self.itpos * self.ittime + self.pos * rtime)
        else:
            self.node.setPos(self.pos)

    # Called when forcing a new position (and need an interpolation)
    def startInterpolation(self):
        if not self.hidden:
            self.ittime = 1.
            self.itpos = engine.Point3(self.getVisualPos())

    # Called when forcing a new position (and don't need an interpolation)
    def stopInterpolation(self):
        self.ittime = -1
        self.itpos = None

    # Hashable position (ints)
    def getHashPos(self):
        return int(self.pos.getX()), int(self.pos.getY())
