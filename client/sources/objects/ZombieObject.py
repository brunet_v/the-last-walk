import time
import random as rand
import sources.config.constants as const
import sources.utils.resources as resources
import sources.objects.utils as utils
import panda3d.core as engine

from sources.objects.MovingObject import MovingObject

"""
    Specific Zombies class, easy wrapper around MovingObject,
    also handle animations
"""
class ZombieObject(MovingObject):

    def __init__(self, id, culling_grid, world_collisions, ptree, type, x, y, z, rotation):
        utils.InstanceCounter.add("ZombieObject")
        MovingObject.__init__(self, id, culling_grid, world_collisions, ptree)
        if rotation is None:
            rotation = rand.randint(0, 359)
        self.node = resources.create_object(type, nearest=False)
        self.node.setHpr(rotation, 0, 0)
        if not self.shadowing:
            self.node.hide(engine.BitMask32.bit(1)) # Sun/Moon shadow caster
        self.setPos(engine.Point3(x, y, z), no_recursion=True)
        self.type = type
        self.rotation = rotation
        self.stats = const.OBJECT_LIST[type]['stats']
        self.analysed = time.time()
        self.goal = None
        self.attacking = None
        self.hide()
        self.birth = time.time() + 0.5
        self.rotation_correction = const.ROTATION_FLIPS[1]
        self.ipos = None
        self.walkanim = 'walk%d' % (id % 3 + 1)
        self.attackanims = ['attack%d-1' % (id % 3 + 1),]
        self.lsmooth = 6.
        self.itratio = 6.

    def __del__(self):
        utils.InstanceCounter.rm("ZombieObject")
        self.clear()
        MovingObject.__del__(self)

    def clear(self):
        MovingObject.clear(self)

    # Attacking event
    def attack(self, x, y, z, delay):
        self.attacking = time.time() + delay
        self.attack_pos = utils.Struct({
            "x": x,
            "y": y,
            "z": self.getZ(),
        })

    # Interpolate mouvements if needed, otherwise use normal mouvement
    def setPos(self, new_pos, world_blocks = None, no_recursion = False):
        return MovingObject.setPos(self, new_pos, world_blocks, no_recursion)
        if not no_recursion and not world_blocks: # interpolate movement
            self.ipos = self.getPos()
            self.ielapsed = 0.0
            self.igoal = new_pos
        else:
            MovingObject.setPos(self, new_pos, world_blocks, no_recursion)

    def getAnimName(self, anim):
        return {
            'WALK': self.walkanim,
            'ATTACK': rand.choice(self.attackanims),
        }.get(anim, 'wait')
        # TODO : randomize animation with id / type
        return {
            'DIE': 'player-zombie-die',
            'HIT': 'player-zombie-hit',
            'BIRTH': 'player-zombie-wait',
            'WAIT': 'player-zombie-wait',
            'WALK': 'player-zombie-walk',
            'ATTACK': 'player-zombie-attack',
        }[anim]

    # Called each frame
    def frameActions(self, frametime):
        # MovingObject method
        MovingObject.frameActions(self, frametime)
        # Interpolate mouvement in case of server forcing
        if self.ipos and not self.dead:
            self.ielapsed += frametime * 2.0
            if self.ielapsed > 1.0:
                self.ielapsed = 1.0
            npos = self.ipos * (1.0 - self.ielapsed) + self.igoal * self.ielapsed
            if self.ielapsed >= 1.0:
                self.ipos = None
            MovingObject.setPos(self, npos)
        # LookAt
        lpos = None
        if self.attacking:
            lpos = self.attack_pos
        elif self.goal:
            lpos = self.goal
        if lpos:
            self.lookAt(lpos.x, lpos.y, lpos.z, frametime)
