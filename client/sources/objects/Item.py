import panda3d.core as engine
import sources.objects.utils as utils
import sources.config.configfile as configfile
import sources.utils.positioning as positioning

"""
    Class representing item on the ground
"""
class Item(object):

    def __init__(self, model):
        utils.InstanceCounter.add("Item")
        self.hidden = False
        self.model = model
        self.shadowing = True
        if configfile.get("squality") <= 2048:
            self.model.hide(engine.BitMask32.bit(1)) # Sun/Moon shadow caster
            self.shadowing = False
        self.hide()
        self.cleared = False

    def __del__(self):
        utils.InstanceCounter.rm("Item")
        self.clear()

    def clear(self):
        if not self.cleared:
            self.model.setLightOff()
            self.model.detachNode()
            self.model.removeNode()
        self.cleared = True

    def applyFog(self, world):
        fog_texture_stage = world['blocks']['fog'][0]
        fog_texture = world['blocks']['fog'][1]
        wsize = world['infos']['map_def']['size']
        self.model.setTexture(fog_texture_stage, fog_texture)
        positioning.position_fog_texture(self.model, fog_texture_stage, 0, 0, 0, wsize)

    def hide(self):
        if not self.hidden:
            if self.shadowing:
                self.model.hide()
            else:
                self.model.hide(engine.BitMask32.bit(0))
            self.hidden = True

    def show(self):
        if self.hidden:
            if self.shadowing:
                self.model.show()
            else:
                self.model.show(engine.BitMask32.bit(0))
            self.hidden = False

    def getX(self):
        return self.model.getX()

    def getY(self):
        return self.model.getY()

    def getZ(self):
        return self.model.getZ()

    def getPos(self):
        return self.model.getPos()

    def setColorScale(self, r, g, b, a):
        return self.model.setColorScale(r, g, b, a)

    def getHashPos(self):
        return int(self.model.getX()), int(self.model.getY())

