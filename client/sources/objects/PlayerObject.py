import time
import math
import random as rand
import panda3d.core as engine
import sources.objects.utils as utils
import sources.config.constants as const
import sources.config.object_config as object_config
import sources.utils.resources as resources
import sources.utils.positioning as positioning

from sources.objects.MovingObject import MovingObject
from sources.objects.TextureAnimated import TextureAnimated
from sources.utils.StackStock import StackStock

"""
    Specific Players class, contain every player-specific status and actions
"""
class PlayerObject(MovingObject):

    def __init__(self, id, culling_grid, world_collisions, type, x, y, z, rotation):
        utils.InstanceCounter.add("PlayerObject")
        MovingObject.__init__(self, id, culling_grid, world_collisions)
        # Attributes
        self.inventory = {}
        self.type = type
        self.goal = None
        self.equiped = None
        self.aiming = None
        self.used_item = None
        self.stats = const.OBJECT_LIST[type]['stats']
        self.is_main = False
        self.freezed = False
        self.shotting = False
        self.stmove = False
        self.world = None
        self.forced_animation = None
        self.last_turn = None
        self.please_unaim = False
        self.prev_anim = {}
        self.curr_anim = {}
        self.bones = {}
        self.anim_randomized = {}
        self.item_offset_stack = StackStock()
        self.allieds = set()
        self.prev_lookat = (x, y)
        self.lsmooth = 8.0
        self.itratio = 8.0
        self.mspeed = 0.0
        self.rotation_correction = const.ROTATION_FLIPS[1]
        self.light_on = True
        self.light_updater = None
        self.high_cut = False
        self.pre_goal = engine.Point3()
        self.force_anim_refresh = False
        self.anims = set(object_config.PLAYER_ANIMS)
        self.anim_offset = 0
        # Shoot effect
        self.last_shoot = []
        self.shoot_saving = []
        self.shoot_dummy = []
        self.shoot = []
        self.nb_shoots = 4
        for i in xrange(1 + self.nb_shoots):
            shoot_dummy = engine.NodePath("player%d_shoot_dummy%d" % (self.id, i))
            shoot = TextureAnimated(
                filename="datas/particles/shoot/shoot.egg",
                shadowing=False,
                alpha=True,
            )
            shoot.hide()
            shoot.setLightOff()
            shoot.setTwoSided(False)
            shoot.reparentTo(shoot_dummy)
            self.shoot.append(shoot)
            self.shoot_dummy.append(shoot_dummy)
        for i in xrange(self.nb_shoots):
            self.shoot_saving.append(engine.NodePath("player%d_shoot_saving%d" % (self.id, i)))
        self.shoot_saving_id = 0
        self.extra_fog_applyier = self.applyExtraFog
        # Rendering and datas
        self.node = resources.create_object(type, nearest=False)
        self.node.makeSubpart("legs", ["Legs Origin",])
        self.node.makeSubpart("torso", ["Torso Origin",])
        self.node.enableBlend()
        if not self.shadowing:
            self.node.hide(engine.BitMask32.bit(1)) # Sun/Moon shadow caster
        self.hide()
        self.setPos(engine.Point3(x, y, z))
        self.lookAt(x - 5.0 + rand.random() * 10, y - 5.0 + rand.random() * 10, 0)
        self.lookpoint = engine.Point3(self.getPos() * 2.0)
        self.createText()
        self.createBonesLinks()
        self.tired = None
        self.sprint = False
        self.flashing = False
        # Stats
        self.max_life = const.OBJECT_LIST[type]['stats']['life']
        self.life = self.max_life
        self.energy = 100.
        self.stress = 0.
        self.thirst = 0.
        self.hunger = 0.
        # Leg node
        self.legnode = engine.NodePath("player%d_leg_dummy" % self.id)
        legs_bone = self.bones["Legs Origin"]
        self.legnode.setHpr(legs_bone.getHpr())
        self.legnode.setPos(legs_bone.getPos())
        self.legnode_rotation = legs_bone.getH()
        self.node.controlJoint(self.legnode, "legs", "Legs Origin")
        # Start anims
        self.legsAnimation("WAIT")
        self.torsoAnimation("WAIT")

    def __del__(self):
        utils.InstanceCounter.rm("PlayerObject")
        self.clear()
        MovingObject.__del__(self)

    def clear(self):
        MovingObject.clear(self)
        self.world = None
        self.light_updater = None
        self.item_offset_stack.clear()
        for id in self.inventory.keys():
            self.popItem(id)
        if self.msg:
            self.msg.billboard.detachNode()
            self.msg.billboard.removeNode()
            self.msg.node.detachNode()
            self.msg.node.removeNode()
            self.msg = None
        if self.shoot:
            for shoot in self.shoot:
                shoot.clear()
            self.shoot = None
        if self.shoot_dummy:
            for shoot_dummy in self.shoot_dummy:
                shoot_dummy.detachNode()
                shoot_dummy.removeNode()
            self.shoot_dummy = None
        if self.shoot_saving:
            for shoot_saving in self.shoot_saving:
                shoot_saving.detachNode()
                shoot_saving.removeNode()
            self.shoot_saving = None
        if self.legnode:
            self.legnode.detachNode()
            self.legnode.removeNode()
            self.legnode = None

    def applyExtraFog(self, world):
        for i, shoot in enumerate(self.shoot):
            if i:
                shoot.applyFog(world)

    # Create bones attachs
    def createBonesLinks(self):
        bones = [
            "Torso", # Torse
            "Hips", # Bassin

            "Right Handgrip", # Main droite
            "Left Handgrip", # Main gauche

            "Right Hand", # Main droite
            "Left Hand", # Main gauche

            "Right Arm", # Bras droit
            "Left Arm", # Bras droit

            "Right Thigh", # Cuisse droite
            "Left Thigh", # Cuisse gauche

            "Right Calf", # Mollet droit
            "Left Calf", # Mollet gauche

            "Legs Origin", # Legs dummy
            "Torso Origin", # Torso dummy
        ]
        self.bones = {}
        for name in bones:
            self.bones[name] = self.node.exposeJoint(None, "modelRoot", name)

    # Create text message container
    def createText(self):
        msgtxt = engine.TextNode('player_msgbar_%d' % self.id)
        msgtxt.setWordwrap(20)
        msgtxt.setCardColor(0, 0, 0, 0.7)
        msgtxt.setCardAsMargin(0.45, 0.45, 0.45, 0.45)
        msgtxt.setCardDecal(True)
        msgtmp = self.node.attachNewNode('player_msgbar_%d_dummy' % self.id)
        msgtmp.setZ(10.0)
        msgtmp.setBillboardPointEye()
        msgnode = msgtmp.attachNewNode(msgtxt)
        msgnode.setTexture(resources.load_texture('./datas/ui/button.png'))
        msgnode.setScale(1.35)
        msgnode.setX(2.5)
        msgnode.setBin("fixed", 41)
        msgnode.setDepthTest(False)
        msgnode.setDepthWrite(False)
        msgnode.setBillboardPointEye()
        msgnode.hide()
        self.msg = utils.Struct({
            "billboard": msgtmp,
            "node": msgnode,
            "text": msgtxt,
            "fadetimer": None,
            "hidetimer": None,
        })

    # Display ingame text message
    def showMsg(self, text):
        self.msg.text.setText(text)
        self.msg.fadetimer = time.time() + 8.0
        self.msg.hidetimer = self.msg.fadetimer + 0.5
        self.msg.node.show()
        self.msg.node.setAlphaScale(1.0)

    # When player is designed to be the controlled player
    def hooked(self, world):
        self.is_main = True
        self.world = world
        self.looking = None

    # Update equiped item position (and item light)
    def updateEquiped(self):
        if self.equiped:
            positioning.position_item(
                self.equiped[3], self.equiped[1], 0, 0, 0, 0, 0, 0,
                self.anim_played_name.get("torso", ""), bones=self.bones
            )
        for id, item in self.inventory.iteritems():
            if item[2]:
                if self.equiped and self.equiped[0] == id:
                    item[2].hide()
                else:
                    item[2].show()
        if self.light_updater:
            self.light_updater(self)

    # Triggered when flashing on/off
    def updateFlashing(self):
        self.updateEquiped()

    # Triggered when hide/show
    def visibilityChange(self):
        self.updateEquiped()

    # Triggered when a new animation is played
    def onPlayAnim(self):
        self.updateEquiped()

    # Activate small animated particles (when firing)
    def shootAnimationSprite(self, item_stats):
        # Shoot fire (effect)
        if not self.hidden:
            key_shoot = 'flashing-sprite'
            if key_shoot in item_stats and self.equiped:
                item_flashing_sprite = item_stats[key_shoot]
                # Set shoot_dummy
                shoot_dummy = self.shoot_dummy[0]
                shoot_dummy.setPos(*item_flashing_sprite['pos'])
                shoot_dummy.setScale(*item_flashing_sprite['scale'])
                shoot_dummy.setR(rand.randint(0, 360))
                shoot_dummy.setY(shoot_dummy.getY() + rand.random() * 0.1 - 0.05)
                shoot_dummy.reparentTo(self.equiped[3])
                # Set shoot model
                shoot = self.shoot[0]
                shoot.setHpr(*item_flashing_sprite['hpr'])
                shoot.setScaleRatio(1. - rand.random() / 3.)
                shoot.setAlphaScale(item_flashing_sprite['alpha'])
                # Schedule anim
                shoot.animate(
                    texture=item_flashing_sprite['texture'],
                    timing=item_flashing_sprite['timing'],
                    frames=item_flashing_sprite['frames'],
                    fps=item_flashing_sprite['fps'],
                )
        # Bullet trail (effect)
        key_bullet = 'flashing-bullet'
        if not self.high_cut and key_bullet in item_stats and self.equiped:
            self.last_shoot = []
            item_flashing_bullet = item_stats[key_bullet]
            for i in xrange(item_flashing_bullet['nb']):
                self.shoot_saving_id = (self.shoot_saving_id + 1) % self.nb_shoots
                sid = self.shoot_saving_id
                shoot_saving = self.shoot_saving[sid]
                # Set shoot_dummy
                shoot_dummy = self.shoot_dummy[1 + sid]
                shoot_dummy.setPos(*item_flashing_bullet['pos'])
                shoot_dummy.setR(rand.randint(0, 360))
                dia = item_flashing_bullet['disp']
                rad = dia / 2.
                shoot_dummy.setH(rand.random() * dia - rad)
                shoot_dummy.setP(rand.random() * dia - rad)
                shoot_dummy.reparentTo(shoot_saving)
                # Set shoot model
                shoot = self.shoot[1 + sid]
                shoot.setHpr(*item_flashing_bullet['hpr'])
                shoot.setScale(*item_flashing_bullet['scale'])
                shoot.setAlphaScale(item_flashing_bullet['alpha'])
                # Schedule anim
                shoot.animate(
                    texture=item_flashing_bullet['texture'],
                    timing=item_flashing_bullet['timing'],
                    frames=item_flashing_bullet['frames'],
                    fps=item_flashing_bullet['fps'],
                    epos=item_flashing_bullet['epos'],
                    force_show=False,
                )
                shoot.hide()
                self.last_shoot.append(sid)

    # Item effect callback
    def effectResult(self, infos):
        irange = infos.get('range_effect', 0)
        # Effect confirmed, scheduling visuals
        if irange:
            absolute = self.equiped[3]
            for sid in self.last_shoot:
                shoot_saving = self.shoot_saving[sid]
                shoot_saving.setPos(absolute.getPos(self.parent))
                shoot_saving.setHpr(absolute.getHpr(self.parent))
                shoot_saving.setScale(1.)
                shoot_saving.reparentTo(self.parent)
                self.shoot[sid + 1].stopAtY(irange - 2.)
                self.shoot[sid + 1].show()
            self.last_shoot = []

    # Activated item trigger
    def useItem(self, type):
        if type in const.ITEM_STATS:
            item_stats = const.ITEM_STATS[type]
        else:
            item_stats = const.HAND_STATS
        self.item_stats = item_stats
        self.item_timings = item_stats['timings']
        self.used_item = [time.time(), type, False]
        self.shootAnimationSprite(item_stats)
        self.force_anim_refresh = True

    # When item effect triggered
    def effectItem(self):
        type = self.used_item[1]
        if type in const.ITEM_STATS:
            item_stats = const.ITEM_STATS[type]
        else:
            item_stats = const.HAND_STATS
        self.used_item[2] = True
        if item_stats['need_energy']:
            self.setEnergy(self.energy - item_stats['need_energy'])

    # Put item in inventory (displays on the player too)
    def putItem(self, id, type, load):
        import sources.objects.creation as creation
        back_item = None
        if "visible" in const.ITEM_STATS[type]:
            back_item = creation.create_item(None, type, 0, 0, 0, True)
            positioning.position_item(
                back_item, type, 0, 0, 0, 0, 0, 0,
                "on_back", self.item_offset_stack.addKey(type, id), self.bones
            )
        self.inventory[id] = (type, load, back_item)

    # Pop item from inventory (detach from player too)
    def popItem(self, id):
        if "visible" in const.ITEM_STATS[self.inventory[id][0]]:
            if id in self.inventory:
                self.inventory[id][2].setLightOff()
                self.inventory[id][2].detachNode()
                self.inventory[id][2].removeNode()
                self.item_offset_stack.delKey(self.inventory[id][0], id)
            else:
                print "Error: invalid inventory item deletion", id
        return self.inventory.pop(id)

    # Get true anim name, using player state
    def getAnimName(self, anim, subpart):
        anim_name = anim.lower()
        if self.equiped:
            equiped_stats = const.ITEM_STATS[self.equiped[1]]
            if subpart in equiped_stats.get("custom_anims", {}).get(anim_name, {}):
                anim_name = "%s/%s-%s" % (self.equiped[1], self.equiped[1], anim_name)
        anim_name = object_config.PLAYER_ANIMS_ALIAS.get(anim_name, anim_name)
        return anim_name

    # Get last played animation
    def getPlayingAnim(self, subpart):
        anim = self.anim_played.get(subpart, "")
        anim_name = self.anim_randomized.get(subpart, "")
        if anim and anim_name:
            frame = self.node.getCurrentFrame(anim_name, partName=subpart)
            return anim, frame
        return None, None

    # Get true anim name if randomization is applicable
    def randomizeAnim(self, anim):
        anims = self.anims
        # Standard
        if anim in anims:
            return anim
        # Postfixed
        postfixed = "%s-anim" % anim
        if postfixed in anims:
            return postfixed
        # Randomized
        size = 0
        while "%s%d-anim" % (anim, size + 1) in anims:
            size += 1
        if size:
            self.anim_offset += 1
            return "%s%d-anim" % (anim, self.anim_offset % size + 1)
        # Default
        return anim

    # Simple animation activation
    def playAnim(self, anim, subpart, sframe = None, force=False):
        anim_name = self.getAnimName(anim, subpart)
        diff = self.anim_played_name.get(subpart, "") != anim_name
        if diff or force:
            self.anim_played_name[subpart] = anim_name
            self.anim_played[subpart] = anim
            anim_name = self.randomizeAnim(anim_name)
            diff = self.anim_randomized.get(subpart, "") != anim_name
            self.anim_randomized[subpart] = anim_name
            self.onPlayAnim()
            # Blending
            if diff:
                curr_anim = self.curr_anim.get(subpart, ("",))
                prev_anim = self.prev_anim.get(subpart, ("",))
                if prev_anim[0]:
                    self.node.setControlEffect(prev_anim[0], 0, partName=subpart)
                    self.node.stop(prev_anim[0], partName=subpart)
                timer = time.time() + const.BLENDING_DELAY
                self.prev_anim[subpart] = (curr_anim[0], timer)
                self.curr_anim[subpart] = (anim_name, timer)
                self.node.setControlEffect(anim_name, 0, partName=subpart)
            # Playing on subpart
            if sframe:
                self.node.pose(anim_name, sframe, partName=subpart)
                self.node.loop(anim_name, False, partName=subpart)
            else:
                self.node.loop(anim_name, partName=subpart)

    # Play player animation (on torso)
    def torsoAnimation(self, anim = None):
        if anim:
            self.playAnim(anim, "torso", force=self.force_anim_refresh)
            self.force_anim_refresh = False
        else:
            anim, frame = self.getPlayingAnim("legs")
            if anim == "AIM-WALK":
                self.playAnim("WAIT", "torso")
            elif anim:
                self.playAnim(anim, "torso", frame)

    # Update legs animation
    def legsAnimation(self, anim = None):
        self.legnode_rotation = 0.
        if anim:
            if not self.aiming or anim not in ["AIM-WALK", "WAIT", "WAIT-AIM",]:
                self.playAnim(anim, "legs")
            else:
                y = self.getY()
                x = self.getX()
                look_angle = math.atan2(self.lookpoint.y - y, self.lookpoint.x - x)
                if anim == "AIM-WALK":
                    goal = self.goal
                    if not goal:
                        goal = self.pre_goal
                    goal_angle = math.atan2(goal.y - y, goal.x - x)
                    true_angle = math.degrees(look_angle - goal_angle)
                    walk_angle = (int(round(true_angle / 90.0)) * 90) % 360
                    self.playAnim("%s-%d" % (anim, walk_angle), "legs")
                    self.legnode_rotation = walk_angle - true_angle
                if anim in ["WAIT", "WAIT-AIM",]:
                    true_angle = math.degrees(look_angle)
                    feet_angle = (int(round(true_angle / 60.0)) * 60) % 360
                    self.playAnim("%s" % (anim,), "legs")
                    self.legnode_rotation = feet_angle - true_angle
        else:
            anim, frame = self.getPlayingAnim("torso")
            if anim:
                self.playAnim(anim, "legs", frame)

    # Update leg rotation
    def legsRotation(self, frametime):
        old = (self.legnode.getH() + 90.) % 360
        tar = self.legnode_rotation % 360
        diff1 = old - tar
        diff2 = old + 360 - tar
        diff3 = old - 360 - tar
        diff = diff1
        if abs(diff2) < abs(diff):
            diff = diff2
        if abs(diff3) < abs(diff):
            diff = diff3
        speed = 400. * frametime
        diff = max(-speed, min(speed, diff))
        new = old - diff
        self.legnode.setH((new - 90.) % 360)

    # Lock the player and force it to play a specified animation
    def forceAnim(self, anim, delay):
        self.forced_animation = (anim, delay + time.time())

    # Called each frame
    def frameActions(self, frametime, game):
        now = time.time()
        # MovingObject method
        MovingObject.frameActions(self, frametime)
        # Legs rotation
        self.legsRotation(frametime)
        # Animation blending
        for subpart, prev_anim in self.prev_anim.iteritems():
            ratio = (prev_anim[1] - now) / const.BLENDING_DELAY
            if ratio < 0:
                self.node.setControlEffect(prev_anim[0], 0, partName=subpart)
                self.node.stop(prev_anim[0], partName=subpart)
            else:
                self.node.setControlEffect(prev_anim[0], ratio, partName=subpart)
        for subpart, curr_anim in self.curr_anim.iteritems():
            ratio_cur = 1.0 - (curr_anim[1] - now) / const.BLENDING_DELAY
            if ratio_cur > 1.0:
                ratio_cur = 1.0
            self.node.setControlEffect(curr_anim[0], ratio_cur, partName=subpart)
        # Player msg fade out
        fade = self.msg.fadetimer
        hide = self.msg.hidetimer
        if hide and now > hide:
            self.msg.fadetimer = None
            self.msg.hidetimer = None
            self.msg.node.hide()
        elif fade and now > fade:
            self.msg.node.setAlphaScale((hide - now) / (hide - fade))
        # Update lookAt
        if not self.forced_animation:
            # Check case (what to look at)
            lx, ly = None, None
            if self.goal and not self.aiming:
                lx, ly = self.goal.getX(), self.goal.getY()
                # If walking towards mouse (not using keyboard)
                if self.is_main and (game.walk_pressed or game.walk2_pressed):
                    tx, ty = game.smpos[0], game.smpos[1]
                    if max(abs(tx - self.getX()), abs(ty - self.getY())) > 1.0:
                        lx, ly = tx, ty
            elif self.aiming:
                lx, ly = self.lookpoint.getX(), self.lookpoint.getY()
                # Better aiming point if played character
                if self.is_main:
                    lx, ly = game.smpos[0], game.smpos[1]
            # If correct, change player orientation
            if lx is not None:
                lspeed = frametime
                if self.aiming:
                    lspeed *= 2.
                if self.used_item:
                    lspeed *= 2.
                self.lookAt(lx, ly, -1, lspeed)
        # Base stats update
        if self.energy < 5.:
            self.tired = const.TIRED_TIME
        additional = self.getAdditionalCost()
        self.setThirst(self.thirst + frametime * const.THIRST_RATE * additional)
        self.setHunger(self.hunger + frametime * const.HUNGER_RATE * additional)
        self.setEnergy(self.energy + frametime * const.ENERGY_REGEN_RATE)
        self.setLife(self.life + frametime * const.HEALTH_REGEN_RATE)
        if self.tired:
            self.tired -= frametime
            if self.tired < 0:
                self.tired = None
        # Update shoot sprite
        for shoot in self.shoot:
            shoot.frame_actions(frametime)

    def setLife(self, life):
        self.life = max(0., min(self.max_life, life))

    def setEnergy(self, energy):
        if not self.dead:
            self.energy = max(0., min(100.,energy))

    def setStress(self, stress):
        if not self.dead:
            self.stress = max(0., min(100.,stress))

    def setThirst(self, thirst):
        if not self.dead:
            self.thirst = max(0., min(100.,thirst))

    def setHunger(self, hunger):
        if not self.dead:
            self.hunger = max(0., min(100.,hunger))

    def getAdditionalCost(self):
        return 1. + self.stress / 100.
