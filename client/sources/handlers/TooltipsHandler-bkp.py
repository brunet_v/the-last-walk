import os
import hashlib
import panda3d.core as engine
import sources.gui.builder as builder
import sources.objects.utils as utils
import sources.world.init as init
import sources.config.configfile as configfile
import sources.config.build_config as build_config
import time

from panda3d.rocket import *
from sources.handlers.LightHandler import LightHandler
from sources.handlers.SoundsHandler import SoundsHandler
from sources.handlers.ShadersHandler import ShadersHandler
from sources.networking.Master import Master
from sources.gui.menus.ListSource import ListSource
from sources.gui.menus.MenuScene import MenuScene
from sources.Game import Game
from sources.utils.Threader import Threader

from direct.gui.OnscreenText import OnscreenText
from direct.gui.OnscreenImage import OnscreenImage

"""
    Handle all tooltips
"""
class TooltipsHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("TooltipsHandler")
        self.is_showed = False
        self.a_path = engine.Filename.fromOsSpecific(os.getcwd())
        self.magic_scale = 28.0 # lol
        self.magic_size = 528.314846839 # LOL
        self.content = {}
        self.content["yolo"] = "Yolo yolo"
        self.content["logininput"] = "This can be your login or e-mail"
        self.content["lbar"] = "This is your life level"
        self.content["tbar"] = "This is your tiredness level"
        self.content["heart"] = "This is your tiredness level"
        self.content["food"] = "You're hungry, you will lose life!"
        self.content["drink"] = "You're thirsty, you will lose life!"
        self.content["avatar"] = "Press tab key to display informations about other players"
        self.tooltip_nodepath = {}
        self.is_showed = {}

    def __del__(self):
        utils.InstanceCounter.rm("TooltipsHandler")
        self.clear()

    def clear(self):
        pass

    def move_text(self, nodepath, x, y):
        if self.wy == 768 or self.wy == 800:
            ox = -5.
            oy = 30.
        elif self.wy == 864 or self.wy == 900:
            ox = 8.
            oy = 25.
        elif self.wy == 960 or self.wy == 1024:
            ox = 17.
            oy = 20.
        elif self.wy == 1050 or self.wy == 1080:
            ox = 25.
            oy = 15.
        else:
            ox = 35.
            oy = 10.
        if self.mx + self.width > self.wx - ox:
            x = ((self.wx - ox - self.width + 40.) / self.wx) * (self.ratio * 2) - self.ratio
        if self.my < 15.:
            y = ((25. / self.wy) * 2 - 1) * -1
        elif self.my + self.height > self.wy + oy:
            y = (((self.wy + oy - self.height + 10.) / self.wy) * 2 - 1) * -1
        # print self.mx, self.my
        nodepath.setX(x)
        nodepath.setZ(y)

    def load_tooltip(self, value, name, x, y, scale=0.026):
        tooltip = engine.TextNode("tooltip")
        tooltip.setText(value)
        tooltip.setCardColor(0.0, 0.0, 0.0, 0.9)
        tooltip.setCardAsMargin(0.5, 0.5, 0.5, 0.5)
        tooltip.setCardDecal(True)
        tooltip.setAlign(engine.TextNode.ALeft)
        tooltip.setWordwrap(15.0)
        tooltip_nodepath = aspect2d.attachNewNode(tooltip)
        tooltip_nodepath.setBillboardPointEye()
        tooltip_nodepath.show(engine.BitMask32.bit(0))
        tooltip_nodepath.setScale(scale)
        tooltip_nodepath.setX(x)
        # tooltip_nodepath.setY(-1.)
        tooltip_nodepath.setZ(y)
        tooltip_nodepath.setColor(0.8, 0.3, 0.3, 0.9)
        tooltip_nodepath.setBin("fixed", 40)
        tooltip_nodepath.setDepthTest(False)
        tooltip_nodepath.setDepthWrite(False)
        self.tooltip_nodepath[name] = tooltip_nodepath

    def get_menus_path(self, path):
        return engine.Filename(
            "%s/datas/ui/menus/%s" % (self.a_path, path)
        ).toOsSpecific()

    def get_real_pos(self, mx, my):
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        self.wx = wx
        self.wy = wy
        ratio = float(wx) / float(wy)
        self.ratio = ratio
        rx = (mx / wx) * (ratio * 2) - ratio
        ry = ((my / wy) * 2 - 1) * -1
        scale = self.magic_scale / float(wy)
        self.scale = scale
        return rx, ry, scale

    def showTooltip(self, name):
        mp = base.win.getPointer(0)
        self.mx = mp.getX()
        self.my = mp.getY()
        x, y, scale = self.get_real_pos(self.mx + 40, self.my + 10)
        if not name in self.is_showed or self.is_showed[name] == False:
            self.load_tooltip(
                self.content[name], name,
                x,
                y,
                scale
            )
            self.is_showed[name] = True
            pt1, pt2 = self.tooltip_nodepath[name].getTightBounds()
            xDim = pt2.getX() - pt1.getX()
            yDim = pt2.getZ() - pt1.getZ()
            self.width = self.magic_size * xDim
            self.height = self.magic_size * yDim
            self.move_text(self.tooltip_nodepath[name], x, y)
        elif name in self.is_showed and self.is_showed[name] == True:
            self.move_text(self.tooltip_nodepath[name], x, y)

    def showYolo(self):
        self.showTooltip("yolo")
    def hideYolo(self):
        self.hide("yolo")
    def showLogininput(self):
        self.showTooltip("logininput")
    def hideLogininput(self):
        self.hide("logininput")

    def show(self, name):
        self.showTooltip(name)

    def hide(self, name):
        if name in self.is_showed and self.is_showed[name] == True:
            self.tooltip_nodepath[name].hide()
            self.is_showed[name] = False
