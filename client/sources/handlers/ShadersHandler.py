import panda3d.core as engine
import sources.objects.utils as utils
import sources.config.configfile as configfile

# This is shit
from direct.filter.CommonFilters import CommonFilters
# This is shit too
from direct.filter.FilterManager import FilterManager

"""
    Handle scenes shaders
"""
class ShadersHandler(object):

    def __init__(self, menus):
        utils.InstanceCounter.add("ShadersHandler")
        self.menus = menus
        # self.filters = CommonFilters(base.win, base.cam)
        self.blurred = False
        self.add_menu_blurr()
        # Activate ambient occlusion
        # if configfile.get("decalnumber") >= 100:
        #     pass

        # Test with FilterManager
        # filterMan = FilterManager(base.win, base.cam)
        # self.colorTex = engine.Texture()
        # self.finalQuad = filterMan.renderSceneInto(colortex=self.colorTex)
        # self.finalQuad.setShader(loader.loadShader("./sources/shaders/filter.sha"))
        # self.finalQuad.setShaderInput("color", self.colorTex)
        # tempnode = engine.NodePath(engine.PandaNode("temp node"))
        # tempnode.setShader(loader.loadShader("./sources/shaders/lightingGen.sha"))
        # base.cam.node().setInitialState(tempnode.getState())

        # normalsBuffer = base.win.makeTextureBuffer("normalsBuffer", 0, 0)
        # normalsBuffer.setClearColor(engine.Vec4(0.5, 0.5, 0.5, 1))
        # self.normalsBuffer = normalsBuffer
        # normalsCamera = base.makeCamera(normalsBuffer, lens=base.cam.node().getLens())
        # normalsCamera.node().setScene(render)
        # tempnode = engine.NodePath(engine.PandaNode("temp node"))
        # tempnode.setShader(loader.loadShader("./sources/shaders/normalGen.sha"))
        # normalsCamera.node().setInitialState(tempnode.getState())

        # drawnScene = normalsBuffer.getTextureCard()
        # drawnScene.setTransparency(1)
        # drawnScene.setColor(1, 1, 1, 0)
        # drawnScene.reparentTo(render2d)
        # self.drawnScene = drawnScene

        # self.separation = 0.0005
        # self.cutoff = 0.1
        # inkGen = loader.loadShader("./sources/shaders/inkGen.sha")
        # drawnScene.setShader(inkGen)
        # drawnScene.setShaderInput("separation", engine.Vec4(self.separation, 0, self.separation, 0))
        # drawnScene.setShaderInput("cutoff", engine.Vec4(self.cutoff, self.cutoff, self.cutoff, self.cutoff))

    def __del__(self):
        utils.InstanceCounter.rm("ShadersHandler")
        self.clear()

    def clear(self):
        self.menus = None
        self.stop_menu_blurr()

    def shader_path(self, path):
        return engine.Filename("./datas/shaders/%s" % path)

    def add_menu_blurr(self):
        if not self.blurred:
            self.blurred = True
            # self.filters.setVolumetricLighting(
            #     caster=self.menus.lights.light
            # )
            # self.filters.setInverted()
            # self.filters.setAmbientOcclusion(
            #     numsamples=32,
            # )
            # self.filters.setBloom(
            # )
            # self.filters.setBlurSharpen(amount=0.75)

    def stop_menu_blurr(self):
        if self.blurred:
            self.blurred = False
            # self.filters.delBlurSharpen()

    def frame_actions(self, frametime):
        pass
        # self.finalQuad.setShaderInput("color", self.colorTex)
