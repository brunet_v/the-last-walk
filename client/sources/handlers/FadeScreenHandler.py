import time
import panda3d.core as engine
import sources.objects.utils as utils
import sources.config.build_config as build_config

from direct.gui.OnscreenImage import OnscreenImage

class FadeScreenHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("FadeScreenHandler")
        self.img = self.load_image("datas/ui/fade.png", 3000, 3000)
        self.img_2 = self.load_image("datas/ui/fadescreen.png", 80, 50)
        self.firsts = time.time() + 1.5
        self.skip_next = False
        self.alpha = 1.
        self.to_alpha = 1.
        self.speed = 0.0
        self.update_alpha()
        self.debug = build_config.build_option("debug-fade")

    def __del__(self):
        utils.InstanceCounter.rm("FadeScreenHandler")
        self.clear()

    def clear(self):
        pass

    def load_image(self, image_file, xsize, ysize):
        img = OnscreenImage(image = image_file)
        img.setScale(xsize, 1, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.show()
        return img

    def fade_in(self):
        if self.debug:
            print "fade in"
        self.speed = 2.
        self.to_alpha = 1.

    def fade_out(self):
        if self.debug:
            print "fade out"
        self.speed = 2.
        self.to_alpha = 0.

    def fade_in_menu(self):
        if self.debug:
            print "fade in menu"
        self.speed = 10.
        self.to_alpha = 1.

    def fade_out_menu(self):
        if self.debug:
            print "fade out menu"
        self.speed = 10.
        self.to_alpha = 0.

    def fade_out_first(self):
        if self.debug:
            print "fade out first"
        self.speed = 1.
        self.to_alpha = 0.

    def fade_in_option(self):
        if self.debug:
            print "fade in option"
        self.speed = 10.
        self.to_alpha = 1.

    def fade_out_option(self):
        if self.debug:
            print "fade out option"
        self.speed = 1.
        self.to_alpha = 0.
        self.skip_next = 5

    def update_alpha(self):
        self.img.setColor(1., 1., 1., self.alpha)
        if self.to_alpha == 1. and False: # TMP
            self.img_2.setColor(1., 1., 1., self.alpha ** 2)
            self.img_2.setPos(base.win.getXSize() - 120, 0, -base.win.getYSize() + 100)
        else:
            self.img_2.setColor(1., 1., 1., 0.)

    def frame_actions(self, frametime):
        # If need skip frame
        if self.skip_next:
            self.skip_next -= 1
            return None
        # If good (not in the firsts frames)
        if time.time() > self.firsts:
            if self.alpha != self.to_alpha:
                offset = frametime * self.speed
                if self.alpha < self.to_alpha:
                    self.alpha = min(self.to_alpha, self.alpha + offset)
                else:
                    self.alpha = max(self.to_alpha, self.alpha - offset)
                if self.debug:
                    print "fade new alpha: %f (%f)" % (self.alpha, frametime)
                self.update_alpha()
