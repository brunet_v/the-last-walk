import panda3d.core as engine
import sources.utils.trees as trees
import sources.objects.utils as utils
import sources.config.constants as const
import sources.config.build_config as build_config

import time
import math

from sources.utils.Threader import Threader

"""
    Handler of special effects like fog and indoor fading.
    /!\ Require a bust of computation every "effect_delay" to calculate fog zone

    Has been optimised many times (especially the fog algorithm was re-written multiple times),
    the code might be tricky and might require extensive analyzis before editing.

    This fake-thread contain no game-logic code.
"""
class EffectHandler(Threader):

    def __init__(self, world):
        utils.InstanceCounter.add("EffectHandler")
        # options
        self.effect_delay = 1. / 10.
        self.update_delay = 1. / 24.
        self.ray_number = 180
        self.ray_length = 11.5
        self.ray_iteration = 70
        self.ray_offset = float(self.ray_length) / float(self.ray_iteration)
        # inits
        self.world = world
        self.player = None
        self.update_time = time.time()
        self.refresh_time = time.time()
        self.fog_change = time.time()
        self.last_fog = set()
        self.cleared_fog_values = {}
        self.new_fog_value = {}
        self.old_fog_value = {}
        self.saved_status = None
        self.saved_fog = set()
        self.saved_fog_dist = {}
        self.computed_fog_angles = 0
        self.vision_graph = {}
        self.test_numbers = 4
        self.test_padding = 1.0 / float(self.test_numbers)
        self.fog_testgrid = ((0, -1), (-1, 0), (0, 1), (1, 0), (0, 0))
        Threader.__init__(self, "EffectHandler")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("EffectHandler")
        self.clear()

    def clear(self):
        Threader.clear(self)
        self.world = None
        self.player = None
        self.vision_graph = {}

    def run(self, frametime):
        now = time.time()
        timediff = now - self.update_time
        timeref = now - self.refresh_time
        self.bufferize_fog(timediff)
        if timediff > self.effect_delay:
            self.special_effects()
            self.update_time = now
        elif timeref > self.update_delay:
            if not build_config.build_option("no-fog"):
                self.fog_update(self.world['blocks']['fog'])
            self.refresh_time = now

    def special_effects(self):
        world_blocks = self.world['blocks']['datas']
        world_grid = self.world['infos']['world_grid']
        world_view = self.world['infos']['view']
        world_fog = self.world['blocks']['fog']
        maps_rendered = self.world['blocks']['rendered']
        maps_rendered_ns = self.world['blocks']['rendered-ns']

        if self.player:
            tpx = self.player.getX()
            tpy = self.player.getY()
            tpz = self.player.getZ()
            px = int(math.floor(self.player.getX()))
            py = int(math.floor(self.player.getY()))
            pz = int(math.floor(self.player.getZ()))
            mx = int(math.floor(float(px) / float(const.BLOCKS_PER_SIZE)))
            my = int(math.floor(float(py) / float(const.BLOCKS_PER_SIZE)))

            # fetch computed fog range
            fog_range = self.saved_fog
            self.computed_fog_angles = 0
            self.saved_fog = set()
            self.saved_fog_dist = {}

            # indoor hiding
            indoor = False
            squareradius = 2
            for x in xrange(-squareradius, squareradius + 1):
                for y in xrange(-squareradius, squareradius + 1):
                    for i in xrange(1, 7):
                        if trees.read_from_tree(world_blocks, px + x, py + y, pz + i):
                            indoor = True

            # save for fog bufferization
            oldindoor = indoor
            if self.saved_status:
                oldindoor = self.saved_status.indoor
            self.saved_status = utils.Struct({
                'map': maps_rendered,
                'fog': world_fog,
                'blk': world_blocks,
                'tpx': tpx,
                'tpy': tpy,
                'tpz': tpz,
                'tph': self.player.node.getH(),
                'indoor': indoor,
            })
            if indoor:
                world_view['zoomdecal'] = -0.5
            else:
                world_view['zoomdecal'] = 0.
            indoor = oldindoor

            main_camera_mask = engine.BitMask32.bit(0)
            m_pz = math.ceil(self.player.getZ() + 1.0)
            max_display_z = 10000
            for zkey in maps_rendered.keys():
                for ykey in maps_rendered[zkey].keys():
                    for xkey in maps_rendered[zkey][ykey].keys():
                        m_size = trees.read_from_tree(world_grid, xkey, ykey, 0)[1]
                        if indoor and zkey >= m_pz and (
                            mx >= xkey
                            and mx - m_size < xkey
                            and my >= ykey
                            and my - m_size < ykey
                        ):
                            max_display_z = min(zkey, max_display_z)
                            maps_rendered[zkey][ykey][xkey].hide(main_camera_mask)
                            maps_rendered_ns[zkey][ykey][xkey].hide(main_camera_mask)
                        else:
                            maps_rendered[zkey][ykey][xkey].show(main_camera_mask)
                            maps_rendered_ns[zkey][ykey][xkey].show(main_camera_mask)

            for id, player in self.world['players'].iteritems():
                in_fog = True
                x, y = player.getHashPos()
                for tx, ty in self.fog_testgrid:
                    if (x + tx, y + ty) in fog_range:
                        in_fog = False
                        break
                high_cut = (indoor and player.getZ() >= max_display_z)
                player.high_cut = high_cut
                if (high_cut or in_fog) and id != self.player.id:
                    if not player.hidden:
                        player.hide()
                else:
                    if player.hidden:
                        player.show()

            view_size = 17.0 # fog == 15 -> 17 is already too low

            znears = self.world['zombies']['tree'].getNear(px, py, pz, view_size)
            zlist = self.world['zombies']['list']
            for id in znears:
                zombie = zlist[id]
                in_fog = True
                x, y = zombie.getHashPos()
                for tx, ty in self.fog_testgrid:
                    if (x + tx, y + ty) in fog_range:
                        in_fog = False
                        break
                if in_fog or (indoor and zombie.getZ() >= max_display_z):
                    if not zombie.hidden:
                        zombie.hide()
                else:
                    if zombie.hidden:
                        zombie.show()

            inears = self.world['items']['tree'].getNear(px, py, pz, view_size)
            ilist = self.world['items']['list']
            for id in inears:
                item = ilist[id]
                x, y = item.getHashPos()
                in_fog = (x, y) not in fog_range
                if in_fog or (indoor and item.getZ() >= max_display_z):
                    if not item.hidden:
                        item.hide()
                else:
                    if item.hidden:
                        item.show()

            dnears = self.world['doors'].dtree.getNear(px, py, pz, view_size)
            dlist = self.world['doors'].doors
            for id in dnears:
                door = dlist[id]
                if indoor and door.z >= max_display_z:
                    if not door.hidden:
                        door.hide(main_camera_mask)
                else:
                    if door.hidden:
                        door.show(main_camera_mask)

    def bufferize_fog(self, timediff):
        if self.saved_status:
            max_ray = self.ray_number - self.ray_number * ((self.effect_delay - timediff) / self.effect_delay)
            if max_ray > self.ray_number:
                max_ray = self.ray_number
            self.update_fog_mask(
                self.saved_status.map,
                self.saved_status.fog,
                self.saved_status.blk,
                self.saved_status.tpx,
                self.saved_status.tpy,
                self.saved_status.tpz,
                self.saved_status.tph,
                self.saved_status.indoor,
                self.computed_fog_angles,
                max_ray,
            )
            self.computed_fog_angles = max_ray

    def add_to_vision_graph(self, x1, y1, x2, y2, z, size, blocks, indoor):
        iz = int(math.floor(z))
        height = const.VISION_HEIGHT
        if not indoor:
            height /= 2.
        height += z
        walkable = True
        if x2 < 0 or y2 < 0 or x2 >= size or y2 >= size:
            walkable = False
        else:
            xoffset = float(x2 - x1) * self.test_padding
            yoffset = float(y2 - y1) * self.test_padding
            tx = float(x1) + 0.5
            ty = float(y1) + 0.5
            tz = float(z)
            for i in xrange(0, self.test_numbers):
                tx += xoffset
                ty += yoffset
                x = int(tx)
                y = int(ty)
                z = int(tz)
                block = trees.read_from_tree(blocks, x, y, z)
                if block and block['c'].getVisionHeight(tx, ty, tz) > height:
                    walkable = False
                    break
                block = trees.read_from_tree(blocks, x, y, z + 1)
                if block and block['c'].getVisionHeight(tx, ty, tz + 1.0) > height:
                    walkable = False
                    break
        key1 = (x1, y1, x2, y2, iz, indoor)
        key2 = (x2, y2, x1, y1, iz, indoor)
        self.vision_graph[key1] = walkable
        self.vision_graph[key2] = walkable

    def update_fog_mask(self, world_rendered, world_fog, world_blocks, tpx, tpy, tpz, tph, indoor, a_start, a_end):
        iz = int(math.floor(tpz))
        msize = const.BLOCKS_PER_SIZE
        world_size = int(self.world['infos']['map_def']['size'] * msize)
        # Change view distance depending on situation (and iterpolate in day/night transition)
        its = float(self.ray_iteration)
        daytime = self.world['infos']['view']['daytime']
        itmin = its / 1.4
        if 6. < daytime < 10.:
            r = (daytime - 6.) / 4.
            its = its * r + (1. - r) * itmin
        elif 18. < daytime < 22.:
            r = (daytime - 18.) / 4.
            its = itmin * r + (1. - r) * its
        elif daytime <= 8.0 or daytime >= 20.0:
            its = itmin
        its = int(its)
        # Compute ray range and bufferize
        max_view_size = world_size
        angle_offset = 360.0 / float(self.ray_number)
        for i in xrange(int(a_start), int(a_end)):
            degree = angle_offset * i
            angle = math.radians(degree)
            xoffset = math.cos(angle) * self.ray_offset
            yoffset = math.sin(angle) * self.ray_offset
            x = tpx
            y = tpy
            lx = int(x)
            ly = int(y)
            nb_it = its
            # adiff = min(abs(degree - tph), abs(degree - tph - 360))
            for d in xrange(0, int(nb_it)):
                tx = int(x)
                ty = int(y)
                if tx != lx or ty != ly or d == 0:
                    key = (lx, ly, tx, ty, iz, indoor)
                    if key not in self.vision_graph:
                        self.add_to_vision_graph(lx, ly, tx, ty, tpz, max_view_size, world_blocks, indoor)
                    if not self.vision_graph[key] and d != 0:
                        break
                    key = (tx, ty)
                    if key not in self.saved_fog:
                        self.saved_fog.add(key)
                        val = max(0, float(d * 4.) / float(nb_it) - 3.)
                        trees.add_to_tree(self.saved_fog_dist, tx, ty, 0, val)
                    lx = tx
                    ly = ty
                x += xoffset
                y += yoffset
        # Validate buffering when all rays are done
        if a_end >= self.ray_number:
            self.cleared_fog_values = self.old_fog_value
            self.old_fog_value = self.new_fog_value
            self.new_fog_value = {}
            fog_texture = world_fog[1]
            fog_texture_size = world_size
            fog_texture_mult = fog_texture.getNumComponents() * fog_texture.getComponentWidth()
            fog_texture_base = const.UNEXPLORED_BLOCK_FOG * 256
            for bx, by in self.saved_fog:
                index = (by * fog_texture_size + bx) * fog_texture_mult
                ratio = trees.read_from_tree(self.saved_fog_dist, bx, by, 0)
                self.new_fog_value[index] = fog_texture_base * ratio
            for bx, by in self.last_fog - self.saved_fog:
                index = (by * fog_texture_size + bx) * fog_texture_mult
                self.new_fog_value[index] = fog_texture_base
            self.last_fog = self.saved_fog
            self.fog_change = time.time()

    def fog_update(self, world_fog):
        if time.time() - self.fog_change > 0:
            time_ratio = min(1.0, (time.time() - self.fog_change) / self.effect_delay)
            time_ratio_inv = 1.0 - time_ratio
            fog_texture_pointer = world_fog[1].modifyRamImage()
            # fade new fog values
            maxval = const.UNEXPLORED_BLOCK_FOG * 256
            for index, new in self.cleared_fog_values.iteritems():
                fog_texture_pointer.setElement(index, maxval)
            for index, new in self.old_fog_value.iteritems():
                fog_texture_pointer.setElement(index, maxval)
            for index, new in self.new_fog_value.iteritems():
                old = self.old_fog_value.get(index, maxval)
                fog_texture_pointer.setElement(index, new * time_ratio + old * time_ratio_inv)
