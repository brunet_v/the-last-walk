import sources.objects.utils as utils

from sources.gui.builder_utils import Text, ElementList, SearchById, SearchByTag, Block

"""
    The Social Bar
"""
class SocialBarHandler(object):

    def __init__(self, main_menu):
        utils.InstanceCounter.add("SocialBarHandler")
        self.main_menu = main_menu
        self.ox = 0
        self.oy = 0
        self.min_win_size = (150, 50)
        self.z_index = 20
        self.document = None
        self.main_win = False
        self.win_border_width = 3
        self.anim_button = None
        self.tmp_time_anim = 0.
        self.time_anim = 0.15
        self.size_anim = 89
        self.tmp_size_anim = 0
        self.is_button_anim = False
        self.button = None
        self.left = 0

    def __del__(self):
        utils.InstanceCounter.rm("SocialBarHandler")
        self.clear()

    def clear(self):
        pass

    def update(self):
        self.

    def update_button_pos(self):
        self.left = wx - 60
        self.button.node.style.left = str(self.left) + "px"
        self.button.node.style.top = str(wy - 110) + "px"

    def init(self, document):
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        self.document = document
        social_bar = SearchById(document, 'social-bar')
        button = Block(document, 'div', 'main-social-win')
        button.event('click', self.add_main_win)
        button.event("mouseover", self.button_hover)
        button.event("mouseout", self.button_out)
        button.node.inner_rml = "Social"
        social_bar.append(button)
        self.button = button.node
        self.update_button_pos()

    def button_hover(inst):
        inst.tmp_size_anim = 0.
        if inst.is_button_anim:
            inst.tmp_time_anim = inst.time_anim - inst.tmp_time_anim
        else:
            inst.tmp_time_anim = 0.
        inst.anim_button = True
        inst.is_button_anim = True
    def button_out(inst):
        if inst.is_button_anim:
            inst.tmp_time_anim = inst.time_anim - inst.tmp_time_anim
        else:
            inst.tmp_time_anim = 0.
        inst.anim_button = False
        inst.is_button_anim = True

    def dragstart(inst):
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        inst.ox = mx - float(self.parent_node.style.left.replace("px", ""))
        inst.oy = my - float(self.parent_node.style.top.replace("px", ""))
    def drag(inst):
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        x = mx - inst.ox
        y = my - inst.oy
        width = float(self.parent_node.style.width.replace("px", ""))
        height = float(self.parent_node.style.height.replace("px", ""))
        max_x = x + width
        max_y = y + height
        border = inst.win_border_width * 2
        if max_x + border >= wx:
            x = wx - width - border
        if max_y + border >= wy:
            y = wy - height - border
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        self.parent_node.style.left = str(x) + "px"
        self.parent_node.style.top = str(y) + "px"

    def close(inst, elem = None):
        if not elem:
            elem = self
        social_bar = SearchById(document, 'social-bar')
        social_bar.node.RemoveChild(elem.parent_node.parent_node)
    def close_main_win(inst):
        inst.main_win = False
        inst.close(self)

    def build_drag_bar(self):
        bar = Block(self.document, 'div', 'social-title', [
            Block(self.document, 'span', None, [
                Text(self.document, "Social Bar")])
        ])
        bar.event("dragstart", self.dragstart)
        bar.event("drag", self.drag)
        return bar

    def dragstart_left_corner(inst):
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        win = self.parent_node.parent_node
        left = float(win.style.left.replace("px", ""))
        top = float(win.style.top.replace("px", ""))
        height = float(win.style.height.replace("px", ""))
        inst.ox = mx - left
        inst.oy = top + height - my
    def drag_left_corner(inst):
        win = self.parent_node.parent_node
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        left = float(win.style.left.replace("px", ""))
        top = float(win.style.top.replace("px", ""))
        width = float(win.style.width.replace("px", ""))
        l = mx - inst.ox
        w = width + left - l
        h = my - top
        border = inst.win_border_width * 2
        if w < inst.min_win_size[0]:
            tmp = w
            w = inst.min_win_size[0]
            l += (tmp - w)
        if l < 0:
            w += l
            l = 0
        if h < inst.min_win_size[1]:
            h = inst.min_win_size[1]
        if top + h + border > wy:
            h = wy - top - border
        win.style.left = str(l) + "px"
        win.style.width = str(w) + "px"
        win.style.height = str(h) + "px"
        win.child_nodes[1].style.height = str(h - 50) + "px"

    def dragstart_right_corner(inst):
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        win = self.parent_node.parent_node
        left = float(win.style.left.replace("px", ""))
        top = float(win.style.top.replace("px", ""))
        width = float(win.style.width.replace("px", ""))
        height = float(win.style.height.replace("px", ""))
        inst.ox = left + width - mx
        inst.oy = top + height - my
    def drag_right_corner(inst):
        win = self.parent_node.parent_node
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        mp = base.win.getPointer(0)
        mx = mp.getX()
        my = mp.getY()
        left = float(win.style.left.replace("px", ""))
        top = float(win.style.top.replace("px", ""))
        x = mx - left + inst.ox
        y = my - top + inst.oy
        border = inst.win_border_width * 2
        if x < inst.min_win_size[0]:
            x = inst.min_win_size[0]
        if y < inst.min_win_size[1]:
            y = inst.min_win_size[1]
        if left + x + border > wx:
            x = wx - left - border
        if top + y + border > wy:
            y = wy - top - border
        win.style.width = str(x) + "px"
        win.style.height = str(y) + "px"
        win.child_nodes[1].style.height = str(y - 50) + "px"

    def build_resize_bar(self):
        left = Block(self.document, 'span', 'left')
        left.event("dragstart", self.dragstart_left_corner)
        left.event("drag", self.drag_left_corner)
        right = Block(self.document, 'span', 'right')
        right.event("dragstart", self.dragstart_right_corner)
        right.event("drag", self.drag_right_corner)
        return Block(self.document, 'div', 'resize-bar', [
            left,
            right,
        ])

    def build_main_win(self):
        win = Block(self.document, 'div', 'win', [
            self.build_drag_bar(),
            Block(self.document, 'div', 'social-content', [
                Text(self.document, "Yolo?www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www www "),
                Block(self.document, 'input', None, None, {"type": "text"}),
                Block(self.document, 'button', None, [
                    Text(self.document, "New Window")
                ]).event("click", self.add),
                Block(self.document, 'button', None, [
                    Text(self.document, "New Window")
                ]).event("click", self.add),
            ]),
            self.build_resize_bar(),
        ])
        return win
    def build_win(self):
        win = Block(self.document, 'div', 'win', [
            self.build_drag_bar(),
            Block(self.document, 'div', 'social-content', [
                Text(self.document, "Yolo!!!"),
            ]),
            self.build_resize_bar(),
        ])
        return win

    def add(self, main_win = False):
        if not self.document or (main_win and self.main_win):
            return None
        social_bar = SearchById(self.document, 'social-bar')
        if not social_bar:
            return None
        cross = Block(self.document, 'div', 'cross')
        if main_win:
            cross.event("click", self.close_main_win)
            sblock = self.build_main_win()
        else:
            cross.event("click", self.close)
            sblock = self.build_win()
        sblock.node.child_nodes[0].AppendChild(cross.getNode())
        sblock.event("mousedown", self.select_window)
        sblock.node.style.z_index = str(self.z_index) + "px"
        social_bar.append(sblock)
        self.z_index += 1
    def add_main_win(self):
        self.add(True)
        self.main_win = True

    def select_window(inst):
        tmp = 0.
        for element in self.parent_node.child_nodes:
            z_index = float(element.style.z_index.replace("px", ""))
            if z_index > tmp:
                tmp = z_index
        self.style.z_index = str(tmp + 1)

    def frame_actions(self, delay):
        if self.button:
            if not self.is_button_anim:
                return None
            self.tmp_time_anim += delay
            if self.tmp_time_anim >= self.time_anim:
                self.is_button_anim = False
                self.tmp_time_anim = self.time_anim
            x = self.size_anim * self.tmp_time_anim / self.time_anim
            self.tmp_size_anim = x
            if self.anim_button:
                self.button.style.left = str(self.left - x) + "px"
            else:
                self.button.style.left = str(self.left - self.size_anim + x) + "px"
