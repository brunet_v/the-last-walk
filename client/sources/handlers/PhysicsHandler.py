import sources.objects.utils as utils

import panda3d.core as engine
import panda3d.bullet as bullet

"""
    Handle world physics interactions

    After experimentation, this is not currently possible :
        - Bullet library does not contain space partitionning datastructure, which is very bad.
        - Using controlJoint on an actor does not allow do to offseting on the joint model
          (physics are not centered on the joint model, but on the joint origin)
"""
class PhysicsHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("PhysicsHandler")
        # self.nworld = render.attachNewNode('BulletWorld')
        # self.bworld = bullet.BulletWorld()
        # self.bworld.setGravity(engine.Vec3(0, 0, -18.81))

        # # Debug ground
        # shape = bullet.BulletPlaneShape(engine.Vec3(0, 0, 1), 0)
        # np = self.nworld.attachNewNode(bullet.BulletRigidBodyNode('BulletTestGround'))
        # np.node().addShape(shape)
        # np.setPos(0, 0, 0)
        # np.setCollideMask(engine.BitMask32.allOn())
        # self.bworld.attachRigidBody(np.node())

        # if len(sys.argv) > 1 and sys.argv[1] == "debug":
        #     self.debug_node = self.nworld.attachNewNode(bullet.BulletDebugNode('Debug'))
        #     self.debug_node.show()
        #     self.debug_node.node().showBoundingBoxes(True)
        #     self.debug_node.node().showWireframe(True)
        #     self.debug_node.node().showConstraints(True)
        #     self.bworld.setDebugNode(self.debug_node.node())

        # self.bodys = {}
        # self.bodys_datas = {
        #     "head": utils.Struct({
        #         "size": engine.Vec3(0.8, 0.8, 0.8),
        #         "offset": engine.Vec3(0, 0, -1),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 5.0,
        #     }),
        #     "torso": utils.Struct({
        #         "size": engine.Vec3(0.9, 1.5, 0.5),
        #         "offset": engine.Vec3(0, 0, -0.5),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 15.0,
        #     }),
        #     "left arm top": utils.Struct({
        #         "size": engine.Vec3(0.5, 0.5, 1.2),
        #         "offset": engine.Vec3(0, 0, 1.1),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 5.0,
        #     }),
        #     "right arm top": utils.Struct({
        #         "size": engine.Vec3(0.5, 0.5, 1.2),
        #         "offset": engine.Vec3(0, 0, 1.1),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 5.0,
        #     }),
        #     "left leg top": utils.Struct({
        #         "size": engine.Vec3(0.5, 0.5, 1.4),
        #         "offset": engine.Vec3(0, 0, 1.4),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 5.0,
        #     }),
        #     "right leg top": utils.Struct({
        #         "size": engine.Vec3(0.5, 0.5, 1.4),
        #         "offset": engine.Vec3(0, 0, 1.4),
        #         "hpr": engine.Vec3(0, 0, 0),
        #         "mass": 5.0,
        #     }),
        # }

    def __del__(self):
        utils.InstanceCounter.rm("PhysicsHandler")
        self.clear()

    def clear(self):
        # if self.nworld:
        #     self.nworld.removeNode()
        self.nworld = None
        self.bworld = None

    # def add_boxnode(self, id, actor, joint, data):
    #     jointnode = actor.exposeJoint(None, "modelRoot", joint)
    #     jointnode.setPos(data.offset)
    #     jointnode.setHpr(data.hpr)
    #     bulletnode = bullet.BulletRigidBodyNode(id)
    #     shape = bullet.BulletBoxShape(data.size)
    #     np = actor.attachNewNode(bulletnode)
    #     np.node().setMass(data.mass)
    #     np.node().addShape(shape)
    #     np.setPos(jointnode.getPos(render))
    #     np.setHpr(jointnode.getHpr(render))
    #     np.setCollideMask(engine.BitMask32.allOn())
    #     dummy = actor.controlJoint(np, "modelRoot", joint)
    #     dummy.setPos(jointnode.getPos())
    #     dummy.setHpr(jointnode.getHpr())
    #     self.bworld.attachRigidBody(np.node())
    #     return utils.Struct({
    #         "bulletnode": bulletnode,
    #         "pandanode": np,
    #     })

    # def add_zombie(self, zombie, hx, hy, hz, hs):
    #     zombie.node.node().setBounds(engine.OmniBoundingVolume())
    #     zombie.node.node().setFinal(True)
    #     parts = {}
    #     for name, data in self.bodys_datas.iteritems():
    #         parts[name] = self.add_boxnode(
    #             "Bullet%d-%s" % (zombie.id, name),
    #             zombie.node,
    #             name,
    #             data,
    #         )
    #     self.bodys[zombie.id] = parts

    def frame_actions(self, delay):
        pass
        # self.bworld.doPhysics(delay)
        # for name, parts in self.bodys.iteritems():
        #     for pname, part in parts.iteritems():
        #         part.offseter.setPos(part.pandanode.getPos() + self.bodys_datas[pname].offset)
        #         part.offseter.setHpr(part.pandanode.getHpr() + self.bodys_datas[pname].hpr)
