import os
import hashlib
import panda3d.core as engine
import sources.gui.builder as builder
import sources.objects.utils as utils
import sources.world.init as init
import sources.gui.Languages as languages
import sources.config.configfile as configfile
import sources.config.build_config as build_config
import threading
import time

from panda3d.rocket import *
from sources.handlers.LightHandler import LightHandler
from sources.handlers.SoundsHandler import SoundsHandler
from sources.handlers.ShadersHandler import ShadersHandler
from sources.networking.Master import Master
from sources.gui.menus.ListSource import ListSource
from sources.gui.menus.MenuScene import MenuScene
from sources.Game import Game
from sources.utils.Threader import Threader

from direct.gui.OnscreenText import OnscreenText
from direct.gui.OnscreenImage import OnscreenImage

"""
    Handle all tooltips
"""
class TooltipsHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("TooltipsHandler")
        self.is_showed = False
        self.a_path = engine.Filename.fromOsSpecific(os.getcwd())
        self.content = {}
        self.content["yolo"] = "Yolo yolo yolo yolo yolo yolo yolo yolo yolo yolo"
        self.content["logininput"] = languages.i18n("tooltips", "login")
        self.content["lbar"] = languages.i18n("tooltips", "life")
        self.content["tbar"] = languages.i18n("tooltips", "tiredness")
        self.content["heart"] = languages.i18n("tooltips", "stress")
        self.content["food"] = languages.i18n("tooltips", "hungry")
        self.content["drink"] = languages.i18n("tooltips", "thirsty")
        self.content["avatar"] = languages.i18n("tooltips", "players")
        self.content["character"] = languages.i18n("tooltips", "character")
        self.tooltip_nodepath = {}
        self.is_showed = {}
        self.arrow = None
        self.handle_arrow(0, 0)

    def __del__(self):
        utils.InstanceCounter.rm("TooltipsHandler")
        self.clear()

    def clear(self):
        if self.arrow:
            self.arrow.destroy()

    def move_text(self, nodepath, x, y):
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        # print "dest:", x, y
        # print "window:", wx, wy
        # print "mouse:", self.mx, self.my
        # print "size", self.width, self.height
        if self.mx + self.width + 33. > wx:
            x = wx - self.width + 7
        if self.my < 8.:
            y = -18.
        elif self.my + self.height - 8. > wy:
            y = (wy - self.height) * -1 - 18
        # print "new dest:", x, y
        # print "---\n"
        nodepath.setPos(x, 0, y)

    def load_tooltip(self, value, name, x, y, scale=14):
        tooltip = engine.TextNode("tooltip-" + name)
        tooltip.setText(value)
        tooltip.setCardColor(0.03, 0.03, 0.03, 0.95)
        tooltip.setCardAsMargin(0.5, 0.5, 0.5, 0.5)
        tooltip.setCardDecal(True)
        tooltip.setAlign(engine.TextNode.ALeft)
        tooltip.setWordwrap(15.0)
        tooltip_nodepath = pixel2d.attachNewNode(tooltip)
        tooltip_nodepath.show()
        tooltip_nodepath.setScale(scale)
        tooltip_nodepath.setPos(x, 0, y)
        tooltip_nodepath.setColor(0.6, 0.2, 0.2, 1.0)
        # tooltip_nodepath.setBin("fixed", 40)
        tooltip_nodepath.setDepthTest(False)
        tooltip_nodepath.setDepthWrite(False)
        self.tooltip_nodepath[name] = tooltip_nodepath

    def get_menus_path(self, path):
        return engine.Filename(
            "%s/datas/ui/menus/%s" % (self.a_path, path)
        ).toOsSpecific()

    def load_image(self, image_file, x, y, xsize, ysize, xscale = 1, yscale = 1):
        img = OnscreenImage(image = image_file)
        img.setScale((xsize * xscale) / 2.0, 0, (ysize * yscale) / 2.0)
        self.move_image(img, x, y, xsize, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.hide()
        return img

    def move_image(self, image, x, y, xsize, ysize):
        image.setPos(x + xsize / 2, 0, -y - ysize / 2)

    def handle_arrow(self, x, y):
        if self.arrow == None:
            self.arrow = self.load_image(
                'datas/ui/tooltip-arrow.png',
                x, y,
                14, 10
            )
        else:
            self.move_image(self.arrow, x, y, 14, 10)

    def showTooltip(self, name):
        mp = base.win.getPointer(0)
        self.mx = mp.getX()
        self.my = mp.getY()
        x = self.mx + 40
        y = self.my * -1 - 10
        if not name in self.is_showed or self.is_showed[name] == False:
            self.load_tooltip(
                self.content[name], name,
                x,
                y,
                14
            )
            self.is_showed[name] = True
            pt1, pt2 = self.tooltip_nodepath[name].getTightBounds()
            self.width = pt2.getX() - pt1.getX()
            self.height = pt2.getZ() - pt1.getZ()
            self.move_text(self.tooltip_nodepath[name], x, y)
        elif name in self.is_showed and self.is_showed[name] == True:
            self.move_text(self.tooltip_nodepath[name], x, y)
        self.arrow.show()
        self.handle_arrow(x - 21, -y - 14)

    def showYolo(self):
        self.showTooltip("yolo")
    def hideYolo(self):
        self.hide("yolo")
    def showLogininput(self):
        self.showTooltip("logininput")
    def hideLogininput(self):
        self.hide("logininput")

    def show(self, name):
        self.showTooltip(name)

    def hide(self, name):
        if name in self.is_showed and self.is_showed[name] == True:
            self.tooltip_nodepath[name].hide()
            if self.arrow:
                self.arrow.hide()
            self.is_showed[name] = False
