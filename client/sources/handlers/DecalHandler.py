import math
import random
import panda3d.core as engine
import sources.objects.utils as utils
import sources.utils.resources as resources
import sources.config.configfile as configfile
import sources.utils.trees as trees
import sources.config.constants as const

"""
    Handle decals such as blood splits, etc...
"""
class DecalHandler(object):

    def __init__(self, game):
        utils.InstanceCounter.add("DecalHandler")
        self.game = game
        self.world = game.world
        # Blood
        self.blood_decals = []
        self.blood_index = 0
        world_fog = self.world['blocks']['fog']
        for i in xrange(0, configfile.get("decalnumber")):
            bdecal = resources.load_model_file(
                "datas/particles/decals/decal_base.egg",
                "datas/particles/decals/blood%d.png" % random.randint(1, 4)
            )
            bdecal.hide(engine.BitMask32.bit(1)) # Hide from light
            bdecal.hide(engine.BitMask32.bit(0)) # Hide from camera
            bdecal.setPos(-100, -100, 1000)
            bdecal.setTexture(world_fog[0], world_fog[1])
            self.blood_decals.append(bdecal)

    def __del__(self):
        utils.InstanceCounter.rm("DecalHandler")
        self.clear()

    def clear(self):
        self.game = None
        self.world = None
        for decal in self.blood_decals:
            decal.detachNode()
            decal.removeNode()
        self.blood_decals = []

    def get_map_node(self, x, y, z):
        mx = int(x) / const.BLOCKS_PER_SIZE
        my = int(y) / const.BLOCKS_PER_SIZE
        mnode = None
        mz = -10000
        mnodes = self.world['blocks']['rendered-ns']
        for nz, nodemap in mnodes.iteritems():
            node = trees.read_from_tree(mnodes, mx, my, nz)
            if node and nz <= z and nz >= mz:
                mnode = node
                mz = nz
        return mnode

    def place_blood(self, x, y, z):
        if len(self.blood_decals) > 0:
            if z > 0.0:
                z = math.floor(z)
            x += random.random() * 1.5 - 0.75
            y += random.random() * 1.5 - 0.75
            parent = self.get_map_node(x, y, z)
            if parent:
                self.blood_index += 1
                if self.blood_index >= len(self.blood_decals):
                    self.blood_index = 0
                udecal = self.blood_decals[self.blood_index]
                udecal.setHpr(random.randint(0, 360), 0, 0)
                udecal.setScale(random.random() / 2.0 + 0.75)
                udecal.reparentTo(parent)
                udecal.setPos(render, x, y, z + random.random() * 0.02 + 0.02)
                udecal.show(engine.BitMask32.bit(0))

    def frame_actions(self, frametime):
        pass

