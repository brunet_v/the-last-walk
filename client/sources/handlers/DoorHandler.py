import panda3d.core as engine
import sources.config.constants as const
import sources.utils.trees as trees
import sources.utils.resources as resources
import sources.utils.positioning as positioning
import sources.objects.utils as utils

from sources.utils.CollisionTree import CollisionTree
from sources.objects.Door import Door

"""
    Door manager, responsible for adding, opening and closing doors,
    does collision and graphical modifications (for client)
"""
class DoorHandler(object):

    def __init__(self, world, seed):
        utils.InstanceCounter.add("DoorHandler")
        self.seed = seed
        self.world = world
        self.world_blocks = world['blocks']
        self.world_collisions = world['collision_tree']
        self.doors = {}
        self.collisions = None
        self.cid = 20000000
        self.dtree = CollisionTree(const.BLOCKS_PER_SIZE)

    def __del__(self):
        utils.InstanceCounter.rm("DoorHandler")
        self.clear()

    def clear(self):
        Door.need_update = set()
        if self.doors:
            for id, door in self.doors.iteritems():
                door.clear()
        self.dtree.clear()
        self.world = None
        self.world_blocks = None
        self.world_collisions = None
        self.doors = None

    def update(self, delay):
        to_freeze = []
        for door in Door.need_update:
            if not door.update(delay):
                to_freeze.append(door)
        for door in to_freeze:
            Door.need_update.remove(door)

    def openSeed(self, id):
        # Hash the id and the seed (pseudo-random)
        k = int(self.seed * id)
        k *= 357913941;
        k ^= k << 24;
        k += ~357913941;
        k ^= k >> 31;
        k ^= k << 31;
        return abs(k) % 17 < 3

    def getPos(self, door_id):
        door = self.doors[door_id]
        return door.x, door.y, door.z

    def addDoor(self, world_blocks_datas, door_block, door_name, rotation):
        self.cid += 1
        door_id = self.cid
        door_rotation = (door_name[2] + rotation) % 4

        # Load graphics
        world_fog = self.world['blocks']['fog']
        door_node = engine.NodePath("door_%d_dummy" % door_id)
        door_node.reparentTo(self.world['cg'].getNode(
            door_block['x'],
            door_block['y'],
            door_block['z']
        ))
        positioning.position_node(
            door_node,
            door_block['x'],
            door_block['y'],
            door_block['z'],
            door_rotation, 1
        )
        door_model = resources.load_model_file(
            'datas/blocks/indoor/doors/@cfree_%s.egg' % door_name[0],
            'datas/blocks/texture.png',
        )
        door_model.reparentTo(door_node)
        door_model.setTexture(world_fog[0], world_fog[1])
        door_model.setPos(0, 0, 0)
        positioning.position_fog_texture(
            door_model, world_fog[0],
            door_block['x'], door_block['y'], door_block['r'],
            self.world['infos']['map_def']['size']
        )

        # Init door
        self.doors[door_id] = Door(
            door_id,
            door_block,
            door_rotation,
            door_model,
            door_node
        )
        if self.openSeed(door_id):
            self.openDoor(door_id)
        else:
            self.closeDoor(door_id)

    def getClosedPos(self, door_id):
        door = self.doors[door_id]
        xrev, yrev, axisrev = const.ROTATION_FLIPS[door.r]
        dx = 0.5
        dy = 0.0
        if yrev:
            dy = 1.0
        if axisrev:
            dx, dy = dy, dx
        dx += door.x
        dy += door.y
        return dx, dy, door.z, 0.35, 0.35

    def closeDoor(self, door_id):
        door = self.doors[door_id]
        door.setR(0)

        # Set collision block
        dx, dy, dz, sx, sy = self.getClosedPos(door_id)
        self.world_collisions.setPosValue(door_id, dx, dy, door.z, sx, sy)
        door.pos = engine.Point3(dx, dy, door.z)
        door.open = False
        self.dtree.setPosValue(door_id, dx, dy, door.z + 0.5)

        # Update pathing graph (zombies avoiding opened door)
        if self.collisions and door.need_link:
            self.collisions.addLink(door.block, door.need_link)

    def getOpenPos(self, door_id):
        door = self.doors[door_id]
        xrev, yrev, axisrev = const.ROTATION_FLIPS[door.r]
        sx = 0.275
        sy = 0.5
        dx = 0.925
        dy = 0.5
        if xrev:
            dx = 0.1
        if axisrev:
            sx, sy = sy, sx
            dx, dy = dy, dx
        dx += door.x
        dy += door.y
        return dx, dy, door.z, sx, sy

    def openDoor(self, door_id):
        door = self.doors[door_id]
        door.setR(-85)

        # Set collision block
        dx, dy, dz, sx, sy = self.getOpenPos(door_id)
        self.world_collisions.setPosValue(door_id, dx, dy, dz, sx, sy)
        door.pos = engine.Point3(dx, dy, dz)
        door.open = True
        self.dtree.setPosValue(door_id, dx, dy, door.z + 0.5)

        # Update pathing graph (zombies avoiding opened door)
        if self.collisions:
            ndx = int(dx + (dx - 0.5) - door.x)
            ndy = int(dy + (dy - 0.5) - door.y)
            linked = trees.read_from_tree(self.world_blocks['datas'], ndx, ndy, door.z)
            if linked:
                unlinked = self.collisions.delLink(door.block, linked)
                if unlinked:
                    door.need_link = linked

    def delDoor(self, door_id):
        door = self.doors[door_id]

        # Set collision block (then delete it)
        self.closeDoor(door_id)
        self.dtree.deleteId(door_id)
        self.world_collisions.deleteId(door_id)

        # Unlink door
        door.clear()
        self.doors.pop(door_id)
