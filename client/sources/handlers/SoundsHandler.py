import sources.objects.utils as utils
import sources.utils.trees as trees
import sources.utils.resources as resources
import sources.config.constants as const
import sources.config.configfile as config
import random
import math

"""
    Handle music and sound effects
"""
class SoundsHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("SoundsHandler")
        self.load_sounds()
        self._music_volume = float(config.get("volume_music"))
        self._sfx_volume = float(config.get("volume_sfx"))
        self._game_music = False
        self._daytime = 8.
        self._part_of_day = -1
        self._players = []
        self._next_music = None
        self._cur_music = None
        self._prev_music = None
        self._fade_music = 0.
        self._fade_time = 1.5

    def __del__(self):
        utils.InstanceCounter.rm("SoundsHandler")
        self.clear()

    def clear(self):
        pass

    def prepare_sound(self, sound):
        return utils.Struct({
            "sound": sound,
            "volume": 0.
        })

    def load_sounds(self):
        self._sound = {
            "music": {
                "menu": self.prepare_sound(loader.loadSfx("./datas/sounds/music/menu-1.ogg")),
                "menu-options": self.prepare_sound(loader.loadSfx("./datas/sounds/music/menu-options.ogg")),
                "day1": self.prepare_sound(loader.loadSfx("./datas/sounds/music/day-1.ogg")),
                "night1": self.prepare_sound(loader.loadSfx("./datas/sounds/music/night-1.ogg"))
            },
            "sfx": {
                # Menus
                "menu-button-hover": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/menus/menu-button-hover.ogg")),
                "menu-inputs-down": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/menus/menu-inputs-down.ogg")),
                "menu-inputs-up": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/menus/menu-inputs-up.ogg")),
                "select-hover": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/menus/select-hover.ogg")),

                # Grass
                "grassr1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run1.ogg")),
                "grassr2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run2.ogg")),
                "grassr3": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run3.ogg")),
                "grassr4": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run4.ogg")),
                "grassr5": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run5.ogg")),
                "grassr6": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run6.ogg")),
                "grassr7": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run7.ogg")),
                "grassr8": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run8.ogg")),
                "grassr9": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run9.ogg")),
                "grassr10": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/run10.ogg")),
                "grassw1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk1.ogg")),
                "grassw2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk2.ogg")),
                "grassw3": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk3.ogg")),
                "grassw4": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk4.ogg")),
                "grassw5": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk5.ogg")),
                "grassw6": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/grass/walk6.ogg")),

                "open_door1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/houses/open_door1.ogg")),
                "close_door1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/houses/close_door1.ogg")),
                "close_door2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/houses/close_door2.ogg")),
                "close_door3": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/houses/close_door3.ogg")),
                "close_door4": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/houses/close_door4.ogg")),

                # Weapons
                "berretta_m12_9mm1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/berretta_m12_9mm1.ogg")),
                "berretta_m12_9mm2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/berretta_m12_9mm2.ogg")),
                "m16_1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/berretta_m12_9mm1.ogg")),
                "m16_2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/berretta_m12_9mm2.ogg")),
                "katana_1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_1.ogg")),
                "katana_2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_2.ogg")),
                "katana_3": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_3.ogg")),
                "katana_4": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_4.ogg")),
                "katana_5": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_5.ogg")),
                "katana_6": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_6.ogg")),
                "katana_7": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_7.ogg")),
                "katana_8": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/weapons/katana/katana_8.ogg")),

                # Zombies
                "grunt1": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt1.ogg")),
                "grunt2": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt2.ogg")),
                "grunt3": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt3.ogg")),
                "grunt4": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt4.ogg")),
                "grunt5": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt5.ogg")),
                "grunt6": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt6.ogg")),
                "grunt7": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt7.ogg")),
                "grunt8": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt8.ogg")),
                "grunt9": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt9.ogg")),
                "grunt10": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt10.ogg")),
                "grunt11": self.prepare_sound(loader.loadSfx("./datas/sounds/sfx/zombies/grunt11.ogg")),
            }
        }

    def set_volume(self, type, volume):
        if type == "music":
            self._music_volume = volume
        elif type == "sfx":
            self._sfx_volume = volume

    def change_music_volume(inst):
        inst._music_volume = float(self.value)
        for name, music in inst._sound["music"].iteritems():
            music.sound.setVolume(float(self.value) * music.volume)

    def change_sfx_volume(inst):
        inst._sfx_volume = float(self.value)
        for name, sfx in inst._sound["sfx"].iteritems():
            sfx.sound.setVolume(float(self.value) * sfx.volume)

    def status(self, sound):
        return self._sound[sound].status()

    def get_volume(self, type):
        return {
            "music": self._music_volume,
            "sfx": self._sfx_volume
        }[type]

    def get_distance(self, xa, ya, xb, yb):
        return math.sqrt(((xb - xa) ** 2) + ((yb - ya) ** 2))

    """
        Menu sfx: buttons, inputs
    """
    def play_hover_button(self):
        self.play_sfx_menu("menu-button-hover", 0.2)
    def play_active_button(self):
        self.play_sfx_menu("menu-button-active", 0.2)
    def play_inputs_keydown(self):
        self.play_sfx_menu("menu-inputs-down", 0.2)
    def play_inputs_keyup(self):
        self.play_sfx_menu("menu-inputs-up", 0.1)
    def play_hover_option(inst):
        inst.play_sfx_menu("select-hover", 0.1)

    """
        Ingame sfx
    """
    def play_move(self, volume, player, camX, camY):
        tmpp = None
        # Search the player
        for p in self._players:
            if p.id == player.id:
                tmpp = p
        # New player if not found
        if tmpp == None:
            tmpp = utils.Struct({
                "id": player.id,
                "time_run": 0.
            })
            self._players.append(tmpp)
        # Check if the sound can be played
        play = False
        if (tmpp.time_run > 0.45):
            tmpp.time_run = 0.
            play = True

        if play:
            sound = str("grassr" + str(random.randint(1, 10)))
            ppos = player.getPos()
            self.play_sfx(sound, volume, 15, ppos.x, ppos.y, camX, camY)

    def play_use_item(self, volume, player, camX, camY):
        ppos = player.getPos()
        if player.equiped[1] == "m16":
            # Pull the trigger
            sound = str("m16_" + str(random.randint(1, 2)))
            self.play_sfx(sound, volume, 40, ppos.x, ppos.y, camX, camY)
        if player.equiped[1] == "katana":
            # Pull the trigger
            sound = str("katana_" + str(random.randint(1, 8)))
            self.play_sfx(sound, volume, 30, ppos.x, ppos.y, camX, camY)

    def play_door(self, state, volume, door, camX, camY):
        if state == 1:
            sound = str("open_door" + str(random.randint(1, 1)))
            self.play_sfx(sound, volume, 15, door.x, door.y, camX, camY)
        else:
            sound = str("close_door" + str(random.randint(1, 4)))
            self.play_sfx(sound, volume, 15, door.x, door.y, camX, camY)

    def play_previous_music(self):
        if self._prev_music != None:
            self.play_music(self._prev_music.sound, self._prev_music.volume)

    def dump_music(self):
        print "cur:  " + str(self._cur_music)
        print "next: " + str(self._next_music)
        print "fade: " + str(self._fade_music)
        for s, v in self._sound["music"].iteritems():
            if s == "menu" or s == "menu-options":
                print s, v.sound.getVolume()
                print v.sound.status()
        print "\n"

    def play_music(self, sound, volume, type = "menu"):
        if type == "game":
            self._part_of_day = 0
        # Call during an other play
        if self._next_music != None:
            #self.dump_music()
            tmp = self._cur_music
            self._cur_music = self._next_music
            self._next_music = tmp
            self._fade_music = 0.
        # Music already played
        if sound == self._cur_music:
            return None
        # Set current music to sound
        rvolume = volume
        if self._cur_music != None:
            self._next_music = sound
            self._sound["music"][self._cur_music].sound.setLoop(False)
            volume = 0.
        else:
            self._cur_music = sound

        # Save previous music
        self._prev_music = utils.Struct({
            "sound": self._cur_music,
            "volume": self._sound["music"][self._cur_music].volume
        })
        # Launch the sound
        self._sound["music"][sound].volume = rvolume
        self._sound["music"][sound].sound.setVolume(volume * self.get_volume("music"))
        self._sound["music"][sound].sound.setLoop(True)
        self._sound["music"][sound].sound.play()

    def play_sfx(self, sound, volume, sound_radius, posX, posY, camX, camY):
        dist_cam = self.get_distance(camX, camY, posX, posY)
        if dist_cam <= sound_radius:
            # print sound, dist_cam / sound_radius

            volume = volume * self.get_volume("sfx") * (1. - (dist_cam / sound_radius))
            self._sound["sfx"][sound].sound.setVolume(volume)
            self._sound["sfx"][sound].sound.play()

    def play_sfx_menu(self, sound, volume):
        volume *= self.get_volume("sfx")
        self._sound["sfx"][sound].sound.setVolume(volume)
        self._sound["sfx"][sound].sound.play()

    def music_transition(self, frametime):
        if self._next_music != None:
            if self._fade_music < self._fade_time:
                self._fade_music += frametime
                # Current music fade out
                volume = self._sound["music"][self._cur_music].volume * self.get_volume("music") * (1. - (self._fade_music / self._fade_time))
                if volume < 0.:
                    volume = 0.
                self._sound["music"][self._cur_music].sound.setVolume(volume)
                # Next music fade in
                volume = self._sound["music"][self._next_music].volume * self.get_volume("music") * (self._fade_music / self._fade_time)
                if volume > 1.:
                    volume = 1.
                self._sound["music"][self._next_music].sound.setVolume(volume)
            else:
                self._sound["music"][self._cur_music].sound.stop()
                self._cur_music = self._next_music
                self._next_music = None
                self._fade_music = 0.

    def day_night_handler(self, frametime):
        self._daytime += frametime
        if self._daytime >= 24.:
            self._daytime -= 24.
        if self._daytime >= 8. and self._daytime <= 20. and self._part_of_day == 1:
            self._part_of_day = 0
            self.play_music("day1", 0.8)
            print "DAY"
        elif (self._daytime < 8. or self._daytime > 20.) and self._part_of_day == 0:
            self._part_of_day = 1
            self.play_music("night1", 0.8)
            print "NIGHT"

    def frame_actions(self, frametime):
        # Update elapsed time since the last move
        for player in self._players:
            player.time_run += frametime
        self.music_transition(frametime)
        self.day_night_handler(frametime)