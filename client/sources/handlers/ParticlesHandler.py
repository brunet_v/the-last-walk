import time
import random
import sources.objects.utils as utils

from panda3d.core import Filename
from direct.particles.ParticleEffect import ParticleEffect

"""
    Maintain a pool of particles effect for game usage
"""
class ParticlesHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("ParticlesHandler")
        self.permanents = []
        self.waiting = {}
        self.running = {}
        self.disabled = []
        self.load_particles_buffer('blood', 20)

    def __del__(self):
        utils.InstanceCounter.rm("ParticlesHandler")
        self.clear()

    def clear(self):
        # perma
        for effect in self.permanents:
            effect.cleanup()
        self.permanents = []
        # running
        for name, effect_list in self.running.iteritems():
            for effect in effect_list:
                effect[1].cleanup()
        self.running = {}
        # waiting
        for name, effect_list in self.waiting.iteritems():
            for effect in effect_list:
                effect.cleanup()
        self.waiting = {}
        self.disabled = []

    def load_particles_buffer(self, name, size):
        self.waiting[name] = []
        for i in xrange(0, size):
            added = ParticleEffect()
            added.loadConfig(self.get_particle_path(name))
            added.disable()
            self.waiting[name].append(added)

    def get_particle_path(self, name):
        return Filename("datas/particles/%s/%s.ptf" % (name, name))

    def create_permanent_particles(self, name, parent, x = 0, y = 0, z = 0):
        created = ParticleEffect()
        created.loadConfig(self.get_particle_path(name))
        created.setPos(x, y, z)
        created.start(parent, render)
        created.setLightOff()
        self.permanents.append(created)
        return created

    def create_temporary_particles(self, name, parent, lifespan=10.0, x = 0, y = 0, z = 0):
        if name in self.waiting and self.waiting[name]:
            if len(self.waiting[name]):
                used = self.waiting[name].pop()
                used.clearToInitial()
                used.setLightOff()
                used.setPos(x, y, z)
                used.start(parent, render)
                if not name in self.running:
                    self.running[name] = []
                self.running[name].append([time.time() + lifespan, used])

    def frame_actions(self, frametime):
        now = time.time()
        for name, effect_list in self.running.iteritems():
            for effect in effect_list:
                if effect[0]:
                    if now > effect[0]:
                        effect[0] = None
                        effect[1].setPos(-100000, -100000, 100000)
                        self.disabled.append((now + 0.5, name, effect))
        disabled = []
        for timer, name, effect in self.disabled:
            if now > timer:
                effect[1].disable()
                self.waiting[name].append(effect[1])
                self.running[name].remove(effect)
            else:
                disabled.append((timer, name, effect))
        self.disabled = disabled

    def blood_hit(self, x, y, z):
        x += random.random() * 0.1 - 0.05
        y += random.random() * 0.1 - 0.05
        z += 1.1 + random.random() * 0.5
        self.create_temporary_particles("blood", render, 0.7, x, y, z)
