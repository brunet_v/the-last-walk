import random as rand
import sources.objects.utils as utils
import sources.utils.trees as trees
import sources.utils.resources as resources
import sources.config.constants as const

"""
    Handle grass effect
"""
class GrassHandler(object):

    def __init__(self, game = None):
        utils.InstanceCounter.add("GrassHandler")
        self.grass = trees.Tree()
        self.added = []
        # Slice number
        self.nb = 12
        self.height = 0.16
        self.rdia = 0.015
        self.grass_block = []
        if game:
            world_def = game.world_def
            for mx, my, mname, msize, mrotation in world_def['maps']:
                tx = mx * const.BLOCKS_PER_SIZE
                ty = my * const.BLOCKS_PER_SIZE
                parent = self.get_map_node(tx, ty, 0, game.world)
                self.add_grass(parent, mx, my, mname, msize, mrotation)
        self.gtime = 0.
        self.test = 0

    def __del__(self):
        utils.InstanceCounter.rm("GrassHandler")
        self.clear()

    def clear(self):
        for mx, my, mz in self.added:
            value = self.grass.get(mx, my, mz)
            if value:
                a, b, node = value
                node.detachNode()
                node.removeNode()
        self.grass = trees.Tree()
        self.added = []
        self.grass_block = []

    def get_map_node(self, x, y, z, world):
        mx = int(x) / const.BLOCKS_PER_SIZE
        my = int(y) / const.BLOCKS_PER_SIZE
        mnode = None
        mz = -10000
        mnodes = world['blocks']['rendered-ns']
        for nz, nodemap in mnodes.iteritems():
            node = trees.read_from_tree(mnodes, mx, my, nz)
            if node and nz <= z and nz >= mz:
                mnode = node
                mz = nz
        return mnode

    def add_grass(self, parent, mx, my, mname, msize, mrotation):
        block = parent.attachNewNode("grass-%dx%d" % (mx, my))
        dia = self.rdia
        rad = dia / 2.
        for i in xrange(self.nb):
            gnode = resources.load_model_file(
                "./datas/particles/decal_grass_base.egg",
                "./datas/maps/compiled/%s-grass.png" % mname,
                nearest=True,
            )
            for path in xrange(0, gnode.getChildren().getNumPaths()):
                geom = gnode.getChildren().getPath(path)
                geom.reparentTo(block)
                geom.setScale(msize)
                rx = rand.random() * dia - rad
                ry = rand.random() * dia - rad
                geom.setPos(rx, ry, i * self.height / float(self.nb))
            gnode.detachNode()
            gnode.removeNode()
        block.flattenStrong()

        # shader = loader.loadShader("./sources/shaders/grass.sha")
        # block.setShader(shader)
        # block.setShaderInput("time", 0.)
        # self.grass_block.append(block)

        self.grass.set(mx, my, 0, (msize, mrotation, block))
        self.added.append((mx, my, 0))

    def frame_actions(self, frametime):
        # if self.grass_block:
        #     for block in self.grass_block:
        #         if self.gtime < 1.:
        #             self.gtime += frametime
        #         else:
        #             self.gtime = 0.
        #             self.test += 1
        #         if self.test % 2 == 0:
        #             t = 0.
        #         else:
        #             t = 1.
        #         block.setShaderInput("time", t)
        pass
