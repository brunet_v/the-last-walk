import time
import random as rand
import panda3d.core as engine
import sources.objects.utils as utils
import sources.config.constants as const

class FastLightHandler(object):

    def __init__(self, game):
        utils.InstanceCounter.add("FastLightHandler")
        self.game = game
        self.players_datas = {}

    def __del__(self):
        utils.InstanceCounter.rm("FastLightHandler")
        self.clear()

    def clear(self):
        self.game = None
        for key, player_data in self.players_datas.iteritems():
            self.delete_player_light(player_data.player, player_data)
        self.players_datas = {}

    def create_player_light(self, player, player_data):
        # Light type
        light_name = "FastSpotlight%d" % player.id
        lens = engine.PerspectiveLens()
        light = engine.Spotlight(light_name)
        light.setLens(lens)
        # Light Node
        node = engine.NodePath(light)
        # Save created lights
        player_data.light = utils.Struct({
            'light': light,
            'lens': lens,
            'node': node,
            'affecting': set(),
            'main': False,
        })

    def delete_player_light(self, player, player_data):
        if player_data.light:
            # Un-cull the light
            light = player_data.light.node
            for node in player_data.light.affecting:
                node.clearLight(light)
            # Delete the node
            if not player_data.light.main:
                player_data.light.node.detachNode()
                player_data.light.node.removeNode()
                player_data.light = None

    def set_player_main(self, player):
        player_data = self.players_datas[player.id]
        if 'spotlight' in self.game.lights.lights:
            self.delete_player_light(player, player_data)
            light, lens, node = self.game.lights.lights['spotlight']
            player_data.light = utils.Struct({
                'light': light,
                'lens': lens,
                'node': node,
                'affecting': set(),
                'main': True,
            })
            self.update_player_light(player)

    def add_player_light(self, player):
        # Create player structure
        player_data = utils.Struct({
            'lx': -1,
            'ly': -1,
            'lon': None,
            'light': None,
            'hidden': None,
            'flashing': None,
            'last_equiped': None,
            'player': player,
        })
        self.players_datas[player.id] = player_data
        # Set player callbacks
        player.light_updater = self.update_player_light
        player.extra_culling_updater = self.update_player_culling
        # Init entered values
        self.create_player_light(player, player_data)
        self.update_player_light(player)


    def update_player_light_state(self, player, player_data):
        # Get item configuration values
        ldatas = player_data.light
        light, lens, node = ldatas.light, ldatas.lens, ldatas.node
        item_light_stats = None
        # Get light stats from the current item
        if player.equiped:
            if player.light_on:
                item_type = player.equiped[1]
                item_stats = const.ITEM_STATS.get(item_type, {})
                item_light_stats = item_stats.get('light', None)
            if player.flashing:
                flashing = player.item_stats.get('flashing', None)
                if flashing:
                    item_light_stats = flashing
        # If should not cast light
        if not item_light_stats or player.hidden:
            item_light_stats = const.HAND_STATS['light']
        # If need a light, create the light following configuration
        light_color = item_light_stats.get('color', None)
        light_position = item_light_stats.get('pos', None)
        light_rotation = item_light_stats.get('hpr', None)
        light_attenuation = item_light_stats.get('attenuation', None)
        light_exponent = item_light_stats.get('exponent', None)
        light_nearfar = item_light_stats.get('near-far', None)
        light_fov = item_light_stats.get('fov', None)
        # light_rand_pos = item_light_stats.get('rand-pos', None)
        light_rand_hpr = item_light_stats.get('rand-hpr', None)
        # Light options
        if light_position:
            node.setPos(*light_position)
        if light_rotation:
            light_hpr = list(light_rotation)
            if light_rand_hpr:
                light_hpr[0] += (rand.random() - 0.5) * light_rand_hpr[0]
                light_hpr[1] += (rand.random() - 0.5) * light_rand_hpr[1]
                light_hpr[2] += (rand.random() - 0.5) * light_rand_hpr[2]
            node.setHpr(*light_hpr)
        if light_color:
            light.setColor(engine.VBase4(*light_color))
        if light_attenuation:
            light.setAttenuation(engine.Point3(*light_attenuation))
        if light_exponent:
            light.setExponent(light_exponent)
        if light_nearfar and lens:
            lens.setNearFar(*light_nearfar)
        if light_fov and lens:
            lens.setFov(light_fov)
        # Update node parent
        if player.equiped:
            node.reparentTo(player.equiped[3])
        else:
            node.detachNode()
            node.hide()

    def update_player_light(self, player, force=False):
        player_data = self.players_datas[player.id]
        if (player.equiped != player_data.last_equiped
            or player.hidden != player_data.hidden
            or player.light_on != player_data.lon
            or player.flashing != player_data.flashing) or force:
            # Update light infos
            self.update_player_light_state(player, player_data)
            # Update light culling
            self.update_player_culling(player, True)
            # Save state
            player_data.last_equiped = player.equiped
            player_data.flashing = player.flashing
            player_data.hidden = player.hidden
            player_data.lon = player.light_on

    def get_affected_node(self, x, y):
        cg = self.game.world['cg']
        return cg.getNode(x, y)

    def update_player_culling(self, player, force=False):
        player_data = self.players_datas[player.id]
        px = player.getX()
        py = player.getY()
        div = const.BLOCKS_PER_SIZE / 2
        mx = px / div
        my = py / div
        if mx != player_data.lx or my != player_data.ly or force:
            # Update light culling
            if player_data.light:
                # Basic values
                size = const.BLOCKS_PER_SIZE
                dsize = size / 2
                affecteds = set()
                olds = player_data.light.affecting
                light = player_data.light.node
                # Get every affected nodes
                for ox in xrange(2):
                    for oy in xrange(2):
                        affected = self.get_affected_node(
                            int(px - dsize) + size * ox,
                            int(py - dsize) + size * oy
                        )
                        if affected:
                            affecteds.add(affected)
                # Apply changes
                for node in affecteds - olds:
                    node.setLight(light)
                for node in olds - affecteds:
                    node.clearLight(light)
                player_data.light.affecting = affecteds
            # Save state
            player_data.lx = mx
            player_data.ly = my

    def frame_actions(self, frametime):
        now = time.time()
        pass
