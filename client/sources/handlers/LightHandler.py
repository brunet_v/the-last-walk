import math
import copy
import random as rand
import panda3d.core as engine
import sources.config.constants as const
import sources.objects.utils as utils
import sources.config.configfile as configfile

"""
    Create, activate and move game, lights/shadows on demand
"""
class LightHandler(object):

    def __init__(self):
        utils.InstanceCounter.add("LightHandler")
        shadows_filmsize = const.SHADOWS_FILMSIZE
        shadows_resolution = configfile.get("squality")
        # init environment lights
        sunlight = engine.DirectionalLight('sunlight')
        sunlight.setColor(const.SUNLIGHT_COLOR)
        sunlight.getLens().setFilmSize(shadows_filmsize, shadows_filmsize)
        sunlight.getLens().setFar(230)
        sunlight.getLens().setNear(170)
        if shadows_resolution > 0:
            shadows_resolution = max(1024, shadows_resolution)
            sunlight.setShadowCaster(True, shadows_resolution, shadows_resolution)
        sunlight.setCameraMask(engine.BitMask32.bit(1))
        sunlightnode = render.attachNewNode(sunlight)
        ambientlight = engine.AmbientLight('ambientlight')
        ambientlightnode = render.attachNewNode(ambientlight)
        # save lights
        self.lights = {
            'sunlightnode': sunlightnode,
            'sunlight': sunlight,
            'ambientlightnode': ambientlightnode,
            'ambientlight': ambientlight,
        }
        # Extra shadowing light (player light)
        if configfile.get("extrashadow"):
            spotlight_lens = engine.PerspectiveLens()
            spotlight = engine.Spotlight('playerspot')
            spotlight.setLens(spotlight_lens)
            spotlight.setCameraMask(engine.BitMask32.bit(1))
            spotlight.setShadowCaster(True, 1024, 1024)
            spotlight_node = engine.NodePath(spotlight)
            self.lights['spotlight'] = (spotlight, spotlight_lens, spotlight_node)
        # Activate
        self.cleared = False
        self.lights['sunlight'].setActive(True)
        render.setShaderAuto()
        self.to_game()
        # init light variation values
        self.max_light_variation = 0.07
        self.light_variation_interval = 2.
        self.light_variation_time = self.light_variation_interval
        self.light_variation = 0.
        self.light_variation_prev = self.light_variation
        # init hit indicator values
        self.base_ratio = 0.
        self.hit_ratio = 0.
        self.hit_timer = 0.
        self.hit_time = 1.
        self.extra_ratio = 0.

    def __del__(self):
        utils.InstanceCounter.rm("LightHandler")
        self.clear()

    def clear(self):
        if not self.cleared:
            self.lights['sunlightnode'].detachNode()
            self.lights['sunlightnode'].removeNode()
            self.lights['sunlight'].setShadowCaster(False)
            self.lights['sunlight'].setActive(False)
            self.lights['sunlight'].removeAllChildren()
            self.lights['ambientlightnode'].detachNode()
            self.lights['ambientlightnode'].removeNode()
            self.lights['ambientlight'].removeAllChildren()
        self.lights = None
        self.cleared = True

    """
        Activate shadow casters
    """
    def activate(self):
        self.lights['sunlight'].setColor(const.SUNLIGHT_COLOR)
        self.lights['ambientlight'].setColor(const.AMBIENTLIGHT_DAY_COLOR)
        self.lights['sunlight'].setActive(True)
        if 'spotlight' in self.lights:
            spotlight = self.lights['spotlight'][0]
            spotlight.setColor(engine.VBase4(0, 0, 0, 0))

    """
        Init lights for a new game
    """
    def to_game(self):
        self.state = -1
        self.activate()

    """
        Unload lights of previous game
    """
    def off_game(self):
        self.state = -1
        self.extra_ratio = 0.
        self.base_ratio = 0.
        self.hit_ratio = 0.
        self.hit_timer = 0.
        self.activate()

    """
        Color variation on hit
    """
    def set_heart_scale(self, scale):
        # Get an extra ratio (max +/-50%)
        self.extra_ratio = (-(scale - 0.7) / 0.2) * (0.35 - self.hit_timer * 0.25)
        # self.extra_ratio = 1. # Not sure if needed

    def set_player_life(self, life):
        # Base ratio based on current life (max 70%)
        self.base_ratio = ((1. - life) ** 2) * 0.7

    def set_player_hit(self):
        # Additional ratio if player just got hit (max 55%)
        self.hit_ratio = 0.55
        self.hit_timer = 1.
        self.hit_time = 5.

    def set_player_starve(self):
        pass # TODO

    def get_player_color_change(self, r, g, b, a, nored=True):
        # Calc final ratio
        ratio = self.base_ratio
        if self.hit_timer > 0.:
            hit_ratio = self.hit_ratio * (self.hit_timer ** 1.5)
            hit_ratio += self.extra_ratio * self.hit_timer
            ratio += hit_ratio
        ratio = max(0., min(1., ratio))
        # Apply, by substracting colors (and not red)
        rdiff, gdiff, bdiff = r * ratio, g * ratio, b * ratio
        if nored:
            rdiff /= 3.
        else:
            rdiff /= 2.
        return r - rdiff, g - gdiff, b - bdiff, a

    """
        Do slight variation to sun/moon color
    """
    def do_light_variation(self, r, g, b, a):
        if self.light_variation_time > self.light_variation_interval:
            # Random digit function
            def rd():
                v = rand.random() * self.max_light_variation * 2.
                return v - self.max_light_variation
            # Re-init
            self.light_variation_time = 0.
            self.light_variation_prev = self.light_variation
            self.light_variation = rd()
        # Change color and interpolate
        vr, vg, vb = (self.light_variation,) * 3
        pr, pg, pb = (self.light_variation_prev,) * 3
        ratio = self.light_variation_time / self.light_variation_interval
        iratio = 1. - ratio
        # Interpolation function
        def inter(v, t):
            return v * ratio + t * iratio
        # Result
        fr, fg, fb, fa = inter(vr, pr) + r, inter(vg, pg) + g, inter(vb, pb) + b, a
        return self.get_player_color_change(fr, fg, fb, fa)

    """
        Do slight variations to ambient light color
    """
    def do_ambient_variation(self, r, g, b, a):
        return self.get_player_color_change(r, g, b, a, False)

    """
        Apply light position algorithm,
        also define what part of the map is light-ignored,
        use world daytime (0-23h) to compute positions, night is 20h to 8h, day 8h to 20h
    """
    def place_lights(self, world, hour, xpos, ypos, oxpos, oypos):
        nx = int(xpos) / const.BLOCKS_PER_SIZE
        ny = int(ypos) / const.BLOCKS_PER_SIZE
        ox = int(oxpos) / const.BLOCKS_PER_SIZE
        oy = int(oypos) / const.BLOCKS_PER_SIZE
        ratio = (hour + 4) % 12
        state = (int(hour) + 4) % 24 / 12
        culling_grid = world['cg']
        sun = self.lights['sunlightnode']
        sunlight = self.lights['sunlight']
        ambient = self.lights['ambientlightnode']
        ambientlight = self.lights['ambientlight']
        # refresh light culling
        old_ids = set()
        new_ids = set()
        if ox != nx or oy != ny or self.state != state:
            old_ids, old_nodes = culling_grid.getNears(ox, oy)
            new_ids, new_nodes = culling_grid.getNears(nx, ny)
            culling_grid.setViewPos(nx, ny)
            clearing_ids = copy.copy(old_ids)
            setting_ids = copy.copy(new_ids)
            if self.state != -1 and nx >= 0 and ny >= 0:
                clearing_ids -= new_ids
                setting_ids -= old_ids
                for id in clearing_ids:
                    old_nodes[id].clearLight(sun)
                    old_nodes[id].clearLight(ambient)
            for id in setting_ids:
                new_nodes[id].setLight(sun)
                new_nodes[id].setLight(ambient)
        # current light colors
        light = sun
        light_node = sunlight
        if state == 0:
            light_color = const.AMBIENTLIGHT_NIGHT_COLOR
            other_color = const.AMBIENTLIGHT_DAY_COLOR
            direct_color = const.MOONLIGHT_COLOR
        else:
            light_color = const.AMBIENTLIGHT_DAY_COLOR
            other_color = const.AMBIENTLIGHT_NIGHT_COLOR
            direct_color = const.SUNLIGHT_COLOR
        # set light position
        p = 200
        delta = math.radians(50)
        phi = math.radians(math.tanh((ratio / 12.0 - 0.5) * 2.5) * 90)
        diff = 1.0
        ddiff = diff * 2.0
        xpos = math.floor(xpos / ddiff) * ddiff + diff
        ypos = math.floor(ypos / ddiff) * ddiff + diff
        lightx = p * math.cos(delta) * math.sin(phi) + xpos
        lighty = p * math.sin(delta) * math.sin(phi) + ypos
        lightz = p * math.cos(phi)
        light.setPos(lightx, lighty, lightz)
        light.lookAt(xpos, ypos, 0)
        # get light color
        if ratio < 2 or ratio > 10:
            ambient_color = []
            direct_light_color = []
            light_change_ratio = float(math.fabs(ratio - 6) - 4) * 0.25
            inverse_ratio = 1 - light_change_ratio
            inverse_smooth_ratio = min(1.0, 2 - (light_change_ratio * 4))
            for i in xrange(4):
                ambient_color.append(light_change_ratio * other_color[i] + inverse_ratio * light_color[i])
                direct_light_color.append(inverse_smooth_ratio * direct_color[i])
        else:
            ambient_color = light_color
            direct_light_color = direct_color
        # apply new state
        ambientlight.setColor(
            self.do_ambient_variation(*tuple(ambient_color))
        )
        light_node.setColor(
            self.do_light_variation(*tuple(direct_light_color))
        )
        self.state = state

    """
        Called each frame
    """
    def frame_actions(self, frametime):
        self.light_variation_time += frametime
        self.hit_timer -= frametime / self.hit_time
