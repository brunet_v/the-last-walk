import time
import math
import random as rand
import panda3d.core as engine
import sources.config.constants as const
import sources.world.generate as generate
import sources.world.init as init
import sources.world.compute as compute
import sources.objects.utils as utils
import sources.config.build_config as build_config
import sources.utils.multitask as threading

from sources.networking.ServerConnection import ServerConnection
from sources.networking.ServerEvents import ServerEvents
from sources.handlers.DecalHandler import DecalHandler
from sources.handlers.GrassHandler import GrassHandler
from sources.handlers.PhysicsHandler import PhysicsHandler
from sources.handlers.ParticlesHandler import ParticlesHandler
from sources.handlers.FastLightHandler import FastLightHandler
from sources.handlers.EffectHandler import EffectHandler
from sources.gui.ingame.UiHandler import UiHandler
from sources.utils.MousePicker import MousePicker
from sources.utils.Threader import Threader
from sources.world.ZombieUpdater import ZombieUpdater
from sources.world.PlayerUpdater import PlayerUpdater

"""
    Game class, handle server connection, lobby and gameplay
"""
class Game(Threader):

    def __init__(self, medias, menus, server, lights, sounds):
        utils.InstanceCounter.add("Game")
        debug_loading = build_config.build_option("debug-loading")
        if debug_loading:
            print "Creating game instance."
        self.menus = menus
        self.world = None
        self.player = None
        self.physics = None
        self.particles = None
        self.decals = None
        self.grass = None
        self.fastlight = None
        self.lights = lights
        self.sounds = sounds
        self.medias = medias
        self.running = True
        self.m1_pressed = False
        self.m3_pressed = False
        self.walk_pressed = False
        self.walk2_pressed = False
        self.aim_pressed = False
        self.lookaround = False
        self.force_use = False
        self.mouse_picker = MousePicker()
        self.ready = False
        self.tried = False
        self.disc = False
        self.game_starting = False
        self.game_ready = False
        self.server = server
        self.login = server["login"]
        if debug_loading:
            print "Creating game connection."
        self.connection = ServerConnection(self.server["ip"], self.server["port"])
        self.events = ServerEvents(self) # Responsible for network events
        self.effect_handler = None
        self.ui_handler = None
        self.zombie_updater = None
        self.player_updater = None
        self.accepted_events = []
        self.move_vector = utils.Struct({'x': 0, 'y': 0, 'stack': set()})
        self.message_menu = False
        self.world_loaded = False
        self.players_inits = False
        self.go = False
        if debug_loading:
            print "Game instance created."
        Threader.__init__(self, "Game")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("Game")
        self.clear()

    def clear(self):
        Threader.clear(self)
        self.running = False
        self.mouse_picker.clear()
        if self.lights:
            self.lights.off_game()
            self.lights = None
        if self.connection:
            self.connection.clear()
            self.connection = None
        if self.effect_handler:
            self.effect_handler.clear()
            self.effect_handler = None
        if self.ui_handler:
            self.ui_handler.clear()
            self.ui_handler = None
        if self.physics:
            self.physics.clear()
            self.physics = None
        if self.particles:
            self.particles.clear()
            self.particles = None
        if self.decals:
            self.decals.clear()
            self.decals = None
        if self.grass:
            self.grass.clear()
            self.grass = None
        if self.events:
            self.events.clear()
            self.events = None
        if self.zombie_updater:
            self.zombie_updater.clear()
            self.zombie_updater = None
        if self.player_updater:
            self.player_updater.clear()
            self.player_updater = None
        if self.menus:
            self.menus.close_message_menu()
            self.menus = None
        if self.fastlight:
            self.fastlight.clear()
            self.fastlight = None
        self.medias = None
        self.player = None
        self.server = None
        self.login = None
        self.delete_world()
        for event in self.accepted_events:
            base.ignore(event)
        self.accepted_events = []

    def delete_world(self):
        world = self.world
        if world:
            # Clear game actors/items
            for id, player in world['players'].iteritems():
                player.clear()
            for id, zombie in world['zombies']['list'].iteritems():
                zombie.clear()
            for id, item in world['items']['list'].iteritems():
                item.clear()
            # Clear data handlers
            if world['doors']:
                world['doors'].clear()
            if world['collision_tree']:
                world['collision_tree'].clear()
            if world['zombies']['tree']:
                world['zombies']['tree'].clear()
            if world['items']['tree']:
                world['items']['tree'].clear()
            if world['cg']:
                world['cg'].clear()
            # Clear bare map models
            maps_rendered = world['blocks']['rendered']
            maps_rendered_ns = world['blocks']['rendered-ns']
            for zkey in maps_rendered.keys():
                for ykey in maps_rendered[zkey].keys():
                    for xkey in maps_rendered[zkey][ykey].keys():
                        try:
                            maps_rendered[zkey][ykey][xkey].setLightOff()
                            maps_rendered[zkey][ykey][xkey].detachNode()
                            maps_rendered[zkey][ykey][xkey].removeNode()
                            maps_rendered_ns[zkey][ykey][xkey].setLightOff()
                            maps_rendered_ns[zkey][ykey][xkey].detachNode()
                            maps_rendered_ns[zkey][ykey][xkey].removeNode()
                        except:
                            pass
            # Remove references
            world['doors'] = None
            world['players'] = None
            world['zombies'] = None
            world['items'] = None
            world['blocks'] = None
            world['cg'] = None
            self.world = None

    """
        Runtime mouse events handler
    """
    def handle_events(self, type):
        # Usefull values
        now = time.time()
        mx = self.mouse_pos.getX()
        my = self.mouse_pos.getY()
        # Ally position helper
        if type == "help-down":
            self.ui_handler.show_helpers(True)
            self.ui_handler.load_players_infos_win()
        if type == "help-up":
            self.ui_handler.show_helpers(False)
            self.ui_handler.hide_players_infos_win()
        # Rotate camera events
        if type == "rotate-up":
            self.world['infos']['view']['rotationed'] += 15
        if type == "rotate-down":
            self.world['infos']['view']['rotationed'] -= 15
        # Zoom camera events
        world_view = self.world['infos']['view']
        if type == "zoom-down":
            world_view['zoomed'] = min(world_view['zoomed'] + 0.75, const.MAX_CAM_ZOOM)
        if type == "zoom-up":
            world_view['zoomed'] = max(world_view['zoomed'] - 0.75, const.MIN_CAM_ZOOM)
        # Looking around camera events
        if type == "look-down":
            self.lookaround = True
        if type == "look-up":
            self.lookaround = False
        # Walk towards mouse events
        if not self.message_menu:
            if type == "walk-down":
                self.walk_pressed = True
                self.handle_events("refresh-looking")
            if type == "walk-up":
                if not self.walk2_pressed and self.walk_pressed:
                    self.send("p_stop", {})
                self.walk_pressed = False
        # Sprint mode
        if type == "sprint-down":
            self.send("p_sprint", True)
        if type == "sprint-up":
            self.send("p_sprint", False)
        # Aim mode
        if type == "aim-down":
            self.aim_pressed = True
            self.handle_events("aim")
        if type == "aim-up":
            self.aim_pressed = False
            self.handle_events("unaim")
        # Ingame message menu activation events
        if type == "msg-toggle":
            if not self.message_menu:
                self.menus.load_message_menu()
            else:
                self.menus.close_message_menu()
            self.message_menu = not self.message_menu
        # Maintain a vector of direction for player mouvement
        move_vectors = {
            'up': (0, -1),
            'down': (0, 1),
            'left': (1, 0),
            'right': (-1, 0),
        }
        for key, (x, y) in move_vectors.iteritems():
            if type == key and not self.message_menu:
                if not (x, y) in self.move_vector.stack:
                    self.move_vector.x += x
                    self.move_vector.y += y
                    if (x, y) not in self.move_vector.stack:
                        self.move_vector.stack.add((x, y))
                        self.handle_events("refresh-looking")
            if type == key + "-release":
                if (x, y) in self.move_vector.stack:
                    self.move_vector.x -= x
                    self.move_vector.y -= y
                    self.move_vector.stack.remove((x, y))
                    if self.move_vector.x == 0 and self.move_vector.y == 0:
                        self.send("p_stop", {})
        # Main player manipulations
        if self.player and not self.player.dead:
            if not self.message_menu:
                # Main click trigger action
                if type == "m1-down":
                    if self.ui_handler.m1_click():
                        item_id = self.mouse_picker.getMouseItem()
                        door_id = self.mouse_picker.getMouseDoor()
                        if door_id and (not item_id or rand.randint(0, 1)):
                            self.send("door_use", {
                                'id': door_id,
                            })
                        elif item_id:
                            self.send("item_take", {
                                'id': item_id,
                            })
                        else:
                            if self.move_vector.x == 0 and self.move_vector.y == 0:
                                self.walk2_pressed = True
                                self.handle_events("refresh-looking")
                if type == "m1-up":
                    if self.walk2_pressed:
                        self.send("p_stop", {})
                        self.walk2_pressed = False
                    self.m1_pressed = False
                    if self.ui_handler.m1_unclick():
                        pass
                # Second click trigger aiming mode
                if type == "aim":
                    self.send("p_aim", {})
                if type == "unaim":
                    self.send("p_unaim", {})
                if type == "m3-down":
                    if self.ui_handler.m3_click():
                        self.m3_pressed = True
                        self.force_use = True
                        self.handle_events("aim")
                        self.send("p_use", {})
                if type == "m3-up":
                    self.m3_pressed = False
                    self.force_use = False
                    self.send("p_unuse", {})
                    if not self.aim_pressed:
                        self.handle_events("unaim")
                    if self.ui_handler.m3_unclick():
                        pass
            # On/Off player light
            if type == "toggle-light":
                self.send("p_light", {})
            # Automatic looking update
            if type == "refresh-looking":
                self.ui_handler.periodic_actions()
                self.mouse_picker.getMouseItem()
                self.mouse_picker.getMouseDoor()
                self.mouse_picker.updateLists(self.world, self.player)
                px = self.player.getX()
                py = self.player.getY()
                self.player.stmove = False
                if self.walk_pressed or self.walk2_pressed:
                    dx = mx - px
                    dy = my - py
                    gx = dx * 4. + px
                    gy = dy * 4. + py
                    if dx ** 2 + dy ** 2 > 3:
                        self.player.stmove = True
                        self.player.pre_goal = engine.Point3(gx, gy, -1)
                elif self.move_vector.x != 0 or self.move_vector.y != 0:
                    rotation = -math.radians(self.world['infos']['view']['rotation'])
                    cs = math.cos(rotation)
                    sn = math.sin(rotation)
                    gx = self.move_vector.x * cs - self.move_vector.y * sn
                    gy = self.move_vector.x * sn + self.move_vector.y * cs
                    gx = gx * 32. + px
                    gy = gy * 32. + py
                    self.player.stmove = True
                    self.player.pre_goal = engine.Point3(gx, gy, -1)
                else:
                    gx = px
                    gy = py
                self.send("p_update", {
                    'gx': gx,
                    'gy': gy,
                    'lx': mx,
                    'ly': my,
                })
        # Debugs
        if type == "refresh-debug":
            if build_config.build_option("debug"):
                print "%.2f fps (%.2f p/s)" % (
                    self.fps_counter / (now - self.fps_time),
                    (self.connection.packets - self.packet_counter) / (now - self.fps_time)
                )
                print "In: %.2f Ko/s" % (self.connection.bandwidth.input / 1000. / 2.)
                print "Out: %.2f Ko/s" % (self.connection.bandwidth.output / 1000. / 2.)
                self.connection.bandwidth.input = 0
                self.connection.bandwidth.output = 0
            self.fps_time = now
            self.fps_counter = 0
            self.packet_counter = self.connection.packets
            self.send("m_ping", {'time': time.time()})
        self.smpos = (mx, my)


    """
        Called every frame in-game, do basic actions like getting mouse position
    """
    def frame_basics(self, frametime):
        if self.player:
            nmouse_pos = self.mouse_picker.pick(self.player.getZ() + 1.5)
            if nmouse_pos:
                self.mouse_pos = nmouse_pos
            self.physics.frame_actions(frametime)
            self.particles.frame_actions(frametime)
            self.decals.frame_actions(frametime)
            self.ui_handler.frame_actions(frametime)
            self.grass.frame_actions(frametime)
            self.fastlight.frame_actions(frametime)
            player = self.player
            # Look around
            if self.lookaround:
                max_dist = 6.0
                diff = self.mouse_pos - player.getPos()
                diff.setZ(0)
                dlength = diff.length()
                if dlength > max_dist:
                    diff /= dlength
                    diff *= max_dist
                player.looking = diff + player.getPos()
            else:
                player.looking = None
            if player.aiming:
                mx, my = self.mouse_pos.getX(), self.mouse_pos.getY()
                player.lookpoint = engine.Point3(mx, my, -1)
            self.lights.set_player_life(float(player.life) / float(player.max_life))

    """
        Init event, and save its name
    """
    def accept(self, event, callback, extraArgs=[]):
        base.accept(event, callback, extraArgs=extraArgs)
        self.accepted_events.append(event)

    """
        Init game keyboard event listener, and bind them&
    """
    def init_events(self):
        key_modifiers = [
            "",
            "alt-",
            "shift-",
            "control-",
            "shift-alt-",
            "control-alt-",
            "shift-control-",
            "shift-control-alt-",
        ]
        # Character controls
        arrows = {
            'up': ['arrow_up', 'z', 'w'],
            'down': ['arrow_down', 's', 's'],
            'left': ['arrow_left', 'q', 'a'],
            'right': ['arrow_right', 'd', 'd'],
        }
        for key, (std, extrafr, extraen) in arrows.iteritems():
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, std), self.handle_events, extraArgs=[key])
                self.accept("%s%s-up" % (mod, std), self.handle_events, extraArgs=[key + "-release"])
                self.accept("%s%s" % (mod, extrafr), self.handle_events, extraArgs=[key])
                self.accept("%s%s-up" % (mod, extrafr), self.handle_events, extraArgs=[key + "-release"])
                if extraen != extrafr:
                    self.accept("%s%s" % (mod, extraen), self.handle_events, extraArgs=[key])
                    self.accept("%s%s-up" % (mod, extraen), self.handle_events, extraArgs=[key + "-release"])
        main_click_button = ["mouse1", "t"]
        for button in main_click_button:
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["m1-down"])
                self.accept("%s%s-up" % (mod, button), self.handle_events, extraArgs=["m1-up"])
        second_click_button = ["mouse3", "y"]
        for button in second_click_button:
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["m3-down"])
                self.accept("%s%s-up" % (mod, button), self.handle_events, extraArgs=["m3-up"])
        walk_button = ["mouse4", "mouse5",]
        for button in walk_button:
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["walk-down"])
                self.accept("%s%s-up" % (mod, button), self.handle_events, extraArgs=["walk-up"])
        extra_aim_button = ["control", "shift",]
        for button in extra_aim_button:
            self.accept("%s" % button, self.handle_events, extraArgs=["aim-down"])
            self.accept("%s-up" % button, self.handle_events, extraArgs=["aim-up"])
        sprint_button = ["space",]
        for mod in key_modifiers:
            for button in sprint_button:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["sprint-down"])
                self.accept("%s%s-up" % (mod, button), self.handle_events, extraArgs=["sprint-up"])
        light_button = ["f",]
        for mod in key_modifiers:
            for button in light_button:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["toggle-light"])
        # Camera controls
        for mod in key_modifiers:
            if "shift" not in mod:
                self.accept("%swheel_down" % mod, self.handle_events, extraArgs=["rotate-down"])
                self.accept("%swheel_up" % mod, self.handle_events, extraArgs=["rotate-up"])
            else:
                self.accept("%swheel_down" % mod, self.handle_events, extraArgs=["zoom-down"])
                self.accept("%swheel_up" % mod, self.handle_events, extraArgs=["zoom-up"])
        look_buttons = ["alt", ""]
        for button in look_buttons:
            self.accept("%s" % button, self.handle_events, extraArgs=["look-down"])
            self.accept("%s-up" % button, self.handle_events, extraArgs=["look-up"])
        # Inventory management
        inventory_buttons = {
            "0": ["1", "&", "f1",],
            "1": ["2", "2", "f2",],
            "2": ["3", "\"", "f3",],
            "3": ["4", "'", "f4",],
            "4": ["5", "(", "f5",],
            "5": ["6", "-", "f6",],
            "6": ["7", "7", "f7",],
            "7": ["8", "_", "f8",],
            "8": ["9", "9",],
        }
        for slot, buttons in inventory_buttons.iteritems():
            for button in buttons:
                for mod in key_modifiers:
                    self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["use-slot-%s" % slot])
        # Misc games controls
        positions_buttons = ["tab",]
        for button in positions_buttons:
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["help-down"])
                self.accept("%s%s-up" % (mod, button), self.handle_events, extraArgs=["help-up"])
        message_menu_buttons = ["enter",]
        for button in message_menu_buttons:
            for mod in key_modifiers:
                self.accept("%s%s" % (mod, button), self.handle_events, extraArgs=["msg-toggle"])
        # self.menus.ignore_events()
        self.accept("escape", self.menus.load_game_menu)

    """
        Asynchronously load the game, when connected
    """
    def loading(self):
        # Run when game is starting, do world inits
        world_def = self.world_def
        debug_loading = build_config.build_option("debug-loading")
        if debug_loading:
            print "Loading basic handlers."
        self.physics = PhysicsHandler()
        self.particles = ParticlesHandler()
        self.fastlight = FastLightHandler(self)
        if debug_loading:
            print "Generating world."
        self.world = generate.generate_world(self.medias, world_def)
        self.refresh_times = [
            (0.05, "refresh-looking"),
            (2.00, "refresh-debug"),
        ]
        self.refresh_timers = [time.time()] * len(self.refresh_times)
        self.fps_time = time.time()
        self.fps_counter = 0
        self.packet_counter = 0
        self.send('ready', {})

    """
        Send to server
    """
    def send(self, type, data):
        if self.connection:
            self.connection.send(type, data)

    """
        Initialisations to do, when everybody is ready
    """
    def game_started(self):
        init.init_world(self.medias, self.world, self.lights)
        self.lights.to_game()
        self.menus.unload_document()
        self.effect_handler = EffectHandler(self.world)
        self.decals = DecalHandler(self)
        self.grass = GrassHandler(self)
        self.ui_handler = UiHandler(self)
        self.zombie_updater = ZombieUpdater(self)
        self.player_updater = PlayerUpdater(self)
        self.mouse_pos = engine.Point3(0, 0, 0)
        self.smpos = (0, 0)
        self.init_events()
        self.sounds.play_music("day1", 0.5, "game")
        self.game_ready = True

    """
        Start world loading, on connection
    """
    def load_world(self, world_def):
        if not self.world_loaded:
            self.world_loaded = True
            self.world_def = world_def
            threading.Thread(target=self.loading).start()

    """
        Main fakethread runtime function.
        Called at every frame, do game calculations except some visual effects
        and zombie/players computations
    """
    def run(self, frametime):
        now = time.time()
        if self.connection.connected and self.running:
            # If world not started, connection to server not finished
            if not self.ready or not self.game_ready:
                if not self.tried:
                    self.send("logging", self.login)
                    self.send("rplayers", {})
                    self.tried = True
            # If ready, mean game in progress
            elif self.go:
                # Trigger periodic events
                for i, (timer, event) in enumerate(self.refresh_times):
                    if now - self.refresh_timers[i] > timer:
                        self.refresh_timers[i] = now
                        self.handle_events(event)
                # Global frame action
                self.fps_counter += 1
                self.frame_basics(frametime)
                compute.compute_world(self, self.world, frametime)
        # Check if connection is still alive
        if not self.connection.check_alive() and not self.disc:
            self.menus.ask_leave_lobby()
            self.menus.load_connect_fail(False)
            self.disc = True
