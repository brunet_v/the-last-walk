import panda3d.core as engine
import sources.config.constants as const
import sources.world.camera as camera
import sources.objects.utils as utils
import sources.config.configfile as configfile
import sources.config.build_config as build_config
import sources.utils.resources as recoure

from pandac.PandaModules import ClockObject
from panda3d.core import Filename

"""
    Called only once, before window creation
"""
def init_panda(failed):
    # Basic config
    init_window()
    if not failed:
        # Fluid animations ?
        engine.loadPrcFileData("", "interpolate-frames #t")
        # engine.loadPrcFileData("", "hardware-animated-vertices 1")
        # Vsync
        engine.loadPrcFileData("", "sync-video %d" % (not configfile.get("vsync")))
        # AA
        if configfile.get("anti-alias"):
            engine.loadPrcFileData("", "framebuffer-multisample 1")
            engine.loadPrcFileData("", "multisamples 2")
        # Rdb recommanded ?
        engine.loadPrcFileData("", "gl-force-no-flush 1")
        # Debug util
        engine.loadPrcFileData("", "glfinal 1")

"""
    Init game window, called once at the process start, or at resolution change
"""
def init_window(change = False):
    import sources.config.configfile as configfile
    # init full screen with correct resolution
    win_props = engine.WindowProperties()
    #win_props.setCursorHidden(True)
    win_props.setTitle("The Last Walk")
    win_props.setCursorFilename(Filename(recoure.relative_path("./datas/ui/cursor.ico")))
    win_props.setIconFilename(Filename(recoure.relative_path("./datas/ui/favicon.ico")))
    screen_width = configfile.get("desktop_width")
    screen_height = configfile.get("desktop_height")
    xsize = configfile.get("wxsize")
    ysize = configfile.get("wysize")
    win_props.setFullscreen(configfile.get("fullscreen"))
    win_props.setSize(int(xsize), int(ysize))
    win_props.setFixedSize(True)
    if not configfile.get("fullscreen"):
        win_props.setOrigin(
            int((screen_width - xsize) / 2.0),
            int((screen_height - ysize) / 2.0)
        )
    if change:
        base.win.requestProperties(win_props)
    else:
        engine.WindowProperties.setDefault(win_props)

"""
    Called only once at the start after window creation
"""
def init_process():
    # supported resolutions
    configfile.ConfigFile.init_res(base.pipe.getDisplayInformation())
    # very basics engine initialisations
    base.disableMouse()
    base.enableParticles()
    base.cam.node().setCameraMask(engine.BitMask32.bit(0))
    base.camLens.setNear(1.0)
    if not build_config.build_option("debug-zoom"):
        base.camLens.setFar(50.0)
    # base.setBackgroundColor(256, 256, 256)
    base.setBackgroundColor(0, 0, 0)
    if configfile.get("vsync") and configfile.get("vsync") < 300:
        globalClock.setMode(ClockObject.MLimited)
        globalClock.setFrameRate(configfile.get("vsync"))

    """
        Debug methods
    """
    # Fps metter + debug commands
    if build_config.build_option("debug"):
        base.accept("f12", render.analyze)
        base.accept("f9", utils.InstanceCounter.dump)
        base.setFrameRateMeter(True)
    # Pstats module
    if build_config.build_option("debug-pstats"):
        engine.PStatClient.connect()
    # Camera culling
    if build_config.build_option("extra-debug"):
        base.oobe()
        base.oobeCull(base.cam)
        base.camLens.setFar(30000.0)


"""
    Prepare the loaded game world by setting the basic value and showing the map
"""
def init_world(medias, world, saved_lights):
    # init views values
    world_view = world['infos']['view']
    world_view['xpos'] = 10000
    world_view['ypos'] = 10000
    world_view['zpos'] = 0
    world_view['daytime'] = 10.5
    world_view['rotation'] = 45
    world_view['rotationed'] = 45
    mzoom = const.MAX_CAM_ZOOM
    world_view['zoom'] = mzoom
    world_view['zoomed'] = mzoom
    if build_config.build_option("debug-zoom"):
        world_view['zoom'] = 150
        world_view['zoomed'] = 150
    # activate camera
    camera.init_camera(world)
    # activate map lightning
    world['lights'] = saved_lights
    # activate maps blocks display
    maps_rendered = world['blocks']['rendered']
    maps_rendered_ns = world['blocks']['rendered-ns']
    for zkey in maps_rendered.keys():
        for ykey in maps_rendered[zkey].keys():
            for xkey in maps_rendered[zkey][ykey].keys():
                maps_rendered[zkey][ykey][xkey].reparentTo(world['cg'].getNode(
                    xkey * const.BLOCKS_PER_SIZE,
                    ykey * const.BLOCKS_PER_SIZE,
                    zkey * const.BLOCKS_PER_SIZE
                ))
                maps_rendered_ns[zkey][ykey][xkey].reparentTo(world['cg'].getNode(
                    xkey * const.BLOCKS_PER_SIZE,
                    ykey * const.BLOCKS_PER_SIZE,
                    zkey * const.BLOCKS_PER_SIZE
                ))
                maps_rendered_ns[zkey][ykey][xkey].hide(engine.BitMask32.bit(1))

"""
    Called at the exit of the game
"""
def exit_process():
    exit()
