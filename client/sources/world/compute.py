import sources.world.camera as camera
import sources.config.constants as const

"""
    Basic world updates every frame
"""

def compute_world(game, world, frametime):
    # Adjust camera to center
    camera.adjust_view(world, frametime)
    # World updates
    world['doors'].update(frametime)
    # Update daytime
    daytime = world['infos']['view']['daytime']
    ratio = 1.
    if daytime < 8 or 20 < daytime < 24 + 8:
        ratio = const.DAYTIME_NIGHT_RATIO
    world['infos']['view']['daytime'] += const.DAYTIME_SPEED / const.DAYTIME_DELAY * frametime * ratio
    world['infos']['view']['daytime'] %= 24.0
