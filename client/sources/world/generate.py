import copy
import pickle as serial
import panda3d.core as engine
import sources.config.constants as const
import sources.utils.trees as trees
import sources.utils.resources as resources
import sources.utils.positioning as positioning
import sources.objects.creation as creation
import sources.config.build_config as build_config

from sources.utils.CullingGrid import CullingGrid
from sources.handlers.DoorHandler import DoorHandler
from sources.utils.CollisionTree import CollisionTree

"""
    Load a compiled "map.mpx" into world at the correct offset,
    with the asked rotation and apply/precompute fog, collision and other effects on it.
"""
def load_map(medias, world, path, xoffset, yoffset, size, rotation):
    multiplier = const.BLOCKS_PER_SIZE
    true_xoffset = xoffset * multiplier
    true_yoffset = yoffset * multiplier
    map_size_offset = size * multiplier - 1
    world_def = world['infos']['map_def']
    world_blocks = world['blocks']
    blocks_collision = medias['blocks_collisions']
    map_file = open("datas/maps/compiled/%s" % path, 'rU')
    map_data = serial.loads(map_file.read())
    map_file.close()
    map_blocks = map_data['blocks']
    animateds = []
    x_reverse, y_reverse, axis_reverse = const.ROTATION_FLIPS[rotation]
    # Debug
    collision_dump = False
    if build_config.build_option("debug-collision"):
        collision_dump = True
    # Load map datas
    for zkey in map_blocks.keys():
        for ykey in map_blocks[zkey].keys():
            for xkey in map_blocks[zkey][ykey].keys():
                loaded_block = map_blocks[zkey][ykey][xkey]
                if x_reverse:
                    loaded_block['x'] = map_size_offset - loaded_block['x']
                if y_reverse:
                    loaded_block['y'] = map_size_offset - loaded_block['y']
                if axis_reverse:
                    loaded_block['x'], loaded_block['y'] = loaded_block['y'], loaded_block['x']
                loaded_block['r'] = (loaded_block['r'] + rotation) % 4
                loaded_block['c'] = blocks_collision.get(loaded_block['t'], loaded_block['r'])
                loaded_block['x'] += true_xoffset
                loaded_block['y'] += true_yoffset
                if 'n' in loaded_block:
                    for info in loaded_block['n']:
                        # Door case
                        if info[0].startswith('door'):
                            world['doors'].addDoor(
                                blocks_collision,
                                loaded_block,
                                info,
                                rotation,
                            )
                        # Animated block case
                        if info[0].startswith('anim'):
                            animateds.append(creation.create_animated_static(
                                world,
                                info[1],
                                loaded_block['x'] - true_xoffset,
                                loaded_block['y'] - true_yoffset,
                                loaded_block['z'],
                                info[2]
                            ))
                trees.add_to_tree(
                    world_blocks['datas'],
                    loaded_block['x'],
                    loaded_block['y'],
                    loaded_block['z'],
                    loaded_block
                )
                if collision_dump:
                    loaded_block['c'].dumpTo(render, loaded_block['x'], loaded_block['y'], loaded_block['z'])
    # Load graphic datas
    cutting_edges = map_data['def']['cutting_edges']
    edge_idx = 1
    map_total_size = multiplier * size
    model_rotation = rotation
    fog_texture_stage = world_blocks['fog'][0]
    fog_texture = world_blocks['fog'][1]
    # render of base map
    base_map_node = resources.load_model_file("datas/maps/compiled/%s-%d.bam" % (path, edge_idx), nearest=True)
    base_map_node.setTexture(fog_texture_stage, fog_texture)
    positioning.position_fog_texture(base_map_node, fog_texture_stage, true_xoffset, true_yoffset, rotation, world_def['size'])
    trees.add_to_tree(world_blocks['rendered'], xoffset, yoffset, -10000, base_map_node)
    positioning.position_node(base_map_node, true_xoffset, true_yoffset, 0, model_rotation, map_total_size)
    # render of base map (no-shadow)
    base_map_node_ns = resources.load_model_file("datas/maps/compiled/%s-%d-ns.bam" % (path, edge_idx), nearest=True)
    base_map_node_ns.setTexture(fog_texture_stage, fog_texture)
    positioning.position_fog_texture(base_map_node_ns, fog_texture_stage, true_xoffset, true_yoffset, rotation, world_def['size'])
    trees.add_to_tree(world_blocks['rendered-ns'], xoffset, yoffset, -10000, base_map_node_ns)
    positioning.position_node(base_map_node_ns, true_xoffset, true_yoffset, 0, model_rotation, map_total_size)
    # reparent animated block to correct edge
    min_edge = None
    if cutting_edges:
        min_edge = cutting_edges[0]
    for animated in animateds:
        if not min_edge or animated[1] < min_edge:
            animated[0].reparentTo(base_map_node)
    # render and positionning of cutted map edges
    idx = 0
    for edge_z in cutting_edges:
        edge_idx += 1
        # Normal edge
        higher_edge_node = resources.load_model_file("datas/maps/compiled/%s-%d.bam" % (path, edge_idx), nearest=True)
        higher_edge_node.setTexture(fog_texture_stage, fog_texture)
        positioning.position_fog_texture(higher_edge_node, fog_texture_stage, true_xoffset, true_yoffset, rotation, world_def['size'])
        trees.add_to_tree(world_blocks['rendered'], xoffset, yoffset, edge_z, higher_edge_node)
        positioning.position_node(higher_edge_node, true_xoffset, true_yoffset, 0, model_rotation, map_total_size)
        # No shadow edge
        higher_edge_node_ns = resources.load_model_file("datas/maps/compiled/%s-%d-ns.bam" % (path, edge_idx), nearest=True)
        higher_edge_node_ns.setTexture(fog_texture_stage, fog_texture)
        positioning.position_fog_texture(higher_edge_node_ns, fog_texture_stage, true_xoffset, true_yoffset, rotation, world_def['size'])
        trees.add_to_tree(world_blocks['rendered-ns'], xoffset, yoffset, edge_z, higher_edge_node_ns)
        positioning.position_node(higher_edge_node_ns, true_xoffset, true_yoffset, 0, model_rotation, map_total_size)
        # reparent animated blocks to correct edge
        for animated in animateds:
            if animated[1] >= edge_z:
                if len(cutting_edges) <= idx + 1 or animated[1] < cutting_edges[idx + 1]:
                    animated[0].reparentTo(higher_edge_node)
        idx += 1
    trees.add_to_tree(world['infos']['world_grid'], xoffset, yoffset, 0, (path, size, rotation))


"""
    Generate a 3D world from maps and the world definition
    Initialise the world to be ready for the game (collision, handlers and graphics)
"""
def generate_world(medias, world_def):
    debug_loading = build_config.build_option("debug-loading")
    if debug_loading:
        print "Creating basic world struct."
    world = copy.deepcopy(const.WORLD_STRUCT)
    world['cg'] = CullingGrid()
    world['collision_tree'] = CollisionTree()
    world['infos']['world_grid'] = {}
    world['infos']['map_def'] = world_def
    world['doors'] = DoorHandler(world, world_def['seed'])
    world_size = world_def['size']
    # generate base fog image
    fog_image_size = world_size * const.BLOCKS_PER_SIZE
    fog_image = engine.PNMImage(fog_image_size, fog_image_size)
    fog_image.make_grayscale()
    fog_image.fill(const.UNEXPLORED_BLOCK_FOG)
    if build_config.build_option("no-fog"):
        fog_image.fill(0.)
    fog_ts = engine.TextureStage('ts_fog')
    fog_ts.setMode(engine.TextureStage.MBlend)
    fog_texture = engine.Texture()
    fog_texture.load(fog_image)
    fog_texture.setMinfilter(engine.Texture.FTLinear)
    fog_texture.setMagfilter(engine.Texture.FTLinear)
    # fog_texture.setMagfilter(engine.Texture.FTNearest)
    world['zombies']['tree'] = CollisionTree(const.BLOCKS_PER_SIZE)
    world['items']['tree'] = CollisionTree(const.BLOCKS_PER_SIZE)
    if debug_loading:
        print "Loading maps files."
    world['blocks']['fog'] = (fog_ts, fog_texture)
    for x, y, name, size, rotation in world_def['maps']:
        load_map(medias, world, name, x, y, size, rotation)
    return world
