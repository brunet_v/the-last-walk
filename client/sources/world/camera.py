import math
import sources.config.constants as const
import panda3d.core as engine

"""
    Called at world start. (default camera is used)
"""
def init_camera(world):
    world_view = world['infos']['view']
    world_view['ppos'] = engine.Point3(0, 0, 0)


"""
    Position camera relative to view position and rotation
"""
def place_camera(world):
    world_view = world['infos']['view']
    zoom = world_view['zoom']
    rotation = math.radians(world_view['rotation'])
    base.camera.setPos(
        world_view['xpos'] + math.sin(rotation) * zoom,
        world_view['ypos'] + math.cos(rotation) * zoom,
        world_view['zpos'] + zoom * const.CAMERA_HIGH_RATIO
    )
    base.camera.lookAt(
        world_view['xpos'],
        world_view['ypos'],
        world_view['zpos']
    )

"""
    Smooth camera movements to be centered on main player character
"""
def adjust_view(world, frametime):
    hooked_id = world['infos']['hooked']
    world_view = world['infos']['view']
    # Ratios
    move_ratio = min(1.0, frametime * 3.)
    move_ratio_look = min(1.0, frametime * 2.5)
    zoom_ratio = min(1.0, frametime * 1.5)
    rotation_ratio = min(1.0, frametime * 2.5)
    move_rest = 1.0 - move_ratio
    move_rest_look = 1.0 - move_ratio_look
    zoom_rest = 1. - zoom_ratio
    rotation_rest = 1. - rotation_ratio
    # Position interpolation
    world_view['oxpos'] = world_view['xpos']
    world_view['oypos'] = world_view['ypos']
    world_view['ozpos'] = world_view['zpos']
    if hooked_id:
        hooked = world['players'][hooked_id]
        world_view['oppos'] = world_view['ppos']
        world_view['ppos'] = hooked.getVisualPos()
        if frametime > 0:
            goal = hooked
            ratios = [move_rest, move_ratio]
            if hooked.looking:
                goal = hooked.looking
                ratios = [move_rest_look, move_ratio_look]
            mdiff = 0.065
            if not (abs(world_view['xpos'] - goal.getX()) < mdiff
                and abs(world_view['ypos'] - goal.getY()) < mdiff
                and abs(world_view['zpos'] - goal.getZ()) < mdiff):
                    world_view['xpos'] = (world_view['xpos'] * ratios[0] + goal.getX() * ratios[1])
                    world_view['ypos'] = (world_view['ypos'] * ratios[0] + goal.getY() * ratios[1])
                    world_view['zpos'] = (world_view['zpos'] * ratios[0] + goal.getZ() * ratios[1])
    # Zoom interpolation
    zoomed = world_view['zoomed'] + world_view['zoomdecal']
    zoomed = min(const.MAX_CAM_ZOOM, max(const.MIN_CAM_ZOOM, zoomed))
    if abs(world_view['zoom'] - zoomed) > 0.01:
        world_view['zoom'] = world_view['zoom'] * zoom_rest + zoomed * zoom_ratio
    # Rotation interpolation
    if abs(world_view['rotation'] - world_view['rotationed']) > 0.1:
        world_view['rotation'] = world_view['rotation'] * rotation_rest + world_view['rotationed'] * rotation_ratio
    # Lights
    world['lights'].place_lights(
        world,
        world_view['daytime'],
        world_view['xpos'],
        world_view['ypos'],
        world_view['oxpos'],
        world_view['oypos'],
    )
    place_camera(world)
