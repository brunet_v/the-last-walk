import time
import sources.objects.utils as utils
import sources.config.constants as const

from sources.utils.Threader import Threader

class ZombieUpdater(Threader):

    def __init__(self, game):
        self.game = game
        Threader.__init__(self, "ZombieUpdater")
        utils.InstanceCounter.add("ZombieUpdater")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("ZombieUpdater")
        self.clear()

    def clear(self):
        Threader.clear(self)
        self.game = None

    """
        Compute bahavior with last know server status
        for every zombies in the local zone.
    """
    def run(self, frametime):
        now = time.time()
        world = self.game.world
        hooked_id = world['infos']['hooked']
        wblocks = world['blocks']['datas']
        z_delete = []
        zcontainer = world['zombies']
        # Select close zombies to update
        znears = []
        if hooked_id:
            hooked = world['players'][hooked_id]
            znears = zcontainer['tree'].getNear(hooked.getX(), hooked.getY(), 0, 20.)
        # Iterate over near zombies
        for id in znears:
            zombie = zcontainer['list'][id]
            zombie_pos = zombie.getPos()
            # Birth state (when appearing)
            if zombie.birth:
                if now > zombie.birth:
                    zombie.birth = None
                zombie.playAnim('BIRTH')
            # Dead state
            elif zombie.dead:
                if now - zombie.dead > const.ZDIE_TIME:
                    z_delete.append(id) # schedule deletion
                zombie.playAnim('DIE')
            # Attack state
            elif zombie.attacking:
                if now > zombie.attacking:
                    zombie.attacking = None
                zombie.playAnim('ATTACK')
            # Hit state (taking damages)
            elif zombie.hitted:
                if now - zombie.hitted > const.STUN_TIME:
                    zombie.hitted = None
                zombie.playAnim('HIT')
            # Moving state (something to do)
            elif zombie.goal:
                dist_goal = zombie.goal - zombie_pos
                dist_goal.setZ(0)
                dist_lenght = dist_goal.lengthSquared()
                if dist_lenght > const.ZOMBIE_WALK_MARGIN or len(zombie.goal_stack):
                    if not dist_lenght > const.ZOMBIE_WALK_MARGIN:
                        zombie.goal = zombie.goal_stack.pop(0)
                        dist_goal = zombie.goal - zombie_pos
                        dist_goal.setZ(0)
                    dist_goal.normalize()
                    zombie.setPos(zombie_pos + dist_goal * (frametime * zombie.stats['speed']), wblocks)
                    zombie.playAnim('WALK')
                else:
                    zombie.goal = None
            # Waiting state (nothing to do)
            else:
                zombie.playAnim('WAIT')
            # Updates
            zombie.frameActions(frametime)
        # Delete old-dead zombies
        for id in z_delete:
            zcontainer['list'].pop(id)
            zcontainer['tree'].deleteId(id)
