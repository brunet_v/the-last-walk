import time
import sources.objects.utils as utils
import sources.config.constants as const

from sources.utils.Threader import Threader

class PlayerUpdater(Threader):

    def __init__(self, game):
        self.game = game
        Threader.__init__(self, "PlayerUpdater")
        utils.InstanceCounter.add("PlayerUpdater")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("PlayerUpdater")
        self.clear()

    def clear(self):
        Threader.clear(self)
        self.game = None

    """
        Update states of every player on the map,
        do special fast updates on the hooked (current playing) player

        Compute players states, mouvements and basics stats
    """
    def run(self, frametime):
        now = time.time()
        game = self.game
        world = game.world
        wblocks = world['blocks']['datas']
        plist = world['players']
        # Iterate over players
        for id, player in plist.iteritems():
            player_pos = player.getPos()
            if not player.freezed:

                """
                    Lower body
                """
                # Update position and legs animations
                player.mspeed = 0
                # Player state test
                if not player.forced_animation:
                    if player.goal or player.stmove:
                        far_enough = False
                        if player.goal:
                            player.goal.setZ(player_pos.getZ())
                            dist_goal = player.goal - player_pos
                            far_enough = dist_goal.lengthSquared() > const.PLAYER_WALK_MARGIN
                        if far_enough or player.stmove:
                            if player.aiming:
                                speed = player.stats['aim-speed']
                            elif player.tired:
                                speed = player.stats['slow-speed']
                            elif player.sprint:
                                speed = player.stats['sprint-speed']
                                player.setEnergy(player.energy - frametime * const.ENERGY_SPRINT_RATE)
                            else:
                                speed = player.stats['walk-speed']
                                cost = frametime * const.ENERGY_WALK_RATE * player.getAdditionalCost()
                                player.setEnergy(player.energy - cost)
                            if player.aiming:
                                player.legsAnimation("AIM-WALK")
                            elif player.tired:
                                player.legsAnimation("WALK-SLOW")
                            elif player.sprint:
                                player.legsAnimation("SPRINT")
                            else:
                                player.legsAnimation("WALK")
                            if far_enough:
                                player.stmove = False
                                player.mspeed = speed
                                dist_goal.normalize()
                                player.setPos(player_pos + dist_goal * (frametime * speed), wblocks)
                        else:
                            self.player_legs_wait(player, frametime)
                    else:
                        self.player_legs_wait(player, frametime)
                else:
                    player.legsAnimation()

                """
                    Upper body
                """
                # Blocked state (reloading, dead, hit, etc...)
                if player.forced_animation:
                    player_forced_anim, player_forced_delay = player.forced_animation
                    player.torsoAnimation(player_forced_anim)
                    if now > player_forced_delay:
                        player.forced_animation = None
                    if player.dead and player.dead_time and now > player.dead_time:
                        player.freeze()
                        if player.id == game.player.id and not player.freezed:
                            game.menus.load_death_menu(world['infos']['view']['day'])
                        player.freezed = True
                # Using item state (firing, hitting, etc...)
                elif player.used_item:
                    player.torsoAnimation('USE')
                    # Time since usage
                    item_usage_time = now - player.used_item[0]
                    # Flashing effect
                    flashtime = player.item_stats.get('flashing', {}).get('timing', None)
                    if flashtime:
                        prev_flash = player.flashing
                        player.flashing = False
                        if flashtime[0] <= item_usage_time <= flashtime[1]:
                            player.flashing = True
                        if player.flashing != prev_flash:
                            player.updateFlashing()
                    # On effect activation
                    if not player.used_item[2]:
                        if item_usage_time > player.item_timings[0]:
                            player.effectItem()
                    # When action finished
                    if item_usage_time > player.item_timings[1]:
                        player.used_item = None
                # Aiming state ()
                elif player.aiming:
                    if player.please_unaim:
                        player.aiming = False
                        player.please_unaim = False
                    player.torsoAnimation('AIM')
                # Waiting state (nothing to do, copy legs animation)
                else:
                    player.torsoAnimation()
                # Updates
                player.frameActions(frametime, game)

    # Player legs waiting state
    def player_legs_wait(self, player, frametime):
        player.goal = None
        if player.aiming:
            player.legsAnimation("WAIT-AIM")
        else:
            player.legsAnimation("WAIT")
        player.setEnergy(player.energy + frametime * const.ENERGY_REGEN_RATE)
