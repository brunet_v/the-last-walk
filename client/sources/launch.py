# Check updates
import sources.networking.versionning as versionning
versionning.update()

# Start game
import sources.config.build_config as build_config
import sources.config.configfile as configfile
import sources.utils.resources as resources
import sources.world.init as init

from sources.gui.menus.MainMenu import MainMenu

def prepare_game():
    # Eventually do Benchmarks
    if build_config.build_option("benchs"):
        import sources.utils.CollisionTree as CollisionTree
        CollisionTree.do_bench()
        import sources.utils.BlockCollisions as BlockCollisions
        BlockCollisions.do_bench()
        import sys
        sys.exit()

def preload_context(failed = False):
    # Load configfile
    configfile.ConfigFile.init(failed)
    # Prepare window and basics
    init.init_panda(failed)

def init_game():
    # Context init
    init.init_process()
    # Load medias
    medias = resources.load_resources()
    # Create menu
    MainMenu(medias)
    # Give hand
    run()

def client():
    # First things to do
    prepare_game()
    # Try with fancy config
    try:
        preload_context()
        import direct.directbase.DirectStart
    # Re-try with error config (on error)
    except:
        preload_context(failed=True)
        import direct.directbase.DirectStart
    # When everything's good, start the game
    init_game()

def main():
    # Start the game, save error messages if any.
    try:
        client()
    except:
        import traceback
        with open("error.txt", "w") as f:
            f.write("%s\n" % traceback.format_exc())
