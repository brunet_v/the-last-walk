# -*- coding: utf-8 -*-
import sys
import os
import hashlib
import panda3d.core as engine
import sources.gui.Languages as languages
import sources.config.configfile as configfile
import sources.gui.builder as builder
import sources.objects.utils as utils
import sources.world.init as init
import sources.config.build_config as build_config
import sources.utils.multitask as threading
import time

from panda3d.rocket import *
from sources.handlers.LightHandler import LightHandler
from sources.handlers.SoundsHandler import SoundsHandler
from sources.handlers.ShadersHandler import ShadersHandler
from sources.handlers.TooltipsHandler import TooltipsHandler
from sources.handlers.FadeScreenHandler import FadeScreenHandler
from sources.handlers.SocialBarHandler import SocialBarHandler
from sources.networking.Master import Master
from sources.gui.menus.ListSource import ListSource
from sources.gui.menus.MenuScene import MenuScene
from sources.gui.menus.PlayerMenu import PlayerMenu
from sources.Game import Game
from sources.utils.Threader import Threader
from sources.gui.builder_utils import Text, ElementList, SearchById, SearchByTag

"""
    Main menu (GUI) handler

    Is the main (fake)thread alaways alive, handle game connection/deconnection,
    the menus display, the game loading/unloading

"""
class MainMenu(Threader):

    def __init__(self, medias):
        utils.InstanceCounter.add("MainMenu")
        # Misc
        self.to_exit_lobby = False
        self.to_load_options = False
        self.document = None
        self.game_menu = None
        self.help_menu = None
        self.pop_up = None
        self.options = None
        self.quit = None
        self.mserver = None
        self.msgmenu = None
        self.fadescreen = FadeScreenHandler()
        self.lights = LightHandler()
        self.sounds = SoundsHandler()
        self.medias = medias
        self.a_path = engine.Filename.fromOsSpecific(os.getcwd())
        self.servers = ListSource("servers")
        self.servers.list = []
        self.cusers = ListSource("cusers")
        self.cusers.list = []
        self.scene = MenuScene(self.lights)
        self.shaders = ShadersHandler(self)
        self.player_menu = PlayerMenu(self)
        self.clicked = False
        self.menu_loaded = ""
        LoadFontFace(self.get_menus_path("homizio-medium.ttf"))
        LoadFontFace(self.get_menus_path("simplymono.ttf"))
        # Init Display Region
        self.region = RocketRegion.make('pandaRocket', base.win)
        self.region.setActive(True)
        if build_config.build_option("debug-rocket"):
            self.region.initDebugger()
            self.region.setDebuggerVisible(True)
        # Init input handler
        self.input_handler = RocketInputHandler()
        base.mouseWatcher.attachNewNode(self.input_handler)
        self.region.setInputHandler(self.input_handler)
        # self.region.setCamera(base.cam)
        # Init Context
        self.context = self.region.getContext()
        self.game = None
        self.connection = None
        self.logging = False
        self.logged = False
        self.t1 = time.time()
        self.t2 = time.time()
        self.t3 = time.time()

        # print self.context.name
        # self.cursor_document = self.context.LoadMouseCursor(self.get_menus_path("tooltip.html"))
        # print "=>", self.cursor_document
        # self.context.AddMouseCursor(self.cursor_document)
        # self.context.ShowMouseCursor(True)
        # print self.context.hover_element

        self.prev_events = None
        self.accepted_events = []
        self.toolTips = TooltipsHandler()
        self.display_connect_fail = False
        self.time_loading = 0.
        self.loading_img = 1
        self.loading_img_max = 12
        self.social_bar = SocialBarHandler(self)
        self.load_login(first=True)
        Threader.__init__(self, "MainMenu")

    def __del__(self):
        utils.InstanceCounter.rm("MainMenu")

    def restore_prev_events(self):
        if self.prev_events:
            for event in self.prev_events:
                self.accept(event[0], event[1], event[2])
            self.prev_events = None
    def ignore_events(self):
        for event in self.accepted_events:
            base.ignore(event[0])
        self.accepted_events = []
    def accept(self, event, callback, extraArgs=[]):
        base.accept(event, callback, extraArgs=extraArgs)
        self.accepted_events.append([event, callback, extraArgs])

    """
        Called every frame
    """
    def run(self, delay):
        if base.win.isClosed():
            init.exit_process()
        now = time.time()
        if self.logged:
            # unstack connection buffer
            if self.mserver and now > self.t1:
                self.t1 = now + 1. / 20.
                event = self.mserver.retrieve()
                while event:
                    self.network_event(event['type'], event['data'])
                    event = self.mserver.retrieve()
            if not self.game:
                # check connection availability
                if self.mserver and now > self.t2:
                    self.t2 = now + 5.0
                    if not self.mserver.is_alive():
                        self.load_login()
                        self.load_connect_fail()
                if self.mserver and now > self.t3:
                    self.t3 = now + 5.0
                    self.refresh_servers()
                    self.refresh_users()
            elif self.game and self.game.connection:
                # If in lobby or in game
                if self.game.connection.alive and now > self.t2:
                    self.t2 = now + 6.0
                    self.refresh_lobby_users()
        if self.to_exit_lobby:
            self.exit_lobby()
        if self.to_load_options:
            self.do_load_options()
        self.scene.frame_actions(delay)
        self.lights.frame_actions(delay)
        self.shaders.frame_actions(delay)
        self.sounds.frame_actions(delay)
        self.player_menu.frame_actions(delay)
        self.fadescreen.frame_actions(delay)
        self.update_login_loading(delay)
        self.social_bar.frame_actions(delay)
        if self.mserver:
            self.mserver.is_alive()

    """
        Handle network events
    """
    def network_event(self, type, data):
        if not self.game:
            if type == "mchan":
                builder.build_mchan(self.document, data)
            if type == "jchan":
                builder.build_jchan(self.document, data)
                self.refresh_users()
            if type == "slist":
                self.empty_server_list()
                for server in data["list"]:
                    self.servers.list.append({
                        "name": server["name"],
                        "players": "%s/%s" % (server["players"], server["max_players"]),
                        "addr": server["addr"],
                        "port": server["port"],
                    })
                self.servers.NotifyRowAdd("list", 0, len(self.servers.list))
            if type == "clist":
                l = len(self.cusers.list)
                self.cusers.list = []
                if l:
                    self.cusers.NotifyRowRemove("list", 0, l)
                for user in data["usrs"]:
                    self.cusers.list.append({
                        "name": user,
                    })
                self.cusers.NotifyRowAdd("list", 0, len(self.cusers.list))
        else:
            if type == "smsg":
                builder.build_smsg(self.document, data)
            if type == "rplayers":
                builder.build_rplayers(self.document, data)
        # TODO
        if type == "requestOK":
            self.join_server(data['ip'], data['port'])

    """
        Resolve menu path
    """
    def get_menus_path(self, path):
        return engine.Filename(
            "%s/datas/ui/menus/%s" % (self.a_path, path)
        ).toOsSpecific()

    """
        Load i18n information into template
    """
    def load_i18n(self, document):
        for tag in SearchByTag(document, "i18n"):
            tag = tag.getNode()
            if tag:
                cat = tag.GetAttribute("cat")
                if not cat:
                    cat = ""
                if tag.parent_node:
                    tag.parent_node.inner_rml = languages.i18n(cat, tag.inner_rml)

    """
        Basic document loading procedure
    """
    def preload_document(self, path):
        document = self.context.LoadDocument(
            self.get_menus_path(path)
        )
        for text_input in SearchByTag(document, 'input'):
            text_input.event('keydown', self.sounds.play_inputs_keydown)
            text_input.event('keyup', self.sounds.play_inputs_keyup)
        for button in SearchByTag(document, 'button'):
            button.event('mouseover', self.sounds.play_hover_button)
        # for select in SearchByTag(document, 'select'):
        #     select.event('mouseover', self.sounds.play_hover_option)
        self.load_i18n(document)
        return document

    """
        Unload main rocket node
    """
    def unload_document(self):
        self.close_options()
        self.resume_game_menu()
        self.resume_help_menu()
        if self.document:
            self.document.Close()
            self.document = None
        if self.pop_up:
            self.pop_up.Close()
            self.pop_up = None
        self.logging = False
        self.menu_loaded = ""

    """
        Display a popup on login error
    """
    def load_login_fail(self):
        print "Login failed"
        error = SearchById(self.document, 'error-login')
        error.inner_rml(languages.i18n("login", "login-failed"))
        error.show()

    """
        Display a popup on disconnection
    """
    def load_connect_fail(self, display = True):
        if not display:
            self.display_connect_fail = True
            return None
        else:
            self.display_connect_fail = False
        print "Disconnected"
        error = SearchById(self.document, 'error-login')
        error.inner_rml(languages.i18n("login", "disconnected"))
        error.show()

    def update_login_loading(self, delay):
        if not self.document or self.menu_loaded != "login":
            return None
        prev = self.loading_img - 1
        if prev < 1:
            prev = self.loading_img_max
        doc = self.document
        error = SearchById(doc, 'error-login')
        loginbutton = SearchById(doc, 'loginbutton')
        loading = SearchById(doc, 'loading-' + str(self.loading_img))
        ploading = SearchById(doc, 'loading-' + str(prev))
        if self.logging:
            loginbutton.hide()
            error.hide()
        else:
            loginbutton.show()
            ploading.hide()
            return None
        self.time_loading += delay
        if self.time_loading > 0.05:
            self.time_loading = 0.
            ploading.hide()
            loading.show()
            self.loading_img += 1
            if self.loading_img > self.loading_img_max:
                self.loading_img = 1

    """
        Empty server list content
    """
    def empty_server_list(self):
        l = len(self.servers.list)
        self.servers.list = []
        if l:
            self.servers.NotifyRowRemove("list", 0, l)

    """
        Load lobby screen
    """
    def load_lobby(self, infos):
        if not self.clicked:
            self.clicked = True
            self.fadescreen.fade_in_menu()
            def do_load():
                time.sleep(0.15)
                # Prepare manu
                self.unload_document()
                self.load_menu_scene()
                self.document = self.preload_document('lobby.html')
                self.document.PullToFront()
                self.document.Show()
                doc = self.document
                SearchById(doc, 'serverchatinput').event('keydown', self.help_servchat)
                SearchById(doc, 'leavelobby').event('click', self.ask_leave_lobby)
                if self.mserver:
                    self.mserver.send("unjoin", {})
                self.empty_server_list()
                # Prepare game
                self.game = Game(self.medias, self, infos, self.lights, self.sounds)
                self.fadescreen.fade_out_menu()
                self.clicked = False
            threading.Thread(target=do_load).start()
    def exit_lobby(self):
        if self.game and not self.clicked:
            self.clicked = True
            self.fadescreen.fade_in()
            game = self.game
            self.game = None
            def do_unload():
                time.sleep(0.50)
                self.unload_document()
                game.clear()
                if self.mserver:
                    self.load_menu(force=True, async=False)
                else:
                    self.load_login(first=True, force=True, async=False)
                self.shaders.add_menu_blurr()
                self.fadescreen.fade_out()
                self.clicked = False
            threading.Thread(target=do_unload).start()
        self.to_exit_lobby = False
    def ask_leave_lobby(self):
        self.to_exit_lobby = True

    """
        Load ingame menu
    """
    def load_game_menu(self):
        if not self.game_menu and self.game:
            self.resume_help_menu()
            self.game_menu = self.preload_document('ingame.html')
            self.game_menu.PullToFront()
            self.game_menu.Show()
            builder.build_game_menu(self.game_menu, self.game, self)
            self.prev_events = self.accepted_events
            self.ignore_events()
            self.accept("escape", self.resume_game_menu)
    def resume_game_menu(self):
        self.restore_prev_events()
        if self.game_menu:
            self.game_menu.Close()
            self.game_menu = None
    def leave_game_menu(self):
        self.resume_game_menu()
        self.ask_leave_lobby()
    def options_replace_game_menu(self):
        self.resume_game_menu()
        self.load_options()
    def help_replace_game_menu(self):
        self.resume_game_menu()
        self.load_help_menu()
    def toggle_ally(inst):
        if inst.game:
            inst.game.send("p_toggleally", {
                'id': self.GetAttribute('pid'),
            })

    """
        Load ingame help menu
    """
    def load_help_menu(self):
        if not self.help_menu:
            self.resume_game_menu()
            self.help_menu = self.preload_document('helper.html')
            self.help_menu.PullToFront()
            self.help_menu.Show()
            SearchById(self.help_menu, 'resumebutton').event('click', self.resume_help_menu)
    def resume_help_menu(self):
        if self.help_menu:
            self.help_menu.Close()
            self.help_menu = None

    def clear_tabs(self):
        for i in xrange(1, 5):
            doc = self.options
            SearchById(doc, 'tab-' + str(i) + '-2',).hide()
            SearchById(doc, 'tab-' + str(i) + '-1',).show()
    def active_tab_1(self):
        self.clear_tabs()
        doc = self.options
        SearchById(doc, 'tab-1-1').hide()
        SearchById(doc, 'tab-1-2').show()
    def active_tab_2(self):
        self.clear_tabs()
        doc = self.options
        SearchById(doc, 'tab-2-1').hide()
        SearchById(doc, 'tab-2-2').show()
    def active_tab_3(self):
        self.clear_tabs()
        doc = self.options
        SearchById(doc, 'tab-3-1').hide()
        SearchById(doc, 'tab-3-2').show()
    def active_tab_4(self):
        self.clear_tabs()
        doc = self.options
        SearchById(doc, 'tab-4-1').hide()
        SearchById(doc, 'tab-4-2').show()

    def change_language(self, inst):
        for name in inst:
            print name

    """
        Option panel
    """
    def do_load_options(self):
        if time.time() > self.to_load_options:
            self.to_load_options = False
            if self.options:
                self.close_options()
            self.options = self.preload_document('options.html')
            self.options.PullToFront()
            self.options.Show()
            builder.build_options(self.options, self)
            self.prev_events = self.accepted_events
            self.ignore_events()
            self.accept("escape", self.close_options)
            doc = self.options
            self.active_tab_1()
            SearchById(doc, 'slide-music').event('drag', self.sounds.change_music_volume)
            SearchById(doc, 'slide-sfx').event('drag', self.sounds.change_sfx_volume)
            SearchById(doc, 'tab-1-1').event('click', self.active_tab_1)
            SearchById(doc, 'tab-2-1').event('click', self.active_tab_2)
            SearchById(doc, 'tab-3-1').event('click', self.active_tab_3)
            SearchById(doc, 'tab-4-1').event('click', self.active_tab_4)
            self.sounds.play_music("menu-options", 0.8)
            self.fadescreen.fade_out_option()
            self.clicked = False
    def load_options(self):
        if not self.clicked:
            self.clicked = True
            self.to_load_options = time.time() + 0.15
            self.fadescreen.fade_in_option()
    def apply_options(self):
        configfile.ConfigFile.update(self.options)
        configfile.ConfigFile.save()
        self.close_options()
    def close_options(self):
        self.restore_prev_events()
        if self.options:
            self.options.Close()
            self.options = None
            self.sounds.play_previous_music()

    """
        Load loging screen
    """
    def load_login(self, first=False, force=False, async=True):
        if not self.clicked or force:
            self.clicked = True
            self.mserver = None
            if not first:
                self.fadescreen.fade_in_menu()
            def do_load():
                # Inits
                if async:
                    time.sleep(0.15)
                self.logging = False
                self.logged = False
                self.unload_document()
                self.load_menu_scene()
                # Menu build
                self.document = self.preload_document('login.html')
                self.document.PullToFront()
                self.document.Show()
                doc = self.document
                SearchById(doc, 'loginbutton').event('click', self.try_login)
                SearchById(doc, 'optionsbutton').event('click', self.load_options)
                SearchById(doc, 'logininput').event('keydown', self.help_login)
                SearchById(doc, 'passinput').event('keydown', self.help_login)
                # WTF?
                e = SearchById(doc, 'logininput')
                e.event('mousemove', self.toolTips.showLogininput)
                e.event('mouseout', self.toolTips.hideLogininput)
                self.sounds.play_music("menu", 0.8)
                if self.display_connect_fail == True:
                    self.load_connect_fail()
                self.ignore_events()
                self.accept("escape", self.load_quit_menu)

                self.social_bar.init(doc)

                # Fade out
                if not first:
                    self.fadescreen.fade_out_menu()
                elif not force:
                    self.fadescreen.fade_out_first()
                self.menu_loaded = "login"
                self.clicked = False
            if async:
                threading.Thread(target=do_load).start()
            else:
                do_load()
            # If asked, force use of local server
            if not force and async and build_config.build_option("debug-local"):
                import random as rand
                self.login = {
                    "login": "TestLogin%d" % rand.randint(0, 100)
                }
                time.sleep(1.)
                self.join_server("127.0.0.1", 22458)

    """
        Load server screen
    """
    def load_menu(self, force=False, async=True):
        if not self.clicked or force:
            self.clicked = True
            if not force:
                self.fadescreen.fade_in_menu()
            def do_load():
                if async:
                    time.sleep(0.15)
                self.unload_document()
                self.load_menu_scene()
                self.document = self.preload_document('mainmenu.html')
                self.document.PullToFront()
                self.document.Show()
                doc = self.document
                SearchById(doc, 'chatinput').event('keydown', self.help_msg)
                SearchById(doc, 'serverrefreshbutton').event('click', self.refresh_servers)
                SearchById(doc, 'serveraskbutton').event('click', self.ask_server)
                SearchById(doc, 'optionsbutton').event('click', self.load_options)
                SearchById(doc, 'serverlist').event('rowadd', self.server_add)
                self.ignore_events()
                self.accept("escape", self.load_quit_menu)
                self.accept("c", self.player_menu.show)
                self.logged = True
                if self.mserver:
                    self.mserver.send("/join", {"arg": "General chat"})
                self.refresh_users()
                self.refresh_servers()
                if not force:
                    self.fadescreen.fade_out_menu()
                self.menu_loaded = "menu"
                self.clicked = False
            if async:
                threading.Thread(target=do_load).start()
            else:
                do_load()

    """
        On death screen
    """
    def load_death_menu(self, day):
        self.unload_document()
        self.document = self.preload_document('death.html')
        self.document.PullToFront()
        self.document.Show()
        doc = self.document
        SearchById(doc, 'nbdays').append(Text(doc, '%d' % day))
        SearchById(doc, 'exitbutton').event('click', self.ask_leave_lobby)

    """
        Ingame message sending interface
    """
    def load_message_menu(self):
        if not self.msgmenu:
            self.msgmenu = self.preload_document('ingame_message.html')
            self.msgmenu.PullToFront()
            self.msgmenu.Show()
    def close_message_menu(self):
        if self.msgmenu:
            if self.game:
                value = self.msgmenu.GetElementById('ingame_message_input').value
                if value:
                    self.game.send("p_msg", {
                        'msg': value[:512],
                    })
            self.msgmenu.Close()
            self.msgmenu = None

    """
        Some simple events handlers
    """
    def help_login(self):
        if event.parameters['key_identifier'] == 72:
            self.try_login()
    def help_msg(self):
        if event.parameters['key_identifier'] == 72:
            self.send_msg()
    def help_servchat(inst):
        if event.parameters['key_identifier'] == 72:
            if inst.game and self.value:
                inst.game.send('smsg', {
                    'msg': self.value[:500],
                })
                self.value = ""
    def server_add(inst):
        start = event.parameters['first_row_added']
        end = start + event.parameters['num_rows_added']
        rows = list(self.rows)[start:end]
        for row in ElementList(rows):
            row.event('dblclick', inst.server_click)
    def server_click(inst):
        if len(inst.servers.list) > self.table_relative_index:
            server = inst.servers.list[self.table_relative_index]
            inst.join_server(server['addr'], server['port'])

    """
        Load menu background 3d scene
    """
    def load_menu_scene(self):
        self.scene.load()

    """
        Unload menu background 3d scene
    """
    def unload_menu_scene(self):
        self.scene.unload()

    """
        Join game lobby
    """
    def join_server(self, addr, port):
        if not self.game:
            print "Joining:", addr, port
            self.load_lobby({
                "ip": addr,
                "port": port,
                "login": self.login,
            })

    """
        Request reload server list
    """
    def refresh_servers(self):
        if self.mserver:
            self.mserver.send("slist", {
                "type": "trusted",
            })

    # TODO
    def ask_server(self):
        if self.mserver:
            self.mserver.send("serverRequest", {
                "size": 1,
            })

    """
        Refresh list of user in the channel
    """
    def refresh_users(self):
        if self.mserver:
            self.mserver.send("clist", {})

    """
        Refresh list of user in the lobby
    """
    def refresh_lobby_users(self):
        if self.game and not self.game.player:
            self.game.send("rplayers", {})


    """
        Send chat message event to master
    """
    def send_msg(self):
        chatinput = self.document.GetElementById('chatinput')
        msg = chatinput.value[:500]
        if msg and msg[0] == '/':
            data = msg.split(" ", 1)
            if len(data) >= 2:
                self.mserver.send(data[0], {
                    'arg': data[1],
                })
        elif msg:
            self.mserver.send('msg', {
                'msg': msg,
            })
        chatinput.value = ""

    """
        Executed asynchronously, wait for login confirmation
    """
    def wait_login(self):
        self.logging = True
        logininput = self.document.GetElementById('logininput')
        passinput = self.document.GetElementById('passinput')
        mserver = Master('player', {
            'login': logininput.value,
            'pass': hashlib.sha1(passinput.value).hexdigest(),
        })
        timeout = time.time() + 3.0
        while time.time() < timeout:
            if mserver.connected == True:
                received = mserver.retrieve()
                if received:
                    if received['type'] == "login_success":
                        self.logging = False
                        self.mserver = mserver
                        self.login = received["data"]
                        self.load_menu()
                        return None
                    else:
                        break
            time.sleep(0.01)
        self.load_login_fail()
        self.logging = False

    """
        Login button click event handler
    """
    def try_login(self):
        if not self.logging and not self.mserver:
            threading.Thread(target=self.wait_login).start()

    """
        Quit screen
    """
    def load_quit_menu(self):
        if self.quit:
            self.close_quit()
        self.quit = self.preload_document('quit.html')
        self.quit.PullToFront()
        self.quit.Show()
        if self.quit:
            doc = self.quit
            SearchById(doc, 'applyquitbutton').event('click', self.apply_quit)
            SearchById(doc, 'closequitbutton').event('click', self.close_quit)
        self.prev_events = self.accepted_events
        self.ignore_events()
        self.accept("escape", self.close_quit)
        self.accept("enter", self.apply_quit)
    def apply_quit(self):
        self.close_quit()
        sys.exit(0)
    def close_quit(self):
        self.restore_prev_events()
        if self.quit:
            self.quit.Close()
            self.quit = None
            self.sounds.play_previous_music()
