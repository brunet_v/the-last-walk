import math
import random as rand
import sources.objects.utils as utils
import sources.utils.resources as resources
import panda3d.core as engine

from sources.utils.MousePicker import MousePicker
from sources.handlers.GrassHandler import GrassHandler

"""
    Create / Destroy main menu background scene
"""
class MenuScene(object):

    def __init__(self, lights):
        utils.InstanceCounter.add("MenuScene")
        self.lights = lights
        self.loaded = False
        self.models = []
        self.picker = MousePicker()
        self.scenes = [
            utils.Struct({
                'name': 'mainmenu.mpx',
                'models': [
                    # (file, shadowing, pos, hpr)
                    ("datas/maps/compiled/mainmenu.mpx-1.bam", True, (-100, -100, -100), (0, 0, 0)),
                    ("datas/maps/compiled/mainmenu.mpx-1-ns.bam", False, (-100, -100, -100), (0, 0, 0)),
                ],
                'camera': utils.Struct({
                    'lookat': (-84, -84, -100),
                    'pos': (9, 9, 19), # relative to look-at
                }),
                'sun': utils.Struct({
                    'lookat': (-85, -80, -100),
                    'pos': (-200, -200, 0),
                }),
            }),
        ]
        self.grass = GrassHandler()
        self.load()

    def __del__(self):
        utils.InstanceCounter.rm("MenuScene")
        self.clear()

    def clear(self):
        self.unload_scene()

    def load_scene(self):
        sun = self.lights.lights['sunlightnode']
        ambient = self.lights.lights['ambientlightnode']
        loaded = rand.choice(self.scenes)
        self.models = []
        for mpath, mshad, mpos, mhpr in loaded.models:
            m = resources.load_model_file(mpath)
            m.setPos(*mpos)
            m.setHpr(*mhpr)
            m.setLight(sun)
            m.setLight(ambient)
            if not mshad:
                m.hide(engine.BitMask32.bit(1)) # hide from shadow caster
            m.reparentTo(render)
            self.models.append(m)
        light = loaded.sun
        sun.setPos(*light.pos)
        sun.lookAt(*light.lookat)
        self.lights.activate()
        self.grass.add_grass(m, 0, 0, loaded.name, 1, 0)
        self.camera = loaded.camera
        self.rotation = 0.

    def unload_scene(self):
        for node in self.models:
            node.setLightOff()
            node.detachNode()
            node.removeNode()
        self.models = []
        self.grass.clear()

    def load(self):
        if not self.loaded:
            self.load_scene()
            self.loaded = True

    def unload(self):
        if self.loaded:
            self.unload_scene()
            self.loaded = False

    def frame_actions(self, frametime):
        if self.loaded:
            self.rotation += frametime * 3.
            pos = self.camera.pos
            lookat = self.camera.lookat
            x, y = pos[0], pos[1]
            angle = math.radians(self.rotation)
            cos, sin = math.cos, math.sin
            nx = x * cos(angle) - y * sin(angle)
            ny = x * sin(angle) + y * cos(angle)
            nz = pos[2] + lookat[2]
            camera.setPos(lookat[0] + nx, lookat[1] + ny, nz)
            camera.lookAt(*lookat)
            self.grass.frame_actions(frametime)
