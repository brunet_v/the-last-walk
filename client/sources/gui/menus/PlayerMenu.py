import panda3d.core as engine
import sources.config.constants as const
import sources.objects.utils as utils
import sources.utils.resources as resources
import sources.config.configfile as configfile
import sources.config.object_config as object_config
import time

from direct.gui.OnscreenText import OnscreenText
from direct.gui.OnscreenImage import OnscreenImage
from sources.gui.ingame.UiButton import UiButton
from direct.actor.Actor import Actor

"""
    Select player
"""
class PlayerMenu(object):

    def __init__(self, main_menu):
        utils.InstanceCounter.add("PlayerMenu")
        self.display = False
        self.main_menu = main_menu
        self.mx = 0
        self.my = 0
        self.player_infos = [
            {
                "obj": "male-player1",
                "name": "Walter",
                "desc": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
            }, {
                "obj": "male-player2",
                "name": "Butch",
                "desc": "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."
            }, {
                "obj": "male-player3",
                "name": "Scott",
                "desc": "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."
            },
        ]
        self.prev = None
        self.selected_player = 0
        self.anim = 'wait'
        self.win_width = 1024
        self.win_height = 768
        self.players = object_config.OBJECT_LIST
        self.player = None
        self.border = None
        self.bg_player = None
        self.rotationed = -45
        self.rotation = -45
        self.get_winsize()
        self.buttons = None

    def __del__(self):
        utils.InstanceCounter.rm("PlayerMenu")
        self.clear()

    def clear(self):
        if self.buttons:
            for key, button in self.buttons.iteritems():
                button.clear()

    def next_player(self):
        self.prev = self.player
        self.selected_player += 1
        if self.selected_player > len(self.player_infos) - 1:
            self.selected_player = 0
        self.display_player()
    def prev_player(self):
        self.prev = self.player
        self.selected_player -= 1
        if self.selected_player < 0:
            self.selected_player = len(self.player_infos) - 1
        self.display_player()

    def move_image(self, image, x, y, xsize, ysize):
        image.setPos(x + xsize / 2, 0, -y - ysize / 2)
    def move_text(self, text, x, y, scale=None):
        text.setPos(x, -y)
    def load_text(self, value, x, y, scale):
        text = OnscreenText(mayChange=True, fg=(1, 1, 1, 1))
        text.setScale(scale)
        self.move_text(text, x, y)
        text.setText(value)
        text.reparentTo(pixel2d)
        text.hide()
        return text
    def load_image(self, image_file, x, y, xsize, ysize, xscale = 1, yscale = 1):
        img = OnscreenImage(image = image_file)
        img.setScale((xsize * xscale) / 2.0, 0, (ysize * yscale) / 2.0)
        self.move_image(img, x, y, xsize, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.hide()
        return img

    def rotate_right(self):
        self.rotationed += 15
    def rotate_left(self):
        self.rotationed -= 15

    def load_events(self):
        self.main_menu.ignore_events()
        self.main_menu.accept("enter", self.hide)
        self.main_menu.accept("escape", self.hide)
        self.main_menu.accept("arrow_right", self.next_player)
        self.main_menu.accept("arrow_left", self.prev_player)
        self.main_menu.accept("wheel_up", self.rotate_right)
        self.main_menu.accept("wheel_down", self.rotate_left)
        self.main_menu.accept("mouse1", self.m1_click)

    def display_buttons(self, display):
        if display and not self.buttons:
            self.buttons = {}
            x = self.ox + self.opx - 20
            y = self.oy + self.opy + 170
            self.buttons["left"] = UiButton(self, "", 0, self.prev_player, "menus/img/player-larrow", 30, 60, x, y)
            x = self.ox + self.opx + 370
            y = self.oy + self.opy + 170
            self.buttons["right"] = UiButton(self, "", 0, self.next_player, "menus/img/player-rarrow", 30, 60, x, y)
            x = self.winxsize / 2 - 65
            y = self.winysize / 2 + self.win_height / 2 - 50
            self.buttons["apply"] = UiButton(self, "Apply", 0, self.hide, "button", 130, 30, x, y)
        if self.buttons:
            for key, button in self.buttons.iteritems():
                if display:
                    button.img.show()
                    button.img_hover.hide()
                    button.txt.show()
                else:
                    button.img.hide()
                    button.img_hover.hide()
                    button.txt.hide()

    def update_content(self):
        name = self.main_menu.document.GetElementById('name')
        name.inner_rml = self.player_infos[self.selected_player]["name"]
        description = self.main_menu.document.GetElementById('description')
        description.inner_rml = self.player_infos[self.selected_player]["desc"]

    def display_player(self):
        if self.prev:
            self.hide_player(self.prev)
            self.prev = None

        self.mybuffer = base.win.makeTextureBuffer("player", 1024, 1024)
        self.mybuffer.setClearColor(engine.Vec4(0.03, 0.03, 0.03, 0))
        mytexture = self.mybuffer.getTexture()
        self.mybuffer.setSort(-100)
        mycamera = base.makeCamera(self.mybuffer)

        # Loading player
        self.player = Actor("datas/objects/actors/male-player1/male-player1.egg", {
            self.anim: "datas/objects/actors/male-player1/animations/" + self.anim + ".egg"
        })
        tex = loader.loadTexture("datas/objects/actors/male-player1/textures/" + self.players[self.player_infos[self.selected_player]["obj"]]["txt"])
        self.player.setTexture(tex)
        self.player.setScale(1.2)
        self.player.setPos(0, 0, -4)
        self.player.setHpr(0, 0, 0)

        self.myscene = engine.NodePath("my_scene")
        self.myscene.setTransparency(True)
        self.player.reparentTo(self.myscene)
        mycamera.reparentTo(self.myscene)
        mycamera.setPos(20, 20, 10)
        mycamera.lookAt(0, 0, 2)

        self.plane = loader.loadModel("datas/ui/menus/egg/plane.egg")
        self.plane.setTexture(mytexture, 1)
        self.plane.reparentTo(pixel2d)
        self.plane.setScale(300)
        self.plane.setHpr(0, 90, 0)
        self.plane.setPos(self.ox + self.opx + 150 + 40, 0, -self.oy - self.opy - 150 - 50)

        self.player.play(self.anim)
        self.player.loop(self.anim)

        # Loading base
        self.base = loader.loadModel("datas/ui/menus/egg/base.egg")
        tex = loader.loadTexture("datas/ui/menus/egg/base.png")
        self.base.setTexture(tex, 1)
        self.base.reparentTo(self.player)
        self.base.setPos(0, 0, -4)
        self.base.setScale(3.0)

        self.display = True
        self.display_buttons(True)
        self.update_content()

    def get_winsize(self):
        self.winxsize = base.win.getXSize()
        self.winysize = base.win.getYSize()
        self.dxwin = int(self.winxsize / 2)
        self.dywin = int(self.winysize / 2)

    def show(self):
        self.main_menu.unload_document()
        self.main_menu.load_menu_scene()
        self.main_menu.document = self.main_menu.preload_document('playermenu.html')
        self.main_menu.document.PullToFront()
        self.main_menu.document.Show()

        self.get_winsize()
        self.ox = self.winxsize / 2 - self.win_width / 2
        self.oy = self.winysize / 2 - self.win_height / 2
        self.opx = 70
        self.opy = 50
        self.load_events()
        self.display = True
        if not self.player:
            self.display_player()
        else:
            self.player.play(self.anim)
            self.player.loop(self.anim)
            self.update_content()
        if not self.border:
            self.border = self.load_image(
                'datas/ui/menus/img/border.png',
                self.ox + self.opx + 32, self.oy + self.opy + 42,
                316, 316
            )
        self.border.show()

    def hide_player(self, player):
        self.display = False
        player.stop(self.anim)
        player.detachNode()
        player.cleanup()
        player.removeNode()
        self.base.detachNode()
        self.base.removeNode()
        self.plane.detachNode()
        self.plane.removeNode()
        self.myscene.detachNode()
        self.myscene.removeNode()
        base.graphicsEngine.removeWindow(self.mybuffer)

    def hide(self):
        self.hide_player(self.player)
        self.border.hide()
        self.display_buttons(False)
        self.main_menu.toolTips.hide("character")
        self.main_menu.load_menu()

    def rotate_player(self, value):
        self.player.setHpr(value, 0, 0)

    def m1_click(self):
        if not self.mouse_on_ui():
            return None
        if self.buttons:
            for key, button in self.buttons.iteritems():
                button.click()
    def mouse_on_ui(self):
        if self.buttons:
            for key, button in self.buttons.iteritems():
                if button.hovering():
                    return True
        return False

    def mouse_on_player_screen(self):
        x = self.ox + self.opx + 40
        y = self.oy + self.opy + 50
        if self.mx > x and self.mx < x + 300 and self.my > y and self.my < y + 300:
            return True
        return False

    def frame_actions(self, frametime):
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            self.mx = int(self.dxwin * mpos.getX() + self.dxwin)
            self.my = int(self.dywin * -mpos.getY() + self.dywin)
        if self.display == False:
            return None
        if self.buttons:
            for key, button in self.buttons.iteritems():
                button.hover()
        # Smooth player rotation
        move_ratio = min(1.0, frametime * 2.5)
        move_rest = 1.0 - move_ratio
        if abs(self.rotation - self.rotationed) > 0.1:
            self.rotation = self.rotation * move_rest + self.rotationed * move_ratio
        self.rotate_player(-self.rotation)
        if self.mouse_on_player_screen():
            self.main_menu.toolTips.show("character")
        else:
            self.main_menu.toolTips.hide("character")
