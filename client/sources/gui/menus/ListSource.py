import sources.objects.utils as utils
from panda3d.rocket import DataSource

"""
    Handle data list display
"""
class ListSource(DataSource):

    def __init__(self, name):
        DataSource.__init__(self, name)
        utils.InstanceCounter.add("ListSource")

    def __del__(self):
        utils.InstanceCounter.rm("ListSource")
        self.clear()
        DataSource.__del__(self)

    def clear(self):
        pass

    def GetNumRows(self, name):
        if name == "list":
            return len(self.list)

    def GetRow(self, name, idx, columns):
        if name == "list":
            row = self.list[idx]
            res = []
            for column in columns:
                if column in row:
                    res.append(str(row[column]))
            return res
