
"""
    Base element
"""
class Element:

    def __init__(self, node):
        self.node = node

    def id(self, val):
        return self.attr('id', val)

    def value(self, val=None):
        return self.attr('value', val)

    def select(self):
        return self.attr('selected', 'selected')

    def inner_rml(self, value=None):
        if self.node:
            if value is None:
                return self.node.inner_rml
            else:
                self.node.inner_rml = value
        return self

    def hide(self):
        if self.node:
            self.node.style.display = "none"
        return self

    def show(self, type="block"):
        if self.node:
            self.node.style.display = type
        return self

    def append(self, child):
        if self.node:
            try:
                self.node.AppendChild(child.getNode())
            except:
                self.node.AppendChild(child)
        return self

    def event(self, event, action):
        if self.node:
            self.node.AddEventListener(str(event), action)
        return self

    def attr(self, attr, val=None):
        if self.node:
            if val is None:
                try:
                    for key, value in attr.iteritems():
                        self.node.SetAttribute(str(key), str(value))
                except:
                    return self.node.GetAttribute(str(key))
            else:
                self.node.SetAttribute(str(attr), str(val))
        return self

    def clearChilds(self):
        if self.node:
            while self.node.HasChildNodes():
                self.node.RemoveChild(self.node.first_child)
        return self

    def getNode(self):
        return self.node


"""
    List of elements
"""
class ElementList:

    def __init__(self, raw_nodes):
        self.nodes = []
        for raw_node in raw_nodes:
            self.nodes.append(Element(raw_node))

    def __iter__(self):
        for node in self.nodes:
            yield node


"""
    Simple text element
"""
class Text:

    def __init__(self, doc, value):
        self.node = None
        if doc:
            self.node = doc.CreateTextNode(str(value))

    def setText(self, text):
        if self.node:
            self.node.text = str(text)
        return self

    def getNode(self):
        return self.node


"""
    Normal element
"""
class Block(Element):

    """
        Init by creating the element from scratch
    """
    def __init__(self, doc, tag, type='', childs=None, attrs={}):
        # Try create
        self.node = None
        if doc:
            self.node = doc.CreateElement(tag)
            # Type
            if type:
                self.attr('class', str(type))
            # Attrs
            self.attr(attrs)
            # Childs
            if childs:
                try:
                    for child in childs:
                        self.append(child)
                except:
                    self.append(childs)


"""
    Researched by id element
"""
class SearchById(Element):

    """
        Init by getting element from doc
    """
    def __init__(self, doc, id):
        self.node = None
        if doc:
            self.node = doc.GetElementById(str(id))


"""
    Reasearched by tag
"""
class SearchByTag(ElementList):

    """
        Init by listing element with tag
    """
    def __init__(self, doc, tag):
        raw_nodes = []
        if doc:
            raw_nodes = doc.GetElementsByTagName(str(tag))
        ElementList.__init__(self, raw_nodes)
