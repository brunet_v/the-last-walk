# -*- coding: utf-8 -*-
import sources.config.configfile as configfile

from sources.gui.builder_utils import Block, Text, SearchById

"""
    Called when message coming from channel
"""
def build_mchan(doc, data):
    if doc:
        # Create message structure
        nblock = Block(doc, 'div', 'event', [
            # User "block"
            Block(doc, 'span', 'user', [
                Text(doc, data['login'] + ": "),
            ]),
            # Msg "block"
            Block(doc, 'span', 'msg', [
                Text(doc, data['msg']),
            ]),
        ])

        # Will result in:
        # <div class='event'>
        #    <span class='user'>$Login</span>
        #    <span class='msg'>$Msg</span>
        # </div>

        # Add to doc
        SearchById(doc, 'chatoutput').append(nblock)

"""
    Called when used joined a channel
"""
def build_jchan(doc, data):
    if doc:
        SearchById(doc, 'chatoutput').append(
            Block(doc, 'div', 'special_event', [
                Text(doc, "Joining : %s" % data["chan"])
            ])
        )


"""
    Called when message coming from lobby
"""
def build_smsg(doc, data):
    if doc:
        # Create message structure
        nblock = Block(doc, 'div', 'event', [
            # User "block"
            Block(doc, 'span', 'user', [
                Text(doc, data['login'] + ": "),
            ]),
            # Msg "block"
            Block(doc, 'span', 'msg', [
                Text(doc, data['msg']),
            ]),
        ])
        # Add to doc
        SearchById(doc, 'serverchatoutput').append(nblock)

"""
    Called when refreshing player in lobby
"""
def build_rplayers(doc, data):
    if doc:
        # Clear
        zone = SearchById(doc, "loggedzone")
        zone.clearChilds()
        # Regenerate
        for player in data:
            # Create zone structure
            nblock = Block(doc, 'div', 'window player', [
                Block(doc, 'div', 'name', [
                    Text(doc, player['login']),
                ]),
            ])
            # Add to doc
            zone.append(nblock)

"""
    Called when options opennd
"""
def build_options(doc, inst):
    if doc:
        # End buttons events
        SearchById(doc, 'applyoptionsbutton').event('click', inst.apply_options)
        SearchById(doc, 'closeoptionsbutton').event('click', inst.close_options)
        # Resolution selector
        resolutionselect = SearchById(doc, 'resolutionselect')
        for name, value in configfile.resolutions.iteritems():
            # Add to doc new struct
            resolutionselect.append(
                Block(doc, 'option', childs=[
                    Text(doc, name)
                ]).id("resopt" + name).value(name)
            )
        # Selected resolution
        wxsize, wysize = configfile.get("wxsize"), configfile.get("wysize")
        SearchById(doc, "resopt%dx%d" % (wxsize, wysize)).select()
        # Set selected options to saved ones
        SearchById(doc, "lang-" + configfile.get("language")).select()
        SearchById(doc, "s%d" % configfile.get("squality")).select()
        SearchById(doc, "es%d" % configfile.get("extrashadow")).select()
        SearchById(doc, "vs%d" % configfile.get("vsync")).select()
        SearchById(doc, "dm%d" % configfile.get("fullscreen")).select()
        SearchById(doc, "aa%d" % configfile.get("anti-alias")).select()
        SearchById(doc, "hb%d" % configfile.get("helpbutton")).select()
        SearchById(doc, "dn%d" % configfile.get("decalnumber")).select()
        # Set slider to their values
        SearchById(doc, "slide-music").value(configfile.get("volume_music"))
        SearchById(doc, "slide-sfx").value(configfile.get("volume_sfx"))

"""
    Called when ingame menu opened
"""
def build_game_menu(doc, game, menu):
    if doc:
        mplayer = game.player
        playerlist = SearchById(doc, 'playerlist')
        for id, player in game.world['players'].iteritems():
            if player.id != mplayer.id:
                # Button text
                button_text = 'Click to ally'
                if player.id in mplayer.allieds:
                    button_text = 'Click to unally'
                # Create structure
                nplayer = Block(doc, 'div', 'player', [
                    Block(doc, 'div', 'pname', [
                        Text(doc, player.username),
                    ]),
                    Block(doc, 'button', 'pally', attrs={'pid': player.id}, childs=[
                        Text(doc, button_text)
                    ]).event('click', menu.toggle_ally),
                ])
                # Add to doc
                playerlist.append(nplayer)
        # Button events
        SearchById(doc, 'resumebutton').event('click', menu.resume_game_menu)
        SearchById(doc, 'surrenderbutton').event('click', menu.leave_game_menu)
        SearchById(doc, 'helpbutton').event('click', menu.help_replace_game_menu)
        SearchById(doc, 'optionsbutton').event('click', menu.options_replace_game_menu)

