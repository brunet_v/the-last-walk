# -*- coding: utf-8 -*-
"""
	Supported languages : EN, FR
"""
language_used = "EN"

def generate_languages():
	languages = {}
	language_used = ["EN", "FR",]
	languages_categories = ["", "login", "options", "tooltips", ]
	for lang in language_used:
	    languages[lang] = {}
	    for category in languages_categories:
	        languages[lang][category] = {}
	return languages

def set_language_used(language):
    global language_used
    language_used = language

def i18n(category, name):
    return languages[language_used][category][name]

languages = generate_languages()

# Login
languages["EN"]["login"]["name"] = "Account Name"
languages["FR"]["login"]["name"] = "Nom du Compte"
languages["EN"]["login"]["pass"] = "Password"
languages["FR"]["login"]["pass"] = "Mot de Passe"
languages["EN"]["login"]["login"] = "Connect"
languages["FR"]["login"]["login"] = "Connecter"
# Login errors
languages["EN"]["login"]["login-failed"] = "Login failed.<br/>Please try again."
languages["FR"]["login"]["login-failed"] = "La connexion a échoué.<br/>Veuillez essayer à nouveau."
languages["EN"]["login"]["disconnected"] = "You are disconnected.<br/>Please try to connect again."
languages["FR"]["login"]["disconnected"] = "Vous êtes déconnecté. Veuillez essayer de vous connecter à nouveau."
# Options tab 1
languages["EN"]["options"]["general-title"] = "General"
languages["FR"]["options"]["general-title"] = "Général"
languages["EN"]["options"]["lang-label"] = "Languages: *"
languages["FR"]["options"]["lang-label"] = "Langages : *"
languages["EN"]["options"]["hb-label"] = "Help menus: *"
languages["FR"]["options"]["hb-label"] = "Menus d'aide : *"
# Options tab 2
languages["EN"]["options"]["graphics-title"] = "Graphics"
languages["FR"]["options"]["graphics-title"] = "Graphiques"
languages["EN"]["options"]["display-label"] = "Display mode:"
languages["FR"]["options"]["display-label"] = "Mode d'affichage :"
languages["EN"]["options"]["fullscreen"] = "Fullscreen"
languages["FR"]["options"]["fullscreen"] = "Plein écran"
languages["EN"]["options"]["windowed"] = "Windowed"
languages["FR"]["options"]["windowed"] = "Fenêtré"
languages["EN"]["options"]["resolution-label"] = "Screen resolution:"
languages["FR"]["options"]["resolution-label"] = "Résolution de l'écran :"
languages["EN"]["options"]["shadow-label"] = "Shadows quality: *"
languages["FR"]["options"]["shadow-label"] = "Qualité des ombres : *"
languages["EN"]["options"]["extra-shadow-label"] = "Extra shadows : *"
languages["FR"]["options"]["extra-shadow-label"] = "Ombres supplémentaires : *"
languages["EN"]["options"]["s-very-high"] = "Very High"
languages["FR"]["options"]["s-very-high"] = "Très élevée"
languages["EN"]["options"]["s-high"] = "High"
languages["FR"]["options"]["s-high"] = "Elevée"
languages["EN"]["options"]["s-normal"] = "Normal"
languages["FR"]["options"]["s-normal"] = "Normale"
languages["EN"]["options"]["s-low"] = "Low"
languages["FR"]["options"]["s-low"] = "Faible"
languages["EN"]["options"]["s-none"] = "None"
languages["FR"]["options"]["s-none"] = "Aucune"
languages["EN"]["options"]["dn-label"] = "Details number: *"
languages["FR"]["options"]["dn-label"] = "Quantité de détails : *"
languages["EN"]["options"]["dn-very-high"] = "Very High"
languages["FR"]["options"]["dn-very-high"] = "Très élevé"
languages["EN"]["options"]["dn-high"] = "High"
languages["FR"]["options"]["dn-high"] = "Elevé"
languages["EN"]["options"]["dn-normal"] = "Normal"
languages["FR"]["options"]["dn-normal"] = "Normal"
languages["EN"]["options"]["dn-low"] = "Low"
languages["FR"]["options"]["dn-low"] = "Faible"
languages["EN"]["options"]["dn-none"] = "None"
languages["FR"]["options"]["dn-none"] = "Aucun"
languages["EN"]["options"]["aa-label"] = "Anti-aliasing: *"
languages["FR"]["options"]["aa-label"] = "Anti-aliasing : *"
languages["EN"]["options"]["vsync-label"] = "Vertical synchronization: *"
languages["FR"]["options"]["vsync-label"] = "Synchronisation verticale : *"
languages["EN"]["options"]["vsync-d60"] = "Deactivated (60 fps)"
languages["FR"]["options"]["vsync-d60"] = "Désactivée (60 fps)"
languages["EN"]["options"]["vsync-d90"] = "Deactivated (90 fps)"
languages["FR"]["options"]["vsync-d90"] = "Désactivée (90 fps)"
languages["EN"]["options"]["vsync-d120"] = "Deactivated (120 fps)"
languages["FR"]["options"]["vsync-d120"] = "Désactivée (120 fps)"
languages["EN"]["options"]["vsync-dnolimit"] = "Deactivated (No limit)"
languages["FR"]["options"]["vsync-dnolimit"] = "Désactivée (Aucune limite)"
# Options tab 3
languages["EN"]["options"]["sound-title"] = "Sound"
languages["FR"]["options"]["sound-title"] = "Son"
languages["EN"]["options"]["music-label"] = "Music volume:"
languages["FR"]["options"]["music-label"] = "Volume de la musique :"
languages["EN"]["options"]["sfx-label"] = "Sound effects volume:"
languages["FR"]["options"]["sfx-label"] = "Volume des effets sonores :"
# Options tab 4
languages["EN"]["options"]["controls-title"] = "Controls"
languages["FR"]["options"]["controls-title"] = "Contrôles"
# Options Bbox
languages["EN"]["options"]["apply"] = "Apply"
languages["FR"]["options"]["apply"] = "Appliquer"
languages["EN"]["options"]["close"] = "Close"
languages["FR"]["options"]["close"] = "Fermer"
# Otpions others
languages["EN"]["options"]["indication"] = "(*) Require a game restart to take effect."
languages["FR"]["options"]["indication"] = "(*) Nécessite un redémarrage du jeu pour prendre effet."
languages["EN"]["options"]["activated"] = "Activated"
languages["FR"]["options"]["activated"] = "Activé"
languages["EN"]["options"]["deactivated"] = "Deactivated"
languages["FR"]["options"]["deactivated"] = "Désactivé"

# Tooltips
languages["EN"]["tooltips"]["login"] = "This can be your login or e-mail"
languages["FR"]["tooltips"]["login"] = "Cela peut être votre login ou votre e-mail"
languages["EN"]["tooltips"]["life"] = "This is your life level"
languages["FR"]["tooltips"]["life"] = "Ceci est votre niveau de vie"
languages["EN"]["tooltips"]["tiredness"] = "This is your tiredness level"
languages["FR"]["tooltips"]["tiredness"] = "Ceci est votre niveau de fatigue"
languages["EN"]["tooltips"]["stress"] = "This is your stress indicator"
languages["FR"]["tooltips"]["stress"] = "Ceci est votre indicateur de stress"
languages["EN"]["tooltips"]["hungry"] = "You're hungry, you will lose life!"
languages["FR"]["tooltips"]["hungry"] = "Vous avez faim, vous allez perdre de la vie !"
languages["EN"]["tooltips"]["thirsty"] = "You're thirsty, you will lose life!"
languages["FR"]["tooltips"]["thirsty"] = "Vous avez soif, vous allez perdre de la vie !"
languages["EN"]["tooltips"]["players"] = "Press tab key to display informations about other players"
languages["FR"]["tooltips"]["players"] = "Appuyez sur la touche tabulation pour afficher les informations sur les autres joueurs"
languages["EN"]["tooltips"]["character"] = "Use the scroll wheel to rotate the character"
languages["FR"]["tooltips"]["character"] = "Utilisez la molette pour faire tourner le personnage"

# Misc
languages["EN"][""]["yes"] = "Yes"
languages["FR"][""]["yes"] = "Oui"
languages["EN"][""]["no"] = "No"
languages["FR"][""]["no"] = "Non"
languages["EN"][""]["with"] = "With"
languages["FR"][""]["with"] = "Avec"
languages["EN"][""]["without"] = "Without"
languages["FR"][""]["without"] = "Sans"
