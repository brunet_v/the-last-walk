import Tkinter as tk
import tkFont

"""
    Little waiting window while updating client
"""
class SplashScreen(object):

    def __init__(self):
        # Data
        self.total = None
        self.curr = 0
        self.current = ""
        # Tkinter
        self.root = tk.Tk()
        self.root.overrideredirect(True)
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.bborder = 15
        self.ww = 300
        self.wh = self.ww / 2
        self.root.geometry('%dx%d+%d+%d' % (
            self.ww,
            self.wh,
            self.sw / 2 - self.ww / 2,
            self.sh / 2 - self.wh / 2,
        ))
        self.canvas = tk.Canvas(self.root, width=self.ww, height=self.wh, highlightthickness=0.0)
        self.canvas.pack()
        self.background = tk.PhotoImage(file='./datas/ui/splash.gif')
        self.canvas.create_image(0, 0, image=self.background, anchor=tk.NW)
        self.bigfont = tkFont.Font(size=11)

    def quit(self):
        self.root.quit()
        self.root.destroy()

    def downloading(self, file_path):
        self.current = file_path
        self.update()

    def done(self):
        self.curr += 1
        self.update()

    def updating(self, changes):
        self.total = changes

    def update(self):
        try:
            self.canvas.delete("info")
            if self.total:
                self.canvas.delete("bar1")
                self.canvas.delete("bar2")
                self.canvas.delete("file")
            else:
                pass
        except:
            pass

        if self.total:
            xend = float(self.ww - self.bborder * 2) * (float(self.curr) / float(self.total))
            bord = 3
            # TODO Change bar style
            self.canvas.create_rectangle(
                self.bborder - bord,
                self.wh - self.bborder * 2.5 - bord,
                self.ww - self.bborder + bord,
                self.wh - self.bborder + bord,
                fill="#999999", tag="bar1",
            )
            self.canvas.create_rectangle(
                self.bborder,
                self.wh - self.bborder * 2.5,
                self.bborder + xend,
                self.wh - self.bborder,
                fill="#182a5e", outline="#000", tag="bar2",
            )
            txt = self.current
            if len(txt) > 40:
                txt = txt[:35] + " ..."
            self.canvas.create_text(25, 117, text=txt, fill="#ffffff", tag="file", anchor=tk.NW)
            self.canvas.create_text(60, 100, text="Updating client ...", fill="#ffffff", tag="info")
        else:
            self.canvas.create_text(155, 115, text="Checking for updates ...", fill="#ffffff", tag="info", font=self.bigfont)
        self.canvas.update()
        self.root.update()
