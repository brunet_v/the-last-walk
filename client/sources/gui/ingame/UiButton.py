import sources.config.constants as const
import sources.objects.utils as utils

"""
    Simple clickable button
"""
class UiButton(object):

    def __init__(self, handler, value, offset, callback, name = "button", sx = 130, sy = 30, ox = -1, oy = -1):
        utils.InstanceCounter.add("UiButton")
        self.size_x = sx
        self.size_y = sy
        self.ox = ox
        self.oy = oy
        self.callback = callback
        self.offset = offset
        self.value = value
        self.handler = handler
        self.positions()
        self.img_hover = handler.load_image(
            'datas/ui/' + name + '-hover.png',
            self.xstart,
            self.ystart,
            sx, sy
        )
        self.img = handler.load_image(
            'datas/ui/' + name + '.png',
            self.xstart,
            self.ystart,
            sx, sy
        )
        self.txt = handler.load_text(
            value,
            self.xstart + 65,
            self.ystart + 19,
            16,
        )

    def __del__(self):
        utils.InstanceCounter.rm("UiButton")
        self.clear()

    def clear(self):
        if self.txt:
            self.txt.destroy()
            self.txt = None
        if self.img:
            self.img.destroy()
            self.img = None
        self.handler = None

    def hovering(self):
        if self.handler:
            xdiff = self.handler.mx - self.xstart
            ydiff = self.handler.my - self.ystart
            if 0 <= xdiff <= self.size_x and 0 <= ydiff <= self.size_y:
                return True
        return False

    def hover(self):
        if self.img:
            if self.hovering():
                self.img_hover.show()
                self.img.hide()
            else:
                self.img_hover.hide()
                self.img.show()

    def positions(self):
        if self.handler:
            # self.xstart = self.handler.ui_xstart + const.INVENTORY_SIZE * self.handler.inv_margin - 156
            # self.ystart = self.handler.ui_ystart - 150 + 50 * self.offset
            if self.ox != -1:
                self.xstart = self.ox
                self.ystart = self.oy
                return None
            self.xstart = self.handler.winxsize - self.offset * 50
            self.ystart = 15

    def resize(self):
        if self.handler:
            self.positions()
            self.handler.move_image(self.img_hover, self.xstart, self.ystart, self.size_x, self.size_y)
            self.handler.move_image(self.img, self.xstart, self.ystart, self.size_x, self.size_y)
            self.handler.move_text(self.txt, self.xstart + 65, self.ystart + 19, 16)

    def click(self):
        if self.hovering():
            self.callback()

