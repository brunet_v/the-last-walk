import panda3d.core as engine
import sources.config.constants as const
import sources.objects.utils as utils
import sources.utils.resources as resources
import sources.config.configfile as configfile

from direct.gui.OnscreenText import OnscreenText
from direct.gui.OnscreenImage import OnscreenImage
from sources.gui.ingame.UiButton import UiButton

"""
    GUI manager, load images, manage inventory and other ui componments
"""
class UiHandler(object):

    def __init__(self, game):
        utils.InstanceCounter.add("UiHandler")
        self.game = game
        self.world = game.world
        self.mx = 0
        self.my = 0
        self.items = []
        self.ui_backgrounds = []
        self.ui_loads = []
        self.inv_margin = 65
        self.inv_img_size_x = 60
        self.inv_img_size_y = 54
        self.item_img_size = 48
        self.item_load_xmargin = 37
        self.item_load_ymargin = 56
        self.item_load_scale = 18
        self.item_on_cursor = None
        self.cursor_item_margin = 0
        self.cursor_image_size = 32
        self.m1_pressed = False
        self.m3_pressed = False
        self.show_markers = False
        self.player_markers = {}
        self.cmask = engine.BitMask32.bit(0)
        self.get_winsize()
        for i in xrange(0, const.INVENTORY_SIZE):
            self.items.append(None)
            img = self.load_image(
                'datas/ui/inventory_slot.png',
                self.ui_xstart + i * self.inv_margin,
                self.ui_ystart,
                self.inv_img_size_x, self.inv_img_size_y,
            )
            txt = self.load_text(
                "0",
                self.ui_xstart + i * self.inv_margin + self.item_load_xmargin,
                self.ui_ystart + self.item_load_ymargin,
                self.item_load_scale,
            )
            self.ui_backgrounds.append(img)
            self.ui_loads.append(txt)
        self.update_loads()
        self.menu_button = UiButton(self, "", 1, self.load_menu, "button-menu", 42, 42)
        if configfile.get("helpbutton"):
            self.help_button = UiButton(self, "", 2, self.load_help, "button-help", 42, 42)
        else:
            self.help_button = None
        # life and others player infos
        self.player_bar_x = 10
        self.player_bar_y = 10
        self.player_bar = {
            "cavatar": None,
            "cavatar-bg": None,
            "avatar": None,
            "life": None,
            "lbar": None,
            "lbar-bg": None,
            "tire": None,
            "tbar": None,
            "tbar-bg": None,
            "heart": None,
            "food": None,
            "drink": None,
            "players_infos_win-bg": None
        }
        self.ui_hover = {
            "lbar": ()
        }
        self.win_infos = None
        self.player_bar_offsets = {}
        self.load_player_bar()
        self.load_compass()

        from sources.handlers.TooltipsHandler import TooltipsHandler
        self.toolTips = TooltipsHandler()

    def __del__(self):
        utils.InstanceCounter.rm("UiHandler")
        self.clear()

    def clear(self):
        for item in self.items:
            if item:
                item[3].destroy()
        for background in self.ui_backgrounds:
            background.destroy()
        for load in self.ui_loads:
            load.destroy()
        for id, nodes in self.player_markers.iteritems():
            for i in xrange(1, len(nodes)):
                nodes[i].detachNode()
                nodes[i].removeNode()
        if self.item_on_cursor:
            self.item_on_cursor[1].destroy()
        self.menu_button.clear()
        if self.help_button:
            self.help_button.clear()
        self.game = None
        self.world = None
        for name, obj in self.player_bar.iteritems():
            if obj:
                obj.destroy()
        self.hide_players_infos_win()
        self.unload_compass()

    def get_winsize(self):
        self.winxsize = base.win.getXSize()
        self.winysize = base.win.getYSize()
        self.dxwin = int(self.winxsize / 2)
        self.dywin = int(self.winysize / 2)
        self.ui_xstart = self.winxsize / 2.0 - (const.INVENTORY_SIZE / 2.0) * self.inv_margin + (self.inv_margin - self.inv_img_size_x) / 2.0
        self.ui_ystart = self.winysize - self.inv_margin

    def load_text(self, value, x, y, scale):
        text = OnscreenText(mayChange=True, fg=(1, 1, 1, 1))
        text.setScale(scale)
        self.move_text(text, x, y)
        text.setText(value)
        text.reparentTo(pixel2d)
        text.show()
        return text

    def load_image(self, image_file, x, y, xsize, ysize, xscale = 1, yscale = 1):
        img = OnscreenImage(image = image_file)
        img.setScale((xsize * xscale) / 2.0, 0, (ysize * yscale) / 2.0)
        self.move_image(img, x, y, xsize, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.show()
        return img

    def load_image_scale(self, image_file, x, y, xsize, ysize, xscale = None, yscale = None):
        img = OnscreenImage(image = image_file)
        if xscale == None:
            xs = 1
        else:
            xs = xscale
        if yscale == None:
            ys = 1
        else:
            ys = yscale
        img.setScale((xsize * xs) / 2.0, 0, (ysize * ys) / 2.0)
        if xscale == None:
            xs = 0
        else:
            xs = (xsize * xscale) / 2.0
        if yscale == None:
            ys = 0
        else:
            ys = (ysize * yscale) / 2.0
        img.setColor(255, 255, 255, 100)
        self.move_image(img, x + xs, y + ys, xsize, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.show()
        return img

    def set_image_scale(self, img, x, y, xsize, ysize, xscale, yscale):
        xs = xsize * xscale / 2.
        ys = ysize * yscale / 2.
        img.setScale(xs, 0, ys)
        self.move_image(img, x + xs, y + ys, xsize, ysize)

    def set_indicator(self, img, x, y, hpr, scale, alpha):
        self.move_image(img, x, y, 0., 0.)
        img.setHpr(*hpr)
        img.setScale(*scale)
        img.setColor(1., 1., 1., max(0., min(1., alpha)) ** 3)

    def set_compass_pos(self):
        wx = base.win.getXSize()
        wy = base.win.getYSize()
        self.compass.setPos(wx - 90, 0, wy * -1 + 82)

    def load_compass(self):
        self.compass = resources.load_model_file(
            'datas/ui/ingame/compass',
            'datas/ui/ingame/compass.png',
            alpha=True
        )
        self.compass.reparentTo(pixel2d)
        self.set_compass_pos()
        self.compass.setScale(27)
        self.compass.setAlphaScale(1.)

    def unload_compass(self):
        if self.compass:
            self.compass.detachNode()
            self.compass.removeNode()
            self.compass = None

    def rotate_compass(self, value):
        self.compass.setHpr(0, -25, value)

    def load_player_bar(self):
        self.player_bar["cavatar-bg"] = self.load_image(
            'datas/ui/ingame/cavatar-bg.png',
            self.player_bar_x,
            self.player_bar_y,
            128, 122
        )
        self.player_bar["avatar"] = self.load_image(
            'datas/ui/ingame/test.png',
            self.player_bar_x + 19,
            self.player_bar_y + 16,
            90, 90
        )
        self.player_bar["cavatar"] = self.load_image(
            'datas/ui/ingame/cavatar.png',
            self.player_bar_x,
            self.player_bar_y,
            128, 122
        )
        self.player_bar["hbox"] = self.load_image(
            'datas/ui/heartbox.png',
            self.player_bar_x + 96,
            self.player_bar_y + 86,
            48, 48
        )
        # self.player_bar["avatar"] = self.load_image(
        #     'datas/ui/ingame/test.png',
        #     self.player_bar_x + 13,
        #     self.player_bar_y + 13,
        #     95, 95
        # )
        # self.player_bar["cavatar"] = self.load_image(
        #     'datas/ui/ingame/cavatar.png',
        #     self.player_bar_x,
        #     self.player_bar_y,
        #     121, 121
        # )
        self.player_bar["lbar-bg"] = self.load_image(
            'datas/ui/ingame/bar-bg.png',
            self.player_bar_x + 120,
            self.player_bar_y,
            236, 26
        )
        self.player_bar["life"] = self.load_image_scale(
            'datas/ui/ingame/life.png',
            self.player_bar_x + 134,
            self.player_bar_y + 9,
            1, 4, 208
        )
        self.player_bar["lbar"] = self.load_image(
            'datas/ui/ingame/bar.png',
            self.player_bar_x + 120,
            self.player_bar_y,
            236, 26
        )
        self.player_bar["tbar-bg"] = self.load_image(
            'datas/ui/ingame/bar-bg.png',
            self.player_bar_x + 120,
            self.player_bar_y + 20,
            236, 26
        )
        self.player_bar["tire"] = self.load_image_scale(
            'datas/ui/ingame/tire.png',
            self.player_bar_x + 134,
            self.player_bar_y + 29,
            1, 4, 208
        )
        self.player_bar["tbar"] = self.load_image(
            'datas/ui/ingame/bar.png',
            self.player_bar_x + 120,
            self.player_bar_y + 20,
            236, 26
        )
        # self.player_bar["lbar-bg"] = self.load_image(
        #     'datas/ui/ingame/bar-bg.png',
        #     self.player_bar_x + 120,
        #     self.player_bar_y + 4,
        #     218, 25
        # )
        # self.player_bar["life"] = self.load_image_scale(
        #     'datas/ui/ingame/life.png',
        #     self.player_bar_x + 129,
        #     self.player_bar_y + 13,
        #     1, 7, 200
        # )
        # self.player_bar["lbar"] = self.load_image(
        #     'datas/ui/ingame/bar.png',
        #     self.player_bar_x + 120,
        #     self.player_bar_y + 4,
        #     218, 25
        # )
        # self.player_bar["tbar-bg"] = self.load_image(
        #     'datas/ui/ingame/bar-bg.png',
        #     self.player_bar_x + 120,
        #     self.player_bar_y + 28,
        #     218, 25
        # )
        # self.player_bar["tire"] = self.load_image_scale(
        #     'datas/ui/ingame/tire.png',
        #     self.player_bar_x + 129,
        #     self.player_bar_y + 37,
        #     1, 7, 200
        # )
        # self.player_bar["tbar"] = self.load_image(
        #     'datas/ui/ingame/bar.png',
        #     self.player_bar_x + 120,
        #     self.player_bar_y + 28,
        #     218, 25
        # )
        self.player_bar["heart"] = self.load_image(
            'datas/ui/ingame/heart.png',
            self.player_bar_x + 130,
            self.player_bar_y - 200,
            50, 62
        )
        self.player_bar["food"] = self.load_image(
            'datas/ui/ingame/food.png',
            self.player_bar_x + 202,
            self.player_bar_y - 200,
            57, 46
        )
        self.player_bar["drink"] = self.load_image(
            'datas/ui/ingame/drink.png',
            self.player_bar_x + 290,
            self.player_bar_y - 200,
            44, 60
        )

    def update_player_bar(self, frametime):
        if self.game and self.game.player:
            player = self.game.player
            # Update bars
            # self.set_image_scale(
            #     self.player_bar["life"],
            #     self.player_bar_x + 129,
            #     self.player_bar_y + 8,
            #     1, 8, 200. * player.life / player.max_life, 1.
            # )
            # self.set_image_scale(
            #     self.player_bar["tire"],
            #     self.player_bar_x + 129,
            #     self.player_bar_y + 32,
            #     1, 8, 200. * player.energy / 100., 1.
            # )
            self.set_image_scale(
                self.player_bar["life"],
                self.player_bar_x + 134,
                self.player_bar_y + 9,
                1, 4, 208. * player.life / player.max_life, 1.
            )
            self.set_image_scale(
                self.player_bar["tire"],
                self.player_bar_x + 134,
                self.player_bar_y + 29,
                1, 4, 208. * player.energy / 100., 1.
            )
            # Update indicators
            if player.dead:
                return None
            def periodic(name, delay, variance):
                try:
                    actual = self.player_bar_offsets.get(name, 0.)
                    actual += frametime / (delay / 4.)
                    self.player_bar_offsets[name] = actual
                    raw = actual * variance % (variance * 4.)
                    return abs(raw - variance * 2.) - variance
                except:
                    return 0.
            def ratioed(start, end, ratio):
                return start + (end - start) * (ratio / 100.)
            hdelay = ratioed(0.25, 0.7, 100. - player.stress)
            if player.mspeed == player.stats['sprint-speed']:
                hdelay /= 1.5
            hscale = periodic("hs", hdelay, 0.2) + 0.7
            self.game.lights.set_heart_scale(hscale)
            self.set_indicator(
                self.player_bar["heart"],
                self.player_bar_x + 120,
                self.player_bar_y + 110,
                (0., 0., periodic("hr", hdelay, 3.5)),
                (20. * hscale, 1, 24. * hscale),
                1.
            )
            fdelay = ratioed(0.25, 1.0, 100. - player.hunger)
            fscale = (periodic("fs", fdelay, 0.15) + 1.) * ratioed(0.1, 1.0, player.hunger)
            fra = ratioed(0., 1., player.hunger)
            self.set_indicator(
                self.player_bar["food"],
                self.player_bar_x + 200,
                self.player_bar_y + 80,
                (0., 0., periodic("fr", fdelay * 2., ratioed(1., 20., player.hunger))),
                (27 * fscale, 1, 22 * fscale),
                min(fra, periodic("da", fdelay * 6., fra) + fra)
            )
            ddelay = ratioed(0.25, 1.0, 100. - player.thirst)
            dscale = (periodic("ds", ddelay, 0.15) + 1.) * ratioed(0.1, 1.0, player.thirst)
            dra = ratioed(0., 1., player.thirst)
            self.set_indicator(
                self.player_bar["drink"],
                self.player_bar_x + 285,
                self.player_bar_y + 80,
                (0., 0., periodic("dr", ddelay * 2., ratioed(1., 20., player.thirst))),
                (22 * dscale, 1, 30 * dscale),
                min(dra, periodic("da", ddelay * 6., dra) + dra)
            )

    def load_helpers(self):
        for id, player in self.world['players'].iteritems():
            if not player.is_main:
                namebar = engine.TextNode('player_namebar_%d' % id)
                namebar.setText(player.username)
                namebar.setCardColor(0.0, 0.0, 0.0, 1.0)
                namebar.setCardAsMargin(0.4, 0.4, 0.4, 0.4)
                namebar.setCardDecal(True)
                namebar.setAlign(engine.TextNode.ACenter)
                namebar_nodepath = render.attachNewNode(namebar)
                namebar_nodepath.setBillboardPointEye()
                namebar_nodepath.hide(engine.BitMask32.bit(1))
                namebar_nodepath.setScale(0.2)
                namebar_nodepath.setBin("fixed", 40)
                namebar_nodepath.setDepthTest(False)
                namebar_nodepath.setDepthWrite(False)
                marker = resources.load_model_file('datas/ui/marker.egg', 'datas/ui/marker.png')
                marker.setScale(1.1)
                marker.reparentTo(render)
                marker.hide(engine.BitMask32.bit(1))
                marker.setBin("fixed", 39)
                marker.setDepthTest(False)
                marker.setDepthWrite(False)
                marker.setLight(self.game.lights.lights['sunlightnode'])
                marker.setLight(self.game.lights.lights['ambientlightnode'])
                marker.clearShader()
                self.player_markers[id] = (namebar, namebar_nodepath, marker)
        self.show_helpers(False)

    def show_helpers(self, show):
        for id, nodes in self.player_markers.iteritems():
            self.show_markers = show
            if show:
                nodes[1].show(self.cmask)
                nodes[2].show(self.cmask)
            else:
                nodes[1].hide(self.cmask)
                nodes[2].hide(self.cmask)

    def position_helpers(self, mplayer, pid, text, marker):
        pplayer = self.world['players'][pid]
        mdist = 4.5 ** 2
        if mplayer.id in pplayer.allieds and mplayer.id != pplayer.id:
            pppos = pplayer.getVisualPos()
            mppos = mplayer.getVisualPos()
            diff = pppos - mppos
            if diff.lengthSquared() > mdist:
                diff.normalize()
                fz = diff.getZ() + 0.9
                diff *= 3.
                apos = diff + mppos
                diff *= 1.1
                bpos = diff + mppos
                apos.setZ(fz - 0.15)
                bpos.setZ(fz)
                text.setPos(apos)
                marker.setPos(bpos)
                marker.show(self.cmask)
                marker.lookAt(pppos)
            else:
                ppos = engine.Point3(pppos)
                ppos.setZ(ppos.getZ() + 1.75)
                text.setPos(ppos)
                marker.hide(self.cmask)
        else:
            text.hide(self.cmask)
            marker.hide(self.cmask)

    def move_image(self, image, x, y, xsize, ysize):
        image.setPos(x + xsize / 2, 0, -y - ysize / 2)

    def move_text(self, text, x, y, scale=None):
        text.setPos(x, -y)

    def update_loads(self):
        for i in xrange(0, const.INVENTORY_SIZE):
            item = self.items[i]
            if item and const.ITEM_STATS[item[1]]['load'] > 0:
                self.ui_loads[i].setText("x%d" % item[2])
                self.ui_loads[i].show()
            else:
                self.ui_loads[i].hide()

    def update_item_load(self, item_id, nload):
        for i in xrange(0, const.INVENTORY_SIZE):
            if self.items[i] and self.items[i][0] == item_id:
                self.items[i][2] = nload
        self.update_loads()

    def get_free_inventory_slot(self):
        for i in xrange(0, const.INVENTORY_SIZE):
            if not self.items[i]:
                return i

    def add_item(self, id, type, load):
        idx = self.get_free_inventory_slot()
        img = self.load_image(
            'datas/ui/icons/%s-icon.png' % type,
            6 + self.ui_xstart + idx * self.inv_margin,
            3 + self.ui_ystart,
            self.item_img_size, self.item_img_size
        )
        self.items[idx] = [id, type, load, img]
        self.update_loads()

    def get_item_idx(self, id):
        for i in xrange(0, len(self.items)):
            if self.items[i] and self.items[i][0] == id:
                return i
        return None

    def switch_items(self, idx1, idx2):
        if idx1 != idx2:
            if self.items[idx1]:
                self.move_image(
                    self.items[idx1][3],
                    6 + self.ui_xstart + idx2 * self.inv_margin,
                    3 + self.ui_ystart,
                    self.item_img_size, self.item_img_size
                )
            if self.items[idx2]:
                self.move_image(
                    self.items[idx2][3],
                    6 + self.ui_xstart + idx1 * self.inv_margin,
                    3 + self.ui_ystart,
                    self.item_img_size, self.item_img_size
                )
            self.items[idx1], self.items[idx2] = self.items[idx2], self.items[idx1]
            self.update_loads()

    def del_item(self, id):
        for i in xrange(0, len(self.items)):
            item = self.items[i]
            if item and item[0] == id:
                item[3].destroy()
                self.items[i] = None
                self.update_loads()
                return True
        return False

    def use_slot(self, slot):
        item = self.items[slot]
        if item:
            self.game.send("item_use", {
                'id': item[0]
            })

    def m1_click(self):
        if not self.mouse_on_ui():
            return True
        self.menu_button.click()
        if self.help_button:
            self.help_button.click()
        self.m1_pressed = True
        item_slot = self.get_item_slot_clicked()
        if item_slot:
            self.item_on_cursor = (item_slot[0], self.load_image(
                'datas/ui/icons/%s-icon.png' % item_slot[1],
                self.mx + self.cursor_item_margin,
                self.my + self.cursor_item_margin,
                self.cursor_image_size, self.cursor_image_size
            ))

    def m1_unclick(self):
        self.m1_pressed = False
        if self.item_on_cursor:
            if self.mouse_on_ui():
                item_idx1 = self.get_item_idx_clicked()
                item_idx2 = self.get_item_idx(self.item_on_cursor[0])
                if item_idx1 != None and item_idx2 != None:
                    if False: # if is combinable
                        pass
                        # Eventually combine those items (TODO)
                    else:
                        self.switch_items(item_idx1, item_idx2)
            else:
                self.game.send("item_drop", {
                    'id': self.item_on_cursor[0],
                    'x': self.game.mouse_pos.getX(),
                    'y': self.game.mouse_pos.getY(),
                })
            self.item_on_cursor[1].destroy()
            self.item_on_cursor = None
        return True

    def m3_click(self):
        if not self.mouse_on_ui():
            return True
        slot = self.get_item_idx_clicked()
        if slot != None:
            self.use_slot(slot)
        self.m3_pressed = True

    def m3_unclick(self):
        self.m3_pressed = False
        return True

    def load_player_info(self, sx, sy, id):
        self.win_infos["cavatar-bg" + str(id)] = self.load_image(
            'datas/ui/ingame/cavatar-2.png',
            sx,
            sy,
            78, 78
        )
        self.win_infos["avatar" + str(id)] = self.load_image(
            'datas/ui/ingame/test.png',
            sx + 7,
            sy + 7,
            64, 64
        )
        self.win_infos["cavatar" + str(id)] = self.load_image(
            'datas/ui/ingame/cavatar.png',
            sx,
            sy,
            78, 78
        )
        self.win_infos["lbar-bg" + str(id)] = self.load_image(
            'datas/ui/ingame/bar-bg.png',
            sx + 71,
            sy + 1,
            189, 21
        )
        self.win_infos["life" + str(id)] = self.load_image_scale(
            'datas/ui/ingame/life.png',
            sx + 82,
            sy + 9,
            1, 4, 166
        )
        self.win_infos["lbar" + str(id)] = self.load_image(
            'datas/ui/ingame/bar.png',
            sx + 71,
            sy + 1,
            189, 21
        )
        self.win_infos["tbar-bg" + str(id)] = self.load_image(
            'datas/ui/ingame/bar-bg.png',
            sx + 71,
            sy + 17,
            189, 21
        )
        self.win_infos["tire" + str(id)] = self.load_image_scale(
            'datas/ui/ingame/tire.png',
            sx + 82,
            sy + 25,
            1, 4, 166
        )
        self.win_infos["tbar" + str(id)] = self.load_image(
            'datas/ui/ingame/bar.png',
            sx + 71,
            sy + 17,
            189, 21
        )

    def load_players_infos_win(self):
        self.win_infos = {}
        nb_player = 7
        ox = 20
        oy = 150
        bg_x = 258
        bg_height = 73
        bg_y = bg_height * nb_player + 5
        self.win_infos["players_infos_win-bg"] = self.load_image_scale(
            'datas/ui/ingame/players_infos_win-bg.png',
            ox,
            oy,
            1, 1, bg_x, bg_y
        )
        for i in xrange(0, nb_player):
            sy = i * bg_height
            self.load_player_info(ox, oy + sy, i)

    def hide_players_infos_win(self):
        if self.win_infos:
            for name, obj in self.win_infos.iteritems():
                if obj:
                    obj.destroy()
            self.win_infos = None

    def check_player_infos_hover(self, name):
        if self.player_bar[name]:
            img = self.player_bar[name]
            s = img.getScale()
            sx = s[0] * 2
            sy = s[2] * 2
            p = img.getPos()
            px = p[0] - sx / 2
            py = p[2] * -1 - sy / 2
            if self.mx > px and self.mx < px + sx and self.my > py and self.my < py + sy:
                self.toolTips.show(name)
            else:
                self.toolTips.hide(name)

    def frame_actions(self, frametime):
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            self.mx = int(self.dxwin * mpos.getX() + self.dxwin)
            self.my = int(self.dywin * -mpos.getY() + self.dywin)
        if self.item_on_cursor:
            self.move_image(
                self.item_on_cursor[1],
                self.mx + self.cursor_item_margin,
                self.my + self.cursor_item_margin,
                self.cursor_image_size, self.cursor_image_size
            )
        self.menu_button.hover()
        if self.help_button:
            self.help_button.hover()
        if self.show_markers:
            for id, markers in self.player_markers.iteritems():
                self.position_helpers(self.game.player, id, markers[1], markers[2])
        self.update_player_bar(frametime)
        self.check_player_infos_hover("tbar")
        self.check_player_infos_hover("heart")
        self.check_player_infos_hover("food")
        self.check_player_infos_hover("drink")
        self.check_player_infos_hover("avatar")
        self.check_player_infos_hover("lbar")
        self.rotate_compass(-self.world['infos']['view']['rotation'])

    def periodic_actions(self):
        # Resize ui when necessary
        if base.win.getXSize() != self.winxsize or base.win.getYSize() != self.winysize:
            self.get_winsize()
            for i in xrange(0, const.INVENTORY_SIZE):
                self.move_text(
                    self.ui_loads[i],
                    self.ui_xstart + i * self.inv_margin + self.item_load_xmargin,
                    self.ui_ystart + self.item_load_ymargin
                )
                self.move_image(
                    self.ui_backgrounds[i],
                    self.ui_xstart + i * self.inv_margin,
                    self.ui_ystart,
                    self.inv_img_size_x,
                    self.inv_img_size_y
                )
                if self.items[i]:
                    self.move_image(self.items[i][3], 6 + self.ui_xstart + i * self.inv_margin, 3 + self.ui_ystart, self.item_img_size, self.item_img_size)
            self.menu_button.resize()
            if self.help_button:
                self.help_button.resize()
            self.set_compass_pos()

    def get_item_idx_clicked(self):
        xoffset = (self.mx - self.ui_xstart)
        xblock = xoffset / self.inv_margin
        if (xoffset % self.inv_margin < self.inv_img_size_x
            and xblock >= 0
            and xblock < const.INVENTORY_SIZE
            and self.my > self.ui_ystart
            and self.my < self.ui_ystart + self.inv_img_size_y
        ):
            return int(xblock)
        return None

    def get_item_slot_clicked(self):
        idx = self.get_item_idx_clicked()
        if idx is not None:
            return self.items[self.get_item_idx_clicked()]
        return None

    def mouse_on_ui(self):
        if self.get_item_idx_clicked() is not None:
            return True
        if self.game and self.game.menus:
            menus = self.game.menus
            if menus.game_menu or menus.help_menu or menus.options:
                return True
        if self.menu_button.hovering():
            return True
        if self.help_button and self.help_button.hovering():
            return True
        return False

    def load_menu(self):
        if self.game and self.game.menus:
            self.game.menus.load_game_menu()

    def load_help(self):
        if self.game and self.game.menus:
            self.game.menus.load_help_menu()
