import sources.objects.utils as utils
import sources.networking.events as events

from sources.utils.Threader import Threader

"""
    Network events listing with their handler
"""
EVENT_ACTIONS = {
    # players
    'p_create': events.player_create,
    'p_hook': events.player_hook,
    'p_update': events.player_update,
    'p_stop': events.player_stop,
    'p_equip': events.player_equip_item,
    'p_unequip': events.player_unequip_item,
    'p_aim': events.player_aim,
    'p_unaim': events.player_unaim,
    'p_use': events.player_use,
    'p_effect': events.player_effect,
    'p_block': events.player_block,
    'p_hit': events.player_hit,
    'p_kill': events.player_kill,
    'p_msg': events.player_msg,
    'p_sprint': events.player_sprint,
    'p_light': events.player_light,

    # zombies
    'czomb': events.create_zombie,
    'mzomb': events.order_move_zombie,
    'kzomb': events.kill_zombie,
    'hzomb': events.hit_zombie,
    'azomb': events.attack_zombie,
    'grunt': events.grunt_zombie,

    # environment
    'sdayt': events.set_daytime,
    'nday': events.new_day,

    # doors
    'd_set': events.door_set,
    'd_hit': events.door_hit,
    'd_kill': events.door_kill,

    # items
    'citem': events.create_item,
    'titem': events.take_item,
    'ditem': events.drop_item,
    'uload': events.update_load,

    # misc
    'starting': events.starting,
    'ally': events.ally_update,
    'error': events.get_error,
    'rrequest': events.response_request,
    'world': events.world_loaded,
    'world-load': events.world_loading,
    'player-ready': events.player_ready,
}

"""
    Fake-thread handling networks request from server
"""
class ServerEvents(Threader):

    def __init__(self, game):
        self.game = game
        Threader.__init__(self, "ServerEvents")
        utils.InstanceCounter.add("ServerEvents")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("ServerEvents")
        self.clear()

    def clear(self):
        Threader.clear(self)
        self.game = None

    """
        Resolve every pending network events
    """
    def run(self, delay):
        game = self.game
        while game and game.connection and game.connection.connected:
            event = game.connection.receive()
            if event:
                try:
                    if event.type in EVENT_ACTIONS:
                        EVENT_ACTIONS[event.type](game, game.world, event.data)
                    else:
                        game.menus.network_event(event.type, event.data)
                except Exception as e:
                    print "Server input error (%s): %s" % (event.type, str(e))
                    import traceback
                    traceback.print_exc() # DEBUG
            else:
                return False
        return True
