import time
import random as rand
import sources.config.constants as const
import panda3d.core as engine
import sources.objects.creation as obj_creation
import sources.utils.positioning as positioning
import sources.config.build_config as build_config
import sources.utils.multitask as threading

from sources.objects.Item import Item

"""
    Network events handlers.
    Handle gameplay events occuring on the server.

    Each handler is called when the corresponding event comes from the (game)server
"""

# Events with players
def player_create(game, world, data):
    player = obj_creation.create_player(world, data['type'], data['id'], data['x'], data['y'], data['z'], None)
    player.username = data['login']
    game.fastlight.add_player_light(player)

def player_hook(game, world, data):
    if not game.game_starting:
        game.game_starting = True
        game.menus.fadescreen.fade_in()
        def do_start():
            time.sleep(0.50)
            debug_loading = build_config.build_option("debug-loading")
            if debug_loading:
                print "Player hooked, starting game_started()."
            game.game_started()
            if debug_loading:
                print "game_started() Finished, loading basic values."
            world['infos']['hooked'] = data['id']
            player_hooked = world['players'][data['id']]
            player_hooked.hooked(world)
            player_hooked.refreshCulling()
            player_pos = player_hooked.getPos()
            game.player = player_hooked
            game.effect_handler.player = player_hooked
            game.fastlight.set_player_main(player_hooked)
            world_view = world['infos']['view']
            world_view['xpos'] = player_pos.getX()
            world_view['oxpos'] = player_pos.getX()
            world_view['ypos'] = player_pos.getY()
            world_view['oypos'] = player_pos.getY()
            world_view['zpos'] = player_pos.getZ()
            world_view['ozpos'] = player_pos.getZ()
            game.menus.shaders.stop_menu_blurr()
            game.menus.unload_menu_scene()
            game.ready = True
            if game.players_inits:
                starting(game, world, {})
            game.send('p_started', {})
        threading.Thread(target=do_start).start()

def player_update(game, world, datas):
    uids = set()
    # Update visible players
    for data in datas:
        # For each player in the snapshot
        uids.add(data['id'])
        player = world['players'][data['id']]
        npos = engine.Point3(data['px'], data['py'], data['pz'])
        ppos = player.getPos()
        dist = ppos - npos
        sdiff = dist.lengthSquared()
        if sdiff > const.SOCKET_ERROR_MARGIN:
            player.startInterpolation()
            player.setPos(npos)
        player.goal = None
        if data['mv']:
            ngoal = engine.Point3(data['gx'], data['gy'], -1)
            player.goal = ngoal
        if not player.is_main:
            player.lookpoint = engine.Point3(data['lx'], data['ly'], -1)
        player.setLife(data['st'][0])
        player.setThirst(data['st'][1])
        player.setHunger(data['st'][2])
        player.setEnergy(data['st'][3])
        player.setStress(data['st'][4])
        player.tired = data['st'][5]
    # Hide others players
    for pid, player in world['players'].iteritems():
        if pid not in uids and not player.dead and not player.hidden:
            player.hide()
            player.stopInterpolation()
            player.setPos(engine.Point3(pid, -100001, 0))
            player.goal = None

def player_stop(game, world, data):
    player = world['players'][data['id']]
    player.goal = None
    player.startInterpolation()
    player.setPos(engine.Point3(data['px'], data['py'], data['pz']))

def player_equip_item(game, world, data):
    player = world['players'][data['pid']]
    if player.equiped:
        player_unequip_item(game, world, data)
    nitem = obj_creation.create_item(world, data['type'], 0, 0, 0, True)
    nitem.show()
    player.equiped = (data['id'], data['type'], data['load'], nitem)
    player.updateEquiped()

def player_unequip_item(game, world, data):
    player = world['players'][data['pid']]
    if player.equiped:
        player.equiped[3].setLightOff()
        player.equiped[3].detachNode()
        player.equiped[3].removeNode()
    player.equiped = None
    player.updateEquiped()

def player_aim(game, world, data):
    player = world['players'][data['pid']]
    player.aiming = True

def player_unaim(game, world, data):
    player = world['players'][data['pid']]
    if player.aiming:
        player.please_unaim = True

def player_use(game, world, data):
    player = world['players'][data['pid']]
    player.useItem(data['type'])
    # game.sounds.play_use_item(0.5, player, world['infos']['view']['xpos'], world['infos']['view']['ypos'])

def player_effect(game, world, data):
    player = world['players'][data['pid']]
    player.effectResult(data['infos'])
    # Hitbox debug
    if build_config.build_option('debug-hitbox'):
        import sources.utils.resources as resources
        debug_hitboxs = data['infos'].get('debughitbox', [])
        for x, y, z, rad in debug_hitboxs:
            n = resources.load_model_file(
                "datas/debugs/cylinder.egg",
                "datas/debugs/green.png",
                nearest=True
            )
            n.setScale(rad * 2, rad * 2, 3)
            n.setPos(x, y, z)
            n.reparentTo(render)
            n.show()

def player_block(game, world, data):
    player = world['players'][data['pid']]
    player.forceAnim(data['anim'], data['delay'])

def player_hit(game, world, data):
    player = world['players'][data['pid']]
    game.decals.place_blood(data['hx'], data['hy'], data['hz'])
    if not player.hidden:
        game.particles.blood_hit(player.getX(), player.getY(), player.getZ())
    if player.is_main:
        game.lights.set_player_hit()

def player_kill(game, world, data):
    player = world['players'][data['pid']]
    player.forceAnim('DIE', 10 ** 15) # infinite block
    player.collision_tree.deleteId(player.id)
    player.life = 0.
    player.dead = True
    player.dead_time = time.time() + 0.65
    for i in xrange(0, 3):
        game.decals.place_blood(data['hx'], data['hy'], data['hz'])
    if not player.hidden:
        game.particles.blood_hit(player.getX(), player.getY(), player.getZ())

def player_sprint(game, world, data):
    player = world['players'][data['pid']]
    player.sprint = data['sprint']

def player_msg(game, world, data):
    player = world['players'][data['pid']]
    player.showMsg(data['msg'])

def player_light(game, world, data):
    player = world['players'][data['pid']]
    player.light_on = data['l']
    player.updateEquiped()

# Events with Zombies
def create_zombie(game, world, data):
    obj_creation.create_zombie(world, data['type'], data['id'], data['x'], data['y'], data['z'])

def order_move_zombie(game, world, datas):
    if game.player:
        uids = set()
        for data in datas:
            uids.add(data['id'])
            zomb = world['zombies']['list'][data['id']]
            goal = data['goal']
            stack = data['stack']
            zpos = engine.Point3(data['zx'], data['zy'], data['zz'])
            if (zpos - zomb.getPos()).lengthSquared() > const.SOCKET_ERROR_MARGIN:
                zomb.startInterpolation()
                zomb.setPos(zpos)
            # elif (zpos - game.player.getPos()).lengthSquared() < 4.0:
            #     zomb.startInterpolation()
            #     zomb.setPos(zpos)
            zomb.goal = None
            if goal:
                zomb.goal = engine.Point3(goal[0] + 0.5, goal[1] + 0.5, goal[2])
            zomb.goal_stack = []
            for node in stack:
                zomb.goal_stack.append(engine.Point3(node[0] + 0.5, node[1] + 0.5, node[2]))
            # game.sounds.play_move(0.05, zomb, world['infos']['view']['xpos'], world['infos']['view']['ypos'])
        for zid, zomb in world['zombies']['list'].iteritems():
            if zid not in uids and not zomb.dead and not zomb.hidden:
                zomb.hide()
                zomb.stopInterpolation()
                zomb.setPos(engine.Point3(zid, -100000, 0))
                zomb.goal = None
                zomb.goal_stack = []

def kill_zombie(game, world, data):
    zomb = world['zombies']['list'][data['id']]
    zomb.collision_tree.deleteId(zomb.id)
    zomb.dead = time.time()
    game.decals.place_blood(data['hx'], data['hy'], data['hz'])
    if not zomb.hidden:
        game.particles.blood_hit(zomb.getX(), zomb.getY(), zomb.getZ())
    # Physics ?
    # if (zomb.getPos() - game.player.getPos()).lengthSquared() < 1024:
    #     zomb.freeze()
    #     zomb.show()
    #     game.physics.add_zombie(zomb, data['hx'], data['hy'], data['hz'], da.ta['hs'])
    # else:
    #     zomb.hide() # TODO

def hit_zombie(game, world, data):
    zomb = world['zombies']['list'][data['id']]
    zomb.attacking = None # Cancel attack when hit
    zomb.hitted = time.time()
    game.decals.place_blood(data['hx'], data['hy'], data['hz'])
    if not zomb.hidden:
        game.particles.blood_hit(zomb.getX(), zomb.getY(), zomb.getZ())

def attack_zombie(game, world, data):
    zomb = world['zombies']['list'][data['id']]
    # zomb.setPos(engine.Point3(data['zx'], data['zy'], data['zz']))
    zomb.attack(data['x'], data['y'], data['z'], data['delay'])

def grunt_zombie(game, world, data):
    game.sounds.play_sfx(data["sound"], 0.2, 10, data["x"], data["y"], world['infos']['view']['xpos'], world['infos']['view']['ypos'])

# World Events
def set_daytime(game, world, data):
    world['infos']['view']['daytime'] = data % 24
    # game.sounds.set_daytime(data)

def new_day(game, world, data):
    world['infos']['view']['day'] = data
    print "NEW DAY %d" % int(data) # TODO

# Doors Events
def door_set(game, world, data):
    doors = world['doors']
    door = doors.doors[data['id']]
    if data['open']:
        doors.openDoor(door.id)
        game.sounds.play_door(1, 0.2, door, world['infos']['view']['xpos'], world['infos']['view']['ypos'])
    else:
        doors.closeDoor(door.id)
        game.sounds.play_door(0, 0.2, door, world['infos']['view']['xpos'], world['infos']['view']['ypos'])

def door_hit(game, world, data):
    doors = world['doors']
    door = doors.doors[data['id']]
    door.hit(data['l'])
    # TODO (particles + sound)

def door_kill(game, world, data):
    doors = world['doors']
    door = doors.doors[data['id']]
    doors.delDoor(door.id)
    # TODO (particles + sound)

def get_error(game, world, data):
    print "ERROR:", data['error']
    pass # Display data['error']

# Events with items
def create_item(game, world, data):
    if data['id'] not in world['items']['list']:
        item_model = obj_creation.create_item(world, data['type'], data['x'], data['y'], data['z'])
        item = Item(item_model)
        item.applyFog(world)
        world['items']['list'][data['id']] = item
    else:
        positioning.position_item(world['items']['list'][data['id']].model, data['type'], data['x'], data['y'], data['z'], rand.randint(0, 360), 0, 0, "on_floor")
        world['items']['list'][data['id']].model.reparentTo(world['cg'].getNode(data['x'], data['y'], data['z']))
    world['items']['tree'].setPosValue(data['id'], data['x'], data['y'], data['z'])

def take_item(game, world, data):
    player = world['players'][data['pid']]
    player.putItem(data['id'], data['type'], data['load'])
    world['items']['list'][data['id']].hide()
    world['items']['list'][data['id']].model.setPos(-100000, -100000, 0)
    world['items']['list'][data['id']].model.reparentTo(world['cg'].get(-1, -1))
    world['items']['tree'].setPosValue(data['id'], -100000, -100000, 0)
    if game.player:
        if player.id == game.player.id:
            game.ui_handler.add_item(data['id'], data['type'], data['load'])

def drop_item(game, world, data):
    player = world['players'][data['pid']]
    if player.equiped and player.equiped[0] == data['id']:
        player_unequip_item(game, world, data)
    player.popItem(data['id'])
    if player.id == game.player.id:
        game.ui_handler.del_item(data['id'])

def update_load(game, world, data):
    player = world['players'][data['pid']]
    if player.id == game.player.id:
        game.ui_handler.update_item_load(data['id'], data['nload'])

# miscs
def starting(game, world, data):
    debug_loading = build_config.build_option("debug-loading")
    if debug_loading:
        print "Everything ready, start game."
    if not game.ready:
        game.players_inits = True
    else:
        # ally all players
        team = set()
        for id, player in world['players'].iteritems():
            team.add(id)
        for id, player in world['players'].iteritems():
            player.allieds = team
        game.ui_handler.load_helpers()
        game.go = True
        game.menus.fadescreen.fade_out()
    if debug_loading:
        print "Game started"

def ally_update(game, world, data):
    player = world['players'][data['pid']]
    player.allieds = set(data['allies'])
    if player.id == game.player.id and game.menus.game_menu:
        game.menus.resume_game_menu()
        game.menus.load_game_menu()

def response_request(game, world, data):
    if build_config.build_option("debug"):
        print "request time try:", (time.time() - data['time']) * 1000, "ms"

def world_loaded(game, world, data):
    debug_loading = build_config.build_option("debug-loading")
    if debug_loading:
        print "World infos received, loading world."
    game.load_world(data)
    if debug_loading:
        print "World loaded."

def world_loading(game, world, data):
    pass # TODO

def player_ready(game, world, data):
    login, ready = data["login"], bool(data["ready"])
    pass # TODO
