import os
import sys
import hashlib
import urllib
import json
import glob
import time

try:
    import sources.config.build_config as build_config
    from sources.gui.SplashScreen import SplashScreen
except:
    pass

"""
    Make SURE useless display does not crash critical update
"""
class SafeSplashScreen(object):
    def __init__(self):
        try:
            self.splash = SplashScreen()
        except Exception, e:
            print "Display error:", str(e)
    def updating(self, changes):
        try:
            self.splash.updating(changes)
        except Exception, e:
            print "Display error:", str(e)
    def update(self):
        try:
            self.splash.update()
        except Exception, e:
            print "Display error:", str(e)
    def downloading(self, key):
        try:
            self.splash.downloading(key)
        except Exception, e:
            print "Display error:", str(e)
    def done(self):
        try:
            self.splash.done()
        except Exception, e:
            print "Display error:", str(e)
    def quit(self):
        try:
            self.splash.quit()
        except Exception, e:
            print "Display error:", str(e)

def create_folder(path):
    prev_path = (path.rpartition('/'))[0]
    if not os.path.exists(prev_path) and prev_path != "":
        create_folder((path.rpartition('/'))[0])
    os.mkdir(path)

def listdirectory(path):
    fichier = []
    l = glob.glob(path+'/*')
    for i in l:
        if os.path.isdir(i): fichier.extend(listdirectory(i))
        else: fichier.append(i)
    return fichier

def get_hash(fichier):
    sha1 = hashlib.sha1()
    with open(fichier,'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            sha1.update(chunk)
    return sha1.hexdigest()

def getchecksum(fichier):
    dico = {}
    l = len(fichier)
    i = 0
    while i < l:
        path = fichier[i].replace("\\", "/")
        dico[path] = get_hash(path)
        i += 1
    return dico

def get_master_json():
    urlJSON = urllib.urlopen("http://update-dev.thelastwalk.net/master.json")
    master = json.load(urlJSON)
    return master

def getVersion():
    curent = getchecksum(listdirectory('.'))
    return curent

def compare_version(splash, master, current):
    changes = 0
    splash.update()
    for key, value in master.iteritems():
        try:
            url = "http://update-dev.thelastwalk.net/" + key
            url = url.replace("#", "%23")
            if key not in current or value != current[key]:
                print "Updating:", key
                if not os.path.exists((key.rpartition('/'))[0]):
                    create_folder((key.rpartition('/'))[0])
                splash.update()
                splash.downloading(key)
                urllib.urlretrieve(url, key)
                splash.done()
                changes += 1
        except Exception as e:
            print "Error:", str(e)
    for key, value in current.iteritems():
        if "configuration.json" not in key:
            if key not in master:
                print "Deleting:", key
                splash.update()
                splash.downloading(key)
                splash.done()
                os.remove(key)
    splash.update()
    time.sleep(0.25)
    splash.quit()
    return changes

def restart_game():
    print "Restarting the game..."
    python = sys.executable
    os.execl(python, python, * sys.argv)

def update():
    try:
        splash = SafeSplashScreen()
        splash.update()
        master = get_master_json()
        current = getVersion()
        changes = 0
        for key, value in master.iteritems():
            if key not in current or value != current[key]:
                changes += 1
        for key, value in current.iteritems():
            if "configuration.json" not in key:
                if key not in master:
                    changes += 1
        try:
            # Test if should update (debug mode ?)
            try:
                debug = build_config.build_option("debug")
                no_up = build_config.build_option("no-update")
            except:
                debug = False
                no_up = False
            # Do update
            if changes > 0 and not debug and not no_up:
                splash.updating(changes)
                changes = compare_version(splash, master, current)
                if changes:
                    splash.quit()
                    restart_game()
        except Exception as e:
            print "Update error: %s" % str(e)
        splash.quit()
    except Exception as e:
        print "Critical update error:", str(e)
