import socket
import Queue
import time
import msgpack as serial
import sources.objects.utils as utils
import sources.config.constants as const
import sources.utils.multitask as threading

from sources.utils.Threader import Threader

"""
    Network connection manager,
    will bufferize entries in a fake-Thread,
    and deliver input/output on demand of the main thread.
    Also handle network serialisation
"""
class ServerConnection(Threader):

    def __init__(self, ip, port):
        utils.InstanceCounter.add("ServerConnection")
        self.input = Queue.Queue()
        self.destination = (ip, port)
        self.connected = False
        self.alive = True
        self.alive_time = time.time() + 14.0
        self.receive_buffer = ""
        self.waiting_alive = False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.packets = 0
        self.bandwidth = utils.Struct({
            'input': 0,
            'output': 0,
        })
        threading.Thread(target=self.connect).start()
        Threader.__init__(self, "ServerConnection")

    def __del__(self):
        Threader.__del__(self)
        utils.InstanceCounter.rm("ServerConnection")
        self.clear()

    def clear(self):
        Threader.clear(self)
        if self.socket:
            self.send("leaving", {})
            self.socket.setblocking(False)
            try:
                self.socket.shutdown(socket.SHUT_RDWR)
            except:
                pass
            self.socket.close()
            self.socket = None
            self.connected = False

    def connect(self):
        try:
            self.socket.connect(self.destination)
        except:
            self.alive_time -= 42.
        self.socket.setblocking(False)
        self.connected = True

    def run(self, frametime):
        if self.connected:
            # Fill buffer
            try:
                buff = True
                while buff and len(self.receive_buffer) < const.MAX_BUFFER_SIZE:
                    buff = self.socket.recv(const.SOCKET_SIZE)
                    if buff:
                        self.receive_buffer += buff
            except:
                pass
            # Interpret buffer
            while len(self.receive_buffer) > 5:
                try:
                    size = int(self.receive_buffer[:4]) + 5
                    if len(self.receive_buffer) > size:
                        datas = self.receive_buffer[5:size]
                        self.receive_buffer = self.receive_buffer[size + 1:]
                        loaded = serial.loads(datas)
                        self.waiting_alive = False
                        self.alive_time = time.time() + 15.0
                        if len(loaded) >= 2:
                            self.packets += 1
                            self.bandwidth.input += len(datas)
                            self.input.put(loaded)
                    else:
                        break
                except:
                    break

    def send(self, type, datas):
        if self.socket and self.connected:
            try:
                dump = serial.dumps((type, datas))
                ldump = len(dump)
                self.socket.send("%04d|%s@" % (ldump, dump))
                self.bandwidth.output += ldump
            except socket.error:
                self.alive_time -= 4.0

    def receive(self):
        if not self.input.empty():
            event = self.input.get()
            return utils.Struct({
                "type": event[0],
                "data": event[1],
            })
        return None

    def check_alive(self):
        if time.time() > self.alive_time:
            return False
        if self.connected == None:
            return True
        if time.time() + 9.0 > self.alive_time and not self.waiting_alive:
            self.waiting_alive = True
            self.send("ping", {})
        return True
