import sys
import os
import panda3d.core as engine
import sources.config.constants as const

from pandac.PandaModules import Texture
from direct.actor.Actor import Actor
from panda3d.core import Filename

from sources.utils.BlockCollisions import BlockCollisions

"""
    Following functions are utility fonctions to load game entities from files
"""
def relative_path(path):
    cpath = Filename.fromOsSpecific(
        os.path.abspath(sys.path[0])
    ).getFullpath()
    return cpath + "/" + path

def load_texture(path, nearest=False):
    texture = loader.loadTexture(relative_path(path))
    if texture:
        if nearest:
            texture.setMagfilter(Texture.FTNearest)
            texture.setMinfilter(Texture.FTNearest)
        else:
            texture.setMagfilter(Texture.FTLinear)
            texture.setMinfilter(Texture.FTLinear)
    return texture

def load_model_file(filename, texture=None, alpha=False, nearest=False):
    new_block = loader.loadModel(relative_path(filename))
    new_texture = None
    tfile = "%s_texture.png" % filename.split(".")[0]
    if os.path.exists(tfile):
        new_texture = load_texture(tfile, nearest)
    elif texture:
        new_texture = load_texture(texture, nearest)
    if new_texture:
        new_block.setTexture(new_texture)
    if alpha:
        new_block.setTransparency(engine.TransparencyAttrib.MAlpha)
    else:
        new_block.setTransparency(engine.TransparencyAttrib.MBinary)
    new_block.clearModelNodes()
    new_block.flattenStrong()
    new_block.setTwoSided(False)
    return new_block

def load_resources():
    resources = const.RESOURCES_STRUCT
    resources['blocks_collisions'] = BlockCollisions()
    return resources

static_loaded = {}
def load_static(path, texture):
    global static_loaded
    path = "datas/%s" % path
    if path not in static_loaded:
        new_texture = None
        afile = "%s_anim.egg" % path.split(".")[0]
        tfile = "%s_texture.png" % path.split(".")[0]
        if os.path.exists(tfile):
            new_texture = load_texture(tfile)
        else:
            new_texture = load_texture(texture)
        new_texture.setMagfilter(Texture.FTNearest)
        new_texture.setMinfilter(Texture.FTNearest)
        actor = Actor(relative_path(path), {'idle' : relative_path(afile)})
        actor.setTexture(new_texture)
        actor.setTransparency(engine.TransparencyAttrib.MBinary)
        actor.setLODAnimation(450, 400, 10)
        actor.loop("idle")
        static_loaded[path] = actor
    return static_loaded[path]

def load_animated(path, texture_path, anims): # Unused ?
    node_texture = load_texture(texture_path)
    node_texture.setMagfilter(Texture.FTNearest)
    node_texture.setMinfilter(Texture.FTNearest)
    node_anims = {}
    for anim, anim_path in anims.iteritems():
        node_anims[anim] = relative_path(anim_path)
    actor = Actor(relative_path(path), node_anims)
    actor.setTexture(node_texture)
    actor.setTransparency(engine.TransparencyAttrib.MBinary)
    actor.setLODAnimation(450, 400, 10)
    return actor

def create_object(name, nearest=True):
    path = "./datas/objects/actors/%s" % name
    obj = const.OBJECT_LIST[name]
    object_scale = obj['scale']
    object_anims = {}
    for anim in obj['anims']:
        object_anims[anim] = relative_path("%s/animations/%s.egg" % (path, anim))
    actor = Actor(relative_path("%s/%s.egg" % (path, name)), object_anims)
    actor.setScale(object_scale)
    actor.setLODAnimation(450, 400, 10)
    actor.setTransparency(engine.TransparencyAttrib.MBinary)
    if 'txt' in obj:
        texture_path = "%s/textures/%s" % (path, obj['txt'])
        if os.path.isfile(texture_path):
            object_texture = load_texture(texture_path, nearest)
            actor.setTexture(object_texture)
    return actor
