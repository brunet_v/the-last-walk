import sources.config.constants as const
import sources.objects.utils as utils

"""
    Optimisation datastructure used to help panda3d culling algorithm,
    group every graphical items into small chunks (sorted by grid of x-y axis).
"""
class CullingGrid(object):

    def __init__(self):
        utils.InstanceCounter.add("CullingGrid")
        self.grid = {}
        self.vx = 1
        self.vy = 1
        self.check_grid = (
            (-1,  1), ( 0,  1), ( 1,  1),
            (-1,  0), ( 0,  0), ( 1,  0),
            (-1, -1), ( 0, -1), ( 1, -1),
        )
        self.cleared = False

    def __del__(self):
        utils.InstanceCounter.rm("CullingGrid")
        self.clear()

    def clear(self):
        if not self.cleared:
            for x, xtree in self.grid.iteritems():
                for y, node in self.grid[x].iteritems():
                    node.setLightOff()
                    node.detachNode()
                    node.removeNode()
        self.cleared = True
        self.grid = {}

    def get(self, tx, ty, z = None):
        if tx not in self.grid:
            self.grid[tx] = {}
        if ty not in self.grid[tx]:
            self.grid[tx][ty] = render.attachNewNode('%d:%d_cg' % (tx, ty))
        return self.grid[tx][ty]

    def getNode(self, x, y, z = None):
        tx = int(x) / const.BLOCKS_PER_SIZE
        ty = int(y) / const.BLOCKS_PER_SIZE
        return self.get(tx, ty, z)

    def getNears(self, tx, ty, z = None):
        tx = int(tx)
        ty = int(ty)
        ids = set()
        nodes = {}
        for ox, oy in self.check_grid:
            id = (ox + tx, oy + ty)
            ids.add(id)
            nodes[id] = self.get(ox + tx, oy + ty, z)
        return ids, nodes

    def setViewPos(self, tx, ty):
        self.vx = tx
        self.vy = ty
