import sources.utils.trees as trees
import sources.config.constants as const
import sources.objects.utils as utils

"""
    Optimisation datastructure for getting object in a zone,
    use a grid on x-y axis to sort objects.
"""
class CollisionTree(object):

    def __init__(self, bsize = 1.0):
        utils.InstanceCounter.add("CollisionTree")
        self.block_size = bsize
        self.positions = {}
        self.ids = trees.ListTree()
        self.check_grid = (
            (-1,  1), ( 0,  1), ( 1,  1),
            (-1,  0), ( 0,  0), ( 1,  0),
            (-1, -1), ( 0, -1), ( 1, -1),
        )

    def __del__(self):
        utils.InstanceCounter.rm("CollisionTree")
        self.clear()

    def clear(self):
        self.positions = {}
        self.ids = trees.ListTree()

    def setPosVal(self, id, pos, xsize = const.COLLISION_SIZE, ysize = const.COLLISION_SIZE, zsize = const.COLLISION_SIZE, circle = False):
        self.setPosValue(id, pos.getX(), pos.getY(), pos.getZ(), xsize, ysize, zsize, circle)

    def setPosValue(self, id, x, y, z, xsize = const.COLLISION_SIZE, ysize = const.COLLISION_SIZE, zsize = const.COLLISION_SIZE, circle = False):
        if id in self.positions:
            old = self.positions[id]
            self.ids.pop(old[0], old[1], 0, id)
        px = int(x / self.block_size)
        py = int(y / self.block_size)
        self.positions[id] = (px, py, x, y, z, xsize, ysize, zsize, circle)
        self.ids.put(px, py, 0, id)

    def deleteId(self, id):
        if id in self.positions:
            pos = self.positions[id]
            self.ids.pop(pos[0], pos[1], 0, id)

    def getNear(self, x, y, z, dist):
        if dist > self.block_size:
            raise Exception("Maximum accurate distance : %f, %f given" % (self.block_size, dist))
        result = []
        px = int(x / self.block_size)
        py = int(y / self.block_size)
        for offsets in self.check_grid:
            id_list = self.ids.get(px + offsets[0], py + offsets[1], 0)
            if id_list:
                for id in id_list:
                    pos = self.positions[id]
                    xdist = abs(pos[2] - x)
                    ydist = abs(pos[3] - y)
                    zdist = abs(pos[4] - z)
                    if dist < 0.5:
                        zdist /= 2
                    if xdist < dist and ydist < dist and zdist < dist:
                        result.append(id)
        return result

    def getCircleNear(self, x, y, z, dist):
        if dist > self.block_size:
            raise Exception("Maximum accurate distance : %f, %f given" % (self.block_size, dist))
        result = []
        px = int(x / self.block_size)
        py = int(y / self.block_size)
        dist *= dist
        for offsets in self.check_grid:
            id_list = self.ids.get(px + offsets[0], py + offsets[1], 0)
            if id_list:
                for id in id_list:
                    pos = self.positions[id]
                    rdist = (pos[2] - x) ** 2 + (pos[3] - y) ** 2 + (pos[4] - z) ** 2
                    if rdist < dist:
                        result.append(id)
        return result

    def isWalkable(self, id, pos):
        return self.isValueWalkable(id, pos.getX(), pos.getY(), pos.getZ())

    def isNodeWalkable(self, id, node):
        return self.isValueWalkable(id, node['x'], node['y'], node['z'])

    def isValueWalkable(self, id, x, y, z):
        px = int(x / self.block_size)
        py = int(y / self.block_size)
        for offsets in self.check_grid:
            unit_list = self.ids.get(px + offsets[0], py + offsets[1], 0)
            if unit_list:
                for unit_id in unit_list:
                    if unit_id != id:
                        unit = self.positions[unit_id]
                        xdist = abs(unit[2] - x)
                        ydist = abs(unit[3] - y)
                        zdist = abs(unit[4] - z)
                        if (xdist < unit[5]
                            and ydist < unit[6]
                            and zdist < unit[7]
                        ):
                            if id in self.positions:
                                cunit = self.positions[id]
                                if (abs(unit[2] - cunit[2]) > xdist
                                    or abs(unit[3] - cunit[3]) > ydist
                                    or abs(unit[4] - cunit[4]) > zdist
                                ):
                                    return False
                            else:
                                return False
        return True

"""
    Do some benchmarks
"""

def do_bench():
    import random
    tt = utils.Timer('CollisionTree benchmark total')
    inserts = 1000
    ctrees = [
        CollisionTree(20.0),
        # CollisionTree(40.0),
        # CollisionTree(60.0),
    ]
    ntrees = len(ctrees)
    minpos, maxpos = 0, 256.
    diffpos = maxpos - minpos
    def rpos():
        return random.random() * diffpos + minpos
    def rsize():
        return random.random() + 0.5
    # Insert
    tinsert = utils.Timer('CollisionTree benchmark insert (x%d on %d trees)' % (inserts, ntrees))
    for i in xrange(inserts):
        for ctree in ctrees:
            ctree.setPosValue(i, rpos(), rpos(), rpos(), rsize(), rsize(), rsize())
    tinsert.dump()
    # getNear
    accesses = 10000
    taccess = utils.Timer('CollisionTree benchmark getNear (x%d on %d trees)' % (accesses, ntrees))
    for i in xrange(accesses):
        for ctree in ctrees:
            ctree.getNear(rpos(), rpos(), rpos(), 10.0)
    taccess.dump()
    # getCircleNear
    taccess = utils.Timer('CollisionTree benchmark getCircleNear (x%d on %d trees)' % (accesses, ntrees))
    for i in xrange(accesses):
        for ctree in ctrees:
            ctree.getCircleNear(rpos(), rpos(), rpos(), 10.0)
    taccess.dump()
    # isValueWalkable
    taccess = utils.Timer('CollisionTree benchmark isValueWalkable (x%d on %d trees)' % (accesses, ntrees))
    for i in xrange(accesses):
        for ctree in ctrees:
            ctree.isValueWalkable(random.randint(0, accesses), rpos(), rpos(), rpos())
    taccess.dump()
    tt.dump()