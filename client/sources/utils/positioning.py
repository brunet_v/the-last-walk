import sources.config.constants as const
import panda3d.core as engine

"""
    /!\ Positionning functions with reverse rotation used everywhere
    (probably shouldn't be edited)
"""
def position_node(node, xpos, ypos, zpos, rotation, nsize, zinversion = False, parent = None):
    node.setHpr(rotation * -90, 0, 0)
    corresp = [
        [0, 0],
        [0, 1],
        [1, 1],
        [1, 0],
    ]
    if parent:
        node.setPos(
            parent,
            xpos + corresp[rotation][0] * nsize,
            ypos + corresp[rotation][1] * nsize,
            zpos
        )
    else:
        node.setPos(
            xpos + corresp[rotation][0] * nsize,
            ypos + corresp[rotation][1] * nsize,
            zpos
        )

def position_item(node, type, x, y, z, h, p, r, mode, offset_seed=0, bones=None):
    offsets = (0, 0, 0, 0, 0, 0, None)
    item_datas = const.ITEM_STATS[type]
    if "display_offsets" in item_datas and mode in item_datas["display_offsets"]:
        offsets = item_datas["display_offsets"][mode]
        if mode == "on_back":
            offsets = offsets[offset_seed % len(offsets)]
    node.setHpr(h + offsets[0], p + offsets[1], r + offsets[2])
    node.setPos(
        (x + offsets[3]),
        (y + offsets[4]),
        (z + offsets[5])
    )
    if bones:
        bone = "Right Handgrip"
        if mode == "on_back":
            bone = "Torso"
        if len(offsets) > 6 and offsets[6]:
            if offsets[6] in bones:
                bone = offsets[6]
        node.reparentTo(bones[bone])

def position_fog_texture(node, texture_stage, xpos, ypos, rotation, world_size):
    texture_ratio = 1.0 / float(const.BLOCKS_PER_SIZE * world_size)
    node.setTexScale(texture_stage, texture_ratio, texture_ratio)
    node.setTexGen(texture_stage, engine.TexGenAttrib.MWorldPosition)
