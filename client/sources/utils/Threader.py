import direct.task.Task as task
import time
import sources.objects.utils as utils

next_threader_id = 1

"""
    Used as a fake-thread parent class,
    will create a task that call "run" method every frame

    (Usefull to do recurrent unrelated actions)
"""
class Threader(object):

    def __init__(self, name):
        utils.InstanceCounter.add("Threader")
        global next_threader_id
        self.id = "%s(%d)" % (name, next_threader_id)
        self.removed = False
        self.running = True
        self.framing = False
        self.name = name
        self.frametime = time.time()
        taskMgr.add(self.frame, self.id, sort=next_threader_id)
        next_threader_id += 1

    def frame(self, tasks):
        self.framing = True
        if self.running:
            # print "Frame of %s" % self.name
            now = time.time()
            delay = now - self.frametime
            self.run(delay)
            self.frametime = now
            self.framing = False
            return task.cont
        self.framing = False
        return task.done

    def stop(self):
        self.running = False

    def __del__(self):
        utils.InstanceCounter.rm("Threader")
        self.clear()

    def clear(self):
        self.stop()
        if not self.removed:
            while self.framing:
                time.sleep(0.001)
            taskMgr.remove(self.id)
            self.removed = True
