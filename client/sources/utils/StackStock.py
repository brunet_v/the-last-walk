import sources.objects.utils as utils

"""
    Small algorithm used for item position stacking over player,
    use to position a key in the smallest position in a list (and delete)
"""
class StackStock(object):

    def __init__(self):
        utils.InstanceCounter.add("StackStock")
        self.datas = {}

    def __del__(self):
        utils.InstanceCounter.rm("StackStock")
        self.clear()

    def clear(self):
        self.datas = {}

    def addKey(self, stack, id):
        if stack not in self.datas:
            self.datas[stack] = []
        stack_length = len(self.datas[stack])
        for i in xrange(0, stack_length):
            if self.datas[stack][i] == None:
                self.datas[stack][i] = id
                return i
        self.datas[stack].append(id)
        return stack_length

    def delKey(self, stack, id):
        if stack in self.datas:
            for i in xrange(0, len(self.datas[stack])):
                if self.datas[stack][i] == id:
                    self.datas[stack][i] = None
