import panda3d.core as engine
import sources.config.constants as const
import sources.objects.utils as utils

"""
    Handle mouse picking by getting random close item/door under mouse
    or getting direct x-y world position
"""
class MousePicker(object):

    def __init__(self):
        utils.InstanceCounter.add("Picker")
        self.z = 0
        self.near_items = {}
        self.near_doors = {}
        self.last_item = None
        self.last_door = None

    def __del__(self):
        utils.InstanceCounter.rm("Picker")
        self.clear()

    def clear(self):
        self.near_items = {}
        self.near_doors = {}
        self.last_item = None
        self.last_door = None

    """
        Transform the screen x-y mouse position into a 3d world x-y-z position
        by testing ray-collision between camera and the infinite plane defined by:
            z={constant passed as argument}
    """
    def pick(self, z):
        plane = engine.Plane(engine.Vec3(0, 0, 1), engine.Point3(0, 0, z))
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            pos3d = engine.Point3()
            nearPoint = engine.Point3()
            farPoint = engine.Point3()
            base.camLens.extrude(mpos, nearPoint, farPoint)
            if plane.intersectsLine(
                pos3d,
                render.getRelativePoint(camera, nearPoint),
                render.getRelativePoint(camera, farPoint)
            ):
                return pos3d
        return None

    """
        Update near items/near doors list (so precise testing is faster)
    """
    def updateLists(self, world, player):
        # near items update
        self.near_items = {}
        inears = world['items']['tree'].getCircleNear(
            player.getX(), player.getY(), player.getZ(), const.OBJECT_GET_DISTANCE
        )
        for id in inears:
            item = world['items']['list'][id]
            self.near_items[id] = item
        # near doors update
        self.near_doors = {}
        dnears = world['doors'].dtree.getNear(
            player.getX(), player.getY(), player.getZ(), const.DOOR_OPEN_DIST
        )
        for id in dnears:
            door = world['doors'].doors[id]
            self.near_doors[id] = door

    """
        Get the closest item to the mouse cursor within a reasonable range
    """
    def getMouseItem(self):
        selected_item = None
        selected_id = None
        selected_dist = None
        collision_size = const.OBJECT_GET_MARGIN ** 2
        for id, item in self.near_items.iteritems():
            cpos = self.pick(item.getZ())
            if cpos:
                cdist = cpos - item.getPos()
                ddist = cdist.lengthSquared()
                if ddist < collision_size:
                    if not selected_dist or ddist < selected_dist:
                        selected_item = item
                        selected_id = id
                        selected_dist = ddist
        if self.last_item and self.last_item != selected_item:
            self.last_item.setColorScale(1.0, 1.0, 1.0, 1.0)
        if selected_item:
            selected_item.setColorScale(2.5, 2.5, 2.5, 1.0)
        self.last_item = selected_item
        return selected_id

    """
        Get the closest door to the mouse cursor within a reasonable range
    """
    def getMouseDoor(self):
        selected_door = None
        selected_id = None
        selected_dist = None
        collision_size = 1.0
        for id, door in self.near_doors.iteritems():
            cpos = self.pick(door.z + 1.0)
            if cpos and door.pos:
                cpos.setZ(door.z)
                cdist = cpos - door.pos
                ddist = cdist.lengthSquared()
                if ddist < collision_size:
                    if not selected_dist or ddist < selected_dist:
                        selected_door = door.door_model
                        selected_id = id
                        selected_dist = ddist
        if self.last_door and self.last_door != selected_door:
            self.last_door.setColorScale(1.0, 1.0, 1.0, 1.0)
        if selected_door:
            selected_door.setColorScale(2.5, 2.5, 2.5, 1.0)
        self.last_door = selected_door
        return selected_id
