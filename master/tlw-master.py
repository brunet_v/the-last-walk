#!/usr/bin/python
import os
import atexit
import traceback
import sources.logger as logger

from sources.master import Master

def onexit():
    logger.critical("Stopping master")

def main():
    log_dir = '/home/tlw/logs'
    logger.create_logger(log_dir, 'tlw-master')
    logger.critical("Starting master")
    atexit.register(onexit)
    try:
        master = Master("", 16547)
        atexit.register(master.clear)
        master.serve()
    except Exception as e:
        err_file = "%s/tlw-master-%d.error" % (log_dir, os.getpid())
        logger.critical("Crash with: %s (details in %s)" % (str(e), err_file))
        with open(err_file, "w") as f:
            f.write("%s\n" % traceback.format_exc())
            f.write("-> %s\n" % str(e))

main()
