from threading import Thread
from struct import Struct
from Queue import Queue

import ssl
import socket
import sources.utils.instancecounter as inst

"""
    Listen for clients connections
"""
class Listener(Thread):

    def __init__(self, host, port):
        inst.InstanceCounter.add("Listener")
        super(Listener, self).__init__()
        self.running = True
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((host, port))
        self.socket.listen(5)
        self.connections = Queue()
        self.daemon = True

    def __del__(self):
        inst.InstanceCounter.rm("Listener")
        self.socket.close()

    def run(self):
        while self.running:
            try:
                socket, addr = self.socket.accept()
                socket = ssl.wrap_socket(socket,
                    server_side=True,
                    certfile="cert.pem",
                    keyfile="cert.pem",
                    ssl_version=ssl.PROTOCOL_TLSv1,
                )
                self.connections.put(Struct({
                    "socket": socket,
                    "addr": addr,
                }))
            except:
                pass

    def stop(self):
        self.running = False

    def empty(self):
        return self.connections.empty()

    def get(self):
        try:
            connection = self.connections.get()
        except Empty:
            return None
        return connection
