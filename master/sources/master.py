from database import Database
from listener import Listener
from client import Client
from select import select

import os
import sys
import time
import sources.utils.instancecounter as inst
import sources.logger as logger

"""
    Base class of the master server
"""
class Master():

    def __init__(self, host, port):
        inst.InstanceCounter.add("Master")
        self.listener = Listener(host, port)
        self.listener.start()
        self.clients = {}
        self.running = True
        if len(sys.argv) > 1 and sys.argv[1] == "debug":
            # Debug environment
            self.database = Database(
                '127.0.0.1',    # Host
                'epic', '', # Login / Pass
                'epic'          # Database
            )
        else:
            # Normal environment
            self.database = Database(
                '127.0.0.1',            # Host
                'root', 'r5MHw9s8I4Fd', # Login / Pass
                'tlw-website'           # Database
            )
        self.database.start()
        # Dump pid
        with open("./master.pid", "w") as f:
            f.write("%d\n" % os.getpid())

    def __del__(self):
        inst.InstanceCounter.rm("Master")
        self.listener.stop()
        self.database.stop()

    def clear(self):
        for socket, client in self.clients.iteritems():
            client.clear()
        self.clients = {}

    def serve(self):
        while self.running:
            self.sync()

    def sync(self):
        log_time = time.time()
        check_time = time.time()
        while self.running:
            # Accept incomming connections
            while not self.listener.empty():
                new_connection = self.listener.get()
                if new_connection:
                    new_client = Client(new_connection, self.database)
                    self.clients[new_client.socket] = new_client
                    logger.notif('Client connected: %s (%d)' % new_client.addr)
            # List connected clients
            sockets = self.clients.keys()
            # Log activity
            if log_time < time.time():
                ldelay = 60.0
                log_time = time.time() + ldelay
                logger.info("Selecting on : %d sockets (%03.2f p/s)" % (
                    len(sockets),
                    float(Client.total_packets) / ldelay
                ))
                Client.total_packets = 0
                if len(sys.argv) > 1 and sys.argv[1] == "debug":
                    inst.InstanceCounter.dump()
            # Check for dead clients
            deconnected = []
            if check_time < time.time():
                check_time = time.time() + 3.0
                for socket, client in self.clients.iteritems():
                    if not client.check_alive():
                        deconnected.append(socket)
            # Compute client requests
            if len(sockets) > 0:
                self.select(sockets, deconnected)
            else:
                time.sleep(0.01)
            # Remove disconnected clients
            for socket in deconnected:
                if socket in self.clients:
                    client = self.clients.pop(socket)
                    logger.notif('Client disconnected: %s (%d)' % client.addr)
                    client.clear()

    def select(self, sockets, deconnected):
        read, write, error = select(sockets, [], sockets, 2.0)
        # Read socket input
        for socket in read:
            try:
                client = self.clients[socket]
                if not client.receive():
                    deconnected.append(socket)
            except Exception, e:
                logger.debug("Client network error: %s" % str(e))
                deconnected.append(socket)
        # Remove disconnected/bugged clients
        for socket in error:
            if socket in self.clients:
                deconnected.append(socket)

