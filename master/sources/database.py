from threading import Thread
from Queue import Queue
from struct import Struct

import MySQLdb as sql
import time
import sources.utils.instancecounter as inst

"""
    Database wrapper
"""
class Database(Thread):

    def __init__(self, host, login, passwd, db):
        inst.InstanceCounter.add("Database")
        super(Database, self).__init__()
        self.host = host
        self.login = login
        self.passwd = passwd
        self.db = db
        self.running = True
        self.daemon = True
        self.queue = Queue()
        self.connect()

    def __del__(self):
        inst.InstanceCounter.rm("Database")
        self.connection.close()

    def connect(self):
        self.connection = sql.connect(self.host, self.login, self.passwd, self.db)
        self.connection.autocommit(True)

    def execute(self, datas):
        try:
            cursor = self.connection.cursor()
            cursor.execute(datas.request[0], datas.request[1:])
        except (AttributeError, sql.OperationalError):
            self.connect()
            cursor = self.connection.cursor()
            cursor.execute(datas.request[0], datas.request[1:])
        if datas.callback:
            datas.callback[0](cursor.fetchall(), *(datas.callback[1:]))
        cursor.close()

    def run(self):
        while self.running:
            if not self.queue.empty():
                while not self.queue.empty():
                    datas = self.queue.get()
                    if datas:
                        self.execute(datas)
            else:
                time.sleep(0.01)

    def async(self, request, callback=None):
        self.queue.put(Struct({
            "request": request,
            "callback": callback,
        }))

    def stop(self):
        self.running = False
