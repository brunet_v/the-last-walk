import time
import random
import sources.utils.instancecounter as inst

"""
    Handle lobbys datas
"""
class LobbyHandler():

    trusts = set()
    others = set()

    existing_lists = {
        "trusted": {
            "time": time.time() + 2.0,
            "data": [],
        },
        "others": {
            "time": time.time() + 2.0,
            "data": [],
        },
        "all": {
            "time": time.time() + 2.0,
            "data": [],
        },
    }

    def __init__(self, connect, trusted, data, addr):
        inst.InstanceCounter.add("LobbyHandler")
        self.connect = connect
        self.trusted = trusted
        self.data = data
        self.data['addr'] = addr[0]
        self.data['players'] = "0"
        if self.trusted > 0:
            LobbyHandler.trusts.add(self)
        else:
            LobbyHandler.others.add(self)

    def __del__(self):
        inst.InstanceCounter.rm("LobbyHandler")
        self.clear()

    def clear(self):
        if self.connect:
            if self.trusted > 0:
                LobbyHandler.trusts.remove(self)
            else:
                LobbyHandler.others.remove(self)
        self.connect = None

    def input(self, type, data):
        if type == 'lupdate':
            self.data['players'] = str(data['players'])
        if type == 'lcheck':
            if data["login"] in ClientHandler.by_login:
                client = ClientHandler.by_login[data["login"]]
                if client.connect.addr[0] == data["address"]:
                    return None
            self.connect.output("logfail", data)

    @staticmethod
    def build_list(type):
        if type == "trusted":
            ldata = LobbyHandler.existing_lists["trusted"]
            ldata["data"] = []
            for lobby in LobbyHandler.trusts:
                ldata["data"].append(lobby.data)
        if type == "others":
            ldata = LobbyHandler.existing_lists["others"]
            ldata["data"] = []
            for lobby in LobbyHandler.others:
                ldata["data"].append(lobby.data)
        if type == "all":
            ldata = LobbyHandler.existing_lists["all"]
            ldata["data"] = []
            for lobby in LobbyHandler.trusts:
                ldata["data"].append(lobby.data)
            for lobby in LobbyHandler.others:
                ldata["data"].append(lobby.data)

    @staticmethod
    def get_list(type):
        if type in LobbyHandler.existing_lists:
            ldetail = LobbyHandler.existing_lists[type]
            if time.time() > ldetail["time"]:
                ldetail["time"] = time.time() + 2.0
                LobbyHandler.build_list(type)
            return ldetail["data"]
        return None


"""
    Handle client datas
"""
class ClientHandler():

    connected = set()
    channels = {}
    chan_logins = {}
    by_login = {}

    def __init__(self, connect, login):
        inst.InstanceCounter.add("ClientHandler")
        self.connect = connect
        self.login = login
        self.chan = None
        self.friend_list = {}
        self.friend_init = False
        self.connect.database.async((
                # Request for friend list
                "SELECT friend FROM friend_list WHERE player = %s",
                self.login
            ), (
                # Callback
                self.get_friend_list_from_db,
                login
            ))
        ClientHandler.connected.add(self.login)
        ClientHandler.by_login[self.login] = self

    def __del__(self):
        inst.InstanceCounter.rm("ClientHandler")  
        # se signaler offline aux player ayant ce joueur en ami.
        # clean friend list DB
        self.clear()

    def clear(self):
        self.update_friend_db()
        self.notif_friend("offline")
        self.connect = None
        self.unjoin()
        if self.login:
            ClientHandler.connected.remove(self.login)
            ClientHandler.by_login.pop(self.login)
            self.login = None

    def notif_friend(self, status):
        for login, info in self.friend_list.iteritems():
            friend = ClientHandler.by_login.get(login, False)
            if friend != False:
                friend.friend_list[self.login]["status"] = status

    def get_friend_list_from_db(self, result, login):
        for friend in result:
            if ClientHandler.by_login.get(friend[0], False) == False:
                status = "offline"
            else:
                status = "online"
            self.friend_list[friend[0]] = {
                "status" : status,
                "new" : False,
                "del" : False
            }
        self.friend_init = True
        self.notif_friend("online")

    def update_friend_db(self):
        print "Update Friends"
        for login, value in self.friend_list.iteritems():
            if value["new"] == True:
                self.connect.database.async((
                    "INSERT INTO friend_list (id, player, friend) VALUES (NULL, %s, %s);",
                    self.login,
                    login
                ))
                value["new"] = False
            if value["del"] == True:
                self.connect.database.async((
                    "DELETE FROM friend_list WHERE player = %s AND friend = %s",
                    self.login,
                    login
                ))

    def refresh_chan_login(self, channel):
        ClientHandler.chan_logins[channel] = []
        if channel in ClientHandler.channels:
            for client in ClientHandler.channels[channel]:
                ClientHandler.chan_logins[channel].append(client.login)
        if not len(ClientHandler.chan_logins[channel]):
            ClientHandler.chan_logins.pop(channel)

    def unjoin(self):
        if self.chan:
            if self.chan in ClientHandler.channels:
                qchan = ClientHandler.channels[self.chan]
                qchan.remove(self)
                if len(qchan) <= 0:
                    ClientHandler.channels.pop(self.chan)
            self.refresh_chan_login(self.chan)
            self.chan = None

    def join(self, channel):
        if self.connect:
            self.unjoin()
            if channel not in ClientHandler.channels:
                ClientHandler.channels[channel] = set()
            ClientHandler.channels[channel].add(self)
            self.connect.output("jchan", {
                'chan': channel,
            })
            self.refresh_chan_login(channel)
            self.chan = channel

    def add_friend(self, data):
        friend = ClientHandler.by_login.get(data["login"], False)
        if  friend == False or friend.friend_list.get(self.login, False) == False:
            status = "offline"
        else:
            friend.friend_list[self.login]["status"] = "online"
            status = "online"
        self.friend_list[data["login"]] = {
        "status" : status,
        "new" : True,
        "del" : False}
        self.output("friend_list", self.friend_list)

    def del_friend(self, data):
        if self.friend_list.get(data["login"], False) != False:
            self.friend_list[data["login"]]["del"] = True

    def get_friend(self):
        self.output("friend_list", self.friend_list)

    def input(self, type, data):
        if type == "msg" and self.chan and data["msg"]:
            msg = data["msg"][:500]
            for client in ClientHandler.channels[self.chan]:
                client.output("mchan", {
                    'login': self.login,
                    'msg': msg,
                })
        if type == "/join" or type == "/j" and data["arg"]:
            self.join(data["arg"])
        if type == "slist":
            self.output("slist", {
                "list": LobbyHandler.get_list(data["type"]),
            })
        if type == "clist" and self.chan:
            self.output("clist", {
                "usrs": ClientHandler.chan_logins[self.chan],
            })
        if type == "unjoin":
            self.unjoin()
        if type == "serverRequest":
            self.connect.database.async((
                # Request if client is allowed to create server
                "SELECT COUNT(id) FROM fbb_users WHERE username = %s AND right_server_creator = 1",
                self.login,
            ), (
                # Callback
                ServerFactoryHandler.request_server,
                data,
                self
            ))

        if type == "add_friend" and self.friend_init:
            if not self.friend_list.get(data["login"], False):
                self.output("error", {"error" : "Already a friend"})
            else:
                self.add_friend(data)

        if type == "dell_friend" and self.friend_init:
            self.del_friend(data)

        if type == "get_friend" and self.friend_init:
            self.get_friend()

    def output(self, type, data):
        self.connect.output(type, data)

"""
    Handle ServerFactory datas
"""

class ServerFactoryHandler():

    factory = []
    port = 22458 + 1001

    def __init__(self, connect):
        inst.InstanceCounter.add("ServerFactoryHandler")
        self.servers = 0
        self.connect = connect
        ServerFactoryHandler.factory.append(self)

    def __del__(self):
        inst.InstanceCounter.rm("ServerFactoryHandler")
        self.clear()

    def clear(self):
        ServerFactoryHandler.factory.remove(self)

    @staticmethod
    def request_server(result, data, client):
        print len(ServerFactoryHandler.factory)
        if result[0][0] and len(ServerFactoryHandler.factory) > 0:
            #Check if login is allowed to create server
            factory = random.choice(ServerFactoryHandler.factory)
            if ServerFactoryHandler.port >= 22458 + 2000:
                ServerFactoryHandler.port = 22458 + 1001
            else:
                ServerFactoryHandler.port += 1
            factory.output("request", {
                'login' : client.login,
                'size' : data["size"],
                'port' : ServerFactoryHandler.port,
            })
        elif result[0][0] == 0:
            client.output("requestKO", {
                'error' : "You are not allowed to create a server",
            })
        else:
            client.output("requestKO", {
                'error' : "There is no factory available",
            })

    def input(self, type, data):
        if type == "requestOK":
            ClientHandler.by_login[data['login']].output("requestOK", {
                'ip' : data['ip'],
                'port' : data['port'],
           })
        if type == "requestKO":
            ClientHandler.by_login[data['login']].output("requestKO", {
                'error' : "Unabled to create a server, retry later",
            })
        if type == "serverfull":
            result = []
            result[0][0] = 1
            ServerFactoryHandler.request_server(result, data, ClientHandler.by_login[data['login']])

    def output(self, type, data):
        self.connect.output(type, data)