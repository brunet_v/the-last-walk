
"""
    Utility class used to convert dict to usable objects \o/
"""
class Struct:
    def __init__(self, entries):
        self.__dict__.update(**entries)
