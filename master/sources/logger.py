import datetime

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[1;%dm"
BOLD_SEQ = "\033[1m"
COLORS = {
    'CRITICAL': YELLOW,
    'ERROR': RED,
    'WARNING': YELLOW,
    'INFO': BLUE,
    'NOTIF': CYAN,
    'DEBUG': GREEN,
}

file_path = None
file_name = None

def create_logger(path, name):
    global file_path
    global file_name
    file_path = path
    file_name = name

def format(levelname):
    if levelname in COLORS:
        levelname_color = (COLOR_SEQ % (30 + COLORS[levelname])) + levelname + RESET_SEQ
        if levelname in ['CRITICAL',]:
            levelname_color = formatter_message("\033[41m%s" % levelname_color)
        return levelname_color
    return levelname

def formatter_message(message, use_color = True):
    if use_color:
        message = message.replace("$RESET", RESET_SEQ).replace("$BOLD", BOLD_SEQ)
    else:
        message = message.replace("$RESET", "").replace("$BOLD", "")
    return message

def append_log(level, msg):
    try:
        f = formatter_message("[$BOLD%s$RESET] [%s] %s\n")
        now = datetime.datetime.now()
        log = f % (
            now.strftime('%H:%M:%S'),
            format(level),
            msg
        )
        if file_path and file_name:
            logfile = "%s/%s-%s.log" % (file_path, file_name, now.strftime('%Y-%m-%d'))
            with open(logfile, "a") as f:
                f.write(log)
            if level in ['CRITICAL', 'ERROR', 'WARNING']:
                errorfile = "%s.error" % logfile
                with open(errorfile, "a") as f:
                    f.write(log)
    except:
        pass

def debug(msg):
    append_log("DEBUG", msg)

def notif(msg):
    append_log("NOTIF", msg)

def info(msg):
    append_log("INFO", msg)

def warning(msg):
    append_log("WARNING", msg)

def error(msg):
    append_log("ERROR", msg)

def critical(msg):
    append_log("CRITICAL", msg)
