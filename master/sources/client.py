import socket
import handlers
import json
import time

import sources.utils.instancecounter as inst
import sources.logger as logger

"""
    Base client class
"""
class Client():

    total_packets = 0

    def __init__(self, connection, database):
        inst.InstanceCounter.add("Client")
        self.socket = connection.socket
        self.addr = connection.addr
        self.buffer = ""
        self.handler = None
        self.database = database
        self.alive = True
        self.last_alive = time.time() + 25.0
        self.hearbeat_sent = False
        self.quota = 0

    def __del__(self):
        inst.InstanceCounter.rm("Client")
        self.clear()

    def clear(self):
        self.alive = False
        if self.socket:
            try:
                self.socket.shutdown(socket.SHUT_RDWR)
            except socket.error:
                pass
            self.socket.close()
            self.socket = None
        if self.handler:
            self.handler.clear()
            self.handler = None

    def check_alive(self):
        if time.time() + 10.0 > self.last_alive:
            if not self.hearbeat_sent:
                self.hearbeat_sent = True
                try:
                    self.send("alive?", {})
                except:
                    self.alive = False
        if time.time() > self.last_alive:
            self.alive = False
        return self.alive

    def send(self, type, data):
        self.output(type, data)

    def receive(self):
        try:
            buf = self.socket.read(1024)
            if not buf:
                return False
            if len(self.buffer) < 10000:
                self.buffer += buf
                self.quota += 1
            else:
                return False
            while len(self.buffer) > 9:
                try:
                    size = int(self.buffer[:8]) + 9
                    if len(self.buffer) > size:
                        datas = self.buffer[9:size]
                        self.buffer = self.buffer[size + 1:]
                        loaded = json.loads(datas)
                        if len(loaded) >= 2:
                            Client.total_packets += 1
                            self.input(loaded[0], loaded[1])
                    else:
                        break
                except Exception as e:
                    logger.debug("Client receive error: %s" % str(e))
        except socket.error:
            return False
        return True

    def identification(self, type, data):
        # Verify user identifiers
        if type == "player":
            self.database.async((
                # Request if valid login
                "SELECT COUNT(id) FROM fbb_users " +
                "WHERE (BINARY username = %s OR email = %s) AND BINARY password = %s",
                data["login"], data["login"], data["pass"],
            ), (
                # Callback data
                self.login_result,
                data,
            ))
            self.quota += 100
        elif type == "lobby":
            self.database.async((
                # Request if trusted server
                "SELECT COUNT(id) FROM trusted_addr WHERE addr = %s",
                self.addr[0],
            ), (
                # Callback
                self.grade_result,
                data,
            ))
            self.quota += 100
        elif type == "serverFactory":
            self.database.async((
                # Request if trusted serverFactory
                "SELECT COUNT(id) FROM trusted_addr WHERE addr = %s",
                self.addr[0],
            ), (
                # Callback
                self.serverFactory_result,
                data,
            ))
            self.quota += 100

    def input(self, type, data):
        if self.alive:
            self.hearbeat_sent = False
            self.last_alive = time.time() + 25.0
            if type == "alive?":
                self.output("heartbeat", {})
            else:
                if self.handler:
                    self.handler.input(type, data)
                else:
                    self.identification(type, data)

    def output(self, type, data):
        dump = json.dumps((type, data))
        self.socket.write("%08d|%s@" % (len(dump), dump))

    def login_result(self, result, data):
        if self.alive:
            if result[0][0] and data["login"] not in handlers.ClientHandler.connected:
                self.send("login_success", {
                    "login": data["login"],
                })
                self.handler = handlers.ClientHandler(self, data["login"])
            else:
                self.send("login_failure", {})

    def grade_result(self, result, data):
        if self.alive:
            self.handler = handlers.LobbyHandler(self, result[0][0], data, self.addr)

    def serverFactory_result(self, result, data):
        if self.alive:
            if result[0][0]:
                self.handler = handlers.ServerFactoryHandler(self)