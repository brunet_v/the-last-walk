
"""
    Utility class used to help finding leaks (contain number of alive class by name)
"""
class InstanceCounter(object):

    names = {}

    instances = {}

    @staticmethod
    def add(name):
        if name not in InstanceCounter.names:
            InstanceCounter.names[name] = 0
        InstanceCounter.names[name] += 1

    @staticmethod
    def rm(name):
        if name in InstanceCounter.names:
            InstanceCounter.names[name] -= 1
            if InstanceCounter.names[name] == 0:
                InstanceCounter.names.pop(name)

    @staticmethod
    def add_instance(name, inst):
        InstanceCounter.instances[name] = inst

    @staticmethod
    def dump():
        # print InstanceCounter.names
        for name, number in InstanceCounter.names.iteritems():
            print "Persisting instances: x%d %s" % (number, name)
        for name, instance in InstanceCounter.instances.iteritems():
            InstanceCounter.dump_instance_referers(name)

    @staticmethod
    def dump_instance_referers(name):
        if name in InstanceCounter.instances:
            print name, "instances:"
            import gc
            for ref in gc.get_referrers(InstanceCounter.instances[name]):
                if not ref is InstanceCounter.instances:
                    print ref

    @staticmethod
    def clear():
        InstanceCounter.names = {}
