import os
import re
import sys
import shutil
import json
import glob
import hashlib
import compileall

"""
    Generate checksums file
"""
def listdirectory(path):
    fichier = []
    l = glob.glob(path+'/*')
    for i in l:
        if os.path.isdir(i): fichier.extend(listdirectory(i))
        else: fichier.append(i)
    return fichier

def get_hash(fichier):
    sha1 = hashlib.sha1()
    with open(fichier,'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            sha1.update(chunk)
    return sha1.hexdigest()

def getchecksum(fichier):
    dico = {}
    l = len(fichier)
    i = 0
    while i < l:
        if fichier[i] != "./get_master_json.py" and fichier[i] != "./index.html" and fichier[i] != "./master.json":
            dico[fichier[i]] = get_hash(fichier[i])
        i += 1
    return dico

def generate_checksums(path):
    path = path.replace("\\", "/")
    checksums = {}
    dico = getchecksum(listdirectory(path))
    for key, value in dico.iteritems():
        key = key.replace("\\", "/")
        key = key.replace(path, "./")
        print "Keysum for %s : %s" % (key, value)
        checksums[key] = value
    Version = file("%smaster.json" % path, "w")
    dump_json = json.dumps(checksums, indent=2, sort_keys=True, separators=(',', ': '))
    Version.write(dump_json)
    Version.close()


"""
    Export client folder for deployment
"""
def copy_dir(src, dst, ignored = []):
    sep = '/'
    if sys.platform == 'win32':
        sep = '\\'
        src = src.replace('/', sep)
        dst = dst.replace('/', sep)
    if not os.path.exists(dst):
        os.mkdir(dst)
    for files in os.listdir(src):
        good = True
        for pattern in ignored:
            if re.match("^%s$" % pattern, files):
                good = False
        if good:
            cfile = "%s%s" % (src, files)
            dfile = "%s%s" % (dst, files)
            if os.path.isdir(cfile):
                copy_dir("%s%s" % (cfile, sep), "%s%s" % (dfile, sep), ignored)
            else:
                print "Copying %s to %s" % (cfile, dfile)
                shutil.copyfile(cfile, dfile)

def deploy():

    """
        Delete and re-export a given directory
    """
    def reload_directory(src, dst, ignored_patterns):
        if os.path.exists(dst):
            shutil.rmtree(dst)
        copy_dir(src, dst, ignored_patterns)

    client_ignored_patterns = [
        "configuration.json",
        ".+\.mp",
        ".+\.py",
        "_.+\.egg",
        "TODO.txt",
        "CHANGELOG.txt",
        "README.TXT",
    ]
    if not os.path.exists("./deployed/"):
        os.mkdir("./deployed/")
    compileall.compile_dir('./client/', force=True)
    reload_directory("./client/", "./deployed/raw/", client_ignored_patterns)
    reload_directory("./client/", "./deployed/packed/", client_ignored_patterns)
    shutil.copyfile("./client/main.py", "./deployed/packed/main.py") # This is THE exception
    shutil.copyfile("./client/main.py", "./deployed/raw/main.py") # This is THE exception
    generate_checksums("./deployed/raw/")

deploy()
