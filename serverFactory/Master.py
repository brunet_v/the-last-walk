import socket
import json
import ssl
import Queue
import time
import utils

MASTER_ADDRESS = "master.thelastwalk.net"
MASTER_PORT = 16547

"""
    Handle interactions with Master server
"""
class Master(object):

    def __init__(self, type, data, blocking=False):
        utils.InstanceCounter.add("Master")
        self.locked = False
        self.connected = None
        self.buffer = ""
        self.alive = time.time() + 10.0
        self.input = Queue.Queue()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket = ssl.wrap_socket(self.socket,
            cert_reqs=ssl.CERT_NONE,
            ssl_version=ssl.PROTOCOL_TLSv1,
        )
        self.waiting_alive = False
        try:
            self.socket.connect((MASTER_ADDRESS, MASTER_PORT))
            self.socket.setblocking(blocking)
            self.connected = True
        except socket.error:
            self.connected = False
            if type == 'lobby':
                raise Exception("Unable to register server to master")
        if self.connected:
            self.send(type, data)

    def __del__(self):
        utils.InstanceCounter.rm("Master")
        self.clear()

    def clear(self):
        self.connected = False
        self.socket.close()

    def is_alive(self):
        if time.time() > self.alive:
            return False
        if self.connected == None:
            return True
        if time.time() + 7.0 > self.alive and not self.waiting_alive:
            self.waiting_alive = True
            self.send("alive?", {})
        return True

    def acquire(self):
        while self.locked:
            time.sleep(0.00001)
        self.locked = True

    def release(self, ret):
        self.locked = False
        return ret

    def send(self, type, data):
        self.acquire()
        if self.connected:
            try:
                dump = json.dumps((type, data))
                buff = "%08d|%s@" % (len(dump), dump)
                l = len(buff)
                i = 0
                while i < l:
                    i += self.socket.write(buff[i:])
            except Exception, e:
                print "Socket error on %s:" % type, str(e) # DEBUG
                self.waiting_alive = False
                self.alive -= 4.0
                return self.release(False)
        return self.release(True)

    def retrieve(self):
        self.acquire()
        try:
            buf = self.socket.read(1024)
            while buf:
                self.buffer += buf
                buf = self.socket.read(1024)
        except socket.error:
            pass
        try:
            while len(self.buffer) > 9:
                size = int(self.buffer[:8]) + 9
                if len(self.buffer) > size:
                    datas = self.buffer[9:size]
                    self.buffer = self.buffer[size + 1:]
                    loaded = json.loads(datas)
                    if len(loaded) >= 2:
                        self.waiting_alive = False
                        self.alive = time.time() + 15.0
                        if loaded[0] == "alive?":
                            self.mserver.send("heartbeat", {})
                        else:
                            self.input.put({
                                'type': loaded[0],
                                'data': loaded[1],
                            })
                else:
                    break
        except:
            pass
        if not self.input.empty():
            return self.release(self.input.get())
        return self.release(None)
