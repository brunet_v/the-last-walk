from Master import Master
import time
import os
import subprocess

ip = "88.191.133.249"
serverDir = "/home/tlw/thelastwalk-dev/server/"

def create_server(master, data):
	IGNORE = open("/dev/null", 'w')
	subprocess.Popen(["python", serverDir + "main.py", str(data["size"]), str(data["port"])], stdout=IGNORE)
	master.send("requestOK", {
		"login" : data["login"],
		"ip" : ip,
		"port" : data["port"],
		})

def handle_data(master, data):
	if data["type"] == "request":
		create_server(master, data["data"])

def main():
	with open("./serverFactory.pid", "w") as f:
            f.write("%d\n" % os.getpid())
	os.chdir(serverDir)
	master = Master("serverFactory", {})
	while 1:
		if master.is_alive():
			data = master.retrieve()
			if data != None:
				handle_data(master, data)
		else:
			master = Master("serverFactory", {})
		time.sleep(0.5)

main()