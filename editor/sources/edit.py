#!/usr/bin/env python
import direct.directbase.DirectStart
import panda3d.core as engine
import direct.task as task
import sources.utils.grid as grid
import sources.utils.export as export
import sources.utils.objects as objects
import sources.utils.constants as constants
import sources.utils.mouse as mouse
import sources.utils.load_resources as resources
import sources.utils.grass as grass
import sys
import copy
import math
import pickle
import random
import time

from direct.gui.OnscreenText import OnscreenText

last_edit = (-100, -100, -100)
render = render
base = base
taskMgr = taskMgr
loader = loader

"""
    This is the main editor file,
    will manage all real-time change in the editor view/map.

    This is probably the ulgiest code ever, but it works with relativly no lags
"""


"""
    Load map file into editor render zone
"""
def map_to_render(editor):
    map = editor['map']['blocks']
    # load map blocks
    for zkey in map.keys():
        for ykey in map[zkey].keys():
            for xkey in map[zkey][ykey].keys():
                node_list = map[zkey][ykey][xkey]
                for node in node_list:
                    if resources.GetBlockType(editor['blocks'], node['t']) != "animated":
                        new_node = resources.GetBlockModel(
                            editor['blocks'], node['t']
                        ).copyTo(editor['render_map'].get(node['x'], node['y'], node['z'], node['s']))
                    else:
                        new_node = resources.CopyActor(resources.GetBlockModel(
                            editor['blocks'], node['t']
                        ))
                    new_node.setTag('type', resources.GetBlockType(editor['blocks'], node['t']))
                    new_node.setTwoSided(True)
                    new_node.setScale(node.get('sc', 1.))
                    new_node.show()
                    if not node['s']:
                        new_node.hide(engine.BitMask32.bit(1))
                    # apply to match render system
                    objects.position_block(
                        new_node, node['x'], node['y'], node['z'], node['r']
                    )
                    # add to render node tree
                    objects.add_to_list_tree(
                        editor['render_tree'],
                        node['x'],
                        node['y'],
                        node['z'],
                        new_node
                    )
    if 'hoverings' in editor['map']:
        for hovering in editor['map']['hoverings']:
            new_node = resources.GetBlockModel(
                editor['blocks'], hovering['t']
            ).copyTo(editor['render_map'].get(hovering['x'], hovering['y'], hovering['z']))
            new_node.setTwoSided(True)
            new_node.setScale(hovering.get('sc', 1.))
            new_node.show()
            if not hovering['s']:
                new_node.hide(engine.BitMask32.bit(1))
            # pos to match render system
            objects.position_good(
                new_node, hovering['x'], hovering['y'], hovering['z'], hovering['r']
            )
            editor['view']['hoverings'][hovering['id']] = new_node
    else:
        editor['map']['hoverings'] = []
    editor['render_map'].update_all()


"""
    Init editor, create scene, load map into it,
    init editor basic datas
"""
def create_scene_grid(editor):
    map = editor['map']
    base.setFrameRateMeter(True)
    xsize = map['def']['xsize']
    ysize = map['def']['ysize']
    map_grid = grid.ThreeAxisGrid(xsize / 2, ysize / 2, 0, 16, 16)
    map_grid_node = map_grid.create()
    map_grid_node.setPos(xsize / 2, ysize / 2, 0)
    map_grid_node.hide(engine.BitMask32.bit(1))
    # map_grid_node.hide(engine.BitMask32.bit(0))
    map_grid_node.reparentTo(render)
    editor['view']['mgrid'] = map_grid_node
    editor['view']['used']['croot'] = render.attachNewNode('cursor_root')
    cursor = editor['view']['used']['croot']
    cursor.reparentTo(render)
    map_to_render(editor)
    sunlight = engine.DirectionalLight('sunlight')
    sunlight.setColor(engine.VBase4(constants.SUN_LIGHT_COLOR))
    sunlight.getLens().setFilmSize(xsize * 1.5, ysize * 1.5)
    sunlight.getLens().setFar(120)
    sunlight.getLens().setNear(30)
    sunlight.setShadowCaster(True, 2048, 2048)
    sunlight.setCameraMask(engine.BitMask32.bit(1))
    sunlightnp = render.attachNewNode(sunlight)
    ambiantlight = engine.AmbientLight('ambiantlight')
    ambiantlight.setColor(engine.Vec4(constants.AMBIENT_LIGHT_COLOR))
    ambiantlightnp = render.attachNewNode(ambiantlight)
    editor['view']['light']['node'] = sunlightnp
    render.setLight(sunlightnp)
    render.setLight(ambiantlightnp)
    render.setShaderAuto()
    placeholder = resources.LoadTexturedModel(loader, "./ui/placeholder.egg", "./ui/placeholder.png", nearest=True, alpha=True)
    placeholder.hide(engine.BitMask32.bit(1))
    placeholder.setTwoSided(True)
    placeholder.setLightOff()
    placeholder.reparentTo(cursor)
    editor['view']['used']['placeholder']['node'] = placeholder
    grasscursor = resources.LoadTexturedModel(loader, "./ui/grasscursor.egg", "./ui/grasscursor.png", nearest=True, alpha=True)
    grasscursor.setTwoSided(True)
    grasscursor.setPos(-0.5, -0.5, 0)
    grasscursor.hide()
    grasscursor2 = resources.LoadTexturedModel(loader, "./ui/grasscursor2.egg", "./ui/grasscursor.png", nearest=True, alpha=True)
    grasscursor2.setTwoSided(True)
    grasscursor2.setScale(0.5)
    grasscursor2.hide()
    grasscursor_dummy = cursor.attachNewNode('grass_cursor_dummy')
    grasscursor_dummy.hide(engine.BitMask32.bit(1))
    grasscursor_dummy.setLightOff()
    grasscursor.reparentTo(grasscursor_dummy)
    grasscursor2.reparentTo(grasscursor_dummy)
    editor['view']['used']['grasscursor']['node'] = grasscursor_dummy
    editor['view']['used']['grasscursor']['rect'] = grasscursor
    editor['view']['used']['grasscursor']['circle'] = grasscursor2
    editor['view']['grasshandler'] = grass.GrassEditor(map)
    tx = 332
    sy = 18
    def create_text(idx):
        n = OnscreenText(pos=(tx, -(sy + idx * 13)), scale=13, fg=(1, 1, 1, 1), align=engine.TextNode.ALeft)
        n.reparentTo(pixel2d)
        return n
    editor['view']['ui_texts']['rotation'] = create_text(0)
    editor['view']['ui_texts']['position'] = create_text(1)
    editor['view']['ui_texts']['model'] = create_text(2)
    editor['view']['ui_texts']['scale'] = create_text(3)


"""
    Update editor scene view (move cursor and camera)
    > Called each frame
"""
def update_map_view(editor_view):
    delay = time.time() - editor_view['frametime']
    editor_view['frametime'] = time.time()
    editor_view['zoom'] -= (editor_view['zoom'] - editor_view['ozoom']) * delay * 3.0
    editor_view['rotation'] -= (editor_view['rotation'] - editor_view['orotation']) * delay * 3.0
    graped = editor_view['graped']
    xpos = editor_view['xpos']
    ypos = editor_view['ypos']
    zpos = editor_view['zpos']
    r = editor_view['rotation']
    z = editor_view['zoom']
    base.camera.setPos(
        xpos + z * math.cos(math.radians(r)) + 0.5,
        ypos + z * math.sin(math.radians(r)) + 0.5,
        zpos + z * 1.5,
    )
    base.camera.lookAt(xpos + 0.5, ypos + 0.5, zpos)
    mpos = editor_view['mpicker'].pick(zpos)
    if mpos:
        # Move cursor position (depends on the mode)
        mxpos = mpos.getX()
        mypos = mpos.getY()
        if not editor_view['hover'] and not editor_view['grass']:
            ssize = editor_view['size'] - 1
            txpos = int(mxpos)
            typos = int(mypos)
        elif editor_view['aprox']:
            ssize = editor_view['size'] - 0.1
            aprox = 10.0
            txpos = float(int(mxpos * aprox)) / aprox
            typos = float(int(mypos * aprox)) / aprox
        else:
            ssize = editor_view['size'] - 0.05
            txpos = mxpos
            typos = mypos
        editor_view['txpos'] = max(0.0, min(txpos, ssize))
        editor_view['typos'] = max(0.0, min(typos, ssize))
        if graped:
            mtxpos = mxpos - editor_view['xpos']
            mtypos = mypos - editor_view['ypos']
            editor_view['xpos'] = graped[0] - (mtxpos - graped[2])
            editor_view['ypos'] = graped[1] - (mtypos - graped[3])
        editor_view['mpos'] = mpos
    return task.Task.cont


"""
    Update height display and edge-view
    > Called on user event
"""
def update_height_view(editor):
    editor_renders = editor['render_tree']
    for zkey, ytree in editor_renders.iteritems():
        for ykey, xtree in ytree.iteritems():
            for xkey, blist in xtree.iteritems():
                for node in blist:
                    if (zkey > editor['view']['vlimit']
                        or (node.getTag('type') == "admin"
                            and editor['view']['hide_specials'])):
                        node.setAlphaScale(0.0)
                    else:
                        node.setAlphaScale(1.0)
    for hovering in editor['map']['hoverings']:
        node = editor['view']['hoverings'][hovering['id']]
        if hovering['z'] > editor['view']['vlimit']:
            node.setAlphaScale(0.0)
        else:
            node.setAlphaScale(1.0)
    refresh_edges(editor)


"""
    Update scene edit-cursor details
    > Called on each frame
"""
def update_cursor_view(editor):
    editor['ui'].update(editor)
    view = editor['view']
    using = view['used']
    cursor = using['croot']
    cursor.setPos(view['txpos'], view['typos'], view['zpos'])
    if using['id'] != using['oid'] or using['rotation'] != using['orotation']:
        using['oid'] = using['id']
        using['orotation'] = using['rotation']
        if using['cnode']:
            using['cnode'].detachNode()
            using['cnode'].removeNode()
        if using['id']:
            if using['cnode']:
                using['cnode'].detachNode()
                using['cnode'].removeNode()
            if resources.GetBlockType(editor['blocks'], using['id']) != "animated":
                new_node = resources.GetBlockModel(
                    editor['blocks'], using['id']
                ).copyTo(using['croot'])
            else:
                new_node = resources.CopyActor(resources.GetBlockModel(
                    editor['blocks'], using['id']
                ), using['croot'])
            new_node.show()
            new_node.setAlphaScale(0.5)
            if view['hover']:
                objects.position_good(
                    new_node, 0, 0, 0, using['rotation']
                )
            else:
                objects.position_block(
                    new_node, 0, 0, 0, using['rotation']
                )
            using['cnode'] = new_node
        else:
            using['cnode'] = None
    # Every frame update
    if using['cnode']:
        # Shadowing
        if view['shadowing']:
            using['cnode'].show(engine.BitMask32.bit(1))
        else:
            using['cnode'].hide(engine.BitMask32.bit(1))
        # Scale
        using['cnode'].setScale(using['scale'])
    # add
    if using['adding']:
        editor_event_handler(editor, "add_block")
    # Grass mode
    if view['grass']:
        # Hide normal blocks
        if using['cnode']:
            using['cnode'].hide()
        # Adapt scale and display painting cursors
        using['grasscursor']['node'].setScale(using['scale'])
        using['grasscursor']['node'].show(engine.BitMask32.bit(0))
        if using['grasscursorcircle']:
            using['grasscursor']['circle'].show()
            using['grasscursor']['rect'].hide()
        else:
            using['grasscursor']['rect'].show()
            using['grasscursor']['circle'].hide()
    else:
        if using['cnode']:
            using['cnode'].show(engine.BitMask32.bit(0))
        using['grasscursor']['node'].hide(engine.BitMask32.bit(0))
    # placeholder
    if using['placeholder']['show'] and not view['grass']:
        using['placeholder']['node'].show(engine.BitMask32.bit(0))
        if view['hover']:
            using['placeholder']['node'].setHpr(using['rotation'], 0, 0)
        else:
            using['placeholder']['node'].setHpr(0, 0, 0)
    else:
        using['placeholder']['node'].hide(engine.BitMask32.bit(0))
    view['ui_texts']['rotation'].setText("Rotation : %.0f" % using['rotation'])
    view['ui_texts']['model'].setText("Object : %s" % using['id'])
    view['ui_texts']['scale'].setText("Scale : %s" % using['scale'])
    view['ui_texts']['position'].setText(
        "Position : X=%.1f, Y=%.1f, Z=%.0f" % (
        view['txpos'], view['typos'], view['zpos']
    ))
    return task.Task.cont

"""
    Update sunlight position
    > Called each frame
"""
def update_light_view(editor):
    sunlightnp = editor['view']['light']['node']
    xsize = editor['map']['def']['xsize']
    ysize = editor['map']['def']['ysize']
    sunlightnp.setPos(xsize / 2 + 40, ysize / 2 - 40, editor['view']['light']['height'])
    sunlightnp.lookAt(xsize / 2, ysize / 2, 0)
    return task.Task.cont


"""
    Handle sunlight movement events
    > Called on user event
"""
def light_event_handler(editor, type):
    light = editor['view']['light']
    if type == "light_up":
        light['height'] += 1
    if type == "light_down":
        light['height'] -= 1


"""
    Handle cursor, view editor events
    > Called on user event
"""
def view_event_handler(editor, type):
    view = editor['view']
    mdef = editor['map']['def']
    if type == "do-aprox":
        view['aprox'] = True
    if type == "undo-aprox":
        view['aprox'] = False
    if type == "up":
        view['zpos'] += 1
        view = editor['view']
        view['mgrid'].setPos(mdef['xsize'] / 2, mdef['ysize'] / 2, view['zpos'])
    if type == "down":
        view['zpos'] -= 1
        view = editor['view']
        view['mgrid'].setPos(mdef['xsize'] / 2, mdef['ysize'] / 2, view['zpos'])
    # X/Y Pos
    if type == "top":
        view['xpos'] -= 1
    if type == "bot":
        view['xpos'] += 1
    if type == "left":
        view['ypos'] -= 1
    if type == "right":
        view['ypos'] += 1
    # Rotation
    if type == "left_r":
        view['orotation'] += 9
    if type == "right_r":
        view['orotation'] -= 9
    # View Limit
    if type == "uplimit":
        view['vlimit'] += 1
        update_height_view(editor)
        editor['render_map'].update_z(view['vlimit'] - 1)
        editor['render_map'].update_z(view['vlimit'])
    if type == "downlimit":
        view['vlimit'] -= 1
        update_height_view(editor)
        editor['render_map'].update_z(view['vlimit'] + 1)
        editor['render_map'].update_z(view['vlimit'])
    if type == "vcenter":
        view['xpos'] = view['txpos']
        view['ypos'] = view['typos']
    if type == "tgrid":
        if not view['hide_grid']:
            view['mgrid'].hide(engine.BitMask32.bit(0))
        else:
            view['mgrid'].show(engine.BitMask32.bit(0))
        view['hide_grid'] = not view['hide_grid']
    if type == "tblurr":
        view['blurr_cursor'] = (view['blurr_cursor'] + 1) % 7
    if type == "grap_start":
        vx = view['xpos']
        vy = view['ypos']
        mpos = view['mpos']
        if mpos:
            view['graped'] = (
                vx,
                vy,
                mpos.getX() - vx,
                mpos.getY() - vy,
            )
    if type == "grap_end":
        view['graped'] = None

    if type == "zoom":
        view['ozoom'] /= 1.2
    if type == "unzoom":
        view['ozoom'] *= 1.2

    if type == "roll_up":
        if view['graped']:
            view['ozoom'] /= 1.2
        else:
            view['orotation'] -= 5
    if type == "roll_down":
        if view['graped']:
            view['ozoom'] *= 1.2
        else:
            view['orotation'] += 5
    if type == "toggle_placeholder":
        view['used']['placeholder']['show'] = not view['used']['placeholder']['show']

"""
    Handle editor events such as block add, map saving, block delete,
    block selection and move events.
    > Called on user event
"""
def editor_event_handler(editor, type):
    view = editor['view']
    using = view['used']

    truex = view['txpos']
    truey = view['typos']
    truez = int(view['zpos'])

    """
        Delete every nodes at a x-y-z position
    """
    def del_block(cx, cy, cz, last = False):
        if view['grass']:
            view['grasshandler'].del_grass(truex, truey, truez, using['scale'])
        elif view['hover']:
            ndist = None
            nearest = None
            for ho in editor['map']['hoverings']:
                if ho['z'] == truez:
                    d = max(
                        abs(truex - ho['x']),
                        abs(truey - ho['y']),
                    )
                    if not using['id'] or using['id'] == ho['t']:
                        if not ndist or (d < ndist and d < 3.0):
                            nearest = ho
                            ndist = d
            if nearest and nearest['id'] in editor['view']['hoverings']:
                node = editor['view']['hoverings'][nearest['id']]
                node.detachNode()
                node.removeNode()
                editor['view']['hoverings'].pop(nearest['id'])
                for x, y in constants.TEST_GRID:
                    x *= editor['render_map'].bxsize
                    y *= editor['render_map'].bysize
                    editor['render_map'].update(truex + x, truey + y, truez)
                editor['map']['hoverings'].remove(nearest)
        else:
            blks = objects.read_from_tree(editor['map']['blocks'], cx, cy, cz)
            bnodes = objects.read_from_tree(editor['render_tree'], cx, cy, cz)
            if blks:
                i = 0
                while i < len(blks):
                    bl = blks[i]
                    pnode = bnodes[i]
                    if not using['id'] or bl['t'] == using['id']:
                        objects.rm_from_list_tree(editor['map']['blocks'], cx, cy, cz, bl)
                        objects.rm_from_list_tree(editor['render_tree'], cx, cy, cz, pnode)
                        for added in view['last_add']:
                            if added['n'] == pnode:
                                view['last_add'].remove(added)
                                break
                        pnode.detachNode()
                        pnode.removeNode()
                    else:
                        i += 1
            editor['render_map'].update(cx, cy, cz)

    """
        Add a new node at x-y-z position with specified rotation
    """
    def add_block(cx, cy, cz, crotation):
        if view['grass']:
            view['grasshandler'].add_grass(truex, truey, truez, using['scale'])
        elif using['id']:
            if view['hover']:
                global last_edit
                if last_edit != (truex, truey, truez, crotation):
                    last_edit = (truex, truey, truez, crotation)
                    bmode = resources.GetBlockType(editor['blocks'], using['id'])
                    if bmode in ["normal", "admin",]:
                        # Load new model instance
                        new_node = resources.GetBlockModel(
                            editor['blocks'], using['id']
                        ).copyTo(editor['render_map'].get(cx, cy, cz, view['shadowing']))
                        new_node.setTag('type', resources.GetBlockType(
                            editor['blocks'], using['id']
                        ))
                        objects.position_good(
                            new_node, truex, truey, truez, crotation
                        )
                        nid = resources.GetId()
                        if 'hoverings' not in editor['map']:
                            editor['map']['hoverings'] = []
                        editor['map']['hoverings'].append({
                            'id': nid,
                            'x': truex,
                            'y': truey,
                            'z': truez,
                            't': using['id'],
                            'r': using['rotation'],
                            's': view['shadowing'],
                            'm': bmode,
                            'sc': using['scale'],
                        })
                        xsize = editor['render_map'].bxsize
                        ysize = editor['render_map'].bysize
                        new_node.show()
                        new_node.setScale(using['scale'])
                        if not view['shadowing']:
                            new_node.hide(engine.BitMask32.bit(1))
                        for x, y in constants.TEST_GRID:
                            editor['render_map'].update(cx + (x * xsize), cy + (y * ysize), cz)
                        editor['view']['hoverings'][nid] = new_node
            else:
                # Load new model instance
                if resources.GetBlockType(editor['blocks'], using['id']) != "animated":
                    new_node = resources.GetBlockModel(
                        editor['blocks'], using['id']
                    ).copyTo(editor['render_map'].get(cx, cy, cz, view['shadowing']))
                else:
                    new_node = resources.CopyActor(resources.GetBlockModel(
                        editor['blocks'], using['id']
                    ))
                new_node.setTag('type', resources.GetBlockType(
                    editor['blocks'], using['id']
                ))
                new_node.setScale(using['scale'])
                # Avoid same block in the same position
                actuals = objects.read_from_tree(editor['map']['blocks'], cx, cy, cz)
                if actuals:
                    for anode in actuals:
                        if anode['t'] == using['id']:
                            if anode['r'] == crotation or view['randomed']:
                                new_node.detachNode()
                                new_node.removeNode()
                                return None
                # apply to match render system
                objects.position_block(
                    new_node, cx, cy, cz, crotation
                )
                # add to render node tree
                objects.add_to_list_tree(editor['render_tree'], cx, cy, cz, new_node)
                # add to map tree
                objects.add_to_list_tree(editor['map']['blocks'], cx, cy, cz, {
                    'x': cx,
                    'y': cy,
                    'z': cz,
                    'r': crotation,
                    't': using['id'],
                    's': view['shadowing'],
                    'sc': using['scale'],
                })
                view['last_add'].append({
                    'x': cx,
                    'y': cy,
                    'z': cz,
                    'r': crotation,
                    't': using['id'],
                    'n': new_node,
                })
                new_node.show()
                if not view['shadowing']:
                    new_node.hide(engine.BitMask32.bit(1))
                editor['render_map'].update(cx, cy, cz)


    """
        Cancel last block add
    """
    def reset_last():
        added = view['last_add']
        if len(added) > 0:
            added = added[-1]
        if added:
            objs = objects.read_from_tree(editor['map']['blocks'], added['x'], added['y'], added['z'])
            if objs:
                for obj in objs:
                    if obj['r'] == added['r'] and obj['t'] == added['t']:
                        objs.remove(obj)
                        break
            bnodes = objects.read_from_tree(editor['render_tree'], added['x'], added['y'], added['z'])
            if bnodes:
                for bnode in bnodes:
                    if bnode == added['n']:
                        bnodes.remove(bnode)
                        bnode.detachNode()
                        bnode.removeNode()
                        break
            view['last_add'].remove(added)
            editor['render_map'].update(added['x'], added['y'], added['z'])

    x = int(view['txpos'])
    y = int(view['typos'])
    z = int(view['zpos'])
    if type == "toggle_preview_rotation":
        editor['ui'].toggle_preview_rotation()
    if type == "toggle_shadow_mode":
        view['shadowing'] = not view['shadowing']
        editor['ui'].toggle_shadow()
    if type == "toggle_hover_mode":
        using['rotation'] = 0
        view['hover'] = not view['hover']
        editor['ui'].toggle_mode()
    if type == "toggle_grass_mode":
        view['grass'] = not view['grass']
        editor['ui'].toggle_grass()
        view['grasshandler'].toggle_display()
    if type == "clear_cursor":
        if view['grass']:
            view['grasshandler'].del_some_grass(truex, truey, truez, using['scale'])
        else:
            using['id'] = None
            editor['ui'].clear_cursor()
    if type == "copycat":
        if view['hover']:
            ndist = None
            nearest = None
            for ho in editor['map']['hoverings']:
                if ho['z'] == truez:
                    d = max(
                        abs(truex - ho['x']),
                        abs(truey - ho['y']),
                    )
                    if not ndist or (d < ndist and d < 3.0):
                        nearest = ho
                        ndist = d
            if nearest and nearest['id'] in editor['view']['hoverings']:
                using['id'] = nearest['t']
                using['rotation'] = nearest['r']
        else:
            bblocks = objects.read_from_tree(editor['map']['blocks'], x, y, z)
            if bblocks:
                b = random.choice(bblocks)
                using['id'] = b['t']
                using['rotation'] = b['r']
        editor['ui'].change_preview(using['id'])
    if type == "undo":
        reset_last()
    if type == "toggle_edge":
        if view['grass']:
            using['grasscursorcircle'] = not using['grasscursorcircle']
            view['grasshandler'].toggle_paint_style()
        else:
            map_edges = editor['map']['def']['cutting_edges']
            if z not in map_edges:
                map_edges.append(z)
            else:
                map_edges.remove(z)
            refresh_edges(editor)
    if type == "toggle_specials":
        view['hide_specials'] = not view['hide_specials']
        update_height_view(editor)
        editor['render_map'].update_all()
    if type == "toggle_edge_display":
        view['hide_edges'] = not view['hide_edges']
        update_height_view(editor)
        editor['render_map'].update_all()
    # Scale
    if type == "scale_up":
        using['scale'] += 0.05
        using['scale'] = min(40., using['scale'])
    if type == "scale_down":
        using['scale'] -= 0.05
        using['scale'] = max(0.1, using['scale'])
    # Rotation
    decal = (1.0, 1)
    if view['aprox']:
        decal = (15.0, 1)
    if type == "rota_block":
        if view['hover']:
            using['rotation'] = (using['rotation'] + decal[0]) % 360
        else:
            using['rotation'] = (using['rotation'] + decal[1]) % 4
    if type == "rev_rota_block":
        if view['hover']:
            using['rotation'] = (using['rotation'] + 360. - decal[0]) % 360
        else:
            using['rotation'] = (using['rotation'] + 4 - decal[1]) % 4
    if type == "rand_block":
        view['randomed'] = True
        if view['hover']:
            using['rotation'] = random.randint(0, 360)
        else:
            using['rotation'] = random.randint(0, 3)
    if type == "rand_stop":
        view['randomed'] = False
    if type == "del_block":
        del_block(x, y, z)
    if type == "full_block" and not view['hover']:
        for tx in xrange(0, editor['map']['def']['xsize']):
            for ty in xrange(0, editor['map']['def']['ysize']):
                del_block(tx, ty, z)
                add_block(tx, ty, z, using['rotation'])
    if type == "full_rand_block" and not view['hover']:
        for tx in xrange(0, editor['map']['def']['xsize']):
            for ty in xrange(0, editor['map']['def']['ysize']):
                del_block(tx, ty, z)
                add_block(tx, ty, z, random.randint(0, 3))
    if type == "add_block" and editor['ui'].event("click"):
        add_block(x, y, z, using['rotation'])
        using['adding'] = True
    if type == "add_block_stop":
        using['adding'] = False

    """
        Save editor map to files readable by the client/server

        Save the actual map readable/editable by the editor at:
            maps/sources/{Map}.mp

        Save the map datas readable by the client/server at:
            maps/compiled/{Map}.mpx
        Save the map models cut at the edges, displayed by the client at:
            maps/compiled/{MapFile}-{Edge Numbers}.bam
        Save the map models not casting shadows, cut at the edge, at:
            maps/compiled/{MapFile}-{Edge Numbers}-ns.bam
    """
    if type == 'save_map':
        map = editor['map']['blocks']
        output_file = editor['map']['def']['name']
        # cutting edge separations list
        cutting_edges = editor['map']['def']['cutting_edges']
        cutting_edges.sort()
        map_block_edges = []
        for i in xrange(0, len(cutting_edges) + 1):
            map_block_edges.append(render.attachNewNode('copied_map_edge%d' % i))
        map_ghost_edges = []
        for i in xrange(0, len(cutting_edges) + 1):
            map_ghost_edges.append(render.attachNewNode('copied_ghost_edge%d' % i))
        # prepare map nodes and add them to map models
        for zkey in map.keys():
            edge_idx = 0
            for edge in cutting_edges:
                if zkey >= edge:
                    edge_idx += 1
            for ykey in map[zkey].keys():
                for xkey in map[zkey][ykey].keys():
                    for node in map[zkey][ykey][xkey]:
                        if resources.GetBlockType(editor['blocks'], node['t']) == "normal":
                            if node['s']:
                                edge_type = map_block_edges[edge_idx]
                            else:
                                edge_type = map_ghost_edges[edge_idx]
                            new_node = resources.GetBlockModel(
                                editor['blocks'], node['t']
                            ).copyTo(edge_type)
                            new_node.show()
                            # pos to match render system
                            new_node.setScale(node.get('sc', 1.))
                            objects.position_block(
                                new_node, node['x'], node['y'], node['z'], node['r']
                            )
        for hovering in editor['map']['hoverings']:
            if 'm' not in hovering or hovering['m'] == "normal": # don't export model, if admin object
                edge_idx = 0
                for edge in cutting_edges:
                    if hovering['z'] >= edge:
                        edge_idx += 1
                if hovering['s']:
                    edge_type = map_block_edges[edge_idx]
                else:
                    edge_type = map_ghost_edges[edge_idx]
                new_node = resources.GetBlockModel(
                    editor['blocks'], hovering['t']
                ).copyTo(edge_type)
                new_node.show()
                # pos to match render system
                new_node.setScale(hovering.get('sc', 1.))
                objects.position_good(
                    new_node, hovering['x'], hovering['y'], hovering['z'], hovering['r']
                )
        dummy = engine.NodePath("Export Dummy")
        n_idx = 0
        for copied_map_node in map_block_edges:
            n_idx += 1
            copied_map_node.clearModelNodes()
            copied_map_node.flattenStrong()
            copied_map_node.writeBamFile("./maps/compiled/%s-%d.bam" % (output_file + "x", n_idx))
            copied_map_node.reparentTo(dummy)

        n_idx = 0
        for copied_map_node in map_ghost_edges:
            n_idx += 1
            copied_map_node.clearModelNodes()
            copied_map_node.flattenStrong()
            copied_map_node.writeBamFile("./maps/compiled/%s-%d-ns.bam" % (output_file + "x", n_idx))
            copied_map_node.reparentTo(dummy)

        print "> Map stats"
        dummy.analyze()
        for useless_node in map_block_edges + map_ghost_edges:
            useless_node.hide()
            useless_node.detachNode()
            useless_node.removeNode()
        dummy.detachNode()
        dummy.removeNode()

        editor['view']['grasshandler'].export()
        exported = export.export_map(copy.deepcopy(editor['map']), editor)
        if exported:
            pickle.dump(exported, open("./maps/compiled/" + output_file + "x", "wb"))
            pickle.dump(editor['map'], open("./maps/sources/" + output_file, "wb"))
            print "> Map Saved!"
        else:
            print "> Map conflicts, try again!"


"""
    Refresh/toggle display of edges (horizontal cutting marker)
"""
def refresh_edges(editor):
    map_edges = editor['map']['def']['cutting_edges']
    view_edges = editor['view']['edges_nodes']
    for node in view_edges:
        node.detachNode()
        node.removeNode()
    del view_edges[:]
    if editor['view']['hide_edges']:
        return None
    xsize = editor['map']['def']['xsize']
    ysize = editor['map']['def']['ysize']
    for z in map_edges:
        new_grid = grid.ThreeAxisGrid(xsize / 2, ysize / 2, 0, 10, 10, True)
        new_grid_node = new_grid.create()
        new_grid_node.setPos(xsize / 2, ysize / 2, z)
        new_grid_node.reparentTo(render)
        new_grid_node.hide(engine.BitMask32.bit(1))
        view_edges.append(new_grid_node)

"""
    Final editor initialisations, also bind keyboard events
"""
def start_editing_map(editor):

    base.cam.node().setCameraMask(engine.BitMask32.bit(0))
    create_scene_grid(editor)
    refresh_edges(editor)
    editor['ui'].load_folder("", editor['blocks'])
    editor['view']['mpicker'] = mouse.Picker()
    editor['view']['size'] = editor['map']['def']['xsize']

    taskMgr.add(update_map_view, "UpdateViewTask", extraArgs=[editor['view'],])
    taskMgr.add(update_cursor_view, "UpdateCursorTask", extraArgs=[editor,])
    taskMgr.add(update_light_view, "UpdateLightTask", extraArgs=[editor,])

    base.accept("7", light_event_handler, [editor, "light_up",])
    base.accept("4", light_event_handler, [editor, "light_down",])
    base.accept("7-repeat", light_event_handler, [editor, "light_up",])
    base.accept("4-repeat", light_event_handler, [editor, "light_down",])

    base.accept("mouse2", view_event_handler, [editor, "vcenter",])
    base.accept("mouse5", view_event_handler, [editor, "vcenter",])
    base.accept("m", view_event_handler, [editor, "left_r",])
    base.accept("k", view_event_handler, [editor, "right_r",])
    base.accept("o", view_event_handler, [editor, "zoom",])
    base.accept("l", view_event_handler, [editor, "unzoom",])
    base.accept("m-repeat", view_event_handler, [editor, "left_r",])
    base.accept("k-repeat", view_event_handler, [editor, "right_r",])
    base.accept("o-repeat", view_event_handler, [editor, "zoom",])
    base.accept("l-repeat", view_event_handler, [editor, "unzoom",])

    base.accept("shift-z", view_event_handler, [editor, "up",])
    base.accept("shift-s", view_event_handler, [editor, "down",])
    base.accept("z", view_event_handler, [editor, "top",])
    base.accept("s", view_event_handler, [editor, "bot",])
    base.accept("q", view_event_handler, [editor, "left",])
    base.accept("d", view_event_handler, [editor, "right",])
    base.accept("shift-z-repeat", view_event_handler, [editor, "up",])
    base.accept("shift-s-repeat", view_event_handler, [editor, "down",])
    base.accept("z-repeat", view_event_handler, [editor, "top",])
    base.accept("s-repeat", view_event_handler, [editor, "bot",])
    base.accept("q-repeat", view_event_handler, [editor, "left",])
    base.accept("d-repeat", view_event_handler, [editor, "right",])
    base.accept("9", view_event_handler, [editor, "uplimit",])
    base.accept("6", view_event_handler, [editor, "downlimit",])
    base.accept("9-repeat", view_event_handler, [editor, "uplimit",])
    base.accept("6-repeat", view_event_handler, [editor, "downlimit",])
    base.accept("shift", view_event_handler, [editor, "undo-aprox"])
    base.accept("shift-up", view_event_handler, [editor, "do-aprox"])

    base.accept("+", editor_event_handler, [editor, "scale_up",])
    base.accept("-", editor_event_handler, [editor, "scale_down",])
    base.accept("+-repeat", editor_event_handler, [editor, "scale_up",])
    base.accept("--repeat", editor_event_handler, [editor, "scale_down",])

    base.accept("y", editor_event_handler, [editor, "full_block"])
    base.accept("shift-y", editor_event_handler, [editor, "full_rand_block"])

    base.accept("r", editor_event_handler, [editor, "rota_block"])
    base.accept("r-repeat", editor_event_handler, [editor, "rota_block"])
    base.accept("control-r", editor_event_handler, [editor, "rev_rota_block"])
    base.accept("control-r-repeat", editor_event_handler, [editor, "rev_rota_block"])
    base.accept("f", editor_event_handler, [editor, "rand_block"])
    base.accept("f-up", editor_event_handler, [editor, "rand_stop"])
    base.accept("f-repeat", editor_event_handler, [editor, "rand_block"])
    base.accept("f-repeat-up", editor_event_handler, [editor, "rand_stop"])

    base.accept("tab", editor_event_handler, [editor, "toggle_edge"])
    base.accept("delete", editor_event_handler, [editor, "del_block"])
    base.accept("delete-repeat", editor_event_handler, [editor, "del_block"])

    base.accept("control-z", editor_event_handler, [editor, "undo"])
    base.accept("control-z-repeat", editor_event_handler, [editor, "undo"])
    base.accept("control-s", editor_event_handler, [editor, "save_map"])

    base.accept("space", editor_event_handler, [editor, "clear_cursor"])
    base.accept("space-repeat", editor_event_handler, [editor, "clear_cursor"])
    base.accept("mouse1", editor_event_handler, [editor, "add_block"])
    base.accept("mouse1-up", editor_event_handler, [editor, "add_block_stop"])
    base.accept("mouse3", view_event_handler, [editor, "grap_start"])
    base.accept("mouse3-up", view_event_handler, [editor, "grap_end"])
    base.accept("wheel_up", view_event_handler, [editor, "roll_up"])
    base.accept("wheel_down", view_event_handler, [editor, "roll_down"])
    base.accept("control-wheel_up", view_event_handler, [editor, "up"])
    base.accept("control-wheel_down", view_event_handler, [editor, "down"])
    base.accept(",", editor_event_handler, [editor, "toggle_specials"])
    base.accept(";", editor_event_handler, [editor, "toggle_edge_display"])
    base.accept(":", view_event_handler, [editor, "tgrid"])
    base.accept("!", view_event_handler, [editor, "toggle_placeholder"])
    base.accept("a", editor_event_handler, [editor, "copycat"])
    base.accept("c", view_event_handler, [editor, "tblurr"])
    base.accept("enter", render.analyze)

    base.accept("f5", editor['render_map'].update_all)
    base.accept("f8", editor_event_handler, [editor, "toggle_preview_rotation"])
    base.accept("f9", editor_event_handler, [editor, "toggle_hover_mode"])
    base.accept("f10", editor_event_handler, [editor, "toggle_shadow_mode"])
    base.accept("f11", editor_event_handler, [editor, "toggle_grass_mode"])

    run()
