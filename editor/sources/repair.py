import os
import sys
import copy
import sources.utils.objects as objects

def repair(map):
    check_hovers = False
    if len(sys.argv) > 3:
        if sys.argv[3] == "hovers":
            check_hovers = True
    check_blocks = False
    if len(sys.argv) > 3:
        if sys.argv[3] == "blocks":
            check_blocks = True
    replaced_block_types = {}
    deleted_block_types = {}
    replaced_hover_types = {}
    deleted_hover_types = {}
    mdef = map['def']
    mblocks = map['blocks']
    mhovers = map['hoverings']
    print ""
    print "Trying to repair map \"%s\"" % mdef['name']
    print ""
    print "Map contain %d cutting edges : " % len(mdef['cutting_edges'])
    for z in mdef['cutting_edges']:
        print " - z:%d" % z
    print ""
    print "Map supposed to be %d x %d size" % (mdef['xsize'], mdef['ysize'])
    print ""
    print "Map has %d hovering models" % len(mhovers)
    print ""
    print "Checking for invalid blocks..."
    print ""
    for zkey in mblocks.keys():
        for ykey in mblocks[zkey].keys():
            for xkey in mblocks[zkey][ykey].keys():
                node_list = copy.copy(mblocks[zkey][ykey][xkey])
                for block in node_list:
                    if not os.path.isfile(block['t']) or check_blocks:
                        if block['t'] in replaced_block_types:
                            block['t'] = replaced_block_types[block['t']]
                        elif block['t'] in deleted_block_types:
                            objects.rm_from_list_tree(mblocks, zkey, ykey, xkey, block)
                        else:
                            print ""
                            print "\"%s\" to replace." % block['t']
                            print "> Enter a new model path (\"[DEL]\" to delete)"
                            result = raw_input('> ')
                            print ""
                            if result == "":
                                replaced_block_types[block['t']] = block['t']
                            elif result == "[DEL]":
                                deleted_block_types[block['t']] = True
                                objects.rm_from_list_tree(mblocks, zkey, ykey, xkey, block)
                            else:
                                replaced_block_types[block['t']] = result
                                block['t'] = replaced_block_types[block['t']]
    print "Checking for invalid hovers..."
    deleted_hovers = []
    for hover in mhovers:
        if not os.path.isfile(hover['t']) or check_hovers:
            if hover['t'] in replaced_hover_types:
                hover['t'] = replaced_hover_types[hover['t']]
            elif hover['t'] in deleted_hover_types:
                deleted_hovers.append(hover)
            else:
                print ""
                print "\"%s\" to replace." % hover['t']
                print "> Enter a new model path (\"[DEL]\" to delete)"
                result = raw_input('> ')
                print ""
                if result == "":
                    replaced_hover_types[hover['t']] = hover['t']
                elif result == "[DEL]":
                    deleted_hover_types[hover['t']] = True
                    deleted_hovers.append(hover)
                else:
                    replaced_hover_types[hover['t']] = result
                    hover['t'] = replaced_hover_types[hover['t']]
    for hover in deleted_hovers:
        map['hoverings'].remove(hover)
    print ""
    return map
