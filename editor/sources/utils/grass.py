import random as rand
import panda3d.core as engine
import sources.utils.constants as const
import sources.utils.load_resources as resources

from panda3d.core import PNMImage
from panda3d.core import Filename

class GrassEditor():

    def __init__(self, map):
        self.circle = False
        self.display = True
        self.map_name = map['def']['name']
        self.mxsize = map['def']['xsize']
        self.mysize = map['def']['ysize']
        self.base_grass = PNMImage(Filename("./blocks/grass-texture.png"))
        # self.buffer_filename = Filename("./maps/sources/%s-grass.png" % self.map_name)
        self.final_filename = Filename("./maps/compiled/%sx-grass.png" % self.map_name)
        self.last_add = (-1, -1, -1)
        self.last_del = (-1, -1, -1)
        self.size_mult = 64
        self.xsize = self.mxsize * self.size_mult
        self.ysize = self.mysize * self.size_mult
        try:
            self.load_grass()
        except:
            self.create_grass()
        self.init_view()
        self.toggle_display()

    def create_grass(self):
        # self.buffer_grass = PNMImage(self.xsize, self.ysize)
        # self.buffer_grass.make_grayscale()
        # self.buffer_grass.fill(0.)
        self.final_grass = PNMImage(self.xsize, self.ysize)
        self.final_grass.addAlpha()
        for x in xrange(self.xsize):
            for y in xrange(self.ysize):
                self.final_grass.setPixel(x, y, self.base_grass.getPixel(x % 2048, y % 2048))
                self.final_grass.setAlpha(x, y, 0.)

    def load_grass(self):
        # self.buffer_grass = PNMImage(self.buffer_filename)
        # if not self.buffer_grass:
        #     raise Exception("Fail to load buffer")
        if not self.final_filename.exists():
            raise Exception("File does not exist")
        self.final_grass = PNMImage(self.final_filename)

    def export(self):
        print "> Exporting grass texture"
        # self.buffer_grass.write(self.buffer_filename)
        self.final_grass.write(self.final_filename)

    def toggle_display(self):
        self.display = not self.display
        if self.display:
            self.grass_layers.show()
        else:
            self.grass_layers.hide()

    def toggle_paint_style(self):
        self.circle = not self.circle

    def init_view(self):
        self.nb = 12
        self.height = 0.17
        self.grass_layers = render.attachNewNode("grass")
        self.grass_layers.setTransparency(engine.TransparencyAttrib.MAlpha)
        self.grass_texture = engine.Texture("grass_texture")
        for i in xrange(self.nb):
            gnode = resources.LoadTexturedModel(loader, "./ui/decal_grass_base.egg", "./ui/placeholder.png", nearest=True, alpha=True)
            for path in xrange(0, gnode.getChildren().getNumPaths()):
                geom = gnode.getChildren().getPath(path)
                geom.reparentTo(self.grass_layers)
                geom.setPos(0, 0, i * self.height / float(self.nb))
            gnode.detachNode()
            gnode.removeNode()
        self.refresh_texture()

    def refresh_texture(self):
        self.grass_texture.load(self.final_grass)
        self.grass_texture.setMinfilter(engine.Texture.FTNearest)
        self.grass_texture.setMagfilter(engine.Texture.FTNearest)
        self.grass_layers.setTexture(self.grass_texture)

    def change_pixels(self, x, y, size, randomized, transparent):
        xpos = int(self.mxsize * self.size_mult * float(x) / 32.)
        xpos = self.xsize - xpos - 1
        ypos = int(self.mysize * self.size_mult * float(y) / 32.)
        tsize = int(self.mxsize * self.size_mult * float(size) / 32.) / 2
        msize = tsize ** 2
        for i in xrange(tsize * 2):
            for j in xrange(tsize * 2):
                nx = xpos - tsize + i
                ny = ypos - tsize + j
                if not randomized or rand.randint(0, 15) == 1:
                    if nx < self.xsize and ny < self.ysize and nx >= 0 and ny >= 0:
                        if not self.circle or (nx - xpos) ** 2 + (ny - ypos) ** 2 < msize:
                            if not transparent:
                                self.final_grass.setPixel(nx, ny, self.base_grass.getPixel(nx % 2048, ny % 2048))
                                self.final_grass.setAlpha(nx, ny, 255)
                            else:
                                self.final_grass.setAlpha(nx, ny, 0.)
        self.refresh_texture()

    def add_grass(self, x, y, z, size):
        nadd = (x, y, z)
        if nadd != self.last_add:
            self.last_add = nadd
            self.change_pixels(x, y, size, True, False)

    def del_grass(self, x, y, z, size):
        ndel = (x, y, z)
        if ndel != self.last_del:
            self.last_del = ndel
            self.change_pixels(x, y, size, False, True)

    def del_some_grass(self, x, y, z, size):
        ndel = (x, y, z)
        if ndel != self.last_del:
            self.last_del = ndel
            self.change_pixels(x, y, size, True, True)
