import panda3d.core as engine
import sources.utils.constants as const

class Picker(object):

    def __init__(self):
        self.z = 0
        self.plane = None

    def pick(self, z):
        if self.z != z or not self.plane:
            self.plane = engine.Plane(engine.Vec3(0, 0, 1), engine.Point3(0, 0, z))
            self.z = z
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            pos3d = engine.Point3()
            nearPoint = engine.Point3()
            farPoint = engine.Point3()
            base.camLens.extrude(mpos, nearPoint, farPoint)
            if self.plane.intersectsLine(
                pos3d,
                render.getRelativePoint(base.camera, nearPoint),
                render.getRelativePoint(base.camera, farPoint)
            ):
                return pos3d
        return None
