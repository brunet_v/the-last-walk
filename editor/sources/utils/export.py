import re
import copy
import math
import hashlib
import random as rand
import sources.utils.objects as obj
import sources.utils.constants as const
import sources.utils.load_resources as resources
import specials as spe
from panda3d.core import PNMImage
from panda3d.core import Filename

"""
    The single most WTF file in the editor,
    will compile multi-node per case map into a single-node per case
    and generate collisions maps accordingly
"""


"""
    Special block name pattern where additionnal informations saved
"""
SAVED_NAME_BLOCKS = set([
    "^[izp]spawn$",
    "^door[0-9]+$",
])


"""
    Generate collision map for a block-stack
"""
def resolve_block(editor, mixin_created, blocks_datas, hovering_suppls):
    # print blocks_datas
    rhnodes, rhx, rhy = hovering_suppls
    types = set()
    names = []
    for i in xrange(0, len(blocks_datas)):
        bmode = blocks_datas[i][0] # ex: admin ("@"), normal ("_")
        bname = blocks_datas[i][1] # ex: "door1"
        btype = blocks_datas[i][2] # ex: "cnone", "cfree"
        brot = blocks_datas[i][3]  # ex: 2 (180deg)
        bpath = blocks_datas[i][4]
        if btype != 'cfree':
            types.add((btype, brot))
        for pattern in SAVED_NAME_BLOCKS:
            if re.search(pattern, bname):
                names.append((bname, bpath, brot))
        if bmode == "animated":
            names.append(("anim", bpath, brot))
        if bname == "kill-collisions":
            raise Exception("Kill collisions")
    if not len(types):
        types.add(('cfree', 0))
    if len(types) == 1 and len(hovering_suppls[0]) == 0:
        bdata = types.pop()
        return bdata[0], bdata[1], names
    types_list = list(types)
    types_ids = []
    for i in xrange(0, len(types_list)):
        types_ids.append('%s%d' % types_list[i])
    types_ids = sorted(types_ids)
    mixin_id = "-".join(types_ids)
    reps = []
    for h in rhnodes:
        reps.append("%s_%.1f_%.1fx%.1f" % (
            h['t'], h['r'], h['x'] - rhx, h['y'] - rhy)
        )
    mixin_id = "mixin-" + hashlib.sha1(mixin_id + '-'.join(reps)).hexdigest()
    if mixin_id in mixin_created:
        return mixin_id, 0, names
    print "Creating: %s" % mixin_id
    img_size = 64
    new_img = PNMImage(img_size, img_size)
    new_img.fill(0, 0, 0)
    for elem_type in types_list:
        btype = elem_type[0]
        brot = elem_type[1]
        xrev, yrev, axisrev = const.ROTATION_FLIPS[brot]
        bImage = PNMImage(Filename(resources.RelativePath("collisions/%s.png" % btype)))
        for x in xrange(0, img_size):
            for y in xrange(0, img_size):
                tx = x
                ty = y
                if xrev:
                    tx = img_size - x - 1
                if yrev:
                    ty = img_size - y - 1
                if axisrev:
                    tx, ty = ty, tx
                new_img.setRed(x, y, max(bImage.getRed(tx, ty), new_img.getRed(x, y)))
                new_img.setBlue(x, y, max(bImage.getBlue(tx, ty), new_img.getBlue(x, y)))
                new_img.setGreen(x, y, max(bImage.getGreen(tx, ty), new_img.getGreen(x, y)))
    for supl in rhnodes:
        hr, hx, hy = supl['r'], supl['x'], supl['y']
        tpath = resources.GetBlockCollision(editor['blocks'], supl['t'])
        bImage = PNMImage(Filename(resources.RelativePath("collisions/%s.png" % tpath)))
        angle = math.radians(-hr)
        dcos = math.cos(angle)
        dsin = math.sin(angle)
        for x in xrange(0, img_size):
            for y in xrange(0, img_size):
                ox = x - (hx - rhx) * img_size
                oy = y - (hy - rhy) * img_size
                dx = int(ox * dcos - oy * dsin)
                dy = int(ox * dsin + oy * dcos)
                if dx >= 0 and dy >= 0 and dx < img_size and dy < img_size:
                    dx, dy = dy, dx
                    tx, ty = y, x
                    new_img.setRed(tx, ty, max(bImage.getRed(dx, dy), new_img.getRed(tx, ty)))
                    new_img.setBlue(tx, ty, max(bImage.getBlue(dx, dy), new_img.getBlue(tx, ty)))
                    new_img.setGreen(tx, ty, max(bImage.getGreen(dx, dy), new_img.getGreen(tx, ty)))
    new_img.write(Filename(resources.RelativePath("collisions/mixins/%s.png" % mixin_id)))
    mixin_created.add(mixin_id)
    return mixin_id, 0, names


"""
    ?
"""
def relative_collision(datas, rotation):
    cr = (datas['r'] + 360) % 360 / 90
    correct = {
        'x': datas['x'],
        'y': datas['y'],
        'r': (cr + rotation) % 4,
        'c': datas['c'],
    }
    urotation = (4 - rotation) % 4
    if const.ROTATION_FLIPS[urotation][0]:
        correct['x'] = -correct['x']
    if const.ROTATION_FLIPS[urotation][1]:
        correct['y'] = -correct['y']
    if const.ROTATION_FLIPS[urotation][2]:
        correct['x'], correct['y'] = correct['y'], correct['x']
    return correct


"""
    Compile map
"""
def export_map(old_map, editor):
    print "> Generating collisions maps"
    mixin_created = set()
    htree = obj.ListTree()
    for h in editor['map']['hoverings']:
        if 'm' in h and h['m'] == "admin":
            htree.put(h['x'], h['y'], h['z'], h)
    new_map = copy.deepcopy(old_map)
    new_map['blocks'] = {}
    new_map['hoverings'] = []
    new_map_blocks = new_map['blocks']
    for zkey, ytree in old_map['blocks'].iteritems():
        zdatas = obj.ListTree()
        for ykey, xtree in ytree.iteritems():
            for xkey, blist in xtree.iteritems():
                for block in blist:
                    btype = resources.GetBlockType(editor['blocks'], block['t'])
                    bname = resources.GetBlockName(editor['blocks'], block['t'])
                    bcoll = resources.GetBlockCollision(editor['blocks'], block['t'])
                    zdatas.put(xkey, ykey, zkey, (
                        btype,
                        bname,
                        bcoll,
                        block['r'],
                        block['t'],
                    ))
                    for pattern, value in spe.ADDITIONAL_COLLISIONS.iteritems():
                        if re.search("^%s$" % pattern, bname):
                            for d in value:
                                c = relative_collision(d, block['r'])
                                zdatas.put(xkey + c['y'], ykey + c['x'], zkey, (
                                    "admin",
                                    "admin",
                                    c['c'],
                                    c['r'],
                                    "~",
                                ))
        for ykey, xtree in ytree.iteritems():
            for xkey, blist in xtree.iteritems():
                nblock = {
                    'x': xkey,
                    'y': ykey,
                    'z': zkey,
                }
                if zdatas.get(xkey, ykey, zkey):
                    try:
                        nblock['t'], nblock['r'], nblock['n'] = resolve_block(
                            editor, mixin_created,
                            zdatas.get(xkey, ykey, zkey),
                            (htree.getNear(xkey + 0.5, ykey + 0.5, zkey, 2.9, 2.9, 0.5), xkey, ykey),
                        )
                        if not nblock['t']:
                            raise Exception("Could not combine those blocks")
                        if not nblock['n']:
                            nblock.pop('n')
                        obj.add_to_tree(new_map_blocks, xkey, ykey, zkey, nblock)
                    except Exception as e:
                        if e.message != "Kill collisions":
                            print str(e)
                            raise
                else:
                    if obj.read_from_tree(new_map_blocks, xkey, ykey, zkey):
                        raise Exception("WTF?!")
    return new_map
