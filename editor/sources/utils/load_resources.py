#!/usr/bin/env python
import sys, os
import collections
import sources.utils.constants as constants
import sources.utils.struct as struct

import panda3d.core as engine
from direct.actor.Actor import Actor
from pandac.PandaModules import Texture
from panda3d.core import Filename
from panda3d.core import TransparencyAttrib

cid = 0
def GetId():
    global cid
    cid += 1
    return cid

def RelativePath(path):
    cpath = Filename.fromOsSpecific(
        os.path.abspath(sys.path[0])
    ).getFullpath()
    return cpath + "/" + path

def LoadTexturedModel(loader, modelpath, texturepath, nearest=False, alpha=False):
    nTexture = loader.loadTexture(RelativePath(texturepath))
    if nearest:
        nTexture.setMagfilter(Texture.FTNearest)
        nTexture.setMinfilter(Texture.FTNearest)
    else:
        nTexture.setMagfilter(Texture.FTLinear)
        nTexture.setMinfilter(Texture.FTLinear)
    nModel = loader.loadModel(RelativePath(modelpath))
    nModel.setTexture(nTexture)
    nModel.setTwoSided(False)
    if alpha:
        nModel.setTransparency(TransparencyAttrib.MAlpha)
    else:
        nModel.setTransparency(TransparencyAttrib.MBinary)
    return nModel

def GetFileName(filename):
    name = filename.split(".")[0]
    name = name[1:]
    return name.split("_", 1)[1]

def GetFileType(filename):
    return {"@": "admin", "_": "normal", "#": "animated"}[filename[:1]]

def GetFileCollision(filename):
    name = filename.split(".")[0]
    name = name[1:]
    return name.split("_")[0]

def CopyActor(actor, node = render):
    created = node.attachNewNode("anim_placeholder_%d" % GetId())
    actor.instanceTo(created)
    return created

def LoadBlockFile(path, filename, name, nearest=False):
    tfile = "%s_texture.png" % path.split(".")[0]
    if os.path.exists(tfile):
        blocks_texture = loader.loadTexture(RelativePath(tfile))
    else:
        blocks_texture = loader.loadTexture(RelativePath("blocks/texture.png"))
    if nearest:
        blocks_texture.setMagfilter(Texture.FTNearest)
        blocks_texture.setMinfilter(Texture.FTNearest)
    else:
        blocks_texture.setMagfilter(Texture.FTLinear)
        blocks_texture.setMinfilter(Texture.FTLinear)
    type = GetFileType(filename)
    if type != "animated":
        model = loader.loadModel(RelativePath(path))
    else:
        model = Actor(RelativePath(path), {"idle": RelativePath("%s_anim.egg" % path[:-4])})
        model.loop("idle")
    model.setTexture(blocks_texture)
    model.setTransparency(TransparencyAttrib.MBinary)
    model.setTwoSided(False)
    if type != "admin":
        model.setShaderAuto()
    model.flattenStrong()
    return struct.Struct({"m": model, "t": filename, "d": type, "path": path})

def LoadBlockList(main_directory, dir = "blocks"):
    files = {}
    for listing in os.listdir("%s/%s/" % (main_directory, dir)):
        path = "%s/%s/%s" % (main_directory, dir, listing)
        if os.path.isdir(path):
            files[listing] = LoadBlockList(main_directory, "%s/%s" % (dir, listing))
        elif listing.endswith(".egg") or listing.endswith(".dae") and not listing.endswith("_anim.egg"):
            name = GetFileName(listing)
            files[name] = LoadBlockFile("%s/%s" % (dir, listing), listing, name)
    return files

def GetBlockModel(block_list, path):
    folders = path.split("/")
    browsed = block_list
    for folder in folders:
        if folder != "blocks":
            if folder != folders[-1]:
                browsed = browsed[folder]
    return browsed[GetFileName(folders[-1])].m

def GetBlockType(block_list, path):
    folders = path.split("/")
    browsed = block_list
    for folder in folders:
        if folder != "blocks":
            if folder != folders[-1]:
                browsed = browsed[folder]
    return browsed[GetFileName(folders[-1])].d

def GetBlockName(block_list, path):
    folders = path.split("/")
    browsed = block_list
    for folder in folders:
        if folder != "blocks":
            if folder != folders[-1]:
                browsed = browsed[folder]
    return GetFileName(folders[-1])

def GetBlockCollision(block_list, path):
    folders = path.split("/")
    browsed = block_list
    for folder in folders:
        if folder != "blocks":
            if folder != folders[-1]:
                browsed = browsed[folder]
    return GetFileCollision(folders[-1])
