
SUN_LIGHT_COLOR = (0.9, 0.9, 0.9, 1)

AMBIENT_LIGHT_COLOR = (0.6, 0.6, 0.6, 1)

# UTILS
ROTATION_FLIPS = ( # reverse x, reverse y, reverse x <=> y
    (False, False, False),
    (True, False, True),
    (True, True, False),
    (False, True, True),
)

TEST_GRID = (
    (-1,  1), ( 0,  1), ( 1,  1),
    (-1,  0), ( 0,  0), ( 1,  0),
    (-1, -1), ( 0, -1), ( 1, -1),
)
