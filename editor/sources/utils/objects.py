# bluarg simple hacky way
def position_block(node, xpos, ypos, zpos, rotation):
    node.setHpr(rotation * -90, 0, 0)
    corresp = [
        [0, 0],
        [0, 1],
        [1, 1],
        [1, 0],
    ]
    node.setPos(
        xpos + corresp[rotation][0],
        ypos + corresp[rotation][1],
        zpos
    )

# More up-to-date function used for other methods
def position_good(node, xpos, ypos, zpos, rotation):
    node.setHpr(rotation, 0, 0)
    node.setPos(xpos, ypos, zpos)

# Generic classes
class Tree():

    def __init__(self):
        self.datas = {}

    def get(self, x, y, z):
        return read_from_tree(self.datas, x, y, z)

    def set(self, x, y, z, value):
        return add_to_tree(self.datas, x, y, z, value)

    def remove(self, x, y, z):
        return rm_from_tree(self.datas, x, y, z)


class ListTree(Tree):

    def put(self, x, y, z, value):
        return add_to_list_tree(self.datas, x, y, z, value)

    def pop(self, x, y, z, value):
        return rm_from_list_tree(self.datas, x, y, z, value)

    def getNear(self, x, y, z, xdist = 1, ydist = 1, zdist = 1):
        return read_from_tree_with_neightboors(self.datas, x, y, z, xdist, ydist, zdist)


# STANDARD TREES
def add_to_tree(tree, x, y, z, value):
    if not z in tree:
        tree[z] = {}
    if not y in tree[z]:
        tree[z][y] = {}
    tree[z][y][x] = value

def read_from_tree(tree, x, y, z):
    if z in tree:
        if y in tree[z]:
            if x in tree[z][y]:
                return tree[z][y][x]
    return None

def read_from_tree_with_neightboors(tree, x, y, z, xdist = 1, ydist = 1, zdist = 1):
    result = []
    for zkey, ytree in tree.iteritems():
        if abs(zkey - z) < zdist:
            for ykey, xtree in ytree.iteritems():
                if abs(ykey - y) < ydist:
                    for xkey, blist in xtree.iteritems():
                        if abs(xkey - x) < xdist:
                            result = result + blist
    return result

def rm_from_tree(tree, x, y, z):
    if z in tree:
        if y in tree[z]:
            if x in tree[z][y]:
                return tree[z][y].pop(x)
    return None


# LIST TREES
def add_to_list_tree(tree, x, y, z, value):
    list = read_from_tree(tree, x, y, z)
    if list is None:
        list = [value,]
    else:
        list.append(value)
    add_to_tree(tree, x, y, z, list)


def rm_from_list_tree(tree, x, y, z, value):
    list = read_from_tree(tree, x, y, z)
    if list is None:
        return value
    if value in list:
        list.remove(value)
    return value


def read_from_list_tree(tree, x, y, z):
    return read_from_tree(tree, x, y, z)

