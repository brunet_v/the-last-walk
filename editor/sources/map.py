#!/usr/bin/env python
import panda3d.core as engine
import pickle
import sources.utils.objects as objects

"""
    Class used to group node in the editor in small groups to minimise node count
"""
class BlockGrid():

    def __init__(self):
        self.rbc = objects.Tree()
        self.rbcnp = objects.Tree()
        self.rbc_ns = objects.Tree()
        self.rbcnp_ns = objects.Tree()
        self.bxsize = 4.0
        self.bysize = 4.0
        self.bzsize = 1.0

    def get(self, x, y, z, shadowing=True):
        x = int(x / self.bxsize)
        y = int(y / self.bysize)
        z = int(z / self.bzsize)
        if shadowing:
            rbcnp = self.rbcnp.get(x, y, z)
            if not rbcnp:
                rbc = engine.RigidBodyCombiner("rbc_%dx%dx%d" % (x, y, z))
                self.rbc.set(x, y, z, rbc)
                rbcnp = engine.NodePath(rbc)
                rbcnp.reparentTo(render)
                self.rbcnp.set(x, y, z, rbcnp)
        else:
            rbcnp = self.rbcnp_ns.get(x, y, z)
            if not rbcnp:
                rbc = engine.RigidBodyCombiner("rbc_ns_%dx%dx%d" % (x, y, z))
                self.rbc_ns.set(x, y, z, rbc)
                rbcnp = engine.NodePath(rbc)
                rbcnp.reparentTo(render)
                self.rbcnp_ns.set(x, y, z, rbcnp)
        return rbcnp

    def update(self, x, y, z, shadowing=None):
        x = int(x / self.bxsize)
        y = int(y / self.bysize)
        z = int(z / self.bzsize)
        if shadowing != False:
            if self.rbc.get(x, y, z):
                rbcnp = self.rbc.get(x, y, z).collect()
        if shadowing != True:
            if self.rbc_ns.get(x, y, z):
                rbcnp = self.rbc_ns.get(x, y, z).collect()

    def update_z(self, z):
        z = int(z / self.bzsize)
        if z in self.rbc.datas:
            for ykey, xtree in self.rbc.datas[z].iteritems():
                for xkey, value in xtree.iteritems():
                    self.update(xkey * self.bxsize, ykey * self.bysize, z * self.bzsize, True)
        if z in self.rbc_ns.datas:
            for ykey, xtree in self.rbc_ns.datas[z].iteritems():
                for xkey, value in xtree.iteritems():
                    self.update(xkey * self.bxsize, ykey * self.bysize, z * self.bzsize, False)

    def update_all(self):
        for zkey, ytree in self.rbc.datas.iteritems():
            for ykey, xtree in ytree.iteritems():
                for xkey, value in xtree.iteritems():
                    self.update(xkey * self.bxsize, ykey * self.bysize, zkey * self.bzsize, True)
        for zkey, ytree in self.rbc_ns.datas.iteritems():
            for ykey, xtree in ytree.iteritems():
                for xkey, value in xtree.iteritems():
                    self.update(xkey * self.bxsize, ykey * self.bysize, zkey * self.bzsize, False)


"""
    Generate an empty new map
"""
def gen_new_map(size, rname):
    size_multiplier = 32
    size = int(size)
    map = {
        'def': {
            'name': rname,
            'cutting_edges': [],
            'xsize': size * size_multiplier,
            'ysize': size * size_multiplier,
        },
        'blocks': {
        },
        'hoverings': [
        ],
    }
    return map

"""
    Load an existing map from file
"""
def load_map(file):
    loaded_file = open("./maps/sources/" + file, 'rU')
    loaded_map = pickle.load(loaded_file)
    loaded_map['def']['name'] = file
    if 'cutting_edges' not in loaded_map['def']:
        loaded_map['def']['cutting_edges'] = []
    loaded_file.close()
    return loaded_map
