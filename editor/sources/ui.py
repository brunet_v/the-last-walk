import panda3d.core as engine
import sources.utils.load_resources as resources
import sources.utils.struct as struct
import sources.utils.grid as grid
import time
import random

from direct.gui.OnscreenImage import OnscreenImage
from direct.gui.OnscreenText import OnscreenText

"""
    Class managing editor user interface, (mouse clicks and such)
"""
class UiManager():

    def __init__(self, main_directory):
        self.hover_mode = True # inverted
        self.grass_mode = True # inverted
        self.shadow_mode = False # inverted
        self.get_winsize()
        self.main_directory = main_directory
        self.selected = None
        self.background_height = 1100
        self.background_width = 150
        self.frametime = time.time()
        self.background = self.load_image(
            "./ui/sidebar.png",
            2, 0, self.background_width, self.background_height
        )
        self.highlight = self.load_image(
            "./ui/highlight.png",
            4, -100, 146, 22
        )
        self.background_preview = self.load_image(
            "./ui/back_preview.png",
            160, -200, 150, 150
        )
        self.icon_size = 16
        self.loaded = []
        self.mx = 0
        self.my = 0
        self.rtime = time.time()
        self.preview = pixel2d.attachNewNode("epreview_")
        self.preview.setDepthWrite(False)
        self.preview.setDepthTest(False)
        self.preview.setPos(160 + 151 / 2, 0, -169)
        self.preview.setScale(52)
        self.preview_sub = self.preview.attachNewNode("spreview_")
        self.previewed = None
        self.preview_rotation = 0
        tmpgrid = grid.ThreeAxisGrid(0.5, 0.5, 0, 0.25, 1)
        self.preview_grid = tmpgrid.create()
        self.preview_grid.flattenStrong()
        self.preview_grid.hide()
        self.preview_grid.reparentTo(self.preview_sub)
        self.mhover = self.load_image(
            "./ui/hover_mode_case2.png",
            160, -200, 50, 50
        )
        self.nhover = self.load_image(
            "./ui/hover_mode_objet.png",
            160, -200, 50, 50
        )
        self.yshadow = self.load_image(
            "./ui/yes-shadow.png",
            160, -200, 50, 50
        )
        self.nshadow = self.load_image(
            "./ui/no-shadow.png",
            160, -200, 50, 50
        )
        self.ygrass = self.load_image(
            "./ui/yes-grass.png",
            160, -200, 50, 50
        )
        self.ngrass = self.load_image(
            "./ui/no-grass.png",
            160, -200, 50, 50
        )
        self.toggle_mode()
        self.toggle_grass()
        self.toggle_shadow()
        self.tfont = loader.loadFont('./ui/font.ttf')
        self.rotate_preview = True

    def toggle_mode(self):
        self.hover_mode = not self.hover_mode
        if self.hover_mode:
            self.move_image(self.nhover, 160, 8, 50, 50)
            self.move_image(self.mhover, 160, -100, 50, 50)
        else:
            self.move_image(self.nhover, 160, -100, 50, 50)
            self.move_image(self.mhover, 160, 8, 50, 50)

    def toggle_shadow(self):
        self.shadow_mode = not self.shadow_mode
        if self.shadow_mode:
            self.move_image(self.yshadow, 217, 8, 50, 50)
            self.move_image(self.nshadow, 217, -100, 50, 50)
        else:
            self.move_image(self.yshadow, 217, -100, 50, 50)
            self.move_image(self.nshadow, 217, 8, 50, 50)

    def toggle_grass(self):
        self.grass_mode = not self.grass_mode
        if self.grass_mode:
            self.move_image(self.ygrass, 274, 8, 50, 50)
            self.move_image(self.ngrass, 274, -100, 50, 50)
        else:
            self.move_image(self.ygrass, 274, -100, 50, 50)
            self.move_image(self.ngrass, 274, 8, 50, 50)

    def toggle_preview_rotation(self):
        self.rotate_preview = not self.rotate_preview
        if self.rotate_preview:
            self.preview_rotation = random.randint(0, 359)

    def load_folder(self, folder, blocks = None):
        if blocks:
            self.blocks = blocks
        self.move_image(self.highlight, 7, -100, 190, 22)
        folders = folder.split("/")
        browsed = self.blocks
        for directory in folders:
            if directory:
                browsed = browsed[directory]
        for img, text, data in self.loaded:
            img.destroy()
            text.destroy()
        self.loaded = []
        i = 0
        if browsed != self.blocks:
            self.loaded.append((
                self.load_image("./ui/back.png", 8, 4, self.icon_size, self.icon_size),
                self.load_text("previous", 30, 17, 13),
                {"t": "back", "name": "back", "link": "/".join(folder.split("/")[:-1])},
            ))
            i += 1
        sorted_l = []
        for key, value in browsed.iteritems():
            sorted_l.append((key, value))
        sorted_l = sorted(sorted_l, key=lambda v: v[0])
        for key, value in sorted_l:
            if type(value) == type({}):
                img = "./ui/folder.png"
                data = {"t": "dir", "name": key, "link":"%s/%s" % (folder, key)}
            else:
                img = "./ui/model.png"
                data = {"t": "file", "name": key, "link": value.path}
            self.loaded.append((
                self.load_image(img, 8, i * 20 + 5, self.icon_size, self.icon_size),
                self.load_text(data["name"], 30, i * 20 + 17, 13),
                data,
            ))
            i += 1

    def get_winsize(self):
        self.winxsize = base.win.getXSize()
        self.winysize = base.win.getYSize()
        self.dxwin = int(self.winxsize / 2)
        self.dywin = int(self.winysize / 2)

    def update(self, editor):
        self.editor = editor
        if time.time() - self.rtime > 0.2:
            self.get_winsize()
            self.rtime = time.time()
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            self.mx = int(self.dxwin * mpos.getX() + self.dxwin)
            self.my = int(self.dywin * -mpos.getY() + self.dywin)
        delay = time.time() - self.frametime
        if self.rotate_preview:
            self.preview_rotation += delay * 45
        self.preview.setHpr(0, 45, 0)
        self.preview_sub.setHpr(self.preview_rotation, 0, 0)
        self.frametime = time.time()

    def event(self, type):
        if self.mx < 152:
            idx = int(self.my / 20.0)
            if idx < len(self.loaded):
                clicked = self.loaded[idx][2]
                if clicked['t'] in ("dir", "back"):
                    self.load_folder(clicked['link'])
                else:
                    self.move_image(self.highlight, 4, idx * 20 + 2, 146, 22)
                    self.change_preview(clicked['link'])
                    self.editor['view']['used']['id'] = clicked['link']
            return False
        return True

    def change_preview(self, id):
        if not id:
            return None
        self.move_image(self.background_preview, 160, 65, 150, 150)
        if self.previewed:
            self.previewed.detachNode()
        node = resources.CopyActor(
            resources.GetBlockModel(self.blocks, id), self.preview_sub
        )
        node.setPos(-0.5, -0.5, 0)
        node.show()
        self.previewed = node
        self.preview_grid.show()

    def clear_cursor(self):
        self.move_image(self.highlight, 4, -200, 146, 22)
        self.move_image(self.background_preview, 160, -200, 150, 150)
        if self.previewed:
            self.previewed.detachNode()
        self.preview_grid.hide()

    def load_text(self, value, x, y, scale):
        text = OnscreenText(
            mayChange = True,
            fg = (1, 1, 1, 1),
            align = engine.TextNode.ALeft,
            font = self.tfont,
        )
        text.setScale(scale)
        self.move_text(text, x, y)
        text.setText(value)
        text.reparentTo(pixel2d)
        text.show()
        return text

    def load_image(self, image_file, x, y, xsize, ysize):
        img = OnscreenImage(image = image_file)
        img.setScale(xsize / 2.0, 0, ysize / 2.0)
        self.move_image(img, x, y, xsize, ysize)
        img.setTransparency(engine.TransparencyAttrib.MAlpha)
        img.reparentTo(pixel2d)
        img.show()
        return img

    def move_image(self, image, x, y, xsize, ysize):
        image.setPos(x + xsize / 2, 0, -y - ysize / 2)

    def move_text(self, text, x, y):
        text.setPos(x, -y)

