EDITOR COMMANDS :

CAMERA:

K/M    Rotate camera (Mouse wheel works too)
O/L    Zoom camera (Right click + Mouse wheel works too)


CURSOR MOUVEMENT:

ZQSD    cursor center on X/Y axis (Right click works too)
Shift-Z Move cursor up on Z (Ctrl + Mouse wheel works too)
Shift-S Move cursor down on Z (Ctrl + Mouse wheel works too)
R       Rotate the current block
Shift-R Rotate the current block in other side
Ctrl-R  Rotate the current block in other side
F       Randomly rotate the current block


CURSOR ACTIONS:

Y       Fill the map with the current block
Shift-Y Fill the map with the current block randomly rotated
A       Select the block under the cursor
TAB     Toggle cutting edge in the current level
SHIFT   Toggle cursor position aproximation (usefull in hovering mode)
+/-     Change element scale
CLICK   Create block in the world
DELETE  Delete the current block under cursor (or the selected block if any)
SPACE   Clear selected block


VISION:

6/9    Change maximum height rendered (10 at start)
7/4    Change sun height
,      Toggle vision on admin blocks
;      Toggle vision on cutting edges
:      Toggle vision on help grid
!      Toggle vision on placeholder


MISC:

Ctrl-Z Undo block add (can't undo delete)
Ctrl-S Save map
F5     Force refresh of the editor display
F9     Toggle hovering mode for blocks
F10    Toggle shadowing mode for blocks
F11    Toggle grass edit mode
ENTER  Debug informations, such as polygon count


GRASS EDIT ACTIONS:

+/-    Change painting zone size
CLICK  Add some grass in painting zone
DELETE Delete grass in painting zone
SPACE  Delete some grass in painting zone
TAB    Change between circle and rectangle painting
