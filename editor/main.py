#!/usr/bin/env python
import os
import sys
import time
import shutil
import sources.repair as repair
import sources.map as map

main_directory = os.path.dirname(os.path.realpath(__file__))

"""
    List map in "/maps" directory (editables) if no arguments
"""
if len(sys.argv) < 2:
    print "USAGE : %s [map] [generate_map_size]" % sys.argv[0]
    print ""
    print " Maps available for edition :"
    for files in os.listdir("%s/maps/sources/" % main_directory):
        print "   ->", files
    print ""
    map_name = raw_input(" Open map: ")
    print ""
else:
    map_name = sys.argv[1]


"""
    Export collision, blocks and maps to client and server if asked
"""
def copy_dir(src, dst):
    sep = '/'
    print sys.platform
    if sys.platform == 'win32':
        sep = '\\'
        src = src.replace('/', sep)
        dst = dst.replace('/', sep)
    if not os.path.exists(dst):
        os.mkdir(dst)
    for files in os.listdir(src):
        cfile = "%s%s" % (src, files)
        dfile = "%s%s" % (dst, files)
        if os.path.isdir(cfile):
            copy_dir("%s%s" % (cfile, sep), "%s%s" % (dfile, sep))
        else:
            print "Copying %s..." % dfile
            shutil.copyfile(cfile, dfile)

if len(sys.argv) == 2 and sys.argv[1] == "export":
    print "Exporting maps to server and client ..."
    copy_dir(main_directory + "/blocks/", main_directory + "/../client/datas/blocks/")
    copy_dir(main_directory + "/blocks/", main_directory + "/../server/datas/blocks/")
    copy_dir(main_directory + "/maps/", main_directory + "/../client/datas/maps/")
    copy_dir(main_directory + "/maps/", main_directory + "/../server/datas/maps/")
    copy_dir(main_directory + "/collisions/", main_directory + "/../client/datas/collisions/")
    copy_dir(main_directory + "/collisions/", main_directory + "/../server/datas/collisions/")
    exit()

"""
    Init editor main data structure and load blocks
"""
def init_editor(edit_map):
    import sources.utils.load_resources as loader
    import sources.ui as ui
    return {
        # editor view structure
        'view': {
            'orotation': 0,
            'rotation': 0,
            'ozoom': 30,
            'zoom': 30,
            'xpos': edit_map['def']['xsize'] / 2,
            'ypos': edit_map['def']['ysize'] / 2,
            'txpos': 0,
            'typos': 0,
            'zpos': 0,
            'vlimit': 10,
            'used': {
                'mode': 'block',
                'id': None,
                'oid': None,
                'rotation': 0,
                'orotation': 0,
                'croot': None,
                'cnode': None,
                'adding': False,
                'scale': 1.,
                'placeholder': {'node': None, 'show': False},
                'grasscursor': {'node': None, 'rect': None, 'circle': None},
                'grasscursorcircle': False,
            },
            'light': {
                'height': 40,
                'rotation': 0,
                'distance': 40,
                'node': None,
            },
            'edges_nodes': [],
            'mpicker': None,
            'size': 0,
            'hide_specials': False,
            'hide_edges': True,
            'hide_grid': False,
            'mgrid': None,
            'lastpos': None,
            'blurr_cursor': 0,
            'last_add': [],
            'graped': None,
            'mpos': None,
            'frametime': time.time(),
            'shadowing': True,
            'grass': False,
            'grasshandler': None,
            'hover': False,
            'hoverings': {},
            'randomed': False,
            'aprox': True,
            'ui_texts': {
                "rotation": None,
                "xpos": None,
                "ypos": None,
                "zpos": None,
            },
        },
        # graphic datas
        'ui': ui.UiManager(main_directory),
        'blocks': loader.LoadBlockList(main_directory),
        'render_map': map.BlockGrid(),
        'render_tree': {},
        # edited map structure
        'map': edit_map,
    }

"""
    Editor entry
"""
def main():
    mname = map_name
    if not mname.endswith(".mp"):
        mname += ".mp"
    if len(sys.argv) > 2:
        if sys.argv[2] == "repair":
            edit_map = repair.repair(map.load_map(mname))
        else:
            edit_map = map.gen_new_map(int(sys.argv[2]), mname)
    else:
        try:
            edit_map = map.load_map(mname)
        except IOError:
            edit_map = map.gen_new_map(1, mname)

    """
        Start panda and the editor
    """
    import sources.edit as edit
    editor = init_editor(edit_map)
    return edit.start_editing_map(editor)

try:
    main()
except:
    import traceback
    with open("error.txt", "w") as f:
        f.write("%s\n" % traceback.format_exc())
