
"""
    Automatic collision map positionning configuration
"""

ADDITIONAL_COLLISIONS = {
    # Match with every walls (walls-00x00)
    'wall-[0-9x]*': (

        # How it looks like :
        #
        #    + Corner rotated by 180 degree
        #    |
        #    |    + Wall rotated by 180 degree
        #    |    |
        #  +----+----+----+
        #  |    |    |    |
        #  |  ##|####|##  |---- Corner rotated by 270 degree
        #  +----+----+----+
        #  |  ##|####|##  |
        #  |    |    |    |---- Corner rotated by 0 degree
        #  +----+----+----+
        #    |    |
        #    |    + Position 0-0 wall (default map)
        #    |
        #    + Corner rotated by 90 degree

        # Add collision on the other side of the wall
        {'x': 0, 'y': -1, 'c': 'cwall', 'r': 180},

        # Add collision for wall sides (2x2 cquarter)
        {'x': 1, 'y': 0, 'c': 'cquarter', 'r': 0},
        {'x': -1, 'y': 0, 'c': 'cquarter', 'r': 90},
        {'x': -1, 'y': -1, 'c': 'cquarter', 'r': 180},
        {'x': 1, 'y': -1, 'c': 'cquarter', 'r': 270},
    ),

    # Match with every wall corner
    'wall-corner-[0-9x]*': (
        # Add collision around the corner
        {'x': -1, 'y': 0, 'c': 'cquarter', 'r': 90},
        {'x': 0, 'y': -1, 'c': 'cquarter', 'r': -90},
        {'x': -1, 'y': -1, 'c': 'cquarter', 'r': 180},
    ),

    # Match with every stair fence
    'stair-fence[0-9x]*': (
        {'x': 0, 'y': -1, 'c': 'cfence', 'r': 180},
        {'x': 1, 'y': 0, 'c': 'cfence-quarter', 'r': 0},
        {'x': -1, 'y': 0, 'c': 'cfence-quarter', 'r': 90},
        {'x': -1, 'y': -1, 'c': 'cfence-quarter', 'r': 180},
        {'x': 1, 'y': -1, 'c': 'cfence-quarter', 'r': 270},
    ),

    'cdemi': (
        # Add collision on the other side of the wall
        {'x': 0, 'y': -1, 'c': 'cwall', 'r': 180},
        # Add collision for wall sides (2x2 cquarter)
        {'x': 1, 'y': 0, 'c': 'cquarter', 'r': 0},
        {'x': -1, 'y': 0, 'c': 'cquarter', 'r': 90},
        {'x': -1, 'y': -1, 'c': 'cquarter', 'r': 180},
        {'x': 1, 'y': -1, 'c': 'cquarter', 'r': 270},
    ),

    'cdemi-walk': (
        # Add collision on the other side of the wall
        {'x': 0, 'y': -1, 'c': 'cdemi-walk', 'r': 180},
        # Add collision for wall sides (2x2 cquarter)
        {'x': 1, 'y': 0, 'c': 'cquarter-walk', 'r': 0},
        {'x': -1, 'y': 0, 'c': 'cquarter-walk', 'r': 90},
        {'x': -1, 'y': -1, 'c': 'cquarter-walk', 'r': 180},
        {'x': 1, 'y': -1, 'c': 'cquarter-walk', 'r': 270},
    ),

    'window-wall-[0-9x]*': (
        {'x': 0, 'y': -1, 'c': 'cfence', 'r': 180},
        {'x': 1, 'y': 0, 'c': 'cfence-quarter', 'r': 0},
        {'x': -1, 'y': 0, 'c': 'cfence-quarter', 'r': 90},
        {'x': -1, 'y': -1, 'c': 'cfence-quarter', 'r': 180},
        {'x': 1, 'y': -1, 'c': 'cfence-quarter', 'r': 270},
    ),
}


"""
    Format used is:

    '[regex of matching names]': (

        # Contain additional collision map list

        {
            'x': [x position relative to the block],
            'y': [y position relative to the block],
            'c': [name of the collision map],
            'r': [rotation relative to the block], # In degree, will be rounded by 90's
        },

    )

    Simple wall blocks example :

    'my-wall': (
        {
            'x': 0,
            'y': -1,
            'c': 'cwall',
            'r': 180,
        },
    )

    Will represent :

        At position 0x -1y, add a new collision map -> +-------+ - - - +
                                                       | cwall |
                Collision map rotated by 180 degree -> |  180  |       |
                                                       |  deg  |
             Position 0x 0y (actual block position) -> +-------+ - - - +
                                                       | cwall |
                     Collision map added by default -> |   0   |       |
                                                       |  def  |
                                                       +-------+ - - - +

                                                       |       |       |

                                                       + - - - + - - - +
"""
