import time
import socket
import sources.utils.world as world
import sources.config.constants as const

from threading import Thread
from sources.Game import Game
from sources.networking.Client import Client
from sources.networking.Master import Master
from sources.utils.CollisionGraph import CollisionGraph

class Lobby(object):

    def __init__(self, options, debug=False):
        self.options = options
        self.clients = []
        self.loaded = False
        self.map_started = False
        self.debug = debug
        # Network init
        self.listener = self.create_listener({
            'listen': self.options['laddr'],
            'port': self.options['port'],
        })
        self.master = Master('lobby', {
            'name': self.options['name'],
            'port': self.options['port'],
            'max_players': self.options['max_clients'],
        })
        # Game init
        self.loader = Thread(target=self.load_resources).start()

    # Resources init
    def load_resources(self):
        self.medias = world.create_medias()
        self.world = world.generate(self.medias)
        self.graph = CollisionGraph(self.world)
        self.loaded = True
        for client in self.clients:
            client.send('world', self.world['infos']['map_def'])
        self.admin_message("Game ready, waiting for everyone to finish loading.")

    # create listening socket
    def create_listener(self, infos):
        listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listener.bind((infos['listen'], infos['port']))
        listener.listen(5)
        listener.setblocking(0)
        return listener

    def admin_message(self, msg, client=None):
        sended = {
            "login": "HOST",
            "msg": msg,
        }
        if client:
            client.send('smsg', sended)
        else:
            for client in self.clients:
                client.send('smsg', sended)

    def refresh_player_list(self):
        for client in self.clients:
            client.pregame('rplayers', {})

    def handle_master_events(self):
        while True:
            mevent = self.master.retrieve()
            if mevent:
                type, data = mevent["type"], mevent["data"]
                # Kick player if master says it's not authentificated
                if type == "logfail" and not self.debug:
                    for client in self.clients:
                        if data["login"] == client.username and data["address"] == client.address[0]:
                            print "Connexion refused for %s from %s" % (data["login"], data["address"])
                            self.clients.remove(client)
                            client.clear()
                            self.refresh_player_list()
                            break
            else:
                return None

    def run_lobby(self):
        all_ready = None
        all_ready_countdown = None
        last_update = time.time()
        last_display = time.time()
        empty_time = time.time()
        while not self.map_started:
            # Eventually accept new clients
            if len(self.clients) < self.options['max_clients']:
                try:
                    new_client = Client(self.listener.accept(), self)
                    if not self.loaded:
                        self.admin_message("Game is loading ...", new_client)
                    self.clients.append(new_client)
                except socket.error:
                    pass
            # Check existing preparing clients
            clients_ready = 0
            for client in self.clients:
                try:
                    if client.prepare() == "logging":
                        self.master.send("lcheck", {
                            "login": client.username,
                            "address": client.address[0],
                        })
                except Exception as e:
                    import traceback
                    print "client error:", str(e)
                    print traceback.format_exc()
                if client.ready and client.player_ready:
                    clients_ready += 1
                if not client.is_alive():
                    self.clients.remove(client)
                    client.clear()
            # Debug logs
            if time.time() - last_display > 2.0:
                last_display = time.time()
                print len(self.clients), "clients connected, and", clients_ready, "clients ready."
            # Start game ?
            if clients_ready == self.options['max_clients'] and self.loaded:
                if not all_ready:
                    all_ready = time.time() + 3.
                    all_ready_countdown = 3
                    self.admin_message("Everyone is ready, starting game in :")
                elif time.time() > all_ready:
                    self.map_started = True
                elif time.time() > all_ready - all_ready_countdown:
                    self.admin_message("%d second(s)" % all_ready_countdown)
                    all_ready_countdown -= 1
            else:
                all_ready = None
            # Update master server status
            if time.time() > last_update:
                last_update = time.time() + 3.0
                self.master.send('lupdate', {
                    'players': clients_ready,
                })
            # Handler master server event
            if not self.master.is_alive():
                raise Exception("Connection with master server aborted.")
            self.handle_master_events()
            # Handle empty server
            if not len(self.clients):
                if time.time() > empty_time + 120.:
                    raise Exception("No client connection for too long.")
            else:
                empty_time = time.time()
            # Don't waste server resources
            time.sleep(0.05)

    def run_game(self):
        self.master.clear()
        self.game = Game(self.clients, self.world, self.graph)
        self.game.start()
