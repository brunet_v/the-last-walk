import time
import socket
import Queue
import msgpack as serial
import sources.objects.utils as utils
import sources.config.constants as const

class Client():

    def __init__(self, listened, lobby):
        self.lobby = lobby
        self.socket = listened[0]
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.socket.setblocking(False) # Not sure ?
        self.address = listened[1]
        self.alive = True
        self.ready = False
        self.alive_time = time.time()
        self.computed_queue = Queue.Queue()
        self.receive_buffer = ""
        self.siblings = lobby.clients
        self.username = None
        self.map_sent = False
        self.player_ready = True # TODO
        self.packets = 0
        self.bandwidth = utils.Struct({
            'input': 0,
            'output': 0,
        })

    def __del__(self):
        self.clear()

    def clear(self):
        self.siblings = None
        if self.lobby:
            username = self.username
            if username:
                self.username = None
                self.lobby.refresh_player_list()
                self.lobby.admin_message("%s left the game." % username)
            self.lobby = None
        if self.socket:
            try:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
            except:
                pass
        self.alive_time = time.time() - const.SOCKET_TIMEOUT - 1.0

    def hook_player(self, pid):
        self.hooked = pid
        self.phooked = self.lobby.world['players'][pid]
        self.phooked.client_id = pid
        self.send('p_hook', {'id': pid})

    def sync(self):
        try:
            value = True
            while value and len(self.receive_buffer) < const.MAX_BUFFER_SIZE:
                value = self.socket.recv(const.SOCKET_SIZE)
                if value:
                    self.receive_buffer += value
        except socket.error:
            pass
        try:
            while 5 < len(self.receive_buffer):
                try:
                    size = int(self.receive_buffer[:4]) + 5
                    if len(self.receive_buffer) > size:
                        datas = self.receive_buffer[5:size]
                        self.receive_buffer = self.receive_buffer[size + 1:]
                        loaded = serial.loads(datas)
                        if len(loaded) >= 2:
                            self.packets += 1
                            self.bandwidth.input += len(datas)
                            self.computed_queue.put(loaded)
                    else:
                        break
                except ValueError:
                    break
        except:
            pass

    def has_data(self):
        return not self.computed_queue.empty()

    def pop_data(self):
        if not self.computed_queue.empty():
            return self.computed_queue.get()
        return None

    def receive(self): # Pre-game mode
        self.sync()
        return self.pop_data()

    def prepare(self): # Pre-game mode
        buf = self.receive()
        if buf:
            type, datas = buf[:2]
            self.pregame(type, datas)
            return type
        else:
            return None

    def pregame(self, type, datas): # Pre-game mode
        if type == 'player-ready' and self.username:
            self.player_ready = bool(datas)
            rdata = {
                "ready": self.player_ready,
                "login": self.username,
            }
            for client in self.siblings:
                client.send(type, rdata)
        if type == 'logging' and not self.map_sent:
            self.map_sent = True
            self.username = datas['login']
            self.lobby.admin_message("%s entered the game!" % self.username)
            self.lobby.refresh_player_list()
            if self.lobby.loaded:
                self.send('world', self.lobby.world['infos']['map_def'])
            else:
                self.send('world-load', {})
        if type == 'ready' and self.map_sent:
            self.ready = True
        if type == 'smsg' and self.username:
            rdata = {
                "msg": datas["msg"],
                "login": self.username,
            }
            for client in self.siblings:
                client.send(type, rdata)
        if type == 'rplayers':
            rdata = []
            for client in self.siblings:
                if client.username:
                    rdata.append({
                        "login": client.username,
                        "ready": client.ready,
                    })
            self.send(type, rdata)
        if type == 'leaving':
            self.clear()

    def send(self, type, datas):
        try:
            dump = serial.dumps((type, datas))
            ldump = len(dump)
            self.socket.send("%04d|%s@" % (ldump, dump))
            self.bandwidth.output += ldump
        except socket.error:
            return False
        return True

    def is_alive(self):
        if time.time() - self.alive_time > const.SOCKET_TIMEOUT:
            self.alive_time = time.time()
            return self.send("ping", {})
        return True
