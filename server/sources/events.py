import time
import sources.config.constants as const
import sources.config.item_config as item_config

from panda3d.core import Point3

# Network Utilities
def send_error(game, id, error_msg):
    game.clients_ids[id].send('error', {'error': error_msg})

def send_client(game, id, type, data):
    game.clients_ids[id].send(type, data)

# Player events
def player_update(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        dest = Point3(data['gx'], data['gy'], player.getZ())
        dist = dest - player.getPos()
        moving = dist.lengthSquared() > const.PLAYER_WALK_MARGIN * 32.
        player.lookpoint = Point3(data['lx'], data['ly'], player.getZ())
        if moving:
            inmove = player.goal
            player.goal = dest
            if not inmove:
                game.player_handler.snapshot_player(id)

def player_stop(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        sended = {
            'id': id,
            'px': player.getX(),
            'py': player.getY(),
            'pz': player.getZ(),
        }
        player.goal = None
        game.send_all('p_stop', sended)

def player_take_item(game, id, data, force=False):
    player = game.world['players'][id]
    if not player.dead:
        item = game.item_handler.item_list[data['id']]
        if item:
            cdist = item.pos - player.getPos()
            if cdist.lengthSquared() < (const.OBJECT_GET_DISTANCE + 1.) ** 2 or force:
                if len(player.inventory) < const.INVENTORY_SIZE:
                    player.inventory[data['id']] = item
                    game.item_handler.item_list.pop(data['id'])
                    sended = {
                        'pid': id,
                        'id': item.id,
                        'type': item.type,
                        'load': item.load,
                    }
                    game.send_all('titem', sended)
                    if not player.equiped and "equipable" in const.ITEM_STATS[item.type]:
                        if not force:
                            player.equiped = item
                            game.send_all('p_equip', sended)
                else:
                    send_error(game, id, 'Inventory is full.')
            else:
                send_error(game, id, "Item is too far.")

def player_drop_item(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        if player.equiped and data['id'] == player.equiped.id:
            player.equiped = None
        if data['id'] in player.inventory:
            item = player.inventory.pop(data['id'])
            x, y, z = game.collisions.getProxyPoint(
                player.getX(),
                player.getY(),
                player.getZ(),
                data['x'],
                data['y'],
                player.getZ(),
                const.OBJECT_GET_DISTANCE / 2,
                0.9
            )
            item.pos = Point3(x, y, z)
            game.send_all('citem', {
                'id': item.id,
                'type': item.type,
                'x': x,
                'y': y,
                'z': z,
            })
            game.send_all('ditem', {
                'pid': id,
                'id': item.id,
            })
            game.item_handler.item_list[item.id] = item

def player_use_item(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        if data['id'] in player.inventory:
            item = player.inventory[data['id']]
            if "equipable" in const.ITEM_STATS[item.type]:
                if player.equiped and player.equiped.id == item.id:
                    player.equiped = None
                    game.send_all('p_unequip', {
                        'pid': id,
                    })
                else:
                    player.equipItem(item)
                    game.send_all('p_equip', {
                        'pid': id,
                        'id': item.id,
                        'type': item.type,
                        'load': item.load,
                    })
            else:
                msg = game.item_handler.use_item(player, item)
                if msg:
                    send_error(game, id, msg)

def player_aim(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        player.please_unaim = False
        if not player.aiming:
            player.aiming_timer = time.time() + const.BLENDING_DELAY
        player.aiming = True
        player.aiming_last = time.time()
        game.send_all('p_aim', {
            'pid': id,
        })

def player_unaim(game, id, data, sending=False):
    player = game.world['players'][id]
    if not player.dead:
        if sending:
            game.send_all('p_unaim', {
                'pid': id,
            })
        else:
            player.please_unaim = (game, id, data)

def player_use(game, id, data):
    player = game.world['players'][id]
    player.try_use = True

def player_unuse(game, id, data):
    player = game.world['players'][id]
    player.try_use = False

def player_use_door(game, id, data):
    player = game.world['players'][id]
    if not player.dead:
        door_handler = game.world['doors']
        doors = door_handler.doors
        if data['id'] in doors:
            door = doors[data['id']]
            dist = player.pos - door.pos
            if dist.lengthSquared() < (const.DOOR_OPEN_DIST + 1.) ** 2:
                if door.open:
                    cx, cy, cz, sx, sy = door_handler.getClosedPos(data['id'])
                    if game.world['collision_tree'].isValueWalkable(-1000000, cx, cy, cz):
                        door_handler.closeDoor(door.id)
                    else:
                        send_error(game, id, "Something is blocking")
                        return None
                else:
                    door_handler.openDoor(door.id)
                game.send_all('d_set', {
                    'id': door.id,
                    'open': door.open,
                })
            else:
                send_error(game, id, "Door too far")

def player_sprint(game, id, data):
    player = game.world['players'][id]
    player.sprint = bool(data)
    game.send_all('p_sprint', {
        'pid': id,
        'sprint': player.sprint,
    })

def player_msg(game, id, data):
    game.send_all('p_msg', {
        'pid': id,
        'msg': data['msg'][:512],
    })

def player_light(game, id, data):
    player = game.world['players'][id]
    player.light_on = not player.light_on
    game.send_all('p_light', {
        'pid': id,
        'l': player.light_on,
    })

def player_started(game, id, data):
    player = game.world['players'][id]
    player.started = True
    # Give an item to a player
    def give_player_item(item_type):
        iid = game.item_handler.create_item(
            player.getX(),
            player.getY(),
            player.getZ(),
            item_type
        )
        player_take_item(game, id, {'id': iid}, True)
    # Basic inventory
    give_player_item('flashlight')
    # give_player_item('flashlight')
    # give_player_item('flashlight')
    # give_player_item('flashlight')
    give_player_item('m16')
    give_player_item('m16')
    give_player_item('m16')
    give_player_item('m16')
    give_player_item('m16')
    give_player_item('katana')
    give_player_item('katana')

def toggle_ally(game, id, data):
    pid = int(data['id'])
    player = game.world['players'][id]
    tplayer = game.world['players'][pid]
    if pid in player.allieds:
        player.allieds.remove(pid)
        if id in tplayer.allieds:
            tplayer.allieds.remove(id)
    else:
        player.allieds.add(pid)
    game.send_all('ally', {
        'pid': id,
        'allies': list(player.allieds),
    })
    game.send_all('ally', {
        'pid': pid,
        'allies': list(tplayer.allieds),
    })

def response_request(game, id, data):
    data['stime'] = time.time()
    send_client(game, id, "rrequest", data)
