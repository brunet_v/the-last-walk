import sources.config.constants as const

class ClientHandler():

    def __init__(self, game):
        self.outpackets = 0
        self.inpackets = 0
        self.game = game
        self.clients = game.clients
        self.inputs = []
        self.outputs = []
        self.specials = []
        for client in self.clients:
            self.inputs.append(client.socket)

    def send_to(self, pid, type, data):
        for client in self.clients:
            if client.phooked.id == pid:
                client.send(type, data)
                self.outpackets += 1

    def send_all(self, type, data, xpos=None, ypos=None):
        diff = const.BLOCKS_PER_SIZE / 1.5
        for client in self.clients:
            inrange = True
            if xpos and ypos:
                px = client.phooked.getX()
                py = client.phooked.getY()
                if abs(xpos - px) > diff or abs(ypos - py) > diff:
                    inrange = False
            if inrange:
                client.send(type, data)
                self.outpackets += 1

    def sync(self):
        for client in self.clients:
            try:
                client.sync()
                pid = client.hooked
                while client.has_data():
                    datas = client.pop_data()
                    self.game.compute_event(pid, datas[0], datas[1])
                    self.inpackets += 1
            except Exception as e:
                print "Client sync error:", str(e)
