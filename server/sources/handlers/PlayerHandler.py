import time
import panda3d.core as engine
import sources.config.constants as const

class PlayerHandler():

    def __init__(self, game):
        self.game = game
        self.world = game.world
        self.last_snapshot = time.time()

    def clear(self):
        if self.game:
            self.game = None
        if self.world:
            self.world = None

    def closests_players_number(self, x, y, z, dist):
        players = self.world['players']
        ref = engine.Point3(x, y, z)
        d = dist ** 2
        result = 0
        for id, player in players.iteritems():
            diff = ref - player.getPos()
            if diff.lengthSquared() < d:
                result += 1
        return result

    def snapshot(self):
        players = self.world['players']
        self.send_snapshot(players)

    def snapshot_player(self, pid):
        players = self.world['players']
        self.send_snapshot({
            pid: players[pid],
        })

    def send_snapshot(self, destinations):
        # Iterate over every player to send their status on network
        diff = const.BLOCKS_PER_SIZE / 1.5
        players = self.world['players']
        buffers = {}
        for id, player in players.iteritems():
            # Player basic infos
            px = player.getX()
            py = player.getY()
            mv = False
            gx = 0
            gy = 0
            if player.goal:
                mv = True
                gx = player.goal.getX()
                gy = player.goal.getY()
            # Data sent for a player
            pdatas = {
                # Location
                'id': id,
                'px': px,
                'py': py,
                'pz': player.getZ(),
                # Goal
                'mv': mv,
                'gx': gx,
                'gy': gy,
                # Look point
                'lx': player.lookpoint.getX(),
                'ly': player.lookpoint.getY(),
                # Status
                'st': (
                    player.life,
                    player.thirst,
                    player.hunger,
                    player.energy,
                    player.stress,
                    player.tired,
                ),
            }
            # Check client receivability
            for pid, client in destinations.iteritems():
                cx = client.getX()
                cy = client.getY()
                receivable = False
                # If is same
                if client.id == player.id:
                    receivable = True
                # If allied
                elif client.id in player.allieds:
                    receivable = True
                # If in range
                elif max(abs(cx - px), abs(cy - py)) < diff:
                    receivable = True
                # Add to client buffer
                if receivable:
                    pl = buffers.get(pid, [])
                    pl.append(pdatas)
                    buffers[pid] = pl
        # Send infos bufferized
        for pid, player in destinations.iteritems():
            self.game.send_to(pid, 'p_update', buffers.get(pid, []))

    def frame_actions(self, frametime):
        # Handle players
        players = self.world['players']
        for id, player in players.iteritems():
            player.frame_actions(frametime)
        # Snapshot players
        now = time.time()
        if now > self.last_snapshot:
            self.snapshot()
            self.last_snapshot = now + const.PLAYER_SNAPSHOT_DELAY
