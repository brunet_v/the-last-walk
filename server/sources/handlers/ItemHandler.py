import time
import random as rand
import sources.events as events
import sources.config.constants as const

from sources.objects.Item import Item

class ItemHandler():

    def __init__(self, game):
        self.item_id = 0
        self.game = game
        self.world = game.world
        self.ispawns = self.world['infos']['ispawns']
        self.players = self.world['players']
        self.spawn_time = time.time()
        self.set_day_difficulty(0)
        self.item_list = {}
        self.debug = const.USE_DEBUG

    def set_day_difficulty(self, day):
        self.max_items = int((const.WORLD_SIZE ** 2) * (5.0 / float(1.0 + day)))
        self.spawn_rate = 1.0
        # self.max_items = 500 # TOREMOVE
        # self.spawn_rate = 0.5

    def get_closest_players(self, zx, zy, zz, radius, ids = False):
        players = self.players
        if ids:
            results = set()
        else:
            results = []
        for id, player in players.iteritems():
            if not player.dead:
                dist = (zx - player.getX()) ** 2
                dist += (zy - player.getY()) ** 2
                dist += (zz - player.getZ()) ** 2
                if dist < radius ** 2:
                    if ids:
                        results.add(id)
                    else:
                        results.append(player)
        return results

    def hit_zombie(self, zombie, hitpos, item_stats):
        zombie.hitted = time.time()
        zombie.life -= item_stats['damage']
        if zombie.life > 0:
            zombie.attack = None # Cancel attack on hit
            self.game.send_all('hzomb', {
                'id': zombie.id,
                'hx': zombie.getX(),
                'hy': zombie.getY(),
                'hz': zombie.getZ(),
            })
        else:
            zombie.dead = True
            zombie.dead_time = time.time()
            zombie.position_tree.deleteId(zombie.id)
            zombie.collision_tree.deleteId(zombie.id)
            self.game.send_all('kzomb', {
                'id': zombie.id,
                'hx': zombie.getX(),
                'hy': zombie.getY(),
                'hz': zombie.getZ(),
                'hs': item_stats['damage'],
            })

    def hit_player(self, player, hitpos, item_stats):
        player.setLife(player.life - item_stats['damage'])
        if player.life > 0:
            self.game.send_all('p_hit', {
                'pid': player.id,
                'life': player.life,
                'hx': player.getX(),
                'hy': player.getY(),
                'hz': player.getZ(),
            })
        else:
            # Drop every item randomly
            while player.inventory:
                for id, item in player.inventory.iteritems():
                    events.player_drop_item(self.game, player.id, {
                        'id': id,
                        'x': player.getX() + (rand.randint(0, 100) - 50) / 20.0,
                        'y': player.getY() + (rand.randint(0, 100) - 50) / 20.0,
                    })
                    break
            player.dead = True
            player.block_time = time.time() + 10 ** 15 # infinite?
            player.collision_tree.deleteId(player.id)
            self.game.send_all('p_kill', {
                'pid': player.id,
                'hx': player.getX(),
                'hy': player.getY(),
                'hz': player.getZ(),
                'hs': item_stats['damage'],
            })

    def hit_door(self, door, hitpos, item_stats):
        door.hit(item_stats['damage'])
        if door.life > 0:
            self.game.send_all('d_hit', {
                'id': door.id,
                'l': door.life,
            })
        else:
            self.game.door_handler.delDoor(door.id)
            self.game.send_all('d_kill', {
                'id': door.id,
            })

    def get_action_infos(self, player, item):
        aiming = player.lookpoint
        if item:
            item_stats = const.ITEM_STATS[item.type]
        else:
            item_stats = const.HAND_STATS
        item_zone = item_stats['zone']
        return aiming, item_stats, item_zone

    def activate_item(self, player, item): # equiped item activation
        aiming, item_stats, item_zone = self.get_action_infos(player, item)
        if item_stats['need_ammo']:
            item.load -= 1
            self.game.send_all('uload', {
                'pid': player.id,
                'id': item.id,
                'nload': item.load,
            })
        if item_stats['need_energy']:
            player.setEnergy(player.energy - item_stats['need_energy'])

        # Infos sent
        infos = {
            'type': item_stats['zone']['type'],
        }

        if self.debug:
            infos['debughitbox'] = []

        def zone_spoint(): # Static point zone
            zx, zy, zz = self.game.collisions.getProxyDirection(
                player.getX(),
                player.getY(),
                player.getZ(),
                aiming.getX(),
                aiming.getY(),
                aiming.getZ(),
                item_zone['dist'],
                1.1,
            )
            players = self.get_closest_players(
                zx, zy, zz, item_zone['radius'],
            )
            zombs = self.game.zombie_handler.closests_zombies(
                zx, zy, zz, item_zone['radius'],
            )
            doors = self.game.door_handler.getCloseDoors(
                zx, zy, zz, item_zone['radius'],
            )
            if self.debug:
                infos['debughitbox'].append((zx, zy, zz, item_zone['radius']))
            return players, zombs, doors

        def zone_rproj(): # Ranged projection zone
            pad = item_zone['size'] / 2.
            zone_list = self.game.collisions.getProxyDirectionListed(
                player.getX(),
                player.getY(),
                player.getZ(),
                aiming.getX(),
                aiming.getY(),
                aiming.getZ(),
                item_zone['range'],
                1.1,
                item_zone['range'] / pad, # iterations nb
            )
            players, zombs, doors = set([player,]), set(), set()
            max_trav = item_zone.get('trav', 10000)
            trav = 0
            for i, (zx, zy, zz) in enumerate(zone_list):
                if trav < max_trav + 1:
                    players |= set(self.get_closest_players(
                        zx, zy, zz, item_zone['size'],
                    ))
                    zombs |= set(self.game.zombie_handler.closests_zombies(
                        zx, zy, zz, item_zone['size'],
                    ))
                    doors |= set(self.game.door_handler.getCloseDoors(
                        zx, zy, zz, item_zone['size'],
                    ))
                    trav = len(players) + len(zombs) + len(doors)
                    infos['range_effect'] = (i + 1) * pad
            if self.debug:
                for zx, zy, zz in zone_list:
                    infos['debughitbox'].append((zx, zy, zz, item_zone['size']))
            return list(players), list(zombs), list(doors)

        try:
            players, zombs, doors = {
                'spoint': zone_spoint,
                'rproj': zone_rproj,
            }[item_stats['zone']['type']]()
            for zomb in zombs:
                self.hit_zombie(zomb, player, item_stats)
            for door in doors:
                self.hit_door(door, player, item_stats)
            for player_hit in players:
                if player_hit.id != player.id:
                    self.hit_player(player_hit, player, item_stats)
            self.game.send_all("p_effect", {
                'pid': player.id,
                'infos': infos,
            })
        except Exception as e:
            print "Item activation problem: ", str(e)

    def use_item(self, player, item): # item manipulation
        return None # TODO

    def random_create_item(self):
        if len(self.item_list) < self.max_items:
            nblock = rand.choice(self.ispawns)
            ntype = rand.choice(const.ITEM_ALLOWED)
            self.create_item(nblock['x'], nblock['y'], nblock['z'], ntype)

    def create_item(self, x, y, z, ntype):
        block = {
            'x': x,
            'y': y,
            'z': z,
        }
        self.item_id += 1
        nitem = Item(self.item_id, ntype, block)
        self.item_list[nitem.id] = nitem
        self.game.send_all('citem', {
            'id': nitem.id,
            'type': nitem.type,
            'x': nitem.x,
            'y': nitem.y,
            'z': nitem.z,
        })
        return nitem.id

    # Executed each frame
    def frame_actions(self, frametime):
        if time.time() - self.spawn_time > self.spawn_rate:
            self.random_create_item()
            self.spawn_time = time.time()
