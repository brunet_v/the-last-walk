import panda3d.core as engine
import sources.utils.trees as trees
import sources.config.constants as const

from sources.objects.Door import Door
from sources.utils.CollisionTree import CollisionTree

"""
    Door manager, responsible for adding, opening and closing doors,
    does collision and graphical modifications (for client)
"""
class DoorHandler():

    def __init__(self, world, seed):
        self.seed = seed
        self.world = world
        self.world_blocks = world['blocks']
        self.world_collisions = world['collision_tree']
        self.doors = {}
        self.positions = {}
        self.collisions = None
        self.cid = 20000000
        self.dtree = CollisionTree(5.)

    def update(self, delay):
        pass

    def openSeed(self, id):
        # Hash the id and the seed (pseudo-random)
        k = int(self.seed * id)
        k *= 357913941;
        k ^= k << 24;
        k += ~357913941;
        k ^= k >> 31;
        k ^= k << 31;
        return abs(k) % 17 < 3

    def getPos(self, door_id):
        door = self.doors[door_id]
        return door.x, door.y, door.z

    def addDoor(self, world_blocks_datas, door_block, door_name, rotation):
        self.cid += 1
        door_id = self.cid
        door_rotation = (door_name[2] + rotation) % 4

        # Init door
        self.doors[door_id] = Door(
            door_id,
            door_block,
            door_rotation
        )
        if self.openSeed(door_id):
            self.openDoor(door_id)
        else:
            self.closeDoor(door_id)

    def getClosedPos(self, door_id):
        door = self.doors[door_id]
        xrev, yrev, axisrev = const.ROTATION_FLIPS[door.r]
        dx = 0.5
        dy = 0.0
        if yrev:
            dy = 1.0
        if axisrev:
            dx, dy = dy, dx
        dx += door.x
        dy += door.y
        return dx, dy, door.z, 0.35, 0.35

    def closeDoor(self, door_id):
        door = self.doors[door_id]

        # Set collision block
        dx, dy, dz, sx, sy = self.getClosedPos(door_id)
        self.world_collisions.setPosValue(door_id, dx, dy, door.z, sx, sy)
        door.pos = engine.Point3(dx, dy, door.z)
        door.open = False
        self.dtree.setPosValue(door_id, dx, dy, door.z + 0.5)

        # Update pathing graph (zombies avoiding opened door)
        if self.collisions and door.need_link:
            self.collisions.addLink(door.block, door.need_link)

    def getOpenPos(self, door_id):
        door = self.doors[door_id]
        xrev, yrev, axisrev = const.ROTATION_FLIPS[door.r]
        sx = 0.275
        sy = 0.5
        dx = 0.925
        dy = 0.5
        if xrev:
            dx = 0.1
        if axisrev:
            sx, sy = sy, sx
            dx, dy = dy, dx
        dx += door.x
        dy += door.y
        return dx, dy, door.z, sx, sy

    def openDoor(self, door_id):
        door = self.doors[door_id]

        # Set collision block
        dx, dy, dz, sx, sy = self.getOpenPos(door_id)
        self.world_collisions.setPosValue(door_id, dx, dy, dz, sx, sy)
        door.pos = engine.Point3(dx, dy, dz)
        door.open = True
        self.dtree.setPosValue(door_id, dx, dy, door.z + 0.5)

        # Update pathing graph (zombies avoiding opened door)
        if self.collisions:
            ndx = int(dx + (dx - 0.5) - door.x)
            ndy = int(dy + (dy - 0.5) - door.y)
            linked = trees.read_from_tree(self.world_blocks['datas'], ndx, ndy, door.z)
            if linked:
                unlinked = self.collisions.delLink(door.block, linked)
                if unlinked:
                    door.need_link = linked

    def delDoor(self, door_id):
        door = self.doors[door_id]

        # Set collision block (then delete it)
        self.closeDoor(door_id)
        self.dtree.deleteId(door_id)
        self.world_collisions.deleteId(door_id)

        # Unlink door
        door.clear()
        self.doors.pop(door_id)

    def getCloseDoors(self, x, y, z, dist=3.):
        results = []
        for door_id in self.dtree.getNear(x, y, z + 0.5, dist):
            door = self.doors[door_id]
            results.append(door)
        return results

    def getCloseDoorsByZones(self, zones, dist):
        results = []
        ids = set()
        for zx, zy, zz in zones:
            ids |= set(self.dtree.getNear(zx, zy, zz + 0.5, dist))
        for door_id in ids:
            door = self.doors[door_id]
            results.append(door)
        return results
