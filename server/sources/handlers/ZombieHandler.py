import time
import random as rand
import panda3d.core as engine
import sources.config.constants as const
import sources.utils.trees as trees

from sources.utils.CollisionTree import CollisionTree
from sources.objects.ZombieObject import ZombieObject

"""
    Handle zombie IAs and status
"""
class ZombieHandler():

    def __init__(self, game):
        self.game = game
        self.world = game.world
        self.collisions = game.collisions

        self.zombies_list = self.world['zombies']['list']
        self.zombies_tree = self.world['zombies']['tree']
        self.block_datas = self.world['blocks']['datas']

        self.last_snapshot = time.time()
        self.last_spawn = time.time()
        self.znum = 0
        self.ptree = CollisionTree(const.STRESS_TEST_SIZE)
        self.set_day_difficulty(0)

    def __del__(self):
        pass

    def set_day_difficulty(self, day):
        self.znum_target = (const.WORLD_SIZE ** 2) * (1 + day * 1.5) * 2
        self.znum_target = 50

    def try_create_zombie(self):
        zspawns = self.world['infos']['zspawns']
        spawn = rand.choice(zspawns)
        if self.world['collision_tree'].isNodeWalkable(-1, spawn):
            self.znum += 1
            new_zombie = ZombieObject(
                self.game.create_id(),
                rand.choice(const.ZOMBIES_LIST),
                spawn, self.game, self.ptree
            )
            self.zombies_list[new_zombie.id] = new_zombie
            self.game.send_all('czomb', {
                'type': new_zombie.type,
                'id': new_zombie.id,
                'x': new_zombie.getX(),
                'y': new_zombie.getY(),
                'z': new_zombie.getZ(),
            })

    def get_pos_block(self, pos):
        if not pos:
            return None
        x = int(pos.getX())
        y = int(pos.getY())
        z = int(pos.getZ())
        block = trees.read_from_tree(self.block_datas, x, y, z)
        if block:
            return block
        return trees.read_from_tree(self.block_datas, x, y, z - 1)

    def order_zombie_attack(self, zombie, pos):
        diff = zombie.pos - pos
        if diff.lengthSquared() < zombie.stats['arange'] and time.time() > zombie.attack_delay:
            zombie.attack_delay = time.time() + zombie.stats['atiming'][2]
            zombie.attack = [
                pos.getX(), pos.getY(), pos.getZ(),
                time.time() + zombie.stats['atiming'][0],
                time.time() + zombie.stats['atiming'][1],
            ]
            self.game.send_all('azomb', {
                'id': zombie.id,
                'x': pos.getX(),
                'y': pos.getY(),
                'z': pos.getZ(),
                'delay': zombie.stats['atiming'][1],
            }, zombie.getX(), zombie.getY())

    def order_zombie_move(self, zombie, pos, attack=True):
        start_block = self.get_pos_block(zombie.getPos())
        end_block = self.get_pos_block(pos)
        path = self.collisions.getPath(start_block, end_block, 30.)
        if not path:
            return
        zombie.goal = None
        zombie.goal_stack = []
        goal = None
        stack = []
        if len(path) > 1:
            for edge in path:
                node = self.collisions.getEdgeNode(edge)
                dest = engine.Point3(node['x'] + 0.5, node['y'] + 0.5, node['z'])
                net_pos = (node['x'], node['y'], node['z'])
                if not zombie.goal:
                    zombie.goal = dest
                    goal = net_pos
                else:
                    zombie.goal_stack.append(dest)
                    stack.append(net_pos)
        else:
            if attack:
                self.order_zombie_attack(zombie, pos)
            zombie.goal = engine.Point3(pos)
            goal = (pos.getX() - 0.5, pos.getY() - 0.5, pos.getZ())
            stack = []
        zombie.nstack = stack
        zombie.ngoal = goal

    def order_zombie_stop(self, zombie):
        zombie.goal = None
        zombie.ngoal = None
        zombie.stack = []
        zombie.nstack = []

    def send_snapshot(self):
        diff = const.BLOCKS_PER_SIZE / 1.5
        players = self.world['players']
        buffers = {}
        for id, zombie in self.zombies_list.iteritems():
            zx = zombie.getX()
            zy = zombie.getY()
            zdatas = {
                'id': id,
                'zx': zx,
                'zy': zy,
                'zz': zombie.getZ(),
                'goal': zombie.ngoal,
                'stack': zombie.nstack[:5],
            }
            for pid, player in players.iteritems():
                px = player.getX()
                py = player.getY()
                if max(abs(zx - px), abs(zy - py)) < diff:
                    pl = buffers.get(pid, [])
                    pl.append(zdatas)
                    buffers[pid] = pl
        for pid, player in players.iteritems():
            self.game.send_to(pid, 'mzomb', buffers.get(pid, []))

    def zombie_attacking(self, zombie):
        zattack = zombie.attack
        # Hit players in zone
        players = self.game.item_handler.get_closest_players(
            zattack[0], zattack[1], zattack[2], zombie.stats['aradius']
        )
        for player in players:
            self.game.item_handler.hit_player(player, zombie, zombie.stats)
        # Hit doors in zone
        doors = self.game.door_handler.getCloseDoors(
            zattack[0], zattack[1], zattack[2], zombie.stats['aradius']
        )
        for door in doors:
            self.game.item_handler.hit_door(door, zombie, zombie.stats)

    def closest_door(self, pos):
        closest = None
        closest_norm = None
        for door in self.game.door_handler.getCloseDoors(pos.getX(), pos.getY(), pos.getZ()):
            dist = door.pos - pos
            dist.setZ(0)
            norm = dist.length()
            if not closest or norm < closest_norm:
                closest = engine.Point3(door.pos)
                closest_norm = norm
        return closest, closest_norm

    def closest_player(self, pos):
        closest = None
        closest_norm = None
        for id, player in self.world['players'].iteritems():
            if not player.dead:
                dist = player.getPos() - pos
                norm = dist.length() ## / player.noises
                if not closest or norm < closest_norm:
                    closest = player.getPos()
                    closest_norm = norm
        return closest, closest_norm

    def closests_zombies(self, x, y, z, dist):
        nears = self.ptree.getCircleNear(x, y, z, dist)
        results = []
        for id in nears:
            zomb = self.zombies_list[id]
            if not zomb.dead and zomb.birth:
                results.append(zomb)
        return results

    def closests_zombies_number(self, x, y, z, dist):
        return len(self.ptree.getCircleNear(x, y, z, dist))

    def get_close_random(self, zombie):
        return engine.Point3(
            zombie.getX() + rand.random() * 4.0 - 2.0,
            zombie.getY() + rand.random() * 4.0 - 2.0,
            zombie.getZ()
        )

    def frame_actions(self, frametime):
        to_delete = []
        # Check if need zombie spawn
        now = time.time()
        if self.znum < self.znum_target and now > self.last_spawn:
            self.last_spawn = now + 0.5
            self.try_create_zombie()
        # Utility
        def next_think(delay):
            return now + delay * rand.random() + delay
        # Update all zombies
        for id, zombie in self.zombies_list.iteritems():
            # If is dead
            if zombie.dead:
                to_delete.append(id)
            # If is in birth
            elif not zombie.birth:
                if now > zombie.birth_time:
                    zombie.birth = True
            # If is thinking
            elif now > zombie.think_time:
                # Check if blocked
                if zombie.blocked:
                    door, distance = self.closest_door(zombie.getPos())
                    if door and distance <= 0.8:
                        zombie.blocked = None
                        zombie.think_time = next_think(0.6)
                        self.order_zombie_stop(zombie)
                        self.order_zombie_attack(zombie, door)
                    elif now > zombie.blocked:
                        zombie.blocked = None
                        zombie.think_time = next_think(0.6)
                        self.order_zombie_move(zombie, self.get_close_random(zombie), False)
                    else:
                        zombie.think_time = next_think(0.5)
                # If ready, do something
                else:
                    player, distance = self.closest_player(zombie.getPos())
                    if distance < 4.:
                        zombie.think_time = next_think(0.2)
                        self.order_zombie_move(zombie, player)
                    elif distance < 14.:
                        zombie.think_time = next_think(0.5)
                        self.order_zombie_move(zombie, player)
                    else:
                        zombie.think_time = next_think(0.9)
                        if rand.randint(0, 4) == 0:
                            self.order_zombie_move(zombie, self.get_close_random(zombie))
                        else:
                            self.order_zombie_stop(zombie)
            # Update zombie
            zombie.frame_actions(frametime)
        # Delete dead zombies
        for id in to_delete:
            self.zombies_list.pop(id)
            self.znum -= 1
        # Snapshot zombies
        if now > self.last_snapshot:
            self.send_snapshot()
            self.last_snapshot = now + const.ZOMBIE_SNAPSHOT_DELAY
