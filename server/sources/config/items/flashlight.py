import sources.config.items.base as base

INFOS = {
    # Basic item infos
    'equipable': True,
    'visible': True,
    'need_ammo': False,
    'need_energy': base.HAND_STATS['need_energy'] + 1,
    'scale': 0.5,
    # Stats
    'load': 0,
    'damage': base.HAND_STATS['damage'] + 1,
    # Timing de l'effet, Timing de la fin de l'animation
    'timings': base.HAND_STATS['timings'],
     # Apply custom animations on certain body parts
    'custom_anims': {
        'walk': base.ONLY_TORSO,
        'walk-slow': base.ONLY_TORSO,
        'sprint': base.ONLY_TORSO,
        'wait': base.ONLY_TORSO,
    },
    # Display point on different animations
    'display_offsets': {
        'on_floor': (0, 0, 0, 0, 0, 0.05),
        'on_back': [
            (6.0, 0, 0, 0.7, 1.5, 0.7, "Torso"),
            (6.0, 0, 0, 0.7, 1.5, -0.7, "Torso"),
            (-6.0, 0, 0, 0.05, 1.1, -0.45, "Right Thigh"),
            (-6.0, 0, 0, 0.05, 1.1, 0.45, "Left Thigh"),
        ],
        'use': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
        'aim': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
        'flashlight/flashlight-walk': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
        'flashlight/flashlight-walk-slow': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
        'flashlight/flashlight-sprint': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
        'flashlight/flashlight-wait': (-102, -128, 12., 0, 0.1, 0, "Right Handgrip"),
    },
    # Zone effect
    'zone': base.HAND_STATS['zone'],
    # "Permanent" light on the weapon
    'light': {
        'color': (0.9, 0.9, 0.9, 0.9),
        'attenuation': (0, 0, 0.1),
        'fov': 70,
        'pos': (0, -0.27, 0),
        'hpr': (-10, 180, 0),
        'exponent': 0.8,
        'near-far': (0.05, 10),
    },
}
