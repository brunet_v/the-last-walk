import sources.config.items.base as base

INFOS = {
    # Basic item infos
    'equipable': True,
    'visible': True,
    'need_ammo': True,
    'need_energy': False,
    'scale': 0.55,
    # Stats
    'load': 45, # Taille d'un chargeur
    'damage': 6,
    # Timing de l'effet, Timing de la fin de l'animation
    'timings': (0.07, 0.14),
     # Apply custom animations on certain body parts
    'custom_anims': {
        'walk': base.ONLY_TORSO,
        'walk-slow': base.ONLY_TORSO,
        'sprint': base.ONLY_TORSO,
        'wait': base.ONLY_TORSO,
        'use': base.ONLY_TORSO,
        'aim': base.ONLY_TORSO,
    },
    # Display point in different states, format:(h p r x y z parent_bone)
    'display_offsets': {
        # When lying on the floor
        'on_floor': (0, 0, 90, 0, 0, 0.05),
        # When in player inventory
        'on_back': [
            (0., -15., 0., -0.65, 1.45, -0.4, "Torso"),
            (0., 15., 180., -0.6, 1.45, 0.4, "Torso"),
        ],
        # Animations
        'm16/m16-walk-slow': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
        'm16/m16-sprint': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
        'm16/m16-aim': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
        'm16/m16-walk': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
        'm16/m16-wait': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
        'm16/m16-use': (-23., 0., 114., 0.15, -0.1, -0.05, "Right Handgrip"),
    },
    # Zone effect
    'zone': {
        'type': 'rproj', # Ranged projection (une balle)
        'range': 15., # Longeur de la trainee de l'impact
        'size': 0.35, # Largeur de la trainee de l'impact
        'trav': 2, # Nombre d'objets traverses
    },
    # "Permanent" light on the weapon
    'light': {
        'color': (0.6, 0.6, 0.6, 0.6),
        'attenuation': (0, 0, 0.15),
        'fov': 70,
        'hpr': (0, 190, 0),
        'pos': (0, -0.7, 0.05),
        'exponent': 0.25,
        'near-far': (0.005, 15),
    },
    # Flashin light apearing when firing
    'flashing': {
        'timing': (0.07, 0.14),
        'color': (5., 5., 5., 1.),
        'attenuation': (0, 0, 0.5),
        'fov': 170,
        'pos': (0, -1, 0),
        'hpr': (0, 250, 0),
        'exponent': 0.9,
        'near-far': (0.005, 15),
        'rand-hpr': (20, 20, 20),
    },
    # Flashing image and the tip of the weapon when firing
    'flashing-sprite': {
        'texture': 'datas/particles/shoot/shoot-%d.png',
        'frames': (0, 1, 2),
        'timing': (0.07, 0.13, False), # start, end, loop
        'fps': 2.8 / 0.06,
        'scale': (0.45, 0.45, 0.45),
        'pos': (0, -0.85, 0.12),
        'hpr': (0, 90, 0),
        'alpha': 1.,
    },
    # Flashing bullet tails in front of the weapon when firing
    'flashing-bullet': {
        'texture': 'datas/particles/shoot/bullet-%d.png',
        'frames': (0,),
        'timing': (0.05, 0.3, False), # start, end, loop
        'fps': 1. / 0.05,
        'pos': (0, -0.85, 0.12),
        'epos': (0., -13., 0.),
        'scale': (0.15, 0.15, 0.35),
        'hpr': (0, 90, 0),
        'alpha': 1.,
        'disp': 4., # Dispertion angle
        'nb': 1, # Number of flashes
    },
}
