import sources.config.items.base as base

INFOS = {
    # Basic item infos
    'equipable': True,
    'visible': True,
    'need_ammo': False,
    'need_energy': 4.,
    'scale': 0.7,
    # Stats
    'load': 0,
    'damage': 10,
    # Timing de l'effet, Timing de la fin de l'animation
    'timings': (0.35, 0.7),
     # Apply custom animations on certain body parts
    'custom_anims': {
        'walk': base.ONLY_TORSO,
        'walk-slow': base.ONLY_TORSO,
        'sprint': base.ONLY_TORSO,
        'wait': base.ONLY_TORSO,
        'use': base.ONLY_TORSO,
        'aim': base.ONLY_TORSO,
    },
    # Display point on different animations
    'display_offsets': {
        'on_floor': (0, 0, 90, 0, 0, 0.15),
        'on_back': [
            (-366, 25, -355, -0.55, 2.6, 1, "Torso"),
            (-366, 335, 165, -0.55, 2.6, -1, "Torso"),
        ],
        'katana/katana-walk': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
        'katana/katana-walk-slow': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
        'katana/katana-sprint': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
        'katana/katana-wait': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
        'katana/katana-aim': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
        'katana/katana-use': (-42, 48, -102, 0.15, 0.4, 0.25, "Right Handgrip"),
    },
    # Zone effect
    'zone': {
        'type': 'spoint', # Simple point
        'dist': 0.45,
        'radius': 0.85,
    },
}
