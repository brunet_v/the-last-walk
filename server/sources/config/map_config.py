
"""
    Contain the map list used to generate the world

    /!\ Server only
"""

MAP_LIST = [
    {'name': 'simple.mpx', 'size': 1, 'pattern': 'simple'},
    {'name': 'mainmenu.mpx', 'size': 1, 'pattern': 'mainmenu'},
    {'name': 'house1.mpx', 'size': 1, 'pattern': 'house1'},
]

PATTERNS = {
    "default": [
        ".....",
        ".   .",
        ".   .",
        ".   .",
        ".....",
    ],
    "simple": [
        "....#",
        ".   #",
        ".   #",
        ".   #",
        "#####",
    ],
    "mainmenu": [
        "....#",
        ".   #",
        ".   #",
        ".   #",
        "....#",
    ],
    "house1": [
        ".....",
        ".   .",
        ".   .",
        ".   .",
        "....#",
    ]
}

MAP_BORDERS = '#'

