import sys

DEBUG_ACTIVATED = True

def build_option(opt):
    if not DEBUG_ACTIVATED:
        return False
    for arg in sys.argv:
        if arg == opt:
            return True
    return False
