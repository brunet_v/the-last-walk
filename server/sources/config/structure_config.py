
"""
    Used to be the common game structure between client and server,
    optimisations datastructure and implementation details now make it different.

    /!\ Do not sync this config file with the client
"""

WORLD_STRUCT = {
    'infos': { # general world states infos
        'view':{
            'daytime': 0,
            'xpos': 0,
            'ypos': 0,
            'zpos': 0,
            'oxpos': 0,
            'oypos': 0,
            'ozpos': 0,
            'rotation': 225,
            'rotationed': 225,
            'zoom': 10,
            'zoomed': 10,
            'clouds': None,
        },
        'hooked': None,
        'size': None,
        'map_def': None,
        'map_grid': None,
        'frametime': None,
        'pspawns': [],
        'zspawns': [],
        'ispawns': [],
        'world_grid': {},
    },
    'lights': {
        'state': -1,
        'sun': {},
        'moon': {},
        'ambient': {},
    },
    'blocks': { # undestructibles objects with collision
        'rendered': {},
        'datas': {},
        'animated': [],
        'fog': None,
    },
    'players': {},  # player list
    'zombies': {    # moving, IA, ennemies
        'tree': {},
        'list': {},
    },
    'doors': None,          # Door handler
    'collision_tree': None, # Collision handler
    'items': {},
}
