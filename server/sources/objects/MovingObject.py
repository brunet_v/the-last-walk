import math
import panda3d.core as engine
import sources.utils.trees as trees
import sources.config.constants as const

"""
    Base class for moving object in the world
"""
class MovingObject():

    def getPos(self):
        return self.pos

    def getX(self):
        return self.pos.getX()

    def getY(self):
        return self.pos.getY()

    def getZ(self):
        return self.pos.getZ()

    def _getHeightFromPos(self, world_blocks, x, y, z):
        block = trees.read_from_tree(world_blocks, math.floor(x), math.floor(y), math.floor(z))
        if block:
            return block['c'].getCollisionHeight(x, y, z)
        else:
            return -100

    def _getMapZPos(self, wb, x, y, z):
        zz = z % 1
        cz = self._getHeightFromPos(wb, x, y, z)
        if zz <= const.WALK_HEIGHT_MARGIN:
            cz = max(self._getHeightFromPos(wb, x, y, z - 1.0), cz)
        if zz >= 1.0 - const.WALK_HEIGHT_MARGIN:
            cz = max(self._getHeightFromPos(wb, x, y, z + 1.0), cz)
        return cz

    def setPos(self, new_pos, world_blocks = None, no_recursion = False):
        if world_blocks:
            old_z = self.getZ()
            nx = new_pos.getX()
            ny = new_pos.getY()
            new_z = self._getMapZPos(world_blocks, nx, ny, old_z)
            if (math.fabs(old_z - new_z) < const.WALK_HEIGHT_MARGIN
                and self.collision_tree.isValueWalkable(self.id, nx, ny, new_z)):
                new_pos.setZ(new_z)
                self.pos = engine.Point3(new_pos)
            elif not no_recursion:
                new_pos.setX(self.getX())
                self.setPos(new_pos, world_blocks, True)
                new_pos.setX(nx)
                new_pos.setY(self.getY())
                self.setPos(new_pos, world_blocks, True)
        else:
            self.pos = new_pos
        self.collision_tree.setPosVal(self.id, self.pos, circle=True)
        if self.position_tree:
            self.position_tree.setPosValue(self.id, self.pos.getX(), self.pos.getY(), self.pos.getZ())

    def __del__(self):
        self.position_tree.deleteId(self.id)
        self.collision_tree.deleteId(self.id)
