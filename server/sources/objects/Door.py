import sources.config.object_config as object_config

"""
    Door datas structure
"""
class Door():

    def __init__(self, id, block, rotation, door_model = None, door_node = None):
        self.id = id
        self.block = block
        self.door_model = door_model
        self.door_node = door_node
        self.r = (rotation + 1) % 4
        self.x = block['x']
        self.y = block['y']
        self.z = block['z']
        self.pos = None
        self.state = None
        self.need_link = False
        self.hidden = False
        self.life = object_config.DOOR_MAX_LIFE

    def clear(self):
        self.block = None
        self.door_model = None
        self.door_node = None
        self.r = None
        self.pos = None
        self.need_link = False
        self.hidden = False

    def hit(self, dmg):
        self.life -= dmg

    def getX(self):
        return self.pos.getX()

    def getY(self):
        return self.pos.getY()

    def getZ(self):
        return self.pos.getZ()
