import time
import random as rand
import panda3d.core as engine
import sources.config.constants as const

from sources.objects.MovingObject import MovingObject

"""
    Zombie character class
"""
class ZombieObject(MovingObject):

    def __init__(self, id, type, spawn, game, ptree = None):
        self.collision_tree = game.world['collision_tree']
        self.position_tree = ptree
        self.game = game
        self.block = None
        self.id = id
        self.type = type
        self.state = 'WAIT'
        self.stats = const.OBJECT_LIST[type]['stats']
        self.setPos(engine.Point3(
            spawn['x'] + float(rand.randint(4, 6)) / 10.0, # spawn random in case
            spawn['y'] + float(rand.randint(4, 6)) / 10.0,
            spawn['z']
        ))
        self.inventory = {}
        self.hitted = None
        self.goal = None
        self.goal_stack = []
        self.ngoal = None
        self.nstack = []
        self.think_time = time.time()
        self.birth = False
        self.birth_time = time.time() + 4.2
        self.dead = False
        self.dead_time = None
        self.hitted = None
        self.max_life = const.OBJECT_LIST[type]['stats']['life']
        self.life = self.max_life
        self.attack = None
        self.attack_delay = None
        self.blocked = None
        self.time_grunt = 0.
        self.max_time_grunt = rand.uniform(10, 20)

    def update_grunt(self, frametime):
        if self.time_grunt >= self.max_time_grunt:
            self.game.send_all("grunt", {
                "sound": str("grunt" + str(rand.randint(1, 11))),
                "x": self.getX(),
                "y": self.getY()
            }, self.getX(), self.getY())
            self.time_grunt = 0.

    def frame_actions(self, frametime):
        blocked = None
        if self.dead:
            pass
        elif self.attack:
            if self.attack[3] and time.time() > self.attack[3]:
                self.game.zombie_handler.zombie_attacking(self)
                self.attack[3] = None
            if time.time() > self.attack[4]:
                self.attack = None
        elif self.hitted:
            if time.time() - self.hitted > const.STUN_TIME:
                self.hitted = None
        elif self.goal:
            self.goal.setZ(self.getZ())
            dist = self.goal - self.pos
            dist_lenght = dist.lengthSquared()
            if dist_lenght > const.ZOMBIE_WALK_MARGIN or len(self.goal_stack):
                if not dist_lenght > const.ZOMBIE_WALK_MARGIN:
                    self.ngoal = self.nstack.pop(0)
                    self.goal = self.goal_stack.pop(0)
                    self.goal.setZ(self.getZ())
                    dist = self.goal - self.pos
                dist.normalize()
                ox = int(self.pos.getX() * 1000)
                oy = int(self.pos.getY() * 1000)
                self.setPos(
                    self.pos + dist * (self.stats['speed'] * frametime),
                    self.game.world['blocks']['datas']
                )
                if ox == int(self.pos.getX() * 1000) and oy == int(self.pos.getY() * 1000):
                    blocked = self.blocked
                    if not self.blocked:
                        blocked = time.time() + 0.8
            else:
                self.goal = None
                self.ngoal = None
        self.blocked = blocked
        self.time_grunt += frametime
        self.update_grunt(frametime)
