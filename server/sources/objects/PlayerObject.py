import time
import random as rand
import panda3d.core as engine
import sources.events as events
import sources.config.constants as const
import sources.config.item_config as item_config

from sources.objects.MovingObject import MovingObject

"""
    Player character class
"""
class PlayerObject(MovingObject):

    def __init__(self, id, type, spawn, game, ptree = None):
        self.collision_tree = game.world['collision_tree']
        self.position_tree = ptree
        self.game = game
        self.id = id
        self.client_id = None
        self.type = type
        self.state = 'WAIT'
        self.stats = const.OBJECT_LIST[type]['stats']
        self.setPos(engine.Point3(
            spawn['x'] + rand.randint(0, 10) / 10.0, # spawn random in case
            spawn['y'] + rand.randint(0, 10) / 10.0,
            spawn['z']
        ))
        self.goal = None
        self.equiped = None
        self.inventory = {}
        self.aiming = None
        self.lookpoint = engine.Point3(self.getPos() * 2.0)
        self.hitted = None
        self.block_time = None
        self.used_item = None
        self.dead = False
        self.allieds = set()
        self.aiming_timer = time.time()
        self.aiming_last = time.time()
        self.please_unaim = False
        self.noises = 2.
        self.tired = None
        self.sprint = False
        self.light_on = True
        self.started = False
        self.try_use = False
        self.please_use = False
        self.error_timer = time.time()
        self.item_stats = None
        # Stats
        self.max_life = const.OBJECT_LIST[type]['stats']['life']
        self.life = self.max_life
        self.setEnergy(100.)
        self.setStress(0.)
        self.setThirst(0.)
        self.setHunger(0.)
        self.stress_eval = time.time()

    def block(self, delay):
        self.block_time = time.time() + delay

    def equipItem(self, item):
        self.equiped = item

    def useItem(self, item):
        if not item:
            self.item_stats = const.HAND_STATS
        else:
            self.item_stats = const.ITEM_STATS[item.type]
        self.item_timings = self.item_stats['timings']
        self.used_item = [time.time(), item, False]

    def frame_actions(self, frametime):
        additional = self.getAdditionalCost()
        now = time.time()
        if self.block_time:
            if now > self.block_time:
                self.block_time = None
        elif self.used_item:
            used_delay = now - self.used_item[0]
            if used_delay > self.item_timings[0] and not self.used_item[2]:
                self.game.item_handler.activate_item(self, self.used_item[1])
                self.used_item[2] = True
            if used_delay > self.item_timings[1]:
                self.used_item = None
        elif self.aiming:
            if self.please_unaim and not self.please_use:
                events.player_unaim(*self.please_unaim, sending=True)
                self.aiming = False
                self.please_unaim = False
        if self.goal and not self.block_time:
            self.goal.setZ(self.pos.getZ())
            dist = self.goal - self.pos
            if dist.lengthSquared() > const.PLAYER_WALK_MARGIN:
                dist.normalize()
                self.noises = 10.0
                if self.aiming:
                    speed = self.stats['aim-speed']
                elif self.tired:
                    speed = self.stats['slow-speed']
                elif self.sprint:
                    speed = self.stats['sprint-speed']
                    self.setEnergy(self.energy - frametime * const.ENERGY_SPRINT_RATE)
                else:
                    speed = self.stats['walk-speed']
                    self.setEnergy(self.energy - frametime * const.ENERGY_WALK_RATE * additional)
                self.setPos(
                    self.pos + dist * (speed * frametime),
                    self.game.world['blocks']['datas']
                )
        else:
            self.noises = 2.
            self.setEnergy(self.energy + frametime * const.ENERGY_REGEN_RATE)
        # Base stats update
        if self.energy < 5.:
            self.tired = const.TIRED_TIME
        self.setThirst(self.thirst + frametime * const.THIRST_RATE * additional)
        self.setHunger(self.hunger + frametime * const.HUNGER_RATE * additional)
        self.setEnergy(self.energy + frametime * const.ENERGY_REGEN_RATE)
        self.setLife(self.life + frametime * const.HEALTH_REGEN_RATE)
        if self.tired:
            self.tired -= frametime
            if self.tired < 0:
                self.tired = None
        # Eval stress
        if now > self.stress_eval:
            self.calcStress()
            self.stress_eval = now + 0.2
        # Update usage if necessary
        if (self.try_use or self.please_use) and not self.dead and self.client_id:
            self.doUse()

    def setLife(self, life):
        self.life = max(0., min(self.max_life, life))

    def setEnergy(self, energy):
        self.energy = max(0., min(100.,energy))

    def setStress(self, stress):
        self.stress = max(0., min(100.,stress))

    def setThirst(self, thirst):
        self.thirst = max(0., min(100.,thirst))

    def setHunger(self, hunger):
        self.hunger = max(0., min(100.,hunger))

    def calcStress(self):
        x, y, z = self.getX(), self.getY(), self.getZ()
        dist = const.STRESS_TEST_SIZE
        znum = self.game.zombie_handler.closests_zombies_number(x, y, z, dist)
        pnum = self.game.player_handler.closests_players_number(x, y, z, dist) - 1
        self.setStress((znum - pnum * 3.) * 10.)

    def getAdditionalCost(self):
        return 1. + self.stress / 100.

    def doUse(self):
        now = time.time()
        # On error
        def send_error(msg):
            if now > self.error_timer:
                self.game.clients_ids[self.client_id].send('error', {'error': msg})
                self.error_timer = now + 1.5
        # Get item stats
        self.please_use = False
        item_type = None
        item_load = 0
        item_need_ammo = False
        item_need_energy = item_config.HAND_STATS['need_energy']
        if self.equiped:
            item_type = self.equiped.type
            item_load = self.equiped.load
            item_need_ammo = self.equiped.need_ammo
            item_need_energy = self.equiped.need_energy
        # Test if usable
        if self.aiming and not self.used_item:
            if not item_need_ammo or item_load > 0:
                if not item_need_energy or self.energy > item_need_energy:
                    if self.aiming_timer < time.time():
                        self.useItem(self.equiped)
                        self.game.send_all('p_use', {
                            'pid': self.client_id,
                            'type': item_type,
                        })
                    else:
                        self.please_use = True
                else:
                    send_error("Too tired to do that")
            else:
                send_error("Need ammo")
