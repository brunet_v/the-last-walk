import random as rand
import panda3d.core as engine
import sources.config.constants as const

class Item():

    def __init__(self, id, type, block):
        self.id = id
        self.type = type
        self.load = 0
        self.need_ammo = False
        self.need_energy = False

        if 'need_ammo' in const.ITEM_STATS[type]:
            self.need_ammo = const.ITEM_STATS[type]['need_ammo']
            if self.need_ammo:
                self.load = const.ITEM_STATS[type]['load']

        if 'need_energy' in const.ITEM_STATS[type]:
            self.need_energy = const.ITEM_STATS[type]['need_energy']

        self.x = block['x'] + float(rand.randint(3, 7)) / 10.0
        self.y = block['y'] + float(rand.randint(3, 7)) / 10.0
        self.z = block['z']
        self.pos = engine.Point3(self.x, self.y, self.z)
