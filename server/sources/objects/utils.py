import time

"""
    Utility class used to convert dict to usable objects \o/
"""
class Struct(object):
    def __init__(self, entries):
        self.__dict__.update(**entries)


"""
    Utility class used to do fast basic profiling
"""
class Timer(object):
    def __init__(self, name = "Timer", smallest = 0.0):
        self.name = name
        self.t = time.clock()
        self.smallest = smallest

    def dump(self, showSmaller = False):
        delay = (time.clock() - self.t) * 1000.0
        if showSmaller or delay > self.smallest:
            print "%s: %.1fm second(s) elapsed" % (self.name, delay)

class ScopeTimer(Timer):
    def __del__(self):
        self.dump()
        Timer.__del__(self)


"""
    Utility class used to help finding leaks (contain number of alive class by name)
"""
class InstanceCounter(object):

    names = {}

    instances = {}

    @staticmethod
    def add(name):
        if name not in InstanceCounter.names:
            InstanceCounter.names[name] = 0
        InstanceCounter.names[name] += 1

    @staticmethod
    def rm(name):
        if name in InstanceCounter.names:
            InstanceCounter.names[name] -= 1
            if InstanceCounter.names[name] == 0:
                InstanceCounter.names.pop(name)

    @staticmethod
    def add_instance(name, inst):
        InstanceCounter.instances[name] = inst

    @staticmethod
    def dump():
        # print InstanceCounter.names
        for name, number in InstanceCounter.names.iteritems():
            print "Persisting instances: x%d %s" % (number, name)
        for name, instance in InstanceCounter.instances.iteritems():
            InstanceCounter.dump_instance_referers(name)

    @staticmethod
    def dump_instance_referers(name):
        if name in InstanceCounter.instances:
            print name, "instances:"
            import gc
            for ref in gc.get_referrers(InstanceCounter.instances[name]):
                if not ref is InstanceCounter.instances:
                    print ref

    @staticmethod
    def clear():
        InstanceCounter.names = {}

