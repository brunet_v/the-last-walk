import time
import events
import traceback
import random as rand
import sources.objects.utils as utils
import sources.config.constants as const

from sources.objects.PlayerObject import PlayerObject
from sources.handlers.ItemHandler import ItemHandler
from sources.handlers.PlayerHandler import PlayerHandler
from sources.handlers.ZombieHandler import ZombieHandler
from sources.handlers.ClientHandler import ClientHandler

class Game():

    network_events_handlers = {
        # Player action
        'p_update': events.player_update,
        'p_sprint': events.player_sprint,
        'p_stop': events.player_stop,
        'p_aim': events.player_aim,
        'p_unaim': events.player_unaim,
        'p_use': events.player_use,
        'p_unuse': events.player_unuse,
        'p_msg': events.player_msg,
        'p_light': events.player_light,
        'p_started': events.player_started,
        'p_toggleally': events.toggle_ally,
        # Doors
        'door_use': events.player_use_door,
        # Items
        'item_take': events.player_take_item,
        'item_use': events.player_use_item,
        'item_drop': events.player_drop_item,
        # Miscs
        'm_ping': events.response_request,
    }

    def __init__(self, clients, world, collisions):
        self.clients = clients
        self.clients_ids = {}
        self.world = world
        self.collisions = collisions
        self.running = True
        self.started = False
        self.id = 0
        self.day = 0
        self.daytime = 10.5 + 24
        self.player_handler = PlayerHandler(self)
        self.zombie_handler = ZombieHandler(self)
        self.item_handler = ItemHandler(self)
        self.client_handler = ClientHandler(self)
        self.door_handler = world['doors']
        self.door_handler.collisions = self.collisions
        self.frametime = time.time()
        self.daytime_update = time.time()
        self.net_update = time.time()
        self.init_players()
        self.logger = utils.Struct({
            "elapsed": 0.,
            "framecount": 0.,
            "sleeptime": 0.,
            "packetcount": 0.,
        })
        self.set_daytime()
        self.starttime = time.time() + 3.

    def init_players(self):
        # Create players objects and hook them to clients
        for client in self.clients:
            spawn = rand.choice(self.world['infos']['pspawns'])
            name = 'male-player1' # TODO
            pid = self.create_id()
            nplayer = PlayerObject(pid, name, spawn, self)
            self.world['players'][pid] = nplayer
            self.clients_ids[pid] = client
            self.send_all('p_create', {
                'id': pid,
                'login': client.username,
                'type': name,
                'x': nplayer.pos.x,
                'y': nplayer.pos.y,
                'z': nplayer.pos.z
            })
            client.hook_player(pid)
        # ally all players
        team = set()
        for id, player in self.world['players'].iteritems():
            team.add(id)
        for id, player in self.world['players'].iteritems():
            player.allieds = team

    def check_started(self):
        # If waiting for some player to load
        cur = self.started
        self.started = True
        for id, player in self.world['players'].iteritems():
            if not player.started:
                self.started = False
        if time.time() > self.starttime:
            self.started = True
        if not cur and self.started:
            self.send_all('starting', {})

    def create_id(self):
        self.id += 1
        return self.id

    def send_to(self, *args):
        self.client_handler.send_to(*args)

    def send_all(self, *args, **kwargs):
        self.client_handler.send_all(*args, **kwargs)

    def compute_event(self, pid, type, datas):
        if type in self.network_events_handlers:
            try:
                self.network_events_handlers[type](self, pid, datas)
            except:
                traceback.print_exc() # DEBUG

    def set_daytime(self):
        self.send_all('sdayt', self.daytime)
        if self.daytime >= 24 + 8:
            self.daytime %= 24
            self.day += 1
            self.send_all('nday', self.day)
            self.zombie_handler.set_day_difficulty(self.day)
            self.item_handler.set_day_difficulty(self.day)

    def sync_actions(self, delay):
        # Check network clients
        self.client_handler.sync()
        for client in self.clients:
            if not client.is_alive():
                self.clients.remove(client)
            if not len(self.clients):
                self.running = False

    def frame_actions(self, delay):
        # Update game logic
        self.player_handler.frame_actions(delay)
        self.zombie_handler.frame_actions(delay)
        self.item_handler.frame_actions(delay)
        # Update lights / daytime
        ratio = 1.
        if self.daytime < 8 or 20 < self.daytime < 24 + 8:
            ratio = const.DAYTIME_NIGHT_RATIO
        self.daytime += const.DAYTIME_SPEED / const.DAYTIME_DELAY * delay * ratio
        daydelay = const.DAYTIME_DELAY
        if self.day <= 1:
            daydelay /= 4
        if time.time() - self.daytime_update > daydelay:
            self.set_daytime()
            self.daytime_update = time.time()
        # Update logger
        self.logger.elapsed += delay
        self.logger.framecount += 1.
        if self.logger.elapsed > 2.:
            print "%.2f Fps with %d zombies, %.2f%% sleep (%.2f in/s, %.2f out/s)" % (
                self.logger.framecount / self.logger.elapsed,
                self.zombie_handler.znum,
                self.logger.sleeptime / self.logger.elapsed * 100.,
                float(self.client_handler.inpackets) / self.logger.elapsed,
                float(self.client_handler.outpackets) / self.logger.elapsed,
            )
            self.logger.elapsed = 0.
            self.logger.framecount = 0.
            self.logger.sleeptime = 0.
            self.logger.packetcount = 0.
            self.client_handler.inpackets = 0
            self.client_handler.outpackets = 0

    def start(self):
        # Frames timing handler
        frametime = 1. / 50.
        synctime = 1. / 200.
        lastframe = time.time()
        lastsync = time.time()
        while self.running:
            now = time.time()
            delay = now - lastframe
            dsync = now - lastsync
            # Network actions
            if dsync >= synctime:
                self.sync_actions(dsync)
                lastsync = now
            # Game actions
            if delay >= frametime:
                if self.started:
                    self.frame_actions(delay)
                else:
                    self.check_started()
                lastframe = now
            # If need sleep
            stime = min(synctime - dsync, frametime - delay)
            if stime > 0:
                snow = time.time()
                time.sleep(stime)
                nnow = time.time()
                self.logger.sleeptime += nnow - snow
