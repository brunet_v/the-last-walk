import os
import sys

from panda3d.core import Filename

def relative_path(path):
    cpath = Filename.fromOsSpecific(
        os.path.abspath(sys.path[0])
    ).getFullpath()
    return cpath + "/" + path
