#!/usr/bin/env python
import copy
import random as rand
import pickle as serial
import sources.utils.trees as trees
import sources.utils.mapdispatch as mapdispatch
import sources.config.constants as const

from sources.utils.BlockCollisions import BlockCollisions
from sources.utils.CollisionTree import CollisionTree
from sources.handlers.DoorHandler import DoorHandler

"""
    Load a compiled "map.mpx" into world at the correct offset,
    with the asked rotation and apply/precompute fog, collision and other effects on it.
"""
def load_map(medias, world, path, xoffset, yoffset, size, rotation):

    multiplier = const.BLOCKS_PER_SIZE
    true_xoffset = xoffset * multiplier
    true_yoffset = yoffset * multiplier
    map_size_offset = size * multiplier - 1

    world_blocks = world['blocks']
    blocks_collision = medias['blocks_collisions']

    file = open("datas/maps/compiled/%s" % path, 'rU')
    map = serial.load(file)
    file.close()

    map_blocks = map['blocks']

    x_reverse, y_reverse, axis_reverse = const.ROTATION_FLIPS[rotation]

    # load map blocks
    for zkey in map_blocks.keys():
        for ykey in map_blocks[zkey].keys():
            for xkey in map_blocks[zkey][ykey].keys():

                loaded_block = map_blocks[zkey][ykey][xkey]

                if x_reverse:
                    loaded_block['x'] = map_size_offset - loaded_block['x']
                if y_reverse:
                    loaded_block['y'] = map_size_offset - loaded_block['y']
                if axis_reverse:
                    loaded_block['x'], loaded_block['y'] = loaded_block['y'], loaded_block['x']

                loaded_block['r'] = (loaded_block['r'] + rotation) % 4
                loaded_block['c'] = blocks_collision.get(loaded_block['t'], loaded_block['r'])

                loaded_block['x'] += true_xoffset
                loaded_block['y'] += true_yoffset

                if 'n' in loaded_block:
                    for info in loaded_block['n']:
                        # Door case
                        if info[0].startswith('door'):
                            world['doors'].addDoor(
                                blocks_collision,
                                loaded_block,
                                info,
                                rotation,
                            )
                        if info[0] == "pspawn":
                            world['infos']['pspawns'].append(loaded_block)

                        if info[0] == "zspawn":
                            world['infos']['zspawns'].append(loaded_block)

                        if info[0] == "ispawn":
                            world['infos']['ispawns'].append(loaded_block)

                trees.add_to_tree(
                    world_blocks['datas'],
                    loaded_block['x'],
                    loaded_block['y'],
                    loaded_block['z'],
                    loaded_block
                )


def generate(medias):
    print "Generating map ..."
    world = copy.deepcopy(const.WORLD_STRUCT)
    world['collision_tree'] = CollisionTree()
    world_def = {
        'size': const.WORLD_SIZE,
        'maps': [],
        'seed': rand.randint(0, 999),
    }
    world['doors'] = DoorHandler(world, world_def['seed'])
    grid = mapdispatch.generate_grid()
    for x, y, name, size, rotation in grid:
        load_map(medias, world, name, x, y, size, rotation)
        world_def['maps'].append((x, y, name, size, rotation))
    world['infos']['map_grid'] = grid
    world['infos']['map_def'] = world_def
    return world

def create_medias():
    resources = const.RESOURCES_STRUCT
    resources['blocks_collisions'] = BlockCollisions()
    return resources
