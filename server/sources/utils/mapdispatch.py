import time
import random as rand
import sources.config.constants as const
import sources.config.map_config as maps
import sources.utils.trees as trees

"""
    Maps interconnections validity handler
"""
class CheckGrid():

    def __init__(self, size):
        self.size = size
        self.filled = trees.Tree()
        self.patterns = trees.Tree()
        self.readable = []
        borderpattern = self.GenerateBorderPattern()
        for i in xrange(size):
            self.patterns.set(-1, i, 0, borderpattern)
            self.patterns.set(i, -1, 0, borderpattern)
            self.patterns.set(i, size, 0, borderpattern)
            self.patterns.set(size, i, 0, borderpattern)

    def Filled(self, x, y):
        if self.filled.get(x, y, 0):
            return True
        return False

    def GetPatternSize(self, pattern):
        if pattern:
            return (len(pattern.datas[0]))
        return 1

    def GenerateBorderPattern(self):
        pattern = trees.Tree()
        dsize = 5
        for x in xrange(dsize):
            for y in xrange(dsize):
                pattern.set(x, y, 0, maps.MAP_BORDERS)
        return pattern

    def DisplayPattern(self, pattern, prefix = "", writer = None):
        if pattern:
            psize = self.GetPatternSize(pattern)
            for y in xrange(psize):
                pl = prefix
                for x in xrange(psize):
                    pl += pattern.get(x, y, 0)
                if writer:
                    writer.write("%s\n" % pl)
                else:
                    print pl

    def Clip(self, pattern, ox, oy, size):
        if size == 1:
            return pattern
        psize = self.GetPatternSize(pattern)
        fsize = psize / size
        result = trees.Tree()
        xs = ox * fsize
        ys = oy * fsize
        for x in xrange(0, fsize):
            for y in xrange(0, fsize):
                value = pattern.get(xs + x, ys + y, 0)
                result.set(x, y, 0, value)
        return result

    def CheckPattern(self, x, y, pattern):
        # Map lookups
        left = self.patterns.get(x - 1, y, 0)
        lsize = self.GetPatternSize(left) - 1
        right = self.patterns.get(x + 1, y, 0)
        top = self.patterns.get(x, y - 1, 0)
        tsize = self.GetPatternSize(top) - 1
        bottom = self.patterns.get(x, y + 1, 0)
        bsize = self.GetPatternSize(bottom) - 1
        psize = self.GetPatternSize(pattern)
        pmax = psize - 1
        for i in xrange(psize):
            if left:
                if pattern.get(0, i, 0) != left.get(lsize, i, 0):
                    return False
            if right:
                if pattern.get(pmax, i, 0) != right.get(0, i, 0):
                    return False
            if top:
                if pattern.get(i, 0, 0) != top.get(i, tsize, 0):
                    return False
            if bottom:
                if pattern.get(i, pmax, 0) != bottom.get(i, bsize, 0):
                    return False
        return True

    def Add(self, name, size, pattern, rotation, x, y):
        # Verif fill
        for ox in xrange(size):
            for oy in xrange(size):
                if self.Filled(x + ox, y + oy):
                    return False
        # Verif patterns
        for ox in xrange(size):
            for oy in xrange(size):
                if not self.CheckPattern(x + ox, y + oy, self.Clip(pattern, ox, oy, size)):
                    return False
        # Adding to map
        for ox in xrange(size):
            for oy in xrange(size):
                self.filled.set(x + ox, y + oy, 0, True)
        # Adding Partern
        for ox in xrange(size):
            for oy in xrange(size):
                self.patterns.set(x + ox, y + oy, 0, self.Clip(pattern, ox, oy, size))
        self.readable.append((y, x, name, size, rotation))
        return True

    def ToReadable(self):
        # Dump to log file?
        # file = open("grid.txt", "w")
        # for data in self.readable:
        #     file.write("For x=%d, y=%d, name=%s, size=%d, rotation=%d\n" % data)
        #     for x in xrange(data[3]):
        #         for y in xrange(data[3]):
        #             self.DisplayPattern(self.patterns.get(data[1] + x, data[0] + y, 0), "", file)
        # file.close()
        return self.readable


"""
    From defined map pattern, generate pattern readable for the algorithm
"""
def generate_pattern(pattern, rotation):
    datas = maps.PATTERNS[pattern]
    result = trees.Tree()
    xrev, yrev, axrev = const.ROTATION_FLIPS[(4 - rotation) % 4]
    ysize = len(datas)
    for y in xrange(0, ysize):
        xsize = len(datas[y])
        if xsize != ysize:
            raise Exception("Patterns must be 1:1 ratio")
        for x in xrange(0, xsize):
            mx = x
            my = y
            if xrev:
                mx = xsize - x - 1
            if yrev:
                my = ysize - y - 1
            if axrev:
                mx, my = my, mx
            result.set(mx, my, 0, datas[y][x])
    return result


"""
    Random pick a map from available list
"""
def pick_map(smax):
    for i in xrange(const.WORLD_RETRY):
        idx = rand.randint(0, len(maps.MAP_LIST) - 1)
        tried = maps.MAP_LIST[idx]
        name = tried['name']
        size = tried['size']
        if 'pattern' in tried:
            pattern = tried['pattern']
        else:
            pattern = "default"
        if size <= smax:
            return name, size, pattern, rand.randint(0, 3)
    fail_generation()


"""
    Meta function, return generated randomized map
"""
def generate_grid():
    size = const.WORLD_SIZE
    trys = const.WORLD_RETRY
    timer = time.time()
    total = 0
    for i in xrange(trys):
        failed = False
        ugrid = CheckGrid(size)
        for x in xrange(size):
            for y in xrange(size):
                if not ugrid.Filled(x, y) and not failed:
                    j = 0
                    good = False
                    while j < trys and not good:
                        name, msize, pattern, rotation = pick_map(min(size - x, size - y))
                        if ugrid.Add(name, msize, generate_pattern(pattern, rotation), rotation, x, y):
                            good = True
                        else:
                            good = False
                        j += 1
                        total += 1
                    if not good:
                        failed = True
        if not failed:
            print "Map generated after %d tentatives. (%f seconds, %d comb)" % (i, time.time() - timer, total)
            return ugrid.ToReadable()
    fail_generation()


"""
    If not enought map are usable, or there is a bug,
    it might be impossible to find a matching map pattern
"""
def fail_generation():
    print "Unable to generate map grid, no patterns matching found."
    exit()

