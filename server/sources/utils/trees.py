"""
    Optimisation / storage data-structure
    Used to create 3d maps of objects accessible by position:
        - By x axis position
        - By y axis position
        - By z axis position
        - By x-y-z raw position
    with O(1) complexity

    Internal data structure looks like :
    {
        10: {
            5: {
                3: Value1, # Position x-y-z = (3, 5, 10)
                1: Value2, # Position x-y-z = (1, 5, 10)
            },
            2: {
                1: Value3, # Position x-y-z = (1, 2, 10)
            },
        },
        7: {
            5: {
                3: Value4, # Position x-y-z = (3, 5, 7)
            },
        },
    }

    /!\ Used everywhere, edit at your own risk
"""

# Generic classes
class Tree():

    def __init__(self):
        self.datas = {}

    def get(self, x, y, z):
        return read_from_tree(self.datas, x, y, z)

    def set(self, x, y, z, value):
        return add_to_tree(self.datas, x, y, z, value)

    def remove(self, x, y, z):
        return rm_from_tree(self.datas, x, y, z)


class ListTree(Tree):

    def put(self, x, y, z, value):
        return add_to_list_tree(self.datas, x, y, z, value)

    def pop(self, x, y, z, value):
        return rm_from_list_tree(self.datas, x, y, z, value)


# STANDARD TREES
def add_to_tree(tree, x, y, z, value):
    if not z in tree:
        tree[z] = {}
    if not y in tree[z]:
        tree[z][y] = {}
    tree[z][y][x] = value

def read_from_tree(tree, x, y, z):
    if z in tree:
        if y in tree[z]:
            if x in tree[z][y]:
                return tree[z][y][x]
    return None

def rm_from_tree(tree, x, y, z):
    if z in tree:
        if y in tree[z]:
            if x in tree[z][y]:
                return tree[z][y].pop(x)
    return None

# LIST TREES
def add_to_list_tree(tree, x, y, z, value):
    list = read_from_tree(tree, x, y, z)
    if list is None:
        list = [value,]
    else:
        list.append(value)
    add_to_tree(tree, x, y, z, list)

def rm_from_list_tree(tree, x, y, z, value):
    list = read_from_tree(tree, x, y, z)
    if list is None:
        return value
    if value in list:
        list.remove(value)
    return value

def read_from_list_tree(tree, x, y, z):
    return read_from_tree(tree, x, y, z)
