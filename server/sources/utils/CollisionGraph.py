import math
import copy
import time
import collections
import random as rand
import sources.config.constants as const
import sources.utils.trees as trees

"""
    Contain pre-computed informations for zombies pathing
    also do computation for pathfinding
"""
class CollisionGraph():

    def __init__(self, world):
        print "Generating collision graph ..."
        generation_time = time.time()
        self.blocks_datas = world['blocks']['datas']
        self.collision_tree = world['collision_tree']
        self.nodes = {}
        self.edges = {}
        test_blocks = world['infos']['pspawns'] + world['infos']['zspawns']
        test_numbers = 6
        test_padding = 1.0 / 6.0
        test_vectors = (
            (-1,  1), ( 0,  1), ( 1,  1),
            (-1,  0), ( 0,  0), ( 1,  0),
            (-1, -1), ( 0, -1), ( 1, -1),
        )
        while len(test_blocks) > 0:
            block = test_blocks.pop()
            if "edge" not in block:
                block['edge'] = "%d:%d:%d" % (block['x'], block['y'], block['z'])
            for vector in test_vectors:
                tx = block['x'] + 0.5
                ty = block['y'] + 0.5
                tz = copy.copy(block['z'])
                cz = self.getPosHeight(tx, ty, tz)
                done = True
                for i in xrange(1, test_numbers + 1):
                    nx = tx + vector[0] * test_padding * i
                    ny = ty + vector[1] * test_padding * i
                    nz = self.getCorrectedPosHeight(nx, ny, cz)
                    if abs(nz - cz) > const.WALK_HEIGHT_MARGIN * 2:
                        done = False
                        break
                    cz = nz
                if done:
                    new_block = trees.read_from_tree(
                        self.blocks_datas,
                        math.floor(nx),
                        math.floor(ny),
                        math.floor(nz),
                    )
                    if not new_block:
                        new_block = trees.read_from_tree(
                            self.blocks_datas,
                            math.floor(nx),
                            math.floor(ny),
                            math.floor(nz - 1),
                        )
                    if "edge" not in new_block:
                        new_block['edge'] = "%d:%d:%d" % (
                            new_block['x'],
                            new_block['y'],
                            new_block['z']
                        )
                        test_blocks.append(new_block)
                    self.addLink(new_block, block)
        print "Collision graph generated %d nodes. (%f seconds)" % (
            len(self.edges), time.time() - generation_time
        )

    def getPosHeight(self, tx, ty, tz):
        block = trees.read_from_tree(
            self.blocks_datas,
            math.floor(tx),
            math.floor(ty),
            math.floor(tz)
        )
        if block:
            return block['c'].getCollisionHeight(tx, ty, tz)
        else:
            return -100

    def getPosItemHeight(self, tx, ty, tz):
        block = trees.read_from_tree(
            self.blocks_datas,
            math.floor(tx),
            math.floor(ty),
            math.floor(tz)
        )
        if block:
            return block['c'].getItemHeight(tx, ty, tz)
        else:
            return -100

    def getCorrectedPosHeight(self, tx, ty, tz):
        if tz % 1 < const.WALK_HEIGHT_MARGIN:
            return max(
                self.getPosHeight(tx, ty, tz),
                self.getPosHeight(tx, ty, tz - 1),
            )
        elif 1 - (tz % 1) < const.WALK_HEIGHT_MARGIN:
            return max(
                self.getPosHeight(tx, ty, tz),
                self.getPosHeight(tx, ty, tz + 1),
            )
        else:
            return self.getPosHeight(tx, ty, tz)

    def getProxyPoint(self, sx, sy, sz, ox, oy, oz, max_dist, max_height_margin, iterations = 20):
        tx, ty, tz = sx, sy, sz
        xoffset = ox - sx
        yoffset = oy - sy
        offset = math.sqrt(float(xoffset) ** 2 + float(yoffset) ** 2)
        xoffset /= offset
        yoffset /= offset
        pad = min(float(max_dist) / float(iterations), float(offset) / float(iterations))
        for i in xrange(1, int(iterations)):
            tempx = tx + xoffset * pad
            tempy = ty + yoffset * pad
            tempz = self.getPosItemHeight(tempx, tempy, sz)
            if abs(tempz - sz) > max_height_margin:
                break
            tx, ty, tz = tempx, tempy, tempz
        return tx, ty, tz

    def getProxyDirection(self, sx, sy, sz, ox, oy, oz, dist, max_height_margin, iterations = 20):
        tx, ty, tz = sx, sy, sz
        xoffset = ox - sx
        yoffset = oy - sy
        offset = math.sqrt(float(xoffset) ** 2 + float(yoffset) ** 2)
        xoffset /= offset
        yoffset /= offset
        pad = dist / float(iterations)
        for i in xrange(1, int(iterations)):
            tempx = tx + xoffset * pad
            tempy = ty + yoffset * pad
            tempz = self.getPosItemHeight(tempx, tempy, sz)
            if abs(tempz - sz) > max_height_margin:
                break
            tx, ty, tz = tempx, tempy, tempz
        return tx, ty, tz

    def getProxyDirectionListed(self, sx, sy, sz, ox, oy, oz, dist, max_height_margin, iterations = 20):
        result = []
        tx, ty, tz = sx, sy, sz
        xoffset = ox - sx
        yoffset = oy - sy
        offset = math.sqrt(float(xoffset) ** 2 + float(yoffset) ** 2)
        xoffset /= offset
        yoffset /= offset
        pad = dist / float(iterations)
        for i in xrange(1, int(iterations)):
            tempx = tx + xoffset * pad
            tempy = ty + yoffset * pad
            tempz = self.getPosItemHeight(tempx, tempy, sz)
            if abs(tempz - sz) > max_height_margin:
                break
            tx, ty, tz = tempx, tempy, tempz
            result.append((tx, ty, tz))
        return result

    def addLink(self, node1, node2):
        node1_edge = node1['edge']
        node2_edge = node2['edge']
        if node1_edge not in self.edges:
            self.edges[node1_edge] = {}
        if node2_edge not in self.edges:
            self.edges[node2_edge] = {}
        self.edges[node1_edge][node2_edge] = node2
        self.edges[node2_edge][node1_edge] = node1
        self.nodes[node1_edge] = node1
        self.nodes[node2_edge] = node2

    def delLink(self, node1, node2):
        deleted = False
        node1_edge = node1['edge']
        node2_edge = node2['edge']
        if self.hasLink(node1_edge, node2_edge):
            self.edges[node1_edge].pop(node2_edge)
            deleted = True
        if self.hasLink(node2_edge, node1_edge):
            self.edges[node2_edge].pop(node1_edge)
            deleted = True
        return deleted

    def hasLink(self, node1, node2):
        if node1 in self.edges:
            if node2 in self.edges[node1]:
                return self.edges[node1][node2]
        return False

    def getEdgeNode(self, edge):
        return self.nodes[edge]

    def getPath(self, start, goal, maxtests=10000000.):
        if not start or not goal or 'edge' not in goal or 'edge' not in start:
            return None

        goal_edge = goal['edge']
        start_edge = start['edge']
        result = []
        came_from = {start_edge: None}
        closed_set = set()
        open_set = set()
        open_set.add(start_edge)
        g_score = collections.defaultdict(int)
        f_score = collections.defaultdict(int)

        def heuristic_cost(n1, n2):
            return (
                (n1['x'] - n2['x']) ** 2
                + (n1['y'] - n2['y']) ** 2
                + (n1['z'] - n2['z']) ** 2
            ) * (rand.random() + 1.0)

        def get_lowest_key(node_dict, test_set):
            min_val = None
            min_edge = None
            for edge in test_set:
                value = node_dict[edge]
                if value < maxtests:
                    if not min_val or value < min_val:
                        min_val = value
                        min_edge = edge
            return min_edge

        def retracePath(current_edge):
            result.insert(0, current_edge)
            parent_edge = came_from[current_edge]
            if not parent_edge or parent_edge == start_edge:
                return result
            return retracePath(parent_edge)

        g_score[start_edge] = 0
        f_score[start_edge] = heuristic_cost(start, goal)
        while open_set:
            current = get_lowest_key(f_score, open_set)
            if not current:
                return None
            if current == goal_edge:
                return retracePath(current)
            open_set.remove(current)
            closed_set.add(current)
            for connection_edge, connection_node in self.edges[current].iteritems():
                if connection_edge not in closed_set:
                    tentative_g_score = g_score[current] + heuristic_cost(self.nodes[current], connection_node)
                    if connection_edge not in open_set or tentative_g_score <= g_score[connection_edge]:
                        came_from[connection_edge] = current
                        g_score[connection_edge] = tentative_g_score
                        f_score[connection_edge] = g_score[connection_edge] + heuristic_cost(connection_node, goal)
                        if connection_edge not in open_set:
                            open_set.add(connection_edge)
        return None
