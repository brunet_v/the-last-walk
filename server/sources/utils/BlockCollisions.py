import math
import sources.config.constants as const
import sources.objects.utils as utils

from panda3d.core import PNMImage

"""
    Contain collision map for a 1x1 block, use red color to get collision height,
    blue to get vision height and green to get item collision height
"""
class BlockCollision(object):

    def __init__(self, value, rotation):
        from sources.utils.resources import relative_path
        utils.InstanceCounter.add("BlockCollision")
        if value.startswith("mixin"):
            value = "mixins/%s" % value
        self.img = PNMImage(relative_path("datas/collisions/%s.png" % value))
        if self.img.getXSize() != self.img.getYSize():
            raise Exception("Collision map must be 1:1 aspect ratio.")
        self.size = int(self.img.getXSize())
        self.reverse_x, self.reverse_y, self.reverse_axis = const.ROTATION_FLIPS[rotation]
        self.max = int(self.size - 1)
        self.red = self.img.getRed
        self.blue = self.img.getBlue
        self.green = self.img.getGreen
        self.rotation = rotation
        self.path = value

    def __del__(self):
        if utils:
            utils.InstanceCounter.rm("BlockCollision")
        self.clear()

    def clear(self):
        self.img.clear()
        self.red = None
        self.blue = None
        self.green = None

    def getCollisionHeight(self, x, y, z):
        x = int(x * self.size) % self.size
        y = int(y * self.size) % self.size
        if self.reverse_x:
            x = self.max - x
        if self.reverse_y:
            y = self.max - y
        if self.reverse_axis:
            return self.red(self.max - y, x) * 1.9922 + math.floor(z)
        return self.red(self.max - x, y) * 1.9922 + math.floor(z)

    def getVisionHeight(self, x, y, z):
        x = int(x * self.size) % self.size
        y = int(y * self.size) % self.size
        if self.reverse_x:
            x = self.max - x
        if self.reverse_y:
            y = self.max - y
        if self.reverse_axis:
            return self.blue(self.max - y, x) * 1.9922 + math.floor(z)
        return self.blue(self.max - x, y) * 1.9922 + math.floor(z)

    def getItemHeight(self, x, y, z):
        x = int(x * self.size) % self.size
        y = int(y * self.size) % self.size
        if self.reverse_x:
            x = self.max - x
        if self.reverse_y:
            y = self.max - y
        if self.reverse_axis:
            return self.green(self.max - y, x) * 1.9922 + math.floor(z)
        return self.green(self.max - x, y) * 1.9922 + math.floor(z)


"""
    Load and contain collision data for each block and rotation
"""
class BlockCollisions(object):

    def __init__(self):
        utils.InstanceCounter.add("BlockCollisions")
        self.loaded = {}

    def __del__(self):
        if utils:
            utils.InstanceCounter.rm("BlockCollisions")
        self.clear()

    def clear(self):
        self.loaded = None

    def get(self, type, rotation):
        rotation = (7 - rotation) % 4
        key = (type, rotation)
        if key not in self.loaded:
            self.loaded[key] = BlockCollision(*key)
        return self.loaded[key]
