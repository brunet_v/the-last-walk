import sys
import random as rand
import sources.config.constants as const

from sources.networking.Lobby import Lobby

# Default options
options = {
    'name': 'testserver%d' % rand.randint(0, 100),
    'port': const.SOCKET_PORT + rand.randint(0, 1000),
    'max_clients': 1,
}

# Command line options editing
debug = False
if len(sys.argv) > 1:
    if sys.argv[1] == "debug": # DEBUG
        debug = True
        options['port'] = const.SOCKET_PORT
        if len(sys.argv) > 2:
            options['max_clients'] = int(sys.argv[2])
    else:
        options['max_clients'] = int(sys.argv[1])
        options['port'] = int(sys.argv[2])

# server start
def main():
    lobby = Lobby({
        'laddr': '0.0.0.0',
        'port': options['port'],
        'name': options['name'],
        'max_clients': options['max_clients'],
    }, debug)
    lobby.run_lobby()
    lobby.run_game()

try:
    main()
except:
    import traceback
    with open("error.txt", "w") as f:
        f.write("%s\n" % traceback.format_exc())
