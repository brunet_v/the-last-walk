####
# Image format
####

64x64pixels PNG format

RED   0->256 determine walk collision height, from 0.0 to 2.0
BLUE  0->256 determine vision collision height, from 0.0 to 2.0
GREEN 0->256 determine items collision height, from 0.0 to 2.0

pixel at  0x 0 == position 0.0x, 0.0y on model
pixel at 64x64 == position 1.0x, 1.0y on model

####
# Filename format
####

c{Name}.png

without spaces in {Name}, preferably few character for the {Name}
